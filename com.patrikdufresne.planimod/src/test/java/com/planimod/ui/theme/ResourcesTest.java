/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.theme;

import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.widgets.Display;
import org.junit.Test;

public class ResourcesTest {

    /**
     * Check if each constant can be loaded as an icon.
     * 
     * @throws SecurityException
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    @Test
    public void testIcons() throws IllegalArgumentException, IllegalAccessException, SecurityException {
        @SuppressWarnings("unused")
        Display display = new Display();
        Field fields[] = Resources.class.getFields();
        for (Field f : fields) {
            if (f.getName().contains("ICON")) {
                String fn = (String) f.get(null);
                // Create descriptor
                ImageDescriptor descriptor = Resources.getImageDescriptor(fn);
                assertNotNull("no ressourrces for " + f.getName() + " - " + fn, descriptor.createImage(false));
            }
        }
    }

}
