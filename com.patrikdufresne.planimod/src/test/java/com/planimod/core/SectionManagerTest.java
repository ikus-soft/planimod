/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.Section;

/**
 * This test case cover all operation done using the section.
 * 
 * @author patapouf
 * 
 */
public class SectionManagerTest extends ManagerTestCase {
    /**
     * Add the object.
     */
    @Test
    public void AddSection() {
        Section section = new Section();
        try {
            managers.getSectionManager().add(Arrays.asList(section));
        } catch (ManagerException e) {
            fail("Error adding Section", e);
        }
    }

    /**
     * Update the object.
     */
    @Test
    public void UpdateSection() {
        Section section = new Section();
        try {
            managers.getSectionManager().add(Arrays.asList(section));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }
        section.setName("Section 1");

        try {
            managers.getSectionManager().update(Arrays.asList(section));
        } catch (ManagerException e) {
            fail("Error updating object", e);
        }
    }

    /**
     * Create and then remove the object.
     */
    @Test
    public void RemoveSection() {
        Section section = new Section();
        try {
            managers.getSectionManager().add(Arrays.asList(section));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }

        int id = section.getId();

        try {
            managers.getSectionManager().remove(Arrays.asList(section));
        } catch (ManagerException e) {
            fail("Error removing object", e);
        }

        try {
            List<Section> list = managers.getSectionManager().list();
            for (Section e : list) {
                if (e.getId() == id) {
                    Assert.fail("Object still exists");
                }
            }
        } catch (ManagerException e) {
            fail("Error listing object", e);
        }

    }
}
