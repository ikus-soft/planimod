/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static com.planimod.core.ManagerTestCase.*;
import static com.planimod.test.TimeUtils.*;

/**
 * Utility class to create shifts.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ShiftUtils {

    public Shift friDay;
    public Shift friEvening;
    public Shift friNight;
    public Shift monDay;
    public Shift monEvening;
    public Shift monEveningRep;
    public Shift monNight;
    public Shift monRep;
    public Shift satDay;
    public Shift satEvening;
    public Shift sunDay;
    public Shift sunEvening;
    public Shift thuDay;
    public Shift thuEvening;
    public Shift thuEveningRep;
    public Shift thuNight;
    public Shift thuRep;
    public Shift tueDay;
    public Shift tueEvening;
    public Shift tueEveningRep;
    public Shift tueNight;
    public Shift tueRep;
    public Shift wedDay;
    public Shift wedEvening;
    public Shift wedEveningRep;
    public Shift wedNight;
    public Shift wedRep;

    public Team weekDay;
    public Team weekDayRep;
    public Date end;
    public Team weekEndDay;
    public Team weekEndEvening;
    public Team weekEvening;
    public Team weekEveningRep;
    public Team weekNight;
    public Date start;

    public Collection<Shift> getShifts() {
        return Arrays.asList(
                friDay,
                friEvening,
                friNight,
                monDay,
                monEvening,
                monEveningRep,
                monNight,
                monRep,
                satDay,
                satEvening,
                sunDay,
                sunEvening,
                thuDay,
                thuEvening,
                thuEveningRep,
                thuNight,
                thuRep,
                tueDay,
                tueEvening,
                tueEveningRep,
                tueNight,
                tueRep,
                wedDay,
                wedEvening,
                wedEveningRep,
                wedNight,
                wedRep);
    }

    /**
     * Create extended shifts.
     * 
     * @param mgr
     */
    public void addExtendedShifts(PlanimodManagers mgr) {
        weekDayRep = addTeam(mgr, "Jours de semaine - Remplacement");
        monRep = addShift(mgr, weekDayRep, time("Mon 8:00"), time("Mon 15:00"));
        tueRep = addShift(mgr, weekDayRep, time("Tue 8:00"), time("Tue 15:00"));
        wedRep = addShift(mgr, weekDayRep, time("Wed 8:00"), time("Wed 15:00"));
        thuRep = addShift(mgr, weekDayRep, time("Thu 8:00"), time("Thu 15:00"));

        weekEveningRep = addTeam(mgr, "Soir de semaine - Remplacement");
        monEveningRep = addShift(mgr, weekEveningRep, time("Mon 18:00"), time("Tue 1:00"));
        tueEveningRep = addShift(mgr, weekEveningRep, time("Tue 18:00"), time("Wed 1:00"));
        wedEveningRep = addShift(mgr, weekEveningRep, time("Wed 18:00"), time("Thu 1:00"));
        thuEveningRep = addShift(mgr, weekEveningRep, time("Thu 18:00"), time("Fri 1:00"));

        weekNight = addTeam(mgr, "Nuit semaine - Salubrité");
        monNight = addShift(mgr, weekNight, time("Mon 22:00"), time("Tue 7:00"));
        tueNight = addShift(mgr, weekNight, time("Tue 22:00"), time("Wed 7:00"));
        wedNight = addShift(mgr, weekNight, time("Wed 22:00"), time("Thu 7:00"));
        thuNight = addShift(mgr, weekNight, time("Thu 22:00"), time("Fri 7:00"));
        friNight = addShift(mgr, weekNight, time("Fri 22:00"), time("Sat 7:00"));
    }

    public void addShifts(PlanimodManagers mgr) {

        // Sets week start & end
        this.start = time("Sun 0:00");
        this.end = time("Sat 23:29");

        // Day
        weekDay = addTeam(mgr, "Jours de semaine");
        monDay = addShift(mgr, weekDay, time("Mon 6:00"), time("Mon 16:00"));
        tueDay = addShift(mgr, weekDay, time("Tue 6:00"), time("Tue 16:00"));
        wedDay = addShift(mgr, weekDay, time("Wed 6:00"), time("Wed 16:00"));
        thuDay = addShift(mgr, weekDay, time("Thu 6:00"), time("Thu 16:00"));

        weekEvening = addTeam(mgr, "Soir de semaine");
        monEvening = addShift(mgr, weekEvening, time("Mon 16:00"), time("Tue 2:00"));
        tueEvening = addShift(mgr, weekEvening, time("Tue 16:00"), time("Wed 2:00"));
        wedEvening = addShift(mgr, weekEvening, time("Wed 16:00"), time("Thu 2:00"));
        thuEvening = addShift(mgr, weekEvening, time("Thu 16:00"), time("Fri 2:00"));

        weekEndDay = addTeam(mgr, "Jour de fin de semaine");
        sunDay = addShift(mgr, weekEndDay, time("Sun 6:00"), time("Sun 18:00"));
        friDay = addShift(mgr, weekEndDay, time("Fri 6:00"), time("Fri 18:00"));
        satDay = addShift(mgr, weekEndDay, time("Sat 6:00"), time("Sat 18:00"));

        weekEndEvening = addTeam(mgr, "Soir de fin de semaine");
        sunEvening = addShift(mgr, weekEndEvening, time("Sun 00:00"), time("Sun 6:00"));
        addShift(mgr, weekEndEvening, time("Sun 18:00"), time("Mon 6:00"));
        friEvening = addShift(mgr, weekEndEvening, time("Fri 18:00"), time("Sat 6:00"));
        satEvening = addShift(mgr, weekEndEvening, time("Sat 18:00"), time("Sat 23:59"));

    }

}