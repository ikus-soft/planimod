/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class HumanNaturalComparatorTest {

    @Test
    public void testCompare() {

        HumanNaturalComparator comparator = new HumanNaturalComparator();

        // String[] strings = new String[] { "1-2", "1-02", "1-20", "10-20",
        // "fred", "jane", "pic01", "pic02", "pic2", "pic02a", "pic3",
        // "pic4", "pic 4 else", "pic 5", "pic05", "pic 5",
        // "pic 5 something", "pic 6", "pic 7", "pic100", "pic100a",
        // "pic120", "pic121", "pic02000", "tom", "x2-g8", "x2-y7",
        // "x2-y08", "x8-y8" };

        assertTrue(comparator.compare("1-2", "1-02") == 0);
        assertTrue(comparator.compare("1-02", "1-20") < 0);
        assertTrue(comparator.compare("1-20", "10-20") < 0);
        assertTrue(comparator.compare("10-20", "fred") < 0);
        assertTrue(comparator.compare("fred", "jane") < 0);
        assertTrue(comparator.compare("jane", "pic01") < 0);
        assertTrue(comparator.compare("pic01", "pic02") < 0);
        assertTrue(comparator.compare("pic02", "pic2") == 0);
        assertTrue(comparator.compare("pic2", "pic02a") < 0);
        assertTrue(comparator.compare("pic02a", "pic3") < 0);
        assertTrue(comparator.compare("pic3", "pic4") < 0);
        assertTrue(comparator.compare("pic4", "pic 4 else") < 0);
        assertTrue(comparator.compare("pic 4 else", "pic 5") < 0);
        assertTrue(comparator.compare("pic 5", "pic05") == 0);
        assertTrue(comparator.compare("pic 5", "pic 5 something") < 0);
        assertTrue(comparator.compare("pic 5 something", "pic 6") < 0);
        assertTrue(comparator.compare("pic 6", "pic   7") < 0);
        assertTrue(comparator.compare("pic   7", "pic100") < 0);
        assertTrue(comparator.compare("pic100", "pic100a") < 0);
        assertTrue(comparator.compare("pic100a", "pic120") < 0);
        assertTrue(comparator.compare("pic120", "pic121") < 0);
        assertTrue(comparator.compare("pic121", "pic02000") < 0);
        assertTrue(comparator.compare("pic02000", "tom") < 0);
        assertTrue(comparator.compare("tom", "x2-g8") < 0);
        assertTrue(comparator.compare("x2-g8", "x2-y7") < 0);
        assertTrue(comparator.compare("x2-y7", "x2-y08") < 0);
        assertTrue(comparator.compare("x2-y08", "x8-y8") < 0);

    }
}
