/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import static com.planimod.test.TimeUtils.date;
import static org.junit.Assert.assertTrue;

import java.util.Comparator;

import org.junit.Test;

import com.planimod.core.Employee;
import com.planimod.core.EmployeePreference;
import com.planimod.core.Section;

public class EmployeePreferenceComparatorsTest {

    Comparator<EmployeePreference> seniority = EmployeePreferenceComparators.bySeniority();

    @Test
    public void testBySeniority_WithoutPreferences() {

        Employee emp1 = new Employee();
        emp1.setHireDate(date("1998-10-07"));
        EmployeePreference pref1 = new EmployeePreference();
        pref1.setEmployee(emp1);

        Employee emp2 = new Employee();
        emp2.setHireDate(date("1977-07-07"));
        EmployeePreference pref2 = new EmployeePreference();
        pref2.setEmployee(emp2);

        assertTrue(seniority.compare(pref1, pref2) > 0);

    }

    @Test
    public void testBySeniority_WithPreferences() {

        Section section = new Section();

        Employee emp1 = new Employee();
        emp1.setHireDate(date("1998-10-07"));
        EmployeePreference pref1 = new EmployeePreference();
        pref1.setEmployee(emp1);
        pref1.setPreferredSection(section);

        Employee emp2 = new Employee();
        emp2.setHireDate(date("1977-07-07"));
        EmployeePreference pref2 = new EmployeePreference();
        pref2.setEmployee(emp2);
        pref2.setPreferredSection(section);

        assertTrue(seniority.compare(pref1, pref2) > 0);

    }

    @Test
    public void testBySeniority_WithAndWithoutPreferences() {

        Section section = new Section();

        Employee emp1 = new Employee();
        emp1.setHireDate(date("1998-10-07"));
        EmployeePreference pref1 = new EmployeePreference();
        pref1.setEmployee(emp1);
        pref1.setPreferredSection(section);

        Employee emp2 = new Employee();
        emp2.setHireDate(date("1977-07-07"));
        EmployeePreference pref2 = new EmployeePreference();
        pref2.setEmployee(emp2);

        assertTrue(seniority.compare(pref1, pref2) < 0);

    }

    @Test
    public void testBySeniority_WithNullHiredate() {

        Section section = new Section();

        Employee emp1 = new Employee();
        emp1.setHireDate(date("1998-10-07"));
        EmployeePreference pref1 = new EmployeePreference();
        pref1.setEmployee(emp1);
        pref1.setPreferredSection(section);

        Employee emp2 = new Employee();
        emp2.setHireDate(null);
        EmployeePreference pref2 = new EmployeePreference();
        pref2.setEmployee(emp2);

        assertTrue(seniority.compare(pref1, pref2) < 0);

    }

    @Test
    public void testBySeniority_WithNull() {

        Section section = new Section();

        Employee emp1 = new Employee();
        emp1.setHireDate(date("1998-10-07"));
        EmployeePreference pref1 = new EmployeePreference();
        pref1.setEmployee(emp1);
        pref1.setPreferredSection(section);

        assertTrue(seniority.compare(pref1, null) < 0);

    }

    @Test
    public void testBySeniority_WithNullEmployee() {

        Employee emp1 = new Employee();
        emp1.setHireDate(date("1998-10-07"));
        EmployeePreference pref1 = new EmployeePreference();
        pref1.setEmployee(emp1);

        EmployeePreference pref2 = new EmployeePreference();
        pref2.setEmployee(null);

        assertTrue(seniority.compare(pref1, pref2) < 0);

    }
}
