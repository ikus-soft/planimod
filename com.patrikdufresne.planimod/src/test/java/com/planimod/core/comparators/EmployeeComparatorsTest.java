/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import static com.planimod.test.TimeUtils.date;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import org.junit.Test;

import com.planimod.core.Employee;
import com.planimod.core.ManagerTestCase;

public class EmployeeComparatorsTest extends ManagerTestCase {

    @Test
    public void testBySeniority() {

        Employee empb = addEmployee(managers, "Employee#b");
        setHireDate(managers, empb, date("2009-01-02"));

        Employee empc = addEmployee(managers, "Employee#c");
        setHireDate(managers, empc, date("2009-01-01"));

        Employee empa = addEmployee(managers, "Employee#a");
        setHireDate(managers, empa, date("2009-01-01"));

        Employee emp5 = addEmployee(managers, "Employee#5");

        Employee emp4 = addEmployee(managers, "Employee#4");

        ArrayList<Employee> list = new ArrayList<Employee>(Arrays.asList(empb, empc, empa, emp5, emp4));
        Collections.sort(list, EmployeeComparators.bySeniority());

        assertEquals("Wrong number of employee", 5, list.size());

        assertEquals("Wrong order", empa, list.get(0));

        assertEquals("Wrong order", empc, list.get(1));

        assertEquals("Wrong order", empb, list.get(2));

        assertEquals("Wrong order", emp4, list.get(3));

        assertEquals("Wrong order", emp5, list.get(4));

        assertTrue("Wrong compare", EmployeeComparators.bySeniority().compare(empc, empa) > 0);

        assertTrue("Wrong compare", EmployeeComparators.bySeniority().compare(emp4, emp5) < 0);

        assertTrue("Wrong compare", EmployeeComparators.bySeniority().compare(emp4, empa) > 0);

    }

}
