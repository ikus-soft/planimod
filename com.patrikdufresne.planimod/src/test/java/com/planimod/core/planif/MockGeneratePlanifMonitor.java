/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import com.planimod.core.planif.ICallbackReason;
import com.planimod.core.planif.IGeneratePlanifProgressMonitor;

public class MockGeneratePlanifMonitor implements IGeneratePlanifProgressMonitor {

    @Override
    public void done() {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean isCanceled() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void beginSearch(int work) {
        // TODO Auto-generated method stub

    }

    @Override
    public void worked(int work) {
        // TODO Auto-generated method stub

    }

    @Override
    public void setStep(GeneratePlanifProgressMonitorStep step) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean callbackContinue(ICallbackReason reason) {
        // TODO Auto-generated method stub
        return true;
    }

}
