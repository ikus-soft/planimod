/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import com.planimod.core.planif.TableTest;

@RunWith(Suite.class)
@Suite.SuiteClasses( {
        ApplicationSettingManagerTest.class,
        TimeRangesTest.class,
        EmployeeManagerTest.class,
        EmployeePreferenceManagerTest.class,
        NonAvailabilityEventManagerTest.class,
        PlanifEventManagerGeneratePlanifTest.class,
        PlanifEventManagerGeneratePlanifTest20110925.class,
        PlanifEventManagerGeneratePlanifTest20120930.class,
        PlanifEventManagerGeneratePlanifTest20121028.class,
        PlanifEventManagerGeneratePlanifTest20121104.class,
        PlanifEventManagerGeneratePlanifTest20121111.class,
        PlanifEventManagerGeneratePlanifTest20121202.class,
        PlanifEventManagerGeneratePlanifTest20121209.class,
        PlanifEventManagerGeneratePlanifTest20130106.class,
        PlanifEventManagerGeneratePlanifTest20130113.class,
        PositionManagerTest.class,
        ProductionEventManagerTest.class,
        ProductManagerTest.class,
        ProductPositionManagersTest.class,
        QualificationManagerTest.class,
        SectionManagerTest.class,
        ShiftManagerTest.class,
        TableTest.class,
        TaskManagerTest.class,
        TeamManagerTest.class,
        com.planimod.core.comparators.AllTests.class })
public class AllTests {

}
