/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.patrikdufresne.managers.ManagerEvent;
import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.Team;

/**
 * This class intend to test the functionnalities related to the shift.
 * 
 * @author patapouf
 * 
 */
public class TeamManagerTest extends ManagerTestCase {

    /**
     * List the shifts.
     */
    @Test
    public void ListShift() {
        Team team1 = addTeam(managers, null);
        Team team2 = addTeam(managers, null);
        Team team3 = addTeam(managers, null);

        List<Team> teams;
        try {
            teams = managers.getTeamManager().list();
        } catch (ManagerException e) {
            fail("Error listing the shift", e);
            return;
        }

        assertEquals("Wrong number of shifts", 3, teams.size());

        assertEquals("Doesn't contains shift1", true, teams.contains(team1));
        assertEquals("Doesn't contains shift2", true, teams.contains(team2));
        assertEquals("Doesn't contains shift3", true, teams.contains(team3));

    }

    /**
     * Add the object.
     */
    @Test
    public void add() {
        Team shift = new Team();
        try {
            managers.getTeamManager().add(Arrays.asList(shift));
        } catch (ManagerException e) {
            fail("Error adding Section", e);
        }
    }

    @Test
    public void update() {
        Team shift = new Team();
        try {
            managers.getTeamManager().add(Arrays.asList(shift));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }

        shift.setName("week-end");

        try {
            managers.getTeamManager().update(Arrays.asList(shift));
        } catch (ManagerException e) {
            fail("Error updating object", e);
        }

        // Reload the shift from it's if
        Team shift2;
        try {
            shift2 = (Team) managers.getTeamManager().get(shift.getId());
        } catch (ManagerException e) {
            fail("Error to reload the object", e);
            return;
        }

        // Check if the modification occured
        Assert.assertEquals("Object not updated", shift.getName(), shift2.getName());

    }

    /**
     * Create and then remove object.
     */
    @Test
    public void remove_WithTeam_ExpectEvent() {

        addManagerObserver(ManagerEvent.REMOVE, Team.class);

        // Add Shift
        Team team = new Team();
        try {
            managers.getTeamManager().add(Arrays.asList(team));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }

        // Remove Shift
        int id = team.getId();
        try {
            managers.getTeamManager().remove(Arrays.asList(team));
        } catch (ManagerException e) {
            fail("Error removing object", e);
        }
        assertManagerEvent(1, getManagerEvents(), ManagerEvent.REMOVE, team);

        // List
        try {
            List<Team> list = managers.getTeamManager().list();
            for (Team s : list) {
                if (s.getId() == id) {
                    Assert.fail("Object still exists");
                }
            }

        } catch (ManagerException e) {
            fail("Error listing object", e);
        }
    }

}
