/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import static com.planimod.test.TimeUtils.date;
import static com.planimod.test.TimeUtils.dateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.planif.GeneratePlanifContext;
import com.planimod.core.planif.MockGeneratePlanifMonitor;

/**
 * This test cases is a real scenario from Grissol Plant on Jan 20, 2013
 * 
 * @author Patrik Dufresne
 */
public class PlanifEventManagerGeneratePlanifTest20130120 extends ManagerTestCase {

    private static List<ProductionEvent> select(List<ProductionEvent> asList, Shift shift) {
        List<ProductionEvent> list = new ArrayList<ProductionEvent>();
        for (ProductionEvent e : asList) {
            if (e != null && e.getShift().equals(shift)) {
                list.add(e);
            }
        }
        return list;
    }

    public PlanifEventManagerGeneratePlanifTest20130120() {
        super("./unittest20130120", true, false);
    }

    Employee empDonaldTheriault;
    Employee empLouiseVeillette;
    Employee empCaroleMorand;
    Employee empLucieGarceau;
    Employee empMichelineDemers;
    Employee empJeanpierreAuger;
    Employee empRejeanBrabant;
    Employee empMoniqueLeblond;
    Employee empRealGosselin;
    Employee empLucieLacoste;
    Employee empJohanneLemieux;
    Employee empManonTremblay;
    Employee empBernardBerube;
    Employee empRobertLazure;
    Employee empLindaBoisvert;
    Employee empSergeRobidoux;
    Employee empMichelDaniel;
    Employee empCaroleRaymond;
    Employee empGillesGosselin;
    Employee empFrancoisBeaulne;
    Employee empFrancineGuerin;
    Employee empJeanguyRicher;
    Employee empMarcelDalphond;
    Employee empMichelMeunier;
    Employee empPierreLamarque;
    Employee empGerardLanteigne;
    Employee empJeanLatour;
    Employee empPierretteDupras;
    Employee empDanielNault;
    Employee empRaynaldStarnaud;
    Employee empNicoleFortin;
    Employee empNormandArsenault;
    Employee empDeniseDaigneault;
    Employee empFrancineLabbe;
    Employee empClaudineRochefort;
    Employee empSuzanneCouturier;
    Employee empMichelTougas;
    Employee empDanielleBeaudry;
    Employee empLucieGuay;
    Employee empRogerDagenais;
    Employee empMichelJeanneau;
    Employee empDenisPilon;
    Employee empSuzanneGagnon;
    Employee empRachelBergevin;
    Employee empRejeanRoy;
    Employee empHuguesDenault;
    Employee empRolandJrBoucher;
    Employee empBernardJolin;
    Employee empSartoTremblay;
    Employee empDianeGirard;
    Employee empMarioPaille;
    Employee empGinetteOuellette;
    Employee empGuylaineGuy;
    Employee empPierretteLamothe;
    Employee empMarcBellemare;
    Employee empMichelineLegault;
    Employee empJoseeConstantineau;
    Employee empMadelaineMarleau;
    Employee empManonPoissant;
    Employee empFrancineLemieux;
    Employee empCarmenBrais;
    Employee empFrancoiseTrudeau;
    Employee empEricRichard;
    Employee empNancyTheoret;
    Employee empLiseCampeau;
    Employee empLucieLeavey;
    Employee empLyndaLajoie;
    Employee empJeanfrancoisBreton;
    Employee empStephaneJacques;
    Employee empBrigitteBouchard;
    Employee empMartinDube;
    Employee empSylviePineault;
    Employee empJoseeLapierre;
    Employee empHachezGabriel;
    Employee empSandraDupuis;
    Employee empLucRoy;
    Employee empLucieCaron;
    Employee empRachelMoise;
    Employee empCatherinePiette;
    Employee empChantalXavier;
    Employee empDominicHoude;
    Employee empFrancoisParent;
    Employee empSolangeGirard;
    Employee empMartinLina;
    Employee empLiseJoncas;
    Employee empNathalieReid;
    Employee empCecileCouillard;
    Employee empSylvainJulien;
    Employee empSylvainCarriere;
    Employee empRichardVaillant;
    Employee empFranceBoyer;
    Employee empIsabelleLeclerc;
    Employee empFrancoisArcoite;
    Employee empSabrinaDupuis;
    Employee empIvanhoeMaisonneuve;
    Employee empMathieuGuy;
    Employee empDaisyBourget;
    Employee empMathewBellemare;
    Employee empAlexandreDube;
    Employee empAnnickPigeon;
    Employee empDanielDuquette;
    Employee empJohanneDuval;
    Employee empCelineVadnais;
    Employee empMarcGrondin;
    Employee empMarcelLegault;
    Employee empNicolasLegault;
    Employee empStefanieReynolds;
    Employee empGinoLemoine;
    Employee empMarioLongtin;
    Employee empRobertAllen;
    Employee empChristopheDeneault;
    Employee empPierreLemelin;
    Employee empRobertShonfield;
    Employee empLouisCarlPage;
    Employee empMaryCamilleGirard;
    Employee empThierryLeHesran;
    Employee empNathalieBedard;
    Employee empWayneDavid;
    Employee empPierreGagne;
    Employee empDianeDugas;
    Employee empDanielGuillemette;

    Position posTolier;
    Position posGeneraleFourAideFournier;
    Position posGeneraleDemouleuse;
    Position posEquarisseur;
    Position posGeneralEmballagePainsMinces;
    Position posGeneralEmballageCroutonsVrac;
    Position posGeneralEmballageSnackBote;
    Position posGeneralSalubrite;
    Position posOperateurLigneAPain;
    Position posPrefarinePremelange;
    Position posPetrisseurAPain;
    Position posOperateurEmballageFmc;
    Position posOperateurBatonCompteuse;
    Position posOperateurDeLigneBiscottebagHorsDoeuvre;
    Position posOperateurEmballageTriangle;
    Position posOperateurLigneFourMelba;
    Position posOperateurEmballageMelba;
    Position posOperateurEmballageChap25lb;
    Position posRecuperateurEmballage;
    Position posOperateurDeRemplacementSnack;
    Position posOperateurEnsacheuseVerticalSnack;
    Position posOperateurPetrisseurSnack;
    Position posOperateurDeLigneBaton;
    Position posPreposeAuMelange;
    Position posOperateurGrispac;
    Position posFournier;
    Position posPreposeSalubrite;
    Position posOperateurGerbeuseVerification;
    Position posPreposeAuxEpicesEtReparation;
    Position posElectrotechniciens;
    Position posMecanicien;
    Position posHuileurGraisseurEntretienPreventif;
    Position posFormation;
    Position posGeneraleEmballageBiscotte;
    Position posGeneraleTrancheuseBiscotte;
    Position posGeneraleEmballageBaguettine;
    Position posGeneraleTrancheuseBaguettine;
    Position posGeneraleEmballageBaton;
    Position posGeneraleEmballageFmc;
    Position posGeneralTrancheuseMelba;
    Position posGeneralEmballageMelba;
    Position posRemplacent;
    Position posAbsent;
    Position posOperateurRemplacementBoul;
    Position posOperateurRemplacementBaton;
    Position posTolierMiniBouchees;
    Position posGeneraleFourMiniBouchees;

    List<Position> group3Boulangerie;
    List<Position> group1Emballage;
    List<Position> group2Snack;
    List<Position> group4Baton;
    List<Position> group5Salubrite;
    List<Position> group6Expedition;
    List<Position> group7Maintenance;
    List<Position> group8Formation;
    List<Position> group9Absent;

    List<ProductionEvent> events1JoursDeSemaineWed;
    List<ProductionEvent> events1JoursDeSemaineTue;
    List<ProductionEvent> events1JoursDeSemaineMon;
    List<ProductionEvent> events1JoursDeSemaineThu;
    List<ProductionEvent> events2JourRemplacementSemaineWed;
    List<ProductionEvent> events2JourRemplacementSemaineTue;
    List<ProductionEvent> events2JourRemplacementSemaineMon;
    List<ProductionEvent> events2JourRemplacementSemaineThu;
    List<ProductionEvent> events3SoirSemaineMon;
    List<ProductionEvent> events3SoirSemaineTue;
    List<ProductionEvent> events3SoirSemaineWed;
    List<ProductionEvent> events3SoirSemaineThu;
    List<ProductionEvent> events4SoirRemplacementSemaineThu;
    List<ProductionEvent> events4SoirRemplacementSemaineTue;
    List<ProductionEvent> events4SoirRemplacementSemaineWed;
    List<ProductionEvent> events4SoirRemplacementSemaineMon;
    List<ProductionEvent> events9NuitSemaineDimJeuWed;
    List<ProductionEvent> events9NuitSemaineDimJeuThu;
    List<ProductionEvent> events9NuitSemaineDimJeuTue;
    List<ProductionEvent> events9NuitSemaineDimJeuMon;
    List<ProductionEvent> events9NuitSemaineDimJeuSun;
    List<ProductionEvent> events10NuitSemaineLunVenFri;
    List<ProductionEvent> events10NuitSemaineLunVenWed;
    List<ProductionEvent> events10NuitSemaineLunVenThu;
    List<ProductionEvent> events10NuitSemaineLunVenMon;
    List<ProductionEvent> events10NuitSemaineLunVenTue;
    List<ProductionEvent> events5JourFinDeSemaineSun;
    List<ProductionEvent> events5JourFinDeSemaineFri;
    List<ProductionEvent> events5JourFinDeSemaineSat;
    List<ProductionEvent> events6JoursRemplacementFinDeSemaineSun;
    List<ProductionEvent> events6JoursRemplacementFinDeSemaineFri;
    List<ProductionEvent> events6JoursRemplacementFinDeSemaineSat;
    List<ProductionEvent> events7SoirDeFinDeSemaineSun;
    List<ProductionEvent> events7SoirDeFinDeSemaineFri;
    List<ProductionEvent> events7SoirDeFinDeSemaineSat;
    List<ProductionEvent> events8SoirRemplacementFinDeSemaineSat;
    List<ProductionEvent> events8SoirRemplacementFinDeSemaineSun;
    List<ProductionEvent> events8SoirRemplacementFinDeSemaineFri;

    Product prod56;
    Product prod69;
    Product prod600;
    Product prod650;
    Product prod660;
    Product prod663;
    Product prod670;
    Product prod680;
    Product prod684;
    Product prod2162;
    Product prod24610;
    Product prod24665;
    Product prod41028;
    Product prod41200;
    Product prod41205;
    Product prod41270;
    Product prod41280;
    Product prod41281;
    Product prod41290;
    Product prod41340;
    Product prod41350;
    Product prod41352;
    Product prod41354;
    Product prod41606;
    Product prod42040;
    Product prod42042;
    Product prod42044;
    Product prod42046;
    Product prod42048;
    Product prod42049;
    Product prod42050;
    Product prod42051;
    Product prod42052;
    Product prod42054;
    Product prod42056;
    Product prod42060;
    Product prod42066;
    Product prod42067;
    Product prod42068;
    Product prod42081;
    Product prod42083;
    Product prod42140;
    Product prod42142;
    Product prod42570;
    Product prod44900;
    Product prod44930;
    Product prod45000;
    Product prod45010;
    Product prod45020;
    Product prod45030;
    Product prod45050;
    Product prod45052;
    Product prod45054;
    Product prod45100;
    Product prod45111;
    Product prod45130;
    Product prod45200;
    Product prod45250;
    Product prod45575;
    Product prod45600;
    Product prod45602;
    Product prod45615;
    Product prod45618;
    Product prod45620;
    Product prod45622;
    Product prod45720;
    Product prod45730;
    Product prod45740;
    Product prod45900;
    Product prod45950;
    Product prod45952;
    Product prod45992;
    Product prod45993;
    Product prod45994;
    Product prod50390;
    Product prod50402;
    Product prod50404;
    Product prod50420;
    Product prod50430;
    Product prod92419;
    Product prod92421;
    Product prod924312;
    Product prod93020;
    Product prodRecuperateur;
    Product prodFabricationPains;
    Product prodPainsBaguette;
    Product prodPetrisseurBoulangerie;
    Product prodFabricationMulsi;
    Product prodGeneralSalubrite;
    Product prodPreposeAuxEpices;
    Product prodOperateurGerbeuse;
    Product prodMecaniciens;
    Product prodElectromecaniciens;
    Product prodHuileur;
    Product prodRemplacent;
    Product prodSalubriteNuit;
    Product prodFormation;
    Product prodOperateursSpooner;
    Product prod924311;
    Product prodOperateurEmballageMelba;
    Product prodAbsent;
    Product prodGeneralEmballage;
    Product prod92416;
    Product prodPreposeSalubrite;

    Section sec3Boulangerie;
    Section sec1Emballage;
    Section sec2Snack;
    Section sec4Baton;
    Section sec5Salubrite;
    Section sec6Expedition;
    Section sec7Maintenance;
    Section sec8Formation;
    Section sec9Absent;

    Shift joursDeSemaineWed;
    Shift joursDeSemaineTue;
    Shift joursDeSemaineMon;
    Shift joursDeSemaineThu;
    Shift jourRemplacementSemaineWed;
    Shift jourRemplacementSemaineTue;
    Shift jourRemplacementSemaineMon;
    Shift jourRemplacementSemaineThu;
    Shift soirSemaineMon;
    Shift soirSemaineTue;
    Shift soirSemaineWed;
    Shift soirSemaineThu;
    Shift soirRemplacementSemaineThu;
    Shift soirRemplacementSemaineTue;
    Shift soirRemplacementSemaineWed;
    Shift soirRemplacementSemaineMon;
    Shift nuitSemaineDimJeuWed;
    Shift nuitSemaineDimJeuThu;
    Shift nuitSemaineDimJeuTue;
    Shift nuitSemaineDimJeuMon;
    Shift nuitSemaineDimJeuSun;
    Shift nuitSemaineLunVenFri;
    Shift nuitSemaineLunVenWed;
    Shift nuitSemaineLunVenThu;
    Shift nuitSemaineLunVenMon;
    Shift nuitSemaineLunVenTue;
    Shift jourFinDeSemaineSun;
    Shift jourFinDeSemaineFri;
    Shift jourFinDeSemaineSat;
    Shift joursRemplacementFinDeSemaineSun;
    Shift joursRemplacementFinDeSemaineFri;
    Shift joursRemplacementFinDeSemaineSat;
    Shift soirDeFinDeSemaineSun;
    Shift soirDeFinDeSemaineFri;
    Shift soirDeFinDeSemaineSat;
    Shift soirRemplacementFinDeSemaineSat;
    Shift soirRemplacementFinDeSemaineSun;
    Shift soirRemplacementFinDeSemaineFri;

    Team team1JoursDeSemaine;
    Team team2JourRemplacementSemaine;
    Team team3SoirSemaine;
    Team team4SoirRemplacementSemaine;
    Team team9NuitSemaineDimJeu;
    Team team10NuitSemaineLunVen;
    Team team5JourFinDeSemaine;
    Team team6JoursRemplacementFinDeSemaine;
    Team team7SoirDeFinDeSemaine;
    Team team8SoirRemplacementFinDeSemaine;
    private ArrayList<ProductionEvent> events;

    /**
     * Create employees.
     * 
     * <pre>
     * SELECT 'Employee ' || camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ';' FROM EMPLOYEE
     * SELECT camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ' = addEmployee(managers, ' || CASEWHEN(EMPLOYEE.REFID IS NOT NULL, '"' || EMPLOYEE.REFID || '"', 'null') || ', "' || EMPLOYEE.FIRSTNAME || '", "' || EMPLOYEE.LASTNAME || '", dateTime("' || FORMATDATETIME(EMPLOYEE.HIREDATE,'yyyy-MM-dd EEE HH:mm') || '"));'  FROM EMPLOYEE
     * </pre>
     */
    private void createEmployees() {

        empDonaldTheriault = addEmployee(managers, "12172", "Malagigi", "Cotuand", dateTime("1970-08-26 Wed 00:00"));
        empLouiseVeillette = addEmployee(managers, "12173", "Thibaut", "Charest", dateTime("1970-10-05 Mon 00:00"));
        empCaroleMorand = addEmployee(managers, "12174", "Kari", "Laramée", dateTime("1971-05-31 Mon 00:00"));
        empLucieGarceau = addEmployee(managers, "12175", "Tyson", "Chaussée", dateTime("1972-08-28 Mon 00:00"));
        empMichelineDemers = addEmployee(managers, "12176", "Didier", "Gougeon", dateTime("1973-02-05 Mon 00:00"));
        empJeanpierreAuger = addEmployee(managers, "12177", "Élisabeth", "Sicard", dateTime("1973-06-19 Tue 00:00"));
        empRejeanBrabant = addEmployee(managers, "12178", "Dorothée", "Carrière", dateTime("1973-11-07 Wed 00:00"));
        empMoniqueLeblond = addEmployee(managers, "12179", "Jules", "Plourde", dateTime("1973-11-07 Wed 01:00"));
        empRealGosselin = addEmployee(managers, "12180", "Iven", "René", dateTime("1973-11-30 Fri 00:00"));
        empLucieLacoste = addEmployee(managers, "12181", "Vaden", "Deserres", dateTime("1974-07-05 Fri 00:00"));
        empJohanneLemieux = addEmployee(managers, "12182", "Germain", "Adler", dateTime("1974-08-15 Thu 00:00"));
        empManonTremblay = addEmployee(managers, "12183", "Germaine", "Rivard", dateTime("1974-11-04 Mon 00:00"));
        empBernardBerube = addEmployee(managers, "12184", "Mirabelle", "Duval", dateTime("1975-09-17 Wed 00:00"));
        empRobertLazure = addEmployee(managers, "12185", "Norris", "Barrientos", dateTime("1976-02-11 Wed 00:00"));
        empLindaBoisvert = addEmployee(managers, "12186", "Vernon", "Corbeil", dateTime("1976-02-23 Mon 00:00"));
        empSergeRobidoux = addEmployee(managers, "12187", "Edmee", "Patenaude", dateTime("1976-02-24 Tue 00:00"));
        empMichelDaniel = addEmployee(managers, "12188", "Jewel", "Gauvin", dateTime("1976-04-16 Fri 00:00"));
        empCaroleRaymond = addEmployee(managers, "12189", "Didiane", "Simon", dateTime("1976-04-28 Wed 00:00"));
        empGillesGosselin = addEmployee(managers, "12190", "Aymon", "Quenneville", dateTime("1976-08-03 Tue 00:00"));
        empFrancoisBeaulne = addEmployee(managers, "12191", "Éric", "Gousse", dateTime("1977-02-14 Mon 00:00"));
        empFrancineGuerin = addEmployee(managers, "12192", "Carole", "Picard", dateTime("1977-02-28 Mon 00:00"));
        empJeanguyRicher = addEmployee(managers, "12193", "Cosette", "Plourde", dateTime("1977-06-01 Wed 00:00"));
        empMarcelDalphond = addEmployee(managers, "12194", "Guy", "Labossière", dateTime("1977-07-07 Thu 00:00"));
        empMichelMeunier = addEmployee(managers, "12195", "Hugh", "Gilbert", dateTime("1978-02-16 Thu 00:00"));
        empPierreLamarque = addEmployee(managers, "12196", "Mavise", "Grondin", dateTime("1979-05-29 Tue 00:00"));
        empGerardLanteigne = addEmployee(managers, "12197", "Laverne", "Laurent", dateTime("1979-08-07 Tue 00:00"));
        empJeanLatour = addEmployee(managers, "12198", "Belle", "Pichette", dateTime("1979-09-13 Thu 00:00"));
        empPierretteDupras = addEmployee(managers, "12199", "Senapus", "Huppé", dateTime("1980-09-04 Thu 00:00"));
        empDanielNault = addEmployee(managers, "12200", "Faye", "Renaud", dateTime("1980-09-23 Tue 00:00"));
        empRaynaldStarnaud = addEmployee(managers, "12201", "Oliver", "Saindon", dateTime("1980-11-26 Wed 00:00"));
        empNicoleFortin = addEmployee(managers, "12202", "Marsilius", "Boileau", dateTime("1981-01-23 Fri 00:00"));
        empNormandArsenault = addEmployee(managers, "12203", "Lance", "Bériault", dateTime("1981-07-13 Mon 00:00"));
        empDeniseDaigneault = addEmployee(managers, "12204", "Adorlee", "Grimard", dateTime("1981-07-13 Mon 02:00"));
        empFrancineLabbe = addEmployee(managers, "12205", "Mireille", "Maheu", dateTime("1981-07-30 Thu 00:00"));
        empClaudineRochefort = addEmployee(managers, "12206", "Marshall", "Parrot", dateTime("1981-08-05 Wed 00:00"));
        empSuzanneCouturier = addEmployee(managers, "12207", "Fealty", "Douffet", dateTime("1981-08-07 Fri 00:00"));
        empMichelTougas = addEmployee(managers, "12208", "Pascal", "St-Jacques", dateTime("1981-08-18 Tue 00:00"));
        empDanielleBeaudry = addEmployee(managers, "12209", "Suzette", "Petrie", dateTime("1981-08-19 Wed 00:00"));
        empLucieGuay = addEmployee(managers, "12210", "Roger", "Douffet", dateTime("1981-08-27 Thu 00:00"));
        empRogerDagenais = addEmployee(managers, "12211", "Vail", "Poulin", dateTime("1981-09-28 Mon 00:00"));
        empMichelJeanneau = addEmployee(managers, "12212", "Christine", "Beaupré", dateTime("1981-10-08 Thu 00:00"));
        empDenisPilon = addEmployee(managers, "12213", "Archaimbau", "Majory", dateTime("1981-10-13 Tue 00:00"));
        empSuzanneGagnon = addEmployee(managers, "12214", "D'Arcy", "Brunelle", dateTime("1981-10-13 Tue 01:00"));
        empRachelBergevin = addEmployee(managers, "12215", "Diane", "Chatigny", dateTime("1981-10-13 Tue 02:00"));
        empRejeanRoy = addEmployee(managers, "12216", "Rule", "Lang", dateTime("1985-02-11 Mon 00:00"));
        empHuguesDenault = addEmployee(managers, "12217", "Baptiste", "Faucher", dateTime("1985-02-12 Tue 00:00"));
        empRolandJrBoucher = addEmployee(managers, "12218", "Davet", "Plourde", dateTime("1985-04-30 Tue 00:00"));
        empBernardJolin = addEmployee(managers, "12219", "Arno", "Labrecque", dateTime("1985-09-03 Tue 00:00"));
        empSartoTremblay = addEmployee(managers, "12220", "Mandel", "Carignan", dateTime("1988-05-16 Mon 00:00"));
        empDianeGirard = addEmployee(managers, "12221", "Mirabelle", "Beaulieu", dateTime("1988-05-20 Fri 00:00"));
        empMarioPaille = addEmployee(managers, "12222", "Brice", "Levasseur", dateTime("1988-05-20 Fri 01:00"));
        empGinetteOuellette = addEmployee(managers, "12223", "Nanna", "Chartré", dateTime("1988-05-20 Fri 02:00"));
        empGuylaineGuy = addEmployee(managers, "12224", "Corette", "Dupuis", dateTime("1989-05-05 Fri 00:00"));
        empPierretteLamothe = addEmployee(managers, "12225", "Gérard", "Jalbert", dateTime("1989-05-19 Fri 00:00"));
        empMarcBellemare = addEmployee(managers, "12226", "Onfroi", "Cloutier", dateTime("1989-11-06 Mon 00:00"));
        empMichelineLegault = addEmployee(managers, "12227", "Anton", "Deniger", dateTime("1990-06-14 Thu 00:00"));
        empJoseeConstantineau = addEmployee(managers, "12228", "Manville", "Poirier", dateTime("1993-07-06 Tue 00:00"));
        empMadelaineMarleau = addEmployee(managers, "12229", "René", "Lachapelle", dateTime("1995-02-21 Tue 00:00"));
        empManonPoissant = addEmployee(managers, "12230", "Zara", "Cloutier", dateTime("1995-02-22 Wed 00:00"));
        empFrancineLemieux = addEmployee(managers, "12231", "Genevre", "Labrosse", dateTime("1995-06-27 Tue 00:00"));
        empCarmenBrais = addEmployee(managers, "12232", "Namo", "Chastain", dateTime("1995-09-05 Tue 00:00"));
        empFrancoiseTrudeau = addEmployee(managers, "12233", "Catherine", "Fresne", dateTime("1995-09-05 Tue 01:00"));
        empEricRichard = addEmployee(managers, "12234", "David", "Tisserand", dateTime("1995-09-12 Tue 00:00"));
        empNancyTheoret = addEmployee(managers, "12235", "Danielle", "Meunier", dateTime("1995-12-04 Mon 00:00"));
        empLiseCampeau = addEmployee(managers, "12236", "Francis", "Aucoin", dateTime("1997-11-10 Mon 00:00"));
        empLucieLeavey = addEmployee(managers, "12237", "Agramant", "Pichette", dateTime("1997-11-19 Wed 00:00"));
        empLyndaLajoie = addEmployee(managers, "12238", "Françoise", "Fecteau", dateTime("1997-11-20 Thu 00:00"));
        empJeanfrancoisBreton = addEmployee(managers, "12239", "Rachelle", "Truchon", dateTime("1998-01-26 Mon 00:00"));
        empStephaneJacques = addEmployee(managers, "12240", "Maslin", "Mailhot", dateTime("1998-01-27 Tue 00:00"));
        empBrigitteBouchard = addEmployee(managers, "12241", "Norris", "Martel", dateTime("1998-01-27 Tue 01:00"));
        empMartinDube = addEmployee(managers, "12242", "Evrard", "Voisine", dateTime("1998-03-17 Tue 00:00"));
        empSylviePineault = addEmployee(managers, "12243", "Robinette", "Parenteau", dateTime("1998-09-17 Thu 00:00"));
        empJoseeLapierre = addEmployee(managers, "12244", "Eliot", "Boulé", dateTime("1998-10-07 Wed 00:00"));
        empHachezGabriel = addEmployee(managers, "12245", "Royale", "Monty", dateTime("1998-10-08 Thu 00:00"));
        empSandraDupuis = addEmployee(managers, "12246", "Pénélope", "Villeneuve", dateTime("1998-12-08 Tue 00:00"));
        empLucRoy = addEmployee(managers, "12247", "Stéphane", "Chastain", dateTime("1999-02-10 Wed 00:00"));
        empLucieCaron = addEmployee(managers, "12248", "Vincent", "Vadnais", dateTime("1999-08-14 Sat 00:00"));
        empRachelMoise = addEmployee(managers, "12249", "Voleta", "Fugère", dateTime("2000-08-22 Tue 00:00"));
        empCatherinePiette = addEmployee(managers, "12250", "Esperanza", "Beaujolie", dateTime("2001-04-24 Tue 00:00"));
        empChantalXavier = addEmployee(managers, "12251", "Fortun", "Dubeau", dateTime("2001-05-26 Sat 00:00"));
        empDominicHoude = addEmployee(managers, "12252", "Eugène", "Laramée", dateTime("2001-08-21 Tue 00:00"));
        empFrancoisParent = addEmployee(managers, "12253", "Élisabeth", "Berger", dateTime("2001-08-26 Sun 00:00"));
        empSolangeGirard = addEmployee(managers, "12254", "Chappell", "Bondy", dateTime("2001-09-01 Sat 00:00"));
        empMartinLina = addEmployee(managers, "12255", "Solaine", "Blanchard", dateTime("2002-10-21 Mon 00:00"));
        empLiseJoncas = addEmployee(managers, "12256", "Lucille", "Soucy", dateTime("2003-01-21 Tue 00:00"));
        empNathalieReid = addEmployee(managers, "12257", "Armand", "Morin", dateTime("2004-03-14 Sun 00:00"));
        empCecileCouillard = addEmployee(managers, "12258", "Clothilde", "Louineaux", dateTime("2004-10-15 Fri 00:00"));
        empSylvainJulien = addEmployee(managers, "12259", "Lance", "Rhéaume", dateTime("2004-10-18 Mon 00:00"));
        empSylvainCarriere = addEmployee(managers, "12260", "Dalmace", "Vertefeuille", dateTime("2005-05-03 Tue 00:00"));
        empRichardVaillant = addEmployee(managers, "12261", "Joy", "Mainville", dateTime("2005-06-22 Wed 00:00"));
        empFranceBoyer = addEmployee(managers, "12262", "Gaetan", "Cormier", dateTime("2005-08-22 Mon 00:00"));
        empIsabelleLeclerc = addEmployee(managers, "12263", "Archard", "Mouet", dateTime("2006-09-05 Tue 00:00"));
        empFrancoisArcoite = addEmployee(managers, "12264", "Valentine", "Rivard", dateTime("2006-09-08 Fri 00:00"));
        empSabrinaDupuis = addEmployee(managers, "12265", "Eliot", "Marcheterre", dateTime("2006-11-27 Mon 00:00"));
        empIvanhoeMaisonneuve = addEmployee(managers, "12266", "Sabine", "Jalbert", dateTime("2007-02-19 Mon 00:00"));
        empMathieuGuy = addEmployee(managers, "12267", "Xavier", "Potvin", dateTime("2007-04-20 Fri 00:00"));
        empDaisyBourget = addEmployee(managers, "12268", "Clarice", "Cliche", dateTime("2007-09-18 Tue 00:00"));
        empMathewBellemare = addEmployee(managers, "12269", "Charles", "Martin", dateTime("2007-09-23 Sun 00:00"));
        empAlexandreDube = addEmployee(managers, "12270", "Catherine", "Laboissonnière", dateTime("2008-05-29 Thu 00:00"));
        empAnnickPigeon = addEmployee(managers, "12271", "Delmar", "Picard", dateTime("2008-09-02 Tue 00:00"));
        empDanielDuquette = addEmployee(managers, "12272", "Leone", "Mercier", dateTime("2008-10-17 Fri 00:00"));
        empJohanneDuval = addEmployee(managers, "12273", "Saville", "Bienvenue", dateTime("2009-08-25 Tue 00:00"));
        empCelineVadnais = addEmployee(managers, "12274", "Evrard", "Reault", dateTime("2009-09-08 Tue 00:00"));
        empMarcGrondin = addEmployee(managers, "12275", "Adrienne", "Lamarre", dateTime("2009-09-14 Mon 00:00"));
        empMarcelLegault = addEmployee(managers, "12276", "Methena", "Boutot", dateTime("2011-03-10 Thu 00:00"));
        empNicolasLegault = addEmployee(managers, "12277", "Alice", "Narcisse", dateTime("2011-03-10 Thu 01:00"));
        empStefanieReynolds = addEmployee(managers, "12278", "Leal", "Gareau", dateTime("2011-03-15 Tue 00:00"));
        empGinoLemoine = addEmployee(managers, "12279", "Forrest", "Begin", dateTime("2011-04-04 Mon 00:00"));
        empMarioLongtin = addEmployee(managers, "12280", "Ogier", "Lajeunesse", dateTime("2011-08-29 Mon 00:00"));
        empRobertAllen = addEmployee(managers, "12281", "Millicent", "Brochu", dateTime("2011-08-29 Mon 01:00"));
        empChristopheDeneault = addEmployee(managers, "12282", "Laurette", "Larivière", dateTime("2011-11-07 Mon 00:00"));
        empPierreLemelin = addEmployee(managers, "12283", "Soren", "Ayot", dateTime("2012-09-17 Mon 11:13"));
        empRobertShonfield = addEmployee(managers, "12284", "Arnou", "Bernier", dateTime("2012-09-19 Wed 11:14"));
        empLouisCarlPage = addEmployee(managers, "12285", "Jeannine", "Léveillé", dateTime("2012-09-21 Fri 11:14"));
        empMaryCamilleGirard = addEmployee(managers, "12286", "Fabrice", "Sabourin", dateTime("2012-10-23 Tue 11:14"));
        empThierryLeHesran = addEmployee(managers, "12287", "Melusina", "Fresne", dateTime("2012-10-09 Tue 11:15"));
        empNathalieBedard = addEmployee(managers, "12288", "Laurence", "Bureau", dateTime("2012-11-12 Mon 00:00"));
        empWayneDavid = addEmployee(managers, "12289", "Madeleine", "Goguen", dateTime("2012-11-12 Mon 02:00"));
        empPierreGagne = addEmployee(managers, "12290", "Michel", "Larocque", dateTime("2012-11-12 Mon 01:00"));
        empDianeDugas = addEmployee(managers, "12291", "Michèle", "Lalonde", dateTime("1973-06-19 Tue 01:00"));
        empDanielGuillemette = addEmployee(managers, "12292", "Pénélope", "Dagenais", dateTime("2012-09-28 Fri 11:15"));

    }

    /**
     * Create non-availabilities.
     * 
     * <pre>
     * SELECT 'addNonAvailability(managers, ' || camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ', dateTime("' || FORMATDATETIME(NONAVAILABILITY.STARTDATE, 'yyyy-MM-dd EEE HH:mm') || '"),  dateTime("' || FORMATDATETIME(NONAVAILABILITY.ENDDATE, 'yyyy-MM-dd EEE HH:mm') || '"));' FROM EMPLOYEE, NONAVAILABILITY WHERE NONAVAILABILITY.EMPLOYEE_ID = EMPLOYEE.ID
     * </pre>
     */
    private void createNonAvailabilities() {

        addNonAvailability(managers, empCecileCouillard, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-01-13 Sun 00:00"));
        addNonAvailability(managers, empNathalieReid, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-10-28 Sun 00:00"));
        addNonAvailability(managers, empMichelJeanneau, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-10-28 Sun 00:00"));
        addNonAvailability(managers, empGillesGosselin, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empLucieGuay, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-11-11 Sun 00:00"));
        addNonAvailability(managers, empDeniseDaigneault, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empDaisyBourget, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empLouiseVeillette, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empRejeanBrabant, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empLucieLacoste, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empSuzanneGagnon, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empGinetteOuellette, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empCarmenBrais, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empMartinDube, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empSylviePineault, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empChantalXavier, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-01-13 Sun 00:00"));
        addNonAvailability(managers, empFrancoisArcoite, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-11-11 Sun 00:00"));
        addNonAvailability(managers, empAnnickPigeon, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 23:59"));
        addNonAvailability(managers, empManonPoissant, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-10-07 Sun 00:00"));
        addNonAvailability(managers, empSylvainCarriere, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-10-07 Sun 00:00"));
        addNonAvailability(managers, empFrancoiseTrudeau, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-10-07 Sun 00:00"));
        addNonAvailability(managers, empAlexandreDube, dateTime("2012-09-30 Sun 00:00"), dateTime("2012-10-07 Sun 00:00"));
        addNonAvailability(managers, empEricRichard, dateTime("2012-09-30 Sun 00:00"), dateTime("2013-10-28 Mon 00:00"));
        addNonAvailability(managers, empLiseCampeau, dateTime("2012-10-28 Sun 00:00"), dateTime("2013-01-13 Sun 00:00"));
        addNonAvailability(managers, empDanielNault, dateTime("2012-11-04 Sun 00:00"), dateTime("2012-11-11 Sun 00:00"));
        addNonAvailability(managers, empCatherinePiette, dateTime("2012-11-04 Sun 00:00"), dateTime("2012-11-11 Sun 00:00"));
        addNonAvailability(managers, empFrancoisArcoite, dateTime("2012-11-04 Sun 00:00"), dateTime("2012-11-11 Sun 00:00"));
        addNonAvailability(managers, empDenisPilon, dateTime("2012-11-04 Sun 00:00"), dateTime("2012-11-11 Sun 00:00"));
        addNonAvailability(managers, empMartinLina, dateTime("2012-11-04 Sun 00:00"), dateTime("2012-11-11 Sun 00:00"));
        addNonAvailability(managers, empStephaneJacques, dateTime("2012-12-09 Sun 00:00"), dateTime("2012-12-16 Sun 00:00"));
        addNonAvailability(managers, empSuzanneCouturier, dateTime("2012-12-09 Sun 00:00"), dateTime("2012-12-16 Sun 00:00"));
        addNonAvailability(managers, empHuguesDenault, dateTime("2013-01-06 Sun 00:00"), dateTime("2013-05-07 Tue 00:00"));
        addNonAvailability(managers, empHachezGabriel, dateTime("2013-01-06 Sun 00:00"), dateTime("2013-01-13 Sun 00:00"));
        addNonAvailability(managers, empSandraDupuis, dateTime("2013-01-06 Sun 00:00"), dateTime("2013-01-13 Sun 00:00"));
        addNonAvailability(managers, empJeanfrancoisBreton, dateTime("2013-01-06 Sun 00:00"), dateTime("2013-01-13 Sun 00:00"));
        addNonAvailability(managers, empNathalieReid, dateTime("2013-01-06 Sun 00:00"), dateTime("2013-01-20 Sun 00:00"));
        addNonAvailability(managers, empJoseeConstantineau, dateTime("2013-01-06 Sun 00:00"), dateTime("2013-01-13 Sun 00:00"));
        addNonAvailability(managers, empSandraDupuis, dateTime("2013-01-13 Sun 00:00"), dateTime("2013-01-14 Mon 00:00"));
        addNonAvailability(managers, empRichardVaillant, dateTime("2013-01-15 Tue 00:00"), dateTime("2013-01-16 Wed 00:00"));
        addNonAvailability(managers, empCecileCouillard, dateTime("2013-01-18 Fri 00:00"), dateTime("2013-01-19 Sat 00:00"));
        addNonAvailability(managers, empRichardVaillant, dateTime("2013-01-20 Sun 00:00"), dateTime("2013-01-27 Sun 00:00"));
        addNonAvailability(managers, empSolangeGirard, dateTime("2013-01-20 Sun 00:00"), dateTime("2013-01-27 Sun 00:00"));
        addNonAvailability(managers, empCecileCouillard, dateTime("2013-01-20 Sun 00:00"), dateTime("2013-01-21 Mon 00:00"));
        addNonAvailability(managers, empRejeanRoy, dateTime("2013-01-21 Mon 13:00"), dateTime("2013-01-21 Mon 15:30"));
        addNonAvailability(managers, empCelineVadnais, dateTime("2013-01-20 Sun 00:00"), dateTime("2013-01-25 Fri 00:00"));

    }

    /**
     * Create Position.
     * 
     * SQL used to create positions.
     * <p>
     * <code>
     * SELECT 'Position ' || camel('pos ' || POSITION.NAME) || ';' FROM POSITION
     * SELECT camel('pos ' || POSITION.NAME) || ' = addPosition(managers, ' || camel('sec ' || SECTION.NAME)|| ', "' || POSITION.NAME || '", ' || CASEWHEN ( POSITION.CLASSIFIED , 'true' , 'false' ) || ', ' || CASEWHEN ( POSITION.SWAPPABLE , 'true' , 'false') || ');' FROM POSITION, SECTION WHERE POSITION.SECTION_ID = SECTION.ID
     * </code>
     * 
     * Create groupd by section
     * <p>
     * <code>
     * SELECT 'List<Position> ' || camel('group ' || SECTION.NAME) ||  ';' FROM SECTION
     * SELECT camel('group ' || SECTION.NAME) ||  ' = Arrays.asList(' || (SELECT GROUP_CONCAT(SELECT camel('pos ' || POSITION.NAME)) FROM POSITION WHERE POSITION.SECTION_ID = SECTION.ID AND POSITION.CLASSIFIED = FALSE) || ');'  FROM SECTION
     * </code>
     * 
     */
    private void createPositions() {

        posTolier = addPosition(managers, sec4Baton, "Tôlier", false, false);
        posGeneraleFourAideFournier = addPosition(managers, sec4Baton, "Générale four (Aide fournier)", false, false);
        posGeneraleDemouleuse = addPosition(managers, sec3Boulangerie, "Générale démouleuse", false, false);
        posEquarisseur = addPosition(managers, sec1Emballage, "Équarisseur", false, false);
        posGeneralEmballagePainsMinces = addPosition(managers, sec2Snack, "Général emballage pains minces", false, false);
        posGeneralEmballageCroutonsVrac = addPosition(managers, sec2Snack, "Général emballage croutons vrac", false, false);
        posGeneralEmballageSnackBote = addPosition(managers, sec2Snack, "Général emballage Snack boîte", false, false);
        posGeneralSalubrite = addPosition(managers, sec5Salubrite, "Général salubrité", false, false);
        posOperateurLigneAPain = addPosition(managers, sec3Boulangerie, "Opérateur ligne à pain", true, false);
        posPrefarinePremelange = addPosition(managers, sec3Boulangerie, "Préfarine / Pré-mélange", true, false);
        posPetrisseurAPain = addPosition(managers, sec3Boulangerie, "Pétrisseur à pain", true, false);
        posOperateurEmballageFmc = addPosition(managers, sec1Emballage, "Opérateur emballage FMC", true, false);
        posOperateurBatonCompteuse = addPosition(managers, sec1Emballage, "Opérateur bâton compteuse", true, false);
        posOperateurDeLigneBiscottebagHorsDoeuvre = addPosition(managers, sec1Emballage, "Opérateur de ligne biscotte,bag., hors d'oeuvre", true, false);
        posOperateurEmballageTriangle = addPosition(managers, sec1Emballage, "Opérateur emballage Triangle", true, false);
        posOperateurLigneFourMelba = addPosition(managers, sec1Emballage, "Opérateur ligne four melba", true, false);
        posOperateurEmballageMelba = addPosition(managers, sec1Emballage, "Opérateur emballage melba", true, false);
        posOperateurEmballageChap25lb = addPosition(managers, sec2Snack, "Opérateur emballage chap 25lb", true, false);
        posRecuperateurEmballage = addPosition(managers, sec1Emballage, "Récupérateur emballage", true, false);
        posOperateurDeRemplacementSnack = addPosition(managers, sec2Snack, "Opérateur de remplacement Snack", true, false);
        posOperateurEnsacheuseVerticalSnack = addPosition(managers, sec2Snack, "Opérateur ensacheuse vertical Snack", true, false);
        posOperateurPetrisseurSnack = addPosition(managers, sec2Snack, "Opérateur pétrisseur Snack", true, false);
        posOperateurDeLigneBaton = addPosition(managers, sec1Emballage, "Opérateur de ligne bâton", true, false);
        posPreposeAuMelange = addPosition(managers, sec4Baton, "Préposé au mélange", true, false);
        posOperateurGrispac = addPosition(managers, sec4Baton, "Opérateur Grispac", true, false);
        posFournier = addPosition(managers, sec4Baton, "Fournier", true, false);
        posPreposeSalubrite = addPosition(managers, sec5Salubrite, "Préposé salubrité", true, false);
        posOperateurGerbeuseVerification = addPosition(managers, sec6Expedition, "Opérateur gerbeuse vérification", true, false);
        posPreposeAuxEpicesEtReparation = addPosition(managers, sec6Expedition, "Préposé aux épices et réparation", true, false);
        posElectrotechniciens = addPosition(managers, sec7Maintenance, "Électrotechniciens", true, false);
        posMecanicien = addPosition(managers, sec7Maintenance, "Mécanicien", true, false);
        posHuileurGraisseurEntretienPreventif = addPosition(managers, sec7Maintenance, "Huileur graisseur entretien préventif", true, false);
        posFormation = addPosition(managers, sec8Formation, "Formation", false, false);
        posGeneraleEmballageBiscotte = addPosition(managers, sec1Emballage, "Générale emballage biscotte", false, true);
        posGeneraleTrancheuseBiscotte = addPosition(managers, sec1Emballage, "Générale trancheuse biscotte", false, true);
        posGeneraleEmballageBaguettine = addPosition(managers, sec1Emballage, "Générale emballage baguettine", false, true);
        posGeneraleTrancheuseBaguettine = addPosition(managers, sec1Emballage, "Générale trancheuse baguettine", false, true);
        posGeneraleEmballageBaton = addPosition(managers, sec1Emballage, "Générale emballage bâton", false, true);
        posGeneraleEmballageFmc = addPosition(managers, sec1Emballage, "Générale emballage FMC", false, true);
        posGeneralTrancheuseMelba = addPosition(managers, sec1Emballage, "Général trancheuse Melba", false, true);
        posGeneralEmballageMelba = addPosition(managers, sec1Emballage, "Général emballage Melba", false, true);
        posRemplacent = addPosition(managers, sec1Emballage, "Remplacent", false, false);
        posAbsent = addPosition(managers, sec9Absent, "Absent", false, false);
        posOperateurRemplacementBoul = addPosition(managers, sec3Boulangerie, "Opérateur remplacement boul", true, false);
        posOperateurRemplacementBaton = addPosition(managers, sec4Baton, "Opérateur remplacement bâton", true, false);
        posTolierMiniBouchees = addPosition(managers, sec4Baton, "Tôlier Mini Bouchées", false, false);
        posGeneraleFourMiniBouchees = addPosition(managers, sec4Baton, "Générale four Mini Bouchées", false, false);

        group3Boulangerie = Arrays.asList(posGeneraleDemouleuse);
        group1Emballage = Arrays.asList(
                posEquarisseur,
                posGeneraleEmballageBiscotte,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaton,
                posGeneraleEmballageFmc,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posRemplacent);
        group2Snack = Arrays.asList(posGeneralEmballagePainsMinces, posGeneralEmballageCroutonsVrac, posGeneralEmballageSnackBote);
        group4Baton = Arrays.asList(posTolier, posGeneraleFourAideFournier, posTolierMiniBouchees, posGeneraleFourMiniBouchees);
        group5Salubrite = Arrays.asList(posGeneralSalubrite);
        group8Formation = Arrays.asList(posFormation);
        group9Absent = Arrays.asList(posAbsent);

    }

    /**
     * Create employee's preferences
     * 
     * <pre>
     * SELECT 'setEmployeePreference(managers, ' || camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ', ' || CASEWHEN(EMPLOYEEPREFERENCE.PREFERREDPOSITION_ID IS NOT NULL, camel( 'pos ' || SELECT NAME FROM POSITION WHERE POSITION.ID = EMPLOYEEPREFERENCE.PREFERREDPOSITION_ID),'null') || ', ' || CASEWHEN(PREFERREDPOSITIONTEAM_ID IS NOT NULL, camel('team ' || SELECT NAME FROM TEAM WHERE TEAM.ID = PREFERREDPOSITIONTEAM_ID),'null') || ', ' || CASEWHEN(PREFERREDSECTION_ID IS NOT NULL, camel('sec ' || SELECT NAME FROM SECTION WHERE SECTION.ID=PREFERREDSECTION_ID), 'null') || ', ' || CASEWHEN( SELECT COUNT(*) FROM EMPLOYEEPREFERENCE_TEAM WHERE EMPLOYEEPREFERENCE.ID = EMPLOYEEPREFERENCE_TEAM.EMPLOYEEPREFERENCE_ID > 0, 'Arrays.asList(' || (SELECT GROUP_CONCAT (camel('team ' || TEAM.NAME)) FROM EMPLOYEEPREFERENCE_TEAM, TEAM WHERE EMPLOYEEPREFERENCE.ID = EMPLOYEEPREFERENCE_TEAM.EMPLOYEEPREFERENCE_ID AND EMPLOYEEPREFERENCE_TEAM.PREFERREDTEAM_ID = TEAM.ID) || ')','null') || ');' FROM EMPLOYEE, EMPLOYEEPREFERENCE WHERE EMPLOYEE.ID = EMPLOYEEPREFERENCE.EMPLOYEE_ID;
     * SELECT 'setEmployeePreferredSeniority(managers, ' || camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ', true);'  FROM EMPLOYEE WHERE EMPLOYEE.PREFERENCIALSENIORITY = TRUE
     * </pre>
     */
    private void createPreferences() throws ManagerException {

        setEmployeePreference(managers, empDonaldTheriault, posMecanicien, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empLouiseVeillette, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empCaroleMorand, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu));
        setEmployeePreference(managers, empLucieGarceau, null, null, sec2Snack, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empMichelineDemers, posOperateurEmballageFmc, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empJeanpierreAuger, null, null, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empDianeDugas, null, null, sec2Snack, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empRejeanBrabant, posOperateurPetrisseurSnack, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen,
                team8SoirRemplacementFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team9NuitSemaineDimJeu));
        setEmployeePreference(managers, empMoniqueLeblond, posOperateurEmballageMelba, team3SoirSemaine, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empRealGosselin, posOperateurGerbeuseVerification, team1JoursDeSemaine, sec6Expedition, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team9NuitSemaineDimJeu,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empLucieLacoste, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empJohanneLemieux, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empManonTremblay, null, null, sec1Emballage, Arrays.asList(
                team2JourRemplacementSemaine,
                team1JoursDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu));
        setEmployeePreference(managers, empBernardBerube, posFournier, team1JoursDeSemaine, sec4Baton, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empRobertLazure, posOperateurGerbeuseVerification, team3SoirSemaine, sec6Expedition, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empLindaBoisvert, posOperateurEmballageTriangle, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empSergeRobidoux, posOperateurLigneFourMelba, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team10NuitSemaineLunVen,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empMichelDaniel, posPreposeSalubrite, team9NuitSemaineDimJeu, sec5Salubrite, Arrays.asList(
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empCaroleRaymond, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team2JourRemplacementSemaine,
                team10NuitSemaineLunVen,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empGillesGosselin, posOperateurGerbeuseVerification, team5JourFinDeSemaine, sec6Expedition, Arrays.asList(
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine));
        setEmployeePreference(managers, empFrancoisBeaulne, posOperateurGrispac, team1JoursDeSemaine, sec4Baton, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empFrancineGuerin, posOperateurDeLigneBaton, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empJeanguyRicher, null, null, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine));
        setEmployeePreference(managers, empMarcelDalphond, posOperateurEmballageMelba, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empMichelMeunier, posHuileurGraisseurEntretienPreventif, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empPierreLamarque, posOperateurEnsacheuseVerticalSnack, team1JoursDeSemaine, sec2Snack, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team2JourRemplacementSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empGerardLanteigne, posPetrisseurAPain, team3SoirSemaine, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empJeanLatour, posPetrisseurAPain, team1JoursDeSemaine, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empPierretteDupras, posOperateurBatonCompteuse, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empDanielNault, posRecuperateurEmballage, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empRaynaldStarnaud, posOperateurLigneAPain, team3SoirSemaine, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empNicoleFortin, posOperateurEmballageMelba, team5JourFinDeSemaine, sec1Emballage, Arrays.asList(
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empNormandArsenault, posPrefarinePremelange, team1JoursDeSemaine, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empDeniseDaigneault, posOperateurDeLigneBiscottebagHorsDoeuvre, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team3SoirSemaine));
        setEmployeePreference(managers, empFrancineLabbe, posPreposeAuxEpicesEtReparation, team1JoursDeSemaine, sec6Expedition, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empClaudineRochefort, null, null, sec1Emballage, Arrays.asList(
                team2JourRemplacementSemaine,
                team1JoursDeSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empSuzanneCouturier, null, null, sec1Emballage, Arrays.asList(
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empMichelTougas, posPreposeSalubrite, team9NuitSemaineDimJeu, sec5Salubrite, Arrays.asList(
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team8SoirRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team1JoursDeSemaine));
        setEmployeePreference(managers, empDanielleBeaudry, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team1JoursDeSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empLucieGuay, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empRogerDagenais, posOperateurLigneAPain, team1JoursDeSemaine, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empMichelJeanneau, posOperateurRemplacementBoul, team1JoursDeSemaine, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empDenisPilon, posPreposeSalubrite, team9NuitSemaineDimJeu, sec5Salubrite, Arrays.asList(
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team5JourFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine));
        setEmployeePreference(managers, empSuzanneGagnon, posOperateurPetrisseurSnack, team3SoirSemaine, sec2Snack, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empRachelBergevin, posOperateurDeRemplacementSnack, team1JoursDeSemaine, sec2Snack, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empRejeanRoy, posOperateurGerbeuseVerification, team1JoursDeSemaine, sec6Expedition, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empHuguesDenault, posRecuperateurEmballage, team3SoirSemaine, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team10NuitSemaineLunVen,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empRolandJrBoucher, posOperateurLigneFourMelba, team3SoirSemaine, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empBernardJolin, posOperateurRemplacementBoul, team3SoirSemaine, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empSartoTremblay, posOperateurEmballageTriangle, team3SoirSemaine, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empDianeGirard, posOperateurEmballageTriangle, team5JourFinDeSemaine, sec1Emballage, Arrays.asList(
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine));
        setEmployeePreference(managers, empMarioPaille, posOperateurRemplacementBaton, team1JoursDeSemaine, sec4Baton, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empGinetteOuellette, null, null, sec1Emballage, Arrays.asList(
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team1JoursDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empGuylaineGuy, null, null, sec5Salubrite, Arrays.asList(
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empPierretteLamothe, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empMarcBellemare, posMecanicien, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empMichelineLegault, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empJoseeConstantineau, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empMadelaineMarleau, posPreposeAuMelange, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empManonPoissant, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empFrancineLemieux, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empCarmenBrais, posPrefarinePremelange, team1JoursDeSemaine, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empFrancoiseTrudeau, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team1JoursDeSemaine,
                team7SoirDeFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine,
                team2JourRemplacementSemaine));
        setEmployeePreference(managers, empEricRichard, posPreposeSalubrite, team1JoursDeSemaine, sec5Salubrite, Arrays.asList(
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empNancyTheoret, posOperateurBatonCompteuse, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empLiseCampeau, null, null, sec1Emballage, Arrays.asList(
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empLucieLeavey, posOperateurDeLigneBiscottebagHorsDoeuvre, team3SoirSemaine, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empLyndaLajoie, posOperateurPetrisseurSnack, team1JoursDeSemaine, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empJeanfrancoisBreton, posPetrisseurAPain, team5JourFinDeSemaine, sec3Boulangerie, Arrays.asList(
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empStephaneJacques, posRecuperateurEmballage, team5JourFinDeSemaine, sec1Emballage, Arrays.asList(
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empBrigitteBouchard, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team4SoirRemplacementSemaine,
                team10NuitSemaineLunVen,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empMartinDube, posPrefarinePremelange, team5JourFinDeSemaine, sec3Boulangerie, Arrays.asList(
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu));
        setEmployeePreference(managers, empSylviePineault, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empJoseeLapierre, posOperateurLigneFourMelba, team5JourFinDeSemaine, sec1Emballage, Arrays.asList(
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empHachezGabriel, posPreposeSalubrite, team1JoursDeSemaine, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empSandraDupuis, null, null, sec1Emballage, Arrays.asList(
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team2JourRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine));
        setEmployeePreference(managers, empLucRoy, posMecanicien, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empLucieCaron, null, null, sec5Salubrite, Arrays.asList(
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empRachelMoise, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empCatherinePiette, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empChantalXavier, null, null, sec1Emballage, Arrays.asList(
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empDominicHoude, posPrefarinePremelange, team3SoirSemaine, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empFrancoisParent, null, null, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empSolangeGirard, posPreposeSalubrite, team9NuitSemaineDimJeu, sec5Salubrite, Arrays.asList(
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empMartinLina, null, null, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empLiseJoncas, null, null, sec4Baton, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empNathalieReid, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empCecileCouillard, null, null, sec1Emballage, Arrays.asList(
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empSylvainJulien, posElectrotechniciens, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empSylvainCarriere, posMecanicien, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empRichardVaillant, null, null, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empFranceBoyer, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team1JoursDeSemaine,
                team2JourRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empIsabelleLeclerc, posPreposeSalubrite, team7SoirDeFinDeSemaine, sec5Salubrite, Arrays.asList(
                team7SoirDeFinDeSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine));
        setEmployeePreference(managers, empFrancoisArcoite, null, null, sec3Boulangerie, Arrays.asList(
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team3SoirSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empSabrinaDupuis, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empIvanhoeMaisonneuve, posElectrotechniciens, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team3SoirSemaine));
        setEmployeePreference(managers, empMathieuGuy, posOperateurGerbeuseVerification, team5JourFinDeSemaine, sec3Boulangerie, Arrays.asList(
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empDaisyBourget, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empMathewBellemare, posMecanicien, team1JoursDeSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empAlexandreDube, posPreposeSalubrite, team7SoirDeFinDeSemaine, sec5Salubrite, Arrays.asList(
                team7SoirDeFinDeSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empAnnickPigeon, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empDanielDuquette, null, null, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empJohanneDuval, null, null, sec1Emballage, Arrays.asList(
                team9NuitSemaineDimJeu,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empCelineVadnais, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team8SoirRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empMarcGrondin, null, null, sec3Boulangerie, Arrays.asList(
                team5JourFinDeSemaine,
                team1JoursDeSemaine,
                team3SoirSemaine,
                team10NuitSemaineLunVen,
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empMarcelLegault, null, null, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empNicolasLegault, null, null, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine));
        setEmployeePreference(managers, empStefanieReynolds, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team2JourRemplacementSemaine,
                team3SoirSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empGinoLemoine, posElectrotechniciens, team3SoirSemaine, sec7Maintenance, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team7SoirDeFinDeSemaine));
        setEmployeePreference(managers, empMarioLongtin, null, null, sec4Baton, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team9NuitSemaineDimJeu,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team10NuitSemaineLunVen));
        setEmployeePreference(managers, empRobertAllen, null, null, sec3Boulangerie, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team4SoirRemplacementSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empChristopheDeneault, null, null, sec1Emballage, Arrays.asList(
                team3SoirSemaine,
                team1JoursDeSemaine,
                team9NuitSemaineDimJeu,
                team5JourFinDeSemaine,
                team10NuitSemaineLunVen,
                team6JoursRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empPierreLemelin, null, null, sec2Snack, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team6JoursRemplacementFinDeSemaine,
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team8SoirRemplacementFinDeSemaine,
                team2JourRemplacementSemaine,
                team10NuitSemaineLunVen,
                team3SoirSemaine,
                team4SoirRemplacementSemaine));
        setEmployeePreference(managers, empRobertShonfield, null, null, sec5Salubrite, Arrays.asList(
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team2JourRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team7SoirDeFinDeSemaine,
                team4SoirRemplacementSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empLouisCarlPage, null, null, sec3Boulangerie, Arrays.asList(
                team1JoursDeSemaine,
                team3SoirSemaine,
                team5JourFinDeSemaine,
                team9NuitSemaineDimJeu,
                team10NuitSemaineLunVen,
                team7SoirDeFinDeSemaine,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empDanielGuillemette, null, null, sec1Emballage, Arrays.asList(
                team1JoursDeSemaine,
                team5JourFinDeSemaine,
                team3SoirSemaine,
                team9NuitSemaineDimJeu,
                team7SoirDeFinDeSemaine,
                team10NuitSemaineLunVen,
                team2JourRemplacementSemaine,
                team4SoirRemplacementSemaine,
                team6JoursRemplacementFinDeSemaine,
                team8SoirRemplacementFinDeSemaine));
        setEmployeePreference(managers, empNathalieBedard, null, null, null, null);
        setEmployeePreference(managers, empWayneDavid, null, null, null, null);
        setEmployeePreference(managers, empPierreGagne, null, null, null, null);

        setEmployeePreferredSeniority(managers, empRichardVaillant, true);

    }

    /**
     * Create production events.
     * 
     * <pre>
     * SELECT 'events.add(addProductionEvent(managers, ' || camel('prod ' || CASEWHEN(PRODUCT.REFID IS NOT NULL AND PRODUCT.REFID != '', PRODUCT.REFID, PRODUCT.NAME)) || ', ' || camel(TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || '));' FROM PRODUCTIONEVENT, PRODUCT, SHIFT, TEAM WHERE PRODUCTIONEVENT.PRODUCT_ID = PRODUCT.ID AND PRODUCTIONEVENT.SHIFT_ID = SHIFT.ID AND SHIFT.TEAM_ID = TEAM.ID AND SHIFT.STARTDATE BETWEEN DATE '2013-01-13' and DATE '2013-01-20'
     * </pre>
     * 
     * SQL to create groups.
     * 
     * <pre>
     * SELECT 'List<ProductionEvent> ' || camel('events ' || TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || ';' FROM SHIFT, TEAM WHERE SHIFT.TEAM_ID = TEAM.ID AND SHIFT.STARTDATE BETWEEN DATE '2013-01-13' and DATE '2013-01-20'
     * SELECT camel('events ' || TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || ' = select(events, ' || camel(TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || ');' FROM SHIFT, TEAM WHERE SHIFT.TEAM_ID = TEAM.ID AND SHIFT.STARTDATE BETWEEN DATE '2013-01-13' and DATE '2013-01-20'
     * </pre>
     */
    private void createProductionEvents() {
        events = new ArrayList<ProductionEvent>();

        events.add(addProductionEvent(managers, prod56, jourFinDeSemaineSat));
        events.add(addProductionEvent(managers, prod650, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prod650, soirSemaineMon));
        events.add(addProductionEvent(managers, prod680, jourFinDeSemaineFri));
        events.add(addProductionEvent(managers, prod2162, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prod2162, soirSemaineTue));
        events.add(addProductionEvent(managers, prod2162, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prod2162, soirSemaineWed));
        events.add(addProductionEvent(managers, prod24610, soirSemaineWed));
        events.add(addProductionEvent(managers, prod24665, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prod41028, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prod41280, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prod41290, soirSemaineWed));
        events.add(addProductionEvent(managers, prod41350, soirSemaineMon));
        events.add(addProductionEvent(managers, prod41354, soirSemaineThu));
        events.add(addProductionEvent(managers, prod41606, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prod42048, jourFinDeSemaineFri));
        events.add(addProductionEvent(managers, prod42051, jourFinDeSemaineSun));
        events.add(addProductionEvent(managers, prod42056, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prod42056, soirSemaineThu));
        events.add(addProductionEvent(managers, prod45050, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prod45052, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prod45054, soirSemaineTue));
        events.add(addProductionEvent(managers, prod45600, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prod50402, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prod92421, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prod93020, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prod93020, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodRecuperateur, soirSemaineWed));
        events.add(addProductionEvent(managers, prodRecuperateur, soirSemaineThu));
        events.add(addProductionEvent(managers, prodRecuperateur, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodRecuperateur, jourFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodRecuperateur, jourFinDeSemaineSun));
        events.add(addProductionEvent(managers, prodRecuperateur, jourFinDeSemaineSat));
        events.add(addProductionEvent(managers, prodRecuperateur, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodRecuperateur, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodRecuperateur, soirSemaineTue));
        events.add(addProductionEvent(managers, prodRecuperateur, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodRecuperateur, soirSemaineMon));
        events.add(addProductionEvent(managers, prodFabricationPains, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodFabricationPains, soirSemaineMon));
        events.add(addProductionEvent(managers, prodFabricationPains, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodFabricationPains, soirSemaineTue));
        events.add(addProductionEvent(managers, prodFabricationPains, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodFabricationPains, soirSemaineWed));
        events.add(addProductionEvent(managers, prodFabricationPains, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodFabricationPains, soirSemaineThu));
        events.add(addProductionEvent(managers, prodPetrisseurBoulangerie, jourFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodPetrisseurBoulangerie, jourFinDeSemaineSat));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenMon));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenMon));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenThu));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenFri));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenWed));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenWed));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenTue));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineDimJeuThu));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineDimJeuTue));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineDimJeuWed));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineDimJeuMon));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenTue));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineDimJeuSun));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenThu));
        events.add(addProductionEvent(managers, prodGeneralSalubrite, nuitSemaineLunVenFri));
        events.add(addProductionEvent(managers, prodPreposeAuxEpices, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodPreposeAuxEpices, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodPreposeAuxEpices, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodPreposeAuxEpices, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, jourFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, soirSemaineTue));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, jourFinDeSemaineSun));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, jourFinDeSemaineSat));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, soirSemaineWed));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, soirSemaineMon));
        events.add(addProductionEvent(managers, prodOperateurGerbeuse, soirSemaineThu));
        events.add(addProductionEvent(managers, prodMecaniciens, jourFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodMecaniciens, soirSemaineTue));
        events.add(addProductionEvent(managers, prodMecaniciens, jourFinDeSemaineSat));
        events.add(addProductionEvent(managers, prodMecaniciens, jourFinDeSemaineSun));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodMecaniciens, soirSemaineWed));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodMecaniciens, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodMecaniciens, soirSemaineThu));
        events.add(addProductionEvent(managers, prodMecaniciens, soirSemaineMon));
        events.add(addProductionEvent(managers, prodElectromecaniciens, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodElectromecaniciens, soirSemaineTue));
        events.add(addProductionEvent(managers, prodElectromecaniciens, soirSemaineWed));
        events.add(addProductionEvent(managers, prodElectromecaniciens, soirSemaineThu));
        events.add(addProductionEvent(managers, prodElectromecaniciens, jourFinDeSemaineSat));
        events.add(addProductionEvent(managers, prodElectromecaniciens, jourFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodElectromecaniciens, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodElectromecaniciens, jourFinDeSemaineSun));
        events.add(addProductionEvent(managers, prodElectromecaniciens, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodElectromecaniciens, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodElectromecaniciens, soirSemaineMon));
        events.add(addProductionEvent(managers, prodHuileur, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodHuileur, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodHuileur, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodHuileur, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodRemplacent, joursRemplacementFinDeSemaineSat));
        events.add(addProductionEvent(managers, prodRemplacent, joursRemplacementFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodRemplacent, joursRemplacementFinDeSemaineSun));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineTue));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineWed));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineMon));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineWed));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineMon));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineThu));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineTue));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineThu));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineTue));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineWed));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineThu));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineMon));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineWed));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineWed));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineThu));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineThu));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineMon));
        events.add(addProductionEvent(managers, prodRemplacent, jourRemplacementSemaineTue));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineMon));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineTue));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineWed));
        events.add(addProductionEvent(managers, prodRemplacent, soirRemplacementSemaineThu));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuSun));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuSun));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuMon));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuMon));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuTue));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuTue));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuWed));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuWed));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuThu));
        events.add(addProductionEvent(managers, prodFormation, nuitSemaineDimJeuThu));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodFormation, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prod924311, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prod92416, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuWed));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuWed));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuWed));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, soirDeFinDeSemaineSat));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, soirDeFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, soirDeFinDeSemaineSun));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, joursDeSemaineTue));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, joursDeSemaineThu));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuThu));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuThu));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuThu));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuSun));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuSun));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuWed));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, joursDeSemaineMon));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuTue));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuTue));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, joursDeSemaineWed));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuTue));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuTue));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuMon));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuMon));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuSun));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuSun));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuMon));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuMon));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, nuitSemaineDimJeuThu));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, soirDeFinDeSemaineSun));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, soirDeFinDeSemaineFri));
        events.add(addProductionEvent(managers, prodPreposeSalubrite, soirDeFinDeSemaineSat));

        events1JoursDeSemaineWed = select(events, joursDeSemaineWed);
        events1JoursDeSemaineTue = select(events, joursDeSemaineTue);
        events1JoursDeSemaineMon = select(events, joursDeSemaineMon);
        events1JoursDeSemaineThu = select(events, joursDeSemaineThu);
        events2JourRemplacementSemaineWed = select(events, jourRemplacementSemaineWed);
        events2JourRemplacementSemaineTue = select(events, jourRemplacementSemaineTue);
        events2JourRemplacementSemaineMon = select(events, jourRemplacementSemaineMon);
        events2JourRemplacementSemaineThu = select(events, jourRemplacementSemaineThu);
        events3SoirSemaineMon = select(events, soirSemaineMon);
        events3SoirSemaineTue = select(events, soirSemaineTue);
        events3SoirSemaineWed = select(events, soirSemaineWed);
        events3SoirSemaineThu = select(events, soirSemaineThu);
        events4SoirRemplacementSemaineThu = select(events, soirRemplacementSemaineThu);
        events4SoirRemplacementSemaineTue = select(events, soirRemplacementSemaineTue);
        events4SoirRemplacementSemaineWed = select(events, soirRemplacementSemaineWed);
        events4SoirRemplacementSemaineMon = select(events, soirRemplacementSemaineMon);
        events9NuitSemaineDimJeuWed = select(events, nuitSemaineDimJeuWed);
        events9NuitSemaineDimJeuThu = select(events, nuitSemaineDimJeuThu);
        events9NuitSemaineDimJeuTue = select(events, nuitSemaineDimJeuTue);
        events9NuitSemaineDimJeuMon = select(events, nuitSemaineDimJeuMon);
        events9NuitSemaineDimJeuSun = select(events, nuitSemaineDimJeuSun);
        events10NuitSemaineLunVenFri = select(events, nuitSemaineLunVenFri);
        events10NuitSemaineLunVenWed = select(events, nuitSemaineLunVenWed);
        events10NuitSemaineLunVenThu = select(events, nuitSemaineLunVenThu);
        events10NuitSemaineLunVenMon = select(events, nuitSemaineLunVenMon);
        events10NuitSemaineLunVenTue = select(events, nuitSemaineLunVenTue);
        events5JourFinDeSemaineSun = select(events, jourFinDeSemaineSun);
        events5JourFinDeSemaineFri = select(events, jourFinDeSemaineFri);
        events5JourFinDeSemaineSat = select(events, jourFinDeSemaineSat);
        events6JoursRemplacementFinDeSemaineSun = select(events, joursRemplacementFinDeSemaineSun);
        events6JoursRemplacementFinDeSemaineFri = select(events, joursRemplacementFinDeSemaineFri);
        events6JoursRemplacementFinDeSemaineSat = select(events, joursRemplacementFinDeSemaineSat);
        events7SoirDeFinDeSemaineSun = select(events, soirDeFinDeSemaineSun);
        events7SoirDeFinDeSemaineFri = select(events, soirDeFinDeSemaineFri);
        events7SoirDeFinDeSemaineSat = select(events, soirDeFinDeSemaineSat);
        events8SoirRemplacementFinDeSemaineSat = select(events, soirRemplacementFinDeSemaineSat);
        events8SoirRemplacementFinDeSemaineSun = select(events, soirRemplacementFinDeSemaineSun);
        events8SoirRemplacementFinDeSemaineFri = select(events, soirRemplacementFinDeSemaineFri);

    }

    /**
     * Create product position.
     * 
     * <pre>
     * SELECT 'addProductPosition(managers, ' || camel('prod ' || CASEWHEN(PRODUCT.REFID IS NOT NULL AND PRODUCT.REFID != '', PRODUCT.REFID, PRODUCT.NAME)) ||',' || (SELECT GROUP_CONCAT( TRIM (TRAILING ',' FROM REPEAT(camel('pos ' || POSITION.NAME) || ',', PRODUCTPOSITION.NUMBER))) FROM PRODUCTPOSITION, POSITION WHERE PRODUCTPOSITION.POSITION_ID = POSITION.ID AND PRODUCTPOSITION.PRODUCT_ID = PRODUCT.ID) || ');' FROM PRODUCT
     * </pre>
     */
    private void createProductPositions() {

        addProductPosition(
                managers,
                prod56,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod69,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(managers, prod600, posGeneraleEmballageFmc, posOperateurEmballageFmc);
        addProductPosition(
                managers,
                prod650,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod660,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(
                managers,
                prod663,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(
                managers,
                prod670,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(managers, prod680, posOperateurEmballageChap25lb);
        addProductPosition(
                managers,
                prod684,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod2162,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(managers, prod24610, posGeneraleEmballageFmc, posOperateurEmballageFmc);
        addProductPosition(
                managers,
                prod24665,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(managers, prod41028, posGeneraleEmballageFmc, posOperateurEmballageFmc);
        addProductPosition(
                managers,
                prod41200,
                posFournier,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleFourAideFournier,
                posOperateurBatonCompteuse,
                posOperateurBatonCompteuse,
                posOperateurDeLigneBaton,
                posOperateurGrispac,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posTolier);
        addProductPosition(
                managers,
                prod41205,
                posFournier,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleFourAideFournier,
                posOperateurBatonCompteuse,
                posOperateurBatonCompteuse,
                posOperateurDeLigneBaton,
                posOperateurGrispac,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posTolier);
        addProductPosition(
                managers,
                prod41270,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod41280,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(managers, prod41281, posGeneralEmballageCroutonsVrac, posGeneralEmballageCroutonsVrac, posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod41290,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod41340,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod41350,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod41352,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod41354,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(managers, prod41606, posGeneraleEmballageBiscotte, posGeneraleEmballageBiscotte, posOperateurEmballageTriangle);
        addProductPosition(
                managers,
                prod42040,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42042,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42044,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42046,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42048,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42049,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageTriangle,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42050,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageTriangle,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42051,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageTriangle,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42052,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageTriangle,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42054,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageTriangle,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42056,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42060,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42066,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42067,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42068,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42081,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageTriangle,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42083,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageTriangle,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42140,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42142,
                posEquarisseur,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralTrancheuseMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod42570,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod44900,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod44930,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posGeneralEmballagePainsMinces,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45000,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleTrancheuseBiscotte,
                posGeneraleTrancheuseBiscotte,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod45010,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleTrancheuseBiscotte,
                posGeneraleTrancheuseBiscotte,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod45020,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleTrancheuseBiscotte,
                posGeneraleTrancheuseBiscotte,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod45030,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleTrancheuseBiscotte,
                posGeneraleTrancheuseBiscotte,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod45050,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod45052,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod45054,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine,
                posOperateurEmballageTriangle,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(managers, prod45100, posGeneralEmballageSnackBote, posGeneralEmballageSnackBote, posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45111,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45130,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(managers, prod45200, posGeneralEmballageSnackBote, posGeneralEmballageSnackBote, posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45250,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45575,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneraleTrancheuseBiscotte,
                posGeneraleTrancheuseBiscotte,
                posOperateurDeLigneBiscottebagHorsDoeuvre);
        addProductPosition(
                managers,
                prod45600,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45602,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45615,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45618,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45620,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45622,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45720,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod45730,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod45740,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageMelba,
                posOperateurLigneFourMelba);
        addProductPosition(
                managers,
                prod45900,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45950,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45952,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45992,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45993,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod45994,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack,
                posOperateurEnsacheuseVerticalSnack);
        addProductPosition(
                managers,
                prod50390,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurEnsacheuseVerticalSnack,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(
                managers,
                prod50402,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(
                managers,
                prod50404,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(
                managers,
                prod50420,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(
                managers,
                prod50430,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageCroutonsVrac,
                posOperateurPetrisseurSnack,
                posOperateurDeRemplacementSnack);
        addProductPosition(
                managers,
                prod92419,
                posFournier,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleFourAideFournier,
                posOperateurDeLigneBaton,
                posOperateurGrispac,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posTolier);
        addProductPosition(
                managers,
                prod92421,
                posFournier,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleFourAideFournier,
                posOperateurDeLigneBaton,
                posOperateurGrispac,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posTolier);
        addProductPosition(managers, prod924312, posGeneralEmballageCroutonsVrac, posOperateurPetrisseurSnack);
        addProductPosition(
                managers,
                prod93020,
                posFournier,
                posGeneraleFourMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleFourMiniBouchees,
                posOperateurDeLigneBaton,
                posOperateurGrispac,
                posOperateurGrispac,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posTolierMiniBouchees);
        addProductPosition(managers, prodRecuperateur, posRecuperateurEmballage);
        addProductPosition(
                managers,
                prodFabricationPains,
                posGeneraleDemouleuse,
                posGeneraleDemouleuse,
                posOperateurLigneAPain,
                posOperateurRemplacementBoul,
                posPetrisseurAPain,
                posPrefarinePremelange);
        addProductPosition(
                managers,
                prodPainsBaguette,
                posGeneraleDemouleuse,
                posGeneraleDemouleuse,
                posOperateurLigneAPain,
                posOperateurRemplacementBoul,
                posPetrisseurAPain);
        addProductPosition(managers, prodPetrisseurBoulangerie, posPetrisseurAPain);
        addProductPosition(
                managers,
                prodFabricationMulsi,
                posGeneraleDemouleuse,
                posGeneraleDemouleuse,
                posOperateurLigneAPain,
                posOperateurRemplacementBoul,
                posPetrisseurAPain,
                posPrefarinePremelange,
                posPrefarinePremelange);
        addProductPosition(managers, prodGeneralSalubrite, posGeneralSalubrite);
        addProductPosition(managers, prodPreposeAuxEpices, posPreposeAuxEpicesEtReparation);
        addProductPosition(managers, prodOperateurGerbeuse, posOperateurGerbeuseVerification);
        addProductPosition(managers, prodMecaniciens, posMecanicien);
        addProductPosition(managers, prodElectromecaniciens, posElectrotechniciens);
        addProductPosition(managers, prodHuileur, posHuileurGraisseurEntretienPreventif);
        addProductPosition(managers, prodRemplacent, posRemplacent);
        addProductPosition(
                managers,
                prodSalubriteNuit,
                posGeneralSalubrite,
                posGeneralSalubrite,
                posGeneralSalubrite,
                posGeneralSalubrite,
                posPreposeSalubrite,
                posPreposeSalubrite);
        addProductPosition(managers, prodFormation, posFormation);
        addProductPosition(managers, prodOperateursSpooner, posOperateurDeLigneBiscottebagHorsDoeuvre, posOperateurEmballageTriangle);
        addProductPosition(managers, prod924311, posOperateurPetrisseurSnack);
        addProductPosition(managers, prodOperateurEmballageMelba, posOperateurEmballageMelba);
        addProductPosition(managers, prodAbsent, posAbsent);
        addProductPosition(managers, prodGeneralEmballage, posGeneralEmballageMelba);
        addProductPosition(
                managers,
                prod92416,
                posGeneraleFourAideFournier,
                posTolier,
                posOperateurGrispac,
                posOperateurDeLigneBaton,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posPreposeAuMelange,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaton);
        addProductPosition(managers, prodPreposeSalubrite, posPreposeSalubrite);

    }

    /**
     * Create products.
     * 
     * <pre>
     * SELECT 'Product ' || camel('prod ' || CASEWHEN(PRODUCT.REFID IS NOT NULL AND PRODUCT.REFID != '', PRODUCT.REFID, PRODUCT.NAME)) || ';' FROM PRODUCT
     * SELECT camel('prod ' || CASEWHEN(PRODUCT.REFID IS NOT NULL AND PRODUCT.REFID != '', PRODUCT.REFID, PRODUCT.NAME)) || ' = addProduct(managers, "' || CASEWHEN(PRODUCT.REFID IS NOT NULL, PRODUCT.REFID , '') || '", "' || PRODUCT.NAME || '", "' || CASEWHEN(PRODUCT.FAMILY IS NOT NULL, PRODUCT.FAMILY, '')  || '");' FROM PRODUCT
     * </pre>
     */
    private void createProducts() {

        prod56 = addProduct(managers, "56", "Melba multi fibre - 12x175g", "Melba");
        prod69 = addProduct(managers, "69", "Melba multi fibre - 12x350g", "Melba");
        prod600 = addProduct(managers, "600", "Bât nature emballage - 250x2", "Pain bâton");
        prod650 = addProduct(managers, "650", "Melba nature - 400x2", "Melba");
        prod660 = addProduct(managers, "660", "Cr. nature - 4.5 Kg", "Croûton Vrac");
        prod663 = addProduct(managers, "663", "Cr. Nature Tournesol - 4.5 Kg", "Croûton Vrac");
        prod670 = addProduct(managers, "670", "Cr. assaisonné - 4.5 Kg", "Croûton Vrac");
        prod680 = addProduct(managers, "680", "Chap. rég. vrac - 25lbs", "Chapelure");
        prod684 = addProduct(managers, "684", "Melba blé 400/2", "Melba");
        prod2162 = addProduct(managers, "2162", "Melba Lunch Costco - 180x1kg", "Melba");
        prod24610 = addProduct(managers, "24610", "Bât sésame emballage - 250x2", "Pain bâton");
        prod24665 = addProduct(managers, "24665", "Cr ail - 4.5 Kg", "Croûton Vrac");
        prod41028 = addProduct(managers, "41028", "Bât. Sésame emballage - lunchpack", "Pain bâton");
        prod41200 = addProduct(managers, "41200", "Bât. nature - 12x200g", "Pain bâton");
        prod41205 = addProduct(managers, "41205", "Bât. sésame  - 12x200g", "Pain bâton");
        prod41270 = addProduct(managers, "41270", "Bag bouché crème sûr & oignion 12x130gr", "Baguettine");
        prod41280 = addProduct(managers, "41280", "Bag bouché ail  & parmesan - 12x130g", "Baguettine");
        prod41281 = addProduct(managers, "41281", "Bag sachet ail  & parmesan - 100x22g", "Baguettine");
        prod41290 = addProduct(managers, "41290", "Bag bouché Bruschetta - 12x130g", "Baguettine");
        prod41340 = addProduct(managers, "41340", "Bag. Tomate/feta - 12x135g", "Baguettine");
        prod41350 = addProduct(managers, "41350", "Bag. herbes du jardin - 12x135g", "Baguettine");
        prod41352 = addProduct(managers, "41352", "Bag. Tom. & basilic - 12x135g", "Baguettine");
        prod41354 = addProduct(managers, "41354", "Bag. sésame & ail - 12x135g", "Baguettine");
        prod41606 = addProduct(managers, "41606", "Bât. sésame - 12x160g", "Bâtonnets");
        prod42040 = addProduct(managers, "42040", "Melba réguliere - 12x200g", "Melba");
        prod42042 = addProduct(managers, "42042", "Melba légume - 12x200g", "Melba");
        prod42044 = addProduct(managers, "42044", "Melba blé - 12x200g", "Melba");
        prod42046 = addProduct(managers, "42046", "Melba sésame - 12x200g", "Melba");
        prod42048 = addProduct(managers, "42048", "Melba sans sel - 12x200g", "Melba");
        prod42049 = addProduct(managers, "42049", "Canapé multi-grain - 12x150g", "Canapé");
        prod42050 = addProduct(managers, "42050", "canapé germe blé - 12x150g", "Canapé");
        prod42051 = addProduct(managers, "42051", "canapé régulier - 12x150g", "Canapé");
        prod42052 = addProduct(managers, "42052", "canapé ail - 12x150g", "Canapé");
        prod42054 = addProduct(managers, "42054", "Canapé sésame - 12x150g", "Canapé");
        prod42056 = addProduct(managers, "42056", "Melba lunch pack - 12x200g", "Melba");
        prod42060 = addProduct(managers, "42060", "Melba régulière - 12x400g", "Melba");
        prod42066 = addProduct(managers, "42066", "Melba multi-grain - 12x400g", "Melba");
        prod42067 = addProduct(managers, "42067", "Melba seigle/sésame - 12x400g", "Melba");
        prod42068 = addProduct(managers, "42068", "Melba sans sel - 12x400g", "Melba");
        prod42081 = addProduct(managers, "42081", "Canapé bruschetta - 12x125g", "Canapé");
        prod42083 = addProduct(managers, "42083", "Canapé légume - 12x150g", "Canapé");
        prod42140 = addProduct(managers, "42140", "Melba blé - 12x400g", "Melba");
        prod42142 = addProduct(managers, "42142", "Melba Loblaws - 6x1kg", "Melba");
        prod42570 = addProduct(managers, "42570", "H.O. nature 2.27Kg - 2.27", "H.O.");
        prod44900 = addProduct(managers, "44900", "F/B multigrain - 12x150g", "Flat bread");
        prod44930 = addProduct(managers, "44930", "F/B sésame/romarin - 12x150g", "Flat bread");
        prod45000 = addProduct(managers, "45000", "Bisc régulière - 12x250g", "Biscotte");
        prod45010 = addProduct(managers, "45010", "Bisc sans sel - 12x250g", "Biscotte");
        prod45020 = addProduct(managers, "45020", "Bisc blé entier - 12x250g", "Biscotte");
        prod45030 = addProduct(managers, "45030", "Bisc musli - 12x250g", "Biscotte");
        prod45050 = addProduct(managers, "45050", "HO nature - 12x125g", "H.O.");
        prod45052 = addProduct(managers, "45052", "Baguet. Croust. 3 fromages - 12x135g", "Baguettine");
        prod45054 = addProduct(managers, "45054", "Baguet. Croust. Romarin/olive - 12x135g", "Baguettine");
        prod45100 = addProduct(managers, "45100", "Chapelure rég. - 12x500g", "Chapelure");
        prod45111 = addProduct(managers, "45111", "Cr césar - 12x150g", "Croûton");
        prod45130 = addProduct(managers, "45130", "Cr césar boni - 12x175g", "Croûton");
        prod45200 = addProduct(managers, "45200", "Chapelure rég. - 12x250g", "Chapelure");
        prod45250 = addProduct(managers, "45250", "Farce - 12x185g", "Farce");
        prod45575 = addProduct(managers, "45575", "Bisc lunchpack - 12x200g", "Biscotte");
        prod45600 = addProduct(managers, "45600", "3 pains régulier - 12x150g", "Croûton");
        prod45602 = addProduct(managers, "45602", "3 pains césar - 12x150g", "Croûton");
        prod45615 = addProduct(managers, "45615", "3 pains ail - 12x150g", "Croûton");
        prod45618 = addProduct(managers, "45618", "Cr.cesar inter - 12x150g", "Croûton");
        prod45620 = addProduct(managers, "45620", "Cr. nature Intern. - 12x150g", "Croûton");
        prod45622 = addProduct(managers, "45622", "Cr. ail Intern. - 12x150g", "Croûton");
        prod45720 = addProduct(managers, "45720", "Melba réguliere Espagne - 12x200g", "Melba");
        prod45730 = addProduct(managers, "45730", "Melba sans sel Espagne - 12x200g", "Melba");
        prod45740 = addProduct(managers, "45740", "Melba sésame Espagne - 12x200g", "Melba");
        prod45900 = addProduct(managers, "45900", "Cr. nature - 12x150g", "Croûton");
        prod45950 = addProduct(managers, "45950", "Cr. ail - 12x150g", "Croûton");
        prod45952 = addProduct(managers, "45952", "Cr. Ail boni - 12x175g", "Croûton");
        prod45992 = addProduct(managers, "45992", "Cr. Old London ail - 12x195g", "Croûton");
        prod45993 = addProduct(managers, "45993", "Cr. Old London césar - 12x195g", "Croûton");
        prod45994 = addProduct(managers, "45994", "Cr. Old London nature - 12x195g", "Croûton");
        prod50390 = addProduct(managers, "50390", "Cr. Boston Pizza", "Croûton Vrac");
        prod50402 = addProduct(managers, "50402", "Cr. LeMarquis ail - 5kg", "Croûton Vrac");
        prod50404 = addProduct(managers, "50404", "CR LeMarquis césar ital - 5kg", "Croûton Vrac");
        prod50420 = addProduct(managers, "50420", "Cr. Trois pains césar - 4.5kg", "Croûton Vrac");
        prod50430 = addProduct(managers, "50430", "Cr. Gourmet ail sans sel - 5kg", "Croûton Vrac");
        prod92419 = addProduct(managers, "92419", "Pain bât. Rama. rég.", "Pain bâton");
        prod92421 = addProduct(managers, "92421", "Pain bât. Rama. sés.", "Pain bâton");
        prod924312 = addProduct(managers, "92431(2)", "Séchage", "Séchage");
        prod93020 = addProduct(managers, "93020", "Pain mini bouchée", "Mini bouchée");
        prodRecuperateur = addProduct(managers, "", "Récupérateur", "Recup");
        prodFabricationPains = addProduct(managers, "", "Fabrication pains", "Boul");
        prodPainsBaguette = addProduct(managers, "", "Pains baguette", "Boul");
        prodPetrisseurBoulangerie = addProduct(managers, "", "Pétrisseur boulangerie", "Boul");
        prodFabricationMulsi = addProduct(managers, "", "Fabrication Mulsi", "Boul");
        prodGeneralSalubrite = addProduct(managers, "", "Général Salubrité", "Salubrité");
        prodPreposeAuxEpices = addProduct(managers, "", "Préposé aux épices", "Expédition");
        prodOperateurGerbeuse = addProduct(managers, "", "Opérateur gerbeuse", "Expédition");
        prodMecaniciens = addProduct(managers, "", "Mécaniciens", "Mécaniciens");
        prodElectromecaniciens = addProduct(managers, "", "Electromécaniciens", "Électro");
        prodHuileur = addProduct(managers, "", "Huileur", "Huileur");
        prodRemplacent = addProduct(managers, "", "Remplacent", "Remplacent");
        prodSalubriteNuit = addProduct(managers, "", "Salubrité Nuit", "Salubrité");
        prodFormation = addProduct(managers, "", "Formation", "Formation");
        prodOperateursSpooner = addProduct(managers, "", "Opérateurs Spooner", "Spooner");
        prod924311 = addProduct(managers, "92431(1)", "Séchage", "Séchage");
        prodOperateurEmballageMelba = addProduct(managers, "", "Opérateur emballage melba", "Melba");
        prodAbsent = addProduct(managers, "", "Absent ", "Absent ");
        prodGeneralEmballage = addProduct(managers, "", "Général emballage", "Général");
        prod92416 = addProduct(managers, "92416", "mini baton sesame", "Pain bâton");
        prodPreposeSalubrite = addProduct(managers, "", "Préposé Salubrité", "prep salubrite");

    }

    /**
     * Create qualifications.
     * 
     * <pre>
     * SELECT 'addQualification(managers, ' || camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ', ' || (SELECT GROUP_CONCAT  (camel('pos ' || POSITION.NAME)) FROM QUALIFICATION, POSITION WHERE QUALIFICATION.EMPLOYEE_ID = EMPLOYEE.ID AND QUALIFICATION.POSITION_ID = POSITION.ID ) || ');' FROM EMPLOYEE;
     * </pre>
     */
    private void createQualifications() {

        addQualification(managers, empDonaldTheriault, posMecanicien);
        addQualification(
                managers,
                empLouiseVeillette,
                posGeneraleEmballageBaton,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBiscotte,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posRemplacent);
        addQualification(
                managers,
                empCaroleMorand,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empLucieGarceau,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empMichelineDemers,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empJeanpierreAuger,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posFormation,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empRejeanBrabant,
                posOperateurRemplacementBaton,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empMoniqueLeblond,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empRealGosselin,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent);
        addQualification(
                managers,
                empLucieLacoste,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posRemplacent);
        addQualification(
                managers,
                empJohanneLemieux,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empManonTremblay,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empBernardBerube,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empRobertLazure,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent);
        addQualification(
                managers,
                empLindaBoisvert,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posPreposeAuxEpicesEtReparation,
                posRemplacent);
        addQualification(
                managers,
                empSergeRobidoux,
                posOperateurDeLigneBaton,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posFormation,
                posAbsent);
        addQualification(
                managers,
                empMichelDaniel,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleDemouleuse,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empCaroleRaymond,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posAbsent);
        addQualification(
                managers,
                empGillesGosselin,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empFrancoisBeaulne,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posFormation,
                posAbsent);
        addQualification(
                managers,
                empFrancineGuerin,
                posOperateurDeLigneBaton,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posPreposeAuxEpicesEtReparation,
                posRemplacent,
                posAbsent,
                posFormation);
        addQualification(
                managers,
                empJeanguyRicher,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empMarcelDalphond,
                posOperateurDeLigneBaton,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurLigneAPain,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posOperateurLigneFourMelba,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(managers, empMichelMeunier, posHuileurGraisseurEntretienPreventif);
        addQualification(
                managers,
                empPierreLamarque,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurEnsacheuseVerticalSnack,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empGerardLanteigne,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPetrisseurAPain,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empJeanLatour,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empPierretteDupras,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posGeneralEmballagePainsMinces,
                posFormation,
                posOperateurBatonCompteuse);
        addQualification(
                managers,
                empDanielNault,
                posPreposeAuMelange,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empRaynaldStarnaud,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurLigneAPain,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent);
        addQualification(
                managers,
                empNicoleFortin,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empNormandArsenault,
                posPreposeAuMelange,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posFormation,
                posAbsent);
        addQualification(
                managers,
                empDeniseDaigneault,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empFrancineLabbe,
                posOperateurDeLigneBaton,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posPreposeAuxEpicesEtReparation,
                posRemplacent);
        addQualification(
                managers,
                empClaudineRochefort,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empSuzanneCouturier,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(managers, empMichelTougas, posGeneraleDemouleuse, posPreposeSalubrite, posGeneralSalubrite, posRemplacent);
        addQualification(
                managers,
                empDanielleBeaudry,
                posOperateurEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empLucieGuay,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posAbsent,
                posFormation);
        addQualification(
                managers,
                empRogerDagenais,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurLigneAPain,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empMichelJeanneau,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(managers, empDenisPilon, posGeneraleDemouleuse, posPreposeSalubrite, posGeneralSalubrite);
        addQualification(
                managers,
                empSuzanneGagnon,
                posOperateurDeLigneBaton,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posRemplacent);
        addQualification(
                managers,
                empRachelBergevin,
                posGeneraleEmballageFmc,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleEmballageBiscotte,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posRemplacent,
                posAbsent,
                posFormation);
        addQualification(
                managers,
                empRejeanRoy,
                posOperateurGrispac,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empHuguesDenault,
                posGeneraleDemouleuse,
                posGeneraleEmballageBaguettine,
                posRecuperateurEmballage,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empRolandJrBoucher,
                posPrefarinePremelange,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posOperateurEmballageTriangle,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posGeneralEmballageSnackBote,
                posOperateurEnsacheuseVerticalSnack,
                posOperateurPetrisseurSnack,
                posOperateurGerbeuseVerification,
                posRemplacent);
        addQualification(
                managers,
                empBernardJolin,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posPrefarinePremelange,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleEmballageBaguettine,
                posGeneralEmballageMelba,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empSartoTremblay,
                posOperateurEmballageTriangle,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageSnackBote,
                posOperateurEnsacheuseVerticalSnack,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empDianeGirard,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posOperateurEmballageTriangle,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posOperateurLigneFourMelba,
                posOperateurEmballageMelba,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empMarioPaille,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posFormation,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posAbsent);
        addQualification(
                managers,
                empGinetteOuellette,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empGuylaineGuy,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent,
                posFormation,
                posOperateurGerbeuseVerification);
        addQualification(
                managers,
                empPierretteLamothe,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(managers, empMarcBellemare, posMecanicien);
        addQualification(
                managers,
                empMichelineLegault,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posFormation);
        addQualification(
                managers,
                empJoseeConstantineau,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posFormation,
                posRemplacent);
        addQualification(
                managers,
                empMadelaineMarleau,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posPreposeAuMelange,
                posRemplacent);
        addQualification(
                managers,
                empManonPoissant,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empFrancineLemieux,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees);
        addQualification(
                managers,
                empCarmenBrais,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empFrancoiseTrudeau,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empEricRichard,
                posPreposeAuMelange,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empNancyTheoret,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empLiseCampeau,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empLucieLeavey,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posOperateurLigneFourMelba,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurEnsacheuseVerticalSnack,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empLyndaLajoie,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empJeanfrancoisBreton,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posFormation,
                posAbsent);
        addQualification(
                managers,
                empStephaneJacques,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empBrigitteBouchard,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posFormation,
                posRemplacent,
                posOperateurDeLigneBaton);
        addQualification(
                managers,
                empMartinDube,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empSylviePineault,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empJoseeLapierre,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posOperateurLigneFourMelba,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurEnsacheuseVerticalSnack,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empHachezGabriel,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empSandraDupuis,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(managers, empLucRoy, posMecanicien);
        addQualification(
                managers,
                empLucieCaron,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empRachelMoise,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurEmballageTriangle,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posOperateurEmballageMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posPreposeAuxEpicesEtReparation,
                posFormation);
        addQualification(
                managers,
                empCatherinePiette,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posFormation);
        addQualification(
                managers,
                empChantalXavier,
                posGeneraleEmballageFmc,
                posOperateurBatonCompteuse,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empDominicHoude,
                posOperateurBatonCompteuse,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empFrancoisParent,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empSolangeGirard,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empMartinLina,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posFormation,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empLiseJoncas,
                posOperateurDeLigneBaton,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empNathalieReid,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empCecileCouillard,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posOperateurDeLigneBiscottebagHorsDoeuvre,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurEmballageChap25lb,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(managers, empSylvainJulien, posElectrotechniciens);
        addQualification(managers, empSylvainCarriere, posMecanicien);
        addQualification(
                managers,
                empRichardVaillant,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posFormation,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posAbsent);
        addQualification(
                managers,
                empFranceBoyer,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empIsabelleLeclerc,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empFrancoisArcoite,
                posOperateurDeLigneBaton,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurLigneAPain,
                posPrefarinePremelange,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posRecuperateurEmballage,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empSabrinaDupuis,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(managers, empIvanhoeMaisonneuve, posElectrotechniciens);
        addQualification(
                managers,
                empMathieuGuy,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurLigneAPain,
                posPrefarinePremelange,
                posPetrisseurAPain,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empDaisyBourget,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(managers, empMathewBellemare, posMecanicien);
        addQualification(
                managers,
                empAlexandreDube,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurPetrisseurSnack,
                posPreposeSalubrite,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empAnnickPigeon,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empDanielDuquette,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posOperateurLigneAPain,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posOperateurLigneFourMelba,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posOperateurDeRemplacementSnack,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posOperateurEnsacheuseVerticalSnack,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posFormation,
                posOperateurPetrisseurSnack,
                posPrefarinePremelange,
                posOperateurRemplacementBoul,
                posPetrisseurAPain);
        addQualification(
                managers,
                empJohanneDuval,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posFormation);
        addQualification(
                managers,
                empCelineVadnais,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(
                managers,
                empMarcGrondin,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posPrefarinePremelange,
                posOperateurRemplacementBoul,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posOperateurGerbeuseVerification,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empMarcelLegault,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empNicolasLegault,
                posOperateurRemplacementBaton,
                posPreposeAuMelange,
                posOperateurGrispac,
                posGeneraleEmballageFmc,
                posFournier,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empStefanieReynolds,
                posGeneraleEmballageFmc,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent);
        addQualification(managers, empGinoLemoine, posElectrotechniciens);
        addQualification(
                managers,
                empMarioLongtin,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier,
                posFormation,
                posPrefarinePremelange,
                posOperateurRemplacementBoul);
        addQualification(
                managers,
                empRobertAllen,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empChristopheDeneault,
                posOperateurEmballageFmc,
                posGeneraleEmballageFmc,
                posTolierMiniBouchees,
                posGeneraleFourMiniBouchees,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBiscotte,
                posGeneraleEmballageBiscotte,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posGeneraleFourAideFournier);
        addQualification(
                managers,
                empPierreLemelin,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posFormation,
                posGeneralSalubrite,
                posRemplacent,
                posTolier,
                posTolierMiniBouchees,
                posGeneraleFourAideFournier,
                posGeneraleFourMiniBouchees,
                posGeneralEmballageSnackBote);
        addQualification(
                managers,
                empRobertShonfield,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballagePainsMinces,
                posGeneralEmballageSnackBote,
                posGeneralEmballageCroutonsVrac,
                posRemplacent,
                posFormation,
                posGeneraleDemouleuse,
                posTolier,
                posTolierMiniBouchees,
                posGeneraleFourAideFournier,
                posGeneraleFourMiniBouchees,
                posGeneralSalubrite,
                posGeneraleTrancheuseBiscotte);
        addQualification(
                managers,
                empLouisCarlPage,
                posGeneraleEmballageBaton,
                posGeneraleDemouleuse,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posRemplacent,
                posFormation,
                posGeneralSalubrite,
                posTolierMiniBouchees,
                posGeneraleFourAideFournier,
                posGeneraleFourMiniBouchees,
                posTolier);
        addQualification(
                managers,
                empNathalieBedard,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageSnackBote,
                posGeneralEmballageCroutonsVrac,
                posRemplacent,
                posGeneraleEmballageBaton);
        addQualification(
                managers,
                empWayneDavid,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageCroutonsVrac,
                posGeneralEmballageSnackBote,
                posRemplacent);
        addQualification(
                managers,
                empPierreGagne,
                posRemplacent,
                posTolier,
                posTolierMiniBouchees,
                posGeneraleFourAideFournier,
                posGeneraleFourMiniBouchees,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posGeneralEmballageSnackBote,
                posGeneraleEmballageBaguettine,
                posGeneraleTrancheuseBaguettine);
        addQualification(
                managers,
                empDanielGuillemette,
                posGeneraleEmballageBaton,
                posGeneraleTrancheuseBaguettine,
                posGeneraleEmballageBaguettine,
                posEquarisseur,
                posGeneralTrancheuseMelba,
                posGeneralEmballageMelba,
                posRemplacent,
                posFormation,
                posAbsent,
                posGeneralSalubrite,
                posGeneraleDemouleuse);

    }

    /**
     * Create Sections
     * 
     * SQL To create section variables.
     * 
     * <pre>
     * SELECT 'Section ' || camel('sec ' || SECTION.NAME) || ';' FROM SECTION
     * SELECT camel('sec ' || SECTION.NAME) || ' = addSection(managers, "' || SECTION.NAME || '");' FROM SECTION
     * </pre>
     */
    private void createSections() {

        sec3Boulangerie = addSection(managers, "3. Boulangerie");
        sec1Emballage = addSection(managers, "1. Emballage");
        sec2Snack = addSection(managers, "2. Snack");
        sec4Baton = addSection(managers, "4. Bâton");
        sec5Salubrite = addSection(managers, "5. Salubrité");
        sec6Expedition = addSection(managers, "6. Expédition");
        sec7Maintenance = addSection(managers, "7. Maintenance");
        sec8Formation = addSection(managers, "8. Formation");
        sec9Absent = addSection(managers, "9. Absent");

    }

    /**
     * 
     * <pre>
     * SELECT 'Shift ' || camel(TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || ';' FROM SHIFT, TEAM WHERE TEAM.ID = SHIFT.TEAM_ID AND SHIFT.STARTDATE BETWEEN DATE '2013-01-13' and DATE '2013-01-20'
     * SELECT camel(TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || ' =  addShift(managers, ' || camel('team ' || TEAM.NAME) || ', dateTime("' || FORMATDATETIME(SHIFT.STARTDATE, 'yyyy-MM-dd EEE HH:mm') || '"), dateTime("' || FORMATDATETIME(SHIFT.ENDDATE,'yyyy-MM-dd EEE HH:mm') || '"));' FROM SHIFT, TEAM WHERE TEAM.ID = SHIFT.TEAM_ID AND SHIFT.STARTDATE BETWEEN DATE '2013-01-13' and DATE '2013-01-20'
     * </pre>
     * 
     */
    private void createShifts() {

        joursDeSemaineWed = addShift(managers, team1JoursDeSemaine, dateTime("2013-01-23 Wed 05:00"), dateTime("2013-01-23 Wed 15:00"));
        joursDeSemaineTue = addShift(managers, team1JoursDeSemaine, dateTime("2013-01-22 Tue 05:00"), dateTime("2013-01-22 Tue 15:00"));
        joursDeSemaineMon = addShift(managers, team1JoursDeSemaine, dateTime("2013-01-21 Mon 05:00"), dateTime("2013-01-21 Mon 15:00"));
        joursDeSemaineThu = addShift(managers, team1JoursDeSemaine, dateTime("2013-01-24 Thu 05:00"), dateTime("2013-01-24 Thu 15:00"));
        jourRemplacementSemaineWed = addShift(managers, team2JourRemplacementSemaine, dateTime("2013-01-23 Wed 08:15"), dateTime("2013-01-23 Wed 14:45"));
        jourRemplacementSemaineTue = addShift(managers, team2JourRemplacementSemaine, dateTime("2013-01-22 Tue 08:15"), dateTime("2013-01-22 Tue 14:45"));
        jourRemplacementSemaineMon = addShift(managers, team2JourRemplacementSemaine, dateTime("2013-01-21 Mon 08:15"), dateTime("2013-01-21 Mon 14:45"));
        jourRemplacementSemaineThu = addShift(managers, team2JourRemplacementSemaine, dateTime("2013-01-24 Thu 08:15"), dateTime("2013-01-24 Thu 14:45"));
        soirSemaineMon = addShift(managers, team3SoirSemaine, dateTime("2013-01-21 Mon 15:00"), dateTime("2013-01-22 Tue 01:00"));
        soirSemaineTue = addShift(managers, team3SoirSemaine, dateTime("2013-01-22 Tue 15:00"), dateTime("2013-01-23 Wed 01:00"));
        soirSemaineWed = addShift(managers, team3SoirSemaine, dateTime("2013-01-23 Wed 15:00"), dateTime("2013-01-24 Thu 01:00"));
        soirSemaineThu = addShift(managers, team3SoirSemaine, dateTime("2013-01-24 Thu 15:00"), dateTime("2013-01-25 Fri 01:00"));
        soirRemplacementSemaineThu = addShift(managers, team4SoirRemplacementSemaine, dateTime("2013-01-24 Thu 18:15"), dateTime("2013-01-25 Fri 00:45"));
        soirRemplacementSemaineTue = addShift(managers, team4SoirRemplacementSemaine, dateTime("2013-01-22 Tue 18:15"), dateTime("2013-01-23 Wed 00:45"));
        soirRemplacementSemaineWed = addShift(managers, team4SoirRemplacementSemaine, dateTime("2013-01-23 Wed 18:15"), dateTime("2013-01-24 Thu 00:45"));
        soirRemplacementSemaineMon = addShift(managers, team4SoirRemplacementSemaine, dateTime("2013-01-21 Mon 18:15"), dateTime("2013-01-22 Tue 00:45"));
        nuitSemaineDimJeuWed = addShift(managers, team9NuitSemaineDimJeu, dateTime("2013-01-23 Wed 22:00"), dateTime("2013-01-24 Thu 06:00"));
        nuitSemaineDimJeuThu = addShift(managers, team9NuitSemaineDimJeu, dateTime("2013-01-24 Thu 22:00"), dateTime("2013-01-25 Fri 06:00"));
        nuitSemaineDimJeuTue = addShift(managers, team9NuitSemaineDimJeu, dateTime("2013-01-22 Tue 22:00"), dateTime("2013-01-23 Wed 06:00"));
        nuitSemaineDimJeuMon = addShift(managers, team9NuitSemaineDimJeu, dateTime("2013-01-21 Mon 22:00"), dateTime("2013-01-22 Tue 06:00"));
        nuitSemaineDimJeuSun = addShift(managers, team9NuitSemaineDimJeu, dateTime("2013-01-20 Sun 22:00"), dateTime("2013-01-21 Mon 06:00"));
        nuitSemaineLunVenFri = addShift(managers, team10NuitSemaineLunVen, dateTime("2013-01-25 Fri 22:00"), dateTime("2013-01-26 Sat 06:00"));
        nuitSemaineLunVenWed = addShift(managers, team10NuitSemaineLunVen, dateTime("2013-01-23 Wed 22:00"), dateTime("2013-01-24 Thu 06:00"));
        nuitSemaineLunVenThu = addShift(managers, team10NuitSemaineLunVen, dateTime("2013-01-24 Thu 22:00"), dateTime("2013-01-25 Fri 06:00"));
        nuitSemaineLunVenMon = addShift(managers, team10NuitSemaineLunVen, dateTime("2013-01-21 Mon 22:00"), dateTime("2013-01-22 Tue 06:00"));
        nuitSemaineLunVenTue = addShift(managers, team10NuitSemaineLunVen, dateTime("2013-01-22 Tue 22:00"), dateTime("2013-01-23 Wed 06:00"));
        jourFinDeSemaineSun = addShift(managers, team5JourFinDeSemaine, dateTime("2013-01-20 Sun 06:00"), dateTime("2013-01-20 Sun 18:00"));
        jourFinDeSemaineFri = addShift(managers, team5JourFinDeSemaine, dateTime("2013-01-25 Fri 06:00"), dateTime("2013-01-25 Fri 18:00"));
        jourFinDeSemaineSat = addShift(managers, team5JourFinDeSemaine, dateTime("2013-01-26 Sat 06:00"), dateTime("2013-01-26 Sat 18:00"));
        joursRemplacementFinDeSemaineSun = addShift(
                managers,
                team6JoursRemplacementFinDeSemaine,
                dateTime("2013-01-20 Sun 08:15"),
                dateTime("2013-01-20 Sun 16:15"));
        joursRemplacementFinDeSemaineFri = addShift(
                managers,
                team6JoursRemplacementFinDeSemaine,
                dateTime("2013-01-25 Fri 08:15"),
                dateTime("2013-01-25 Fri 16:15"));
        joursRemplacementFinDeSemaineSat = addShift(
                managers,
                team6JoursRemplacementFinDeSemaine,
                dateTime("2013-01-26 Sat 08:15"),
                dateTime("2013-01-26 Sat 16:15"));
        soirDeFinDeSemaineSun = addShift(managers, team7SoirDeFinDeSemaine, dateTime("2013-01-20 Sun 18:00"), dateTime("2013-01-21 Mon 05:00"));
        soirDeFinDeSemaineFri = addShift(managers, team7SoirDeFinDeSemaine, dateTime("2013-01-25 Fri 18:00"), dateTime("2013-01-26 Sat 06:00"));
        soirDeFinDeSemaineSat = addShift(managers, team7SoirDeFinDeSemaine, dateTime("2013-01-26 Sat 18:00"), dateTime("2013-01-26 Sat 23:59"));
        soirRemplacementFinDeSemaineSat = addShift(
                managers,
                team8SoirRemplacementFinDeSemaine,
                dateTime("2013-01-26 Sat 20:15"),
                dateTime("2013-01-26 Sat 23:59"));
        soirRemplacementFinDeSemaineSun = addShift(
                managers,
                team8SoirRemplacementFinDeSemaine,
                dateTime("2013-01-20 Sun 20:15"),
                dateTime("2013-01-21 Mon 03:45"));
        soirRemplacementFinDeSemaineFri = addShift(
                managers,
                team8SoirRemplacementFinDeSemaine,
                dateTime("2013-01-25 Fri 20:15"),
                dateTime("2013-01-26 Sat 03:45"));

    }

    /**
     * Create teams.
     * 
     * <pre>
     * SELECT 'Team ' || camel('team ' || TEAM.NAME) || ';' FROM TEAM;
     * SELECT camel('team ' || TEAM.NAME) || ' = addTeam(managers, "' || TEAM.NAME || '");' FROM TEAM;
     * </pre>
     */
    private void createTeams() {

        team1JoursDeSemaine = addTeam(managers, "1. Jours de semaine");
        team2JourRemplacementSemaine = addTeam(managers, "2. Jour remplacement semaine");
        team3SoirSemaine = addTeam(managers, "3. Soir semaine");
        team4SoirRemplacementSemaine = addTeam(managers, "4. Soir remplacement semaine");
        team9NuitSemaineDimJeu = addTeam(managers, "9. Nuit semaine Dim - Jeu");
        team10NuitSemaineLunVen = addTeam(managers, "10. Nuit semaine Lun - Ven");
        team5JourFinDeSemaine = addTeam(managers, "5. Jour fin de semaine");
        team6JoursRemplacementFinDeSemaine = addTeam(managers, "6. Jours remplacement fin de semaine");
        team7SoirDeFinDeSemaine = addTeam(managers, "7. Soir de fin de semaine");
        team8SoirRemplacementFinDeSemaine = addTeam(managers, "8. Soir remplacement fin de semaine");

    }

    /**
     * Restore the database snapshot using managers call.
     * 
     * <pre>
     * CREATE ALIAS CAMEL AS $$
     * import java.util.regex.Matcher;
     * import java.util.regex.Pattern;
     * &#64;CODE
     * String camel(String value) {
     * 	value = value.toLowerCase();
     * 	value = Pattern.compile("[éèêë]").matcher(value).replaceAll("e");
     * 	value = Pattern.compile("[àâ]").matcher(value).replaceAll("a");
     * 	value = Pattern.compile("[àâ]").matcher(value).replaceAll("a");
     * 	value = Pattern.compile("[ùûü]").matcher(value).replaceAll("u");
     * 	value = Pattern.compile("[ôöò]").matcher(value).replaceAll("o");
     * 	value = Pattern.compile("[^a-z0-9 ]").matcher(value).replaceAll("");
     * 	value = value.trim();
     * 	value = Pattern.compile("^[0-9]+").matcher(value).replaceAll("");
     * 	value = value.trim();
     * 	StringBuilder sb = new StringBuilder();
     * 	Matcher m = Pattern.compile("\\s+([a-z0-9])").matcher(value);
     * 	int last = 0;
     * 	while (m.find()) {
     * 		sb.append(value.substring(last, m.start()));
     * 		sb.append(m.group(1).toUpperCase());
     * 		last = m.end();
     * 	}
     * 	sb.append(value.substring(last));
     * 	return sb.toString();
     * }
     * $$:
     * </pre>
     */
    @Before
    public void fillDatabase() throws ManagerException {

        setFirstDayOfWeek(managers, Calendar.SUNDAY);

        // Create sections
        createSections();

        // Create Positions
        createPositions();

        // Create product, product-position
        createProducts();
        createProductPositions();

        // Create teams and shifts
        createTeams();
        createShifts();

        // Create employees
        createEmployees();
        createQualifications();
        createPreferences();
        createNonAvailabilities();
        setArchivedEmployees();

        // Create Production events
        createProductionEvents();

    }

    /**
     * <pre>
     * SELECT 'managers.archiveAll(Arrays.asList(' || camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || '));' FROM EMPLOYEE WHERE ARCHIVEDDATE IS NOT NULL
     * </pre>
     * 
     * @throws ManagerException
     */
    private void setArchivedEmployees() throws ManagerException {

        managers.archiveAll(Arrays.asList(empDianeDugas));

    }

    /**
     * Create the locked tasks.
     * 
     * <pre>
     * SELECT 'setTaskLocked(managers, ' || camel('prod ' || CASEWHEN(PRODUCT.REFID IS NOT NULL AND PRODUCT.REFID != '', PRODUCT.REFID, PRODUCT.NAME)) || ', ' || camel(TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || ', ' || camel('pos ' || POSITION.NAME) || ', ' || CASEWHEN(TASK.EMPLOYEE_ID IS NOT NULL,SELECT camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) FROM EMPLOYEE WHERE EMPLOYEE.ID = TASK.EMPLOYEE_ID, 'null')  || ');' FROM TASK, PRODUCTIONEVENT, PRODUCT, POSITION, SHIFT, TEAM WHERE TASK.PRODUCTIONEVENT_ID = PRODUCTIONEVENT.ID AND TASK.POSITION_ID = POSITION.ID AND TASK.LOCKED = TRUE AND PRODUCTIONEVENT.SHIFT_ID = SHIFT.ID AND PRODUCTIONEVENT.PRODUCT_ID = PRODUCT.ID AND SHIFT.TEAM_ID = TEAM.ID AND TASK.STARTDATE BETWEEN DATE '2013-01-13' and DATE '2013-01-20' ORDER BY TASK.STARTDATE
     * </pre>
     * 
     * @throws ManagerException
     */
    private void setLockedTasks() throws ManagerException {

        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuSun, posFormation, empDanielGuillemette);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuSun, posFormation, empJohanneDuval);
        setTaskLocked(managers, prodPreposeSalubrite, nuitSemaineDimJeuSun, posPreposeSalubrite, empLucieCaron);
        setTaskLocked(managers, prod45050, joursDeSemaineMon, posOperateurDeLigneBiscottebagHorsDoeuvre, empPierreLamarque);
        setTaskLocked(managers, prod93020, joursDeSemaineMon, posOperateurGrispac, empLiseJoncas);
        setTaskLocked(managers, prodOperateurGerbeuse, joursDeSemaineMon, posOperateurGerbeuseVerification, empGuylaineGuy);
        setTaskLocked(managers, prodFormation, joursDeSemaineMon, posFormation, empNancyTheoret);
        setTaskLocked(managers, prodFormation, joursDeSemaineMon, posFormation, empJoseeConstantineau);
        setTaskLocked(managers, prodRecuperateur, soirSemaineMon, posRecuperateurEmballage, empMichelineLegault);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuMon, posFormation, empJohanneDuval);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuMon, posFormation, empDanielGuillemette);
        setTaskLocked(managers, prodPreposeSalubrite, nuitSemaineDimJeuMon, posPreposeSalubrite, empLucieCaron);
        setTaskLocked(managers, prod45052, joursDeSemaineTue, posOperateurDeLigneBiscottebagHorsDoeuvre, empPierreLamarque);
        setTaskLocked(managers, prod93020, joursDeSemaineTue, posOperateurGrispac, empLiseJoncas);
        setTaskLocked(managers, prodFormation, joursDeSemaineTue, posFormation, empNancyTheoret);
        setTaskLocked(managers, prodFormation, joursDeSemaineTue, posFormation, empJoseeConstantineau);
        setTaskLocked(managers, prodFormation, joursDeSemaineTue, posFormation, empGuylaineGuy);
        setTaskLocked(managers, prodRecuperateur, soirSemaineTue, posRecuperateurEmballage, empMichelineLegault);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuTue, posFormation, empDanielGuillemette);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuTue, posFormation, empJohanneDuval);
        setTaskLocked(managers, prodPreposeSalubrite, nuitSemaineDimJeuTue, posPreposeSalubrite, empLucieCaron);
        setTaskLocked(managers, prod41280, joursDeSemaineWed, posOperateurDeLigneBiscottebagHorsDoeuvre, empPierreLamarque);
        setTaskLocked(managers, prodFormation, joursDeSemaineWed, posFormation, empNancyTheoret);
        setTaskLocked(managers, prodFormation, joursDeSemaineWed, posFormation, empJoseeConstantineau);
        setTaskLocked(managers, prodFormation, joursDeSemaineWed, posFormation, empGuylaineGuy);
        setTaskLocked(managers, prod24610, soirSemaineWed, posOperateurEmballageFmc, empManonPoissant);
        setTaskLocked(managers, prodRecuperateur, soirSemaineWed, posRecuperateurEmballage, empMichelineLegault);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuWed, posFormation, empDanielGuillemette);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuWed, posFormation, empJohanneDuval);
        setTaskLocked(managers, prodPreposeSalubrite, nuitSemaineDimJeuWed, posPreposeSalubrite, empLucieCaron);
        setTaskLocked(managers, prodFormation, joursDeSemaineThu, posFormation, empNancyTheoret);
        setTaskLocked(managers, prodFormation, joursDeSemaineThu, posFormation, empJoseeConstantineau);
        setTaskLocked(managers, prodFormation, joursDeSemaineThu, posFormation, empGuylaineGuy);
        setTaskLocked(managers, prod92416, joursDeSemaineThu, posPreposeAuMelange, empLiseJoncas);
        setTaskLocked(managers, prodRecuperateur, soirSemaineThu, posRecuperateurEmballage, empMichelineLegault);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuThu, posFormation, empDanielGuillemette);
        setTaskLocked(managers, prodFormation, nuitSemaineDimJeuThu, posFormation, empJohanneDuval);
        setTaskLocked(managers, prodPreposeSalubrite, nuitSemaineDimJeuThu, posPreposeSalubrite, empLucieCaron);
        setTaskLocked(managers, prod680, jourFinDeSemaineFri, posOperateurEmballageChap25lb, empCecileCouillard);

    }

    /**
     * Check the result of the generate planif.
     * 
     * <pre>
     * SELECT 'assertAssignment(1, tasks, ' || camel('events ' || TEAM.NAME || ' ' || FORMATDATETIME(SHIFT.STARTDATE,'EEE')) || ', ' || CAMEL(CASEWHEN(POSITION.CLASSIFIED = TRUE, 'POS ' || POSITION.NAME, 'group ' || SECTION.NAME )) || ', ' || camel('emp ' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ');' FROM TASK, EMPLOYEE, POSITION, SECTION, SHIFT, TEAM, PRODUCTIONEVENT WHERE TASK.EMPLOYEE_ID = EMPLOYEE.ID AND TASK.POSITION_ID = POSITION.ID AND POSITION.SECTION_ID = SECTION.ID AND TASK.PRODUCTIONEVENT_ID = PRODUCTIONEVENT.ID AND PRODUCTIONEVENT.SHIFT_ID = SHIFT.ID AND SHIFT.TEAM_ID = TEAM.ID AND TASK.STARTDATE BETWEEN DATE '2013-01-13' AND DATE '2013-01-20' ORDER BY EMPLOYEE.HIREDATE, EMPLOYEE.FIRSTNAME, EMPLOYEE.LASTNAME, TASK.STARTDATE
     * </pre>
     * 
     * @throws ManagerException
     * @throws IOException
     */
    @Test
    public void testGeneratePlanif() throws ManagerException, IOException {

        GeneratePlanifContext context;
        context = managers.getTaskManager().createGeneratePlanifContext();
        context.setWeek(date("2013-01-21"));

        // Create tasks
        context.createTasks();

        // Sets locked
        setLockedTasks();

        // Generate planif.
        context.searchSolution(new MockGeneratePlanifMonitor());

        List<Task> tasks = managers.getTaskManager().list(context.getStart(), context.getEnd());

        assertAssignment(1, tasks, events1JoursDeSemaineMon, posMecanicien, empDonaldTheriault);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posMecanicien, empDonaldTheriault);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posMecanicien, empDonaldTheriault);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posMecanicien, empDonaldTheriault);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empCaroleMorand);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empCaroleMorand);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empCaroleMorand);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empCaroleMorand);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group2Snack, empLucieGarceau);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group2Snack, empLucieGarceau);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empLucieGarceau);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group2Snack, empLucieGarceau);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empMichelineDemers);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empMichelineDemers);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurEmballageFmc, empMichelineDemers);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empMichelineDemers);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group3Boulangerie, empJeanpierreAuger);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group3Boulangerie, empJeanpierreAuger);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group3Boulangerie, empJeanpierreAuger);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group3Boulangerie, empJeanpierreAuger);
        assertAssignment(1, tasks, events3SoirSemaineMon, posOperateurEmballageMelba, empMoniqueLeblond);
        assertAssignment(1, tasks, events3SoirSemaineTue, posOperateurEmballageMelba, empMoniqueLeblond);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurEmballageMelba, empMoniqueLeblond);
        assertAssignment(1, tasks, events3SoirSemaineThu, posOperateurEmballageMelba, empMoniqueLeblond);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurGerbeuseVerification, empRealGosselin);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurGerbeuseVerification, empRealGosselin);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurGerbeuseVerification, empRealGosselin);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurGerbeuseVerification, empRealGosselin);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empJohanneLemieux);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empJohanneLemieux);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empJohanneLemieux);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empJohanneLemieux);
        assertAssignment(1, tasks, events2JourRemplacementSemaineMon, group1Emballage, empManonTremblay);
        assertAssignment(1, tasks, events2JourRemplacementSemaineTue, group1Emballage, empManonTremblay);
        assertAssignment(1, tasks, events2JourRemplacementSemaineWed, group1Emballage, empManonTremblay);
        assertAssignment(1, tasks, events2JourRemplacementSemaineThu, group1Emballage, empManonTremblay);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posFournier, empBernardBerube);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posFournier, empBernardBerube);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posFournier, empBernardBerube);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group4Baton, empBernardBerube);
        assertAssignment(1, tasks, events3SoirSemaineMon, posOperateurGerbeuseVerification, empRobertLazure);
        assertAssignment(1, tasks, events3SoirSemaineTue, posOperateurGerbeuseVerification, empRobertLazure);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurGerbeuseVerification, empRobertLazure);
        assertAssignment(1, tasks, events3SoirSemaineThu, posOperateurGerbeuseVerification, empRobertLazure);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurEmballageTriangle, empLindaBoisvert);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurEmballageTriangle, empLindaBoisvert);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurEmballageTriangle, empLindaBoisvert);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurEmballageTriangle, empLindaBoisvert);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurLigneFourMelba, empSergeRobidoux);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurLigneFourMelba, empSergeRobidoux);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurLigneFourMelba, empSergeRobidoux);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurLigneFourMelba, empSergeRobidoux);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuSun, posPreposeSalubrite, empMichelDaniel);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuMon, posPreposeSalubrite, empMichelDaniel);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuTue, posPreposeSalubrite, empMichelDaniel);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuWed, posPreposeSalubrite, empMichelDaniel);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuThu, posPreposeSalubrite, empMichelDaniel);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empCaroleRaymond);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empCaroleRaymond);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empCaroleRaymond);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empCaroleRaymond);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurGrispac, empFrancoisBeaulne);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurGrispac, empFrancoisBeaulne);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurGrispac, empFrancoisBeaulne);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurGrispac, empFrancoisBeaulne);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurDeLigneBaton, empFrancineGuerin);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurDeLigneBaton, empFrancineGuerin);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurDeLigneBaton, empFrancineGuerin);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurDeLigneBaton, empFrancineGuerin);
        assertAssignment(1, tasks, events3SoirSemaineMon, group3Boulangerie, empJeanguyRicher);
        assertAssignment(1, tasks, events3SoirSemaineTue, group3Boulangerie, empJeanguyRicher);
        assertAssignment(1, tasks, events3SoirSemaineWed, group3Boulangerie, empJeanguyRicher);
        assertAssignment(1, tasks, events3SoirSemaineThu, group3Boulangerie, empJeanguyRicher);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurEmballageMelba, empMarcelDalphond);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurEmballageMelba, empMarcelDalphond);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurEmballageMelba, empMarcelDalphond);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurEmballageMelba, empMarcelDalphond);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posHuileurGraisseurEntretienPreventif, empMichelMeunier);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posHuileurGraisseurEntretienPreventif, empMichelMeunier);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posHuileurGraisseurEntretienPreventif, empMichelMeunier);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posHuileurGraisseurEntretienPreventif, empMichelMeunier);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurDeLigneBiscottebagHorsDoeuvre, empPierreLamarque);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurDeLigneBiscottebagHorsDoeuvre, empPierreLamarque);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurDeLigneBiscottebagHorsDoeuvre, empPierreLamarque);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurEnsacheuseVerticalSnack, empPierreLamarque);
        assertAssignment(1, tasks, events3SoirSemaineMon, posPetrisseurAPain, empGerardLanteigne);
        assertAssignment(1, tasks, events3SoirSemaineTue, posPetrisseurAPain, empGerardLanteigne);
        assertAssignment(1, tasks, events3SoirSemaineWed, posPetrisseurAPain, empGerardLanteigne);
        assertAssignment(1, tasks, events3SoirSemaineThu, posPetrisseurAPain, empGerardLanteigne);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posPetrisseurAPain, empJeanLatour);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posPetrisseurAPain, empJeanLatour);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posPetrisseurAPain, empJeanLatour);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posPetrisseurAPain, empJeanLatour);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empPierretteDupras);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empPierretteDupras);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empPierretteDupras);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empPierretteDupras);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posRecuperateurEmballage, empDanielNault);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posRecuperateurEmballage, empDanielNault);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posRecuperateurEmballage, empDanielNault);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posRecuperateurEmballage, empDanielNault);
        assertAssignment(1, tasks, events3SoirSemaineMon, posOperateurLigneAPain, empRaynaldStarnaud);
        assertAssignment(1, tasks, events3SoirSemaineTue, posOperateurLigneAPain, empRaynaldStarnaud);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurLigneAPain, empRaynaldStarnaud);
        assertAssignment(1, tasks, events3SoirSemaineThu, posOperateurLigneAPain, empRaynaldStarnaud);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, group1Emballage, empNicoleFortin);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posOperateurEmballageMelba, empNicoleFortin);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, posOperateurEmballageMelba, empNicoleFortin);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posPrefarinePremelange, empNormandArsenault);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posPrefarinePremelange, empNormandArsenault);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posPrefarinePremelange, empNormandArsenault);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posPrefarinePremelange, empNormandArsenault);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posPreposeAuxEpicesEtReparation, empFrancineLabbe);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posPreposeAuxEpicesEtReparation, empFrancineLabbe);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posPreposeAuxEpicesEtReparation, empFrancineLabbe);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posPreposeAuxEpicesEtReparation, empFrancineLabbe);
        assertAssignment(1, tasks, events2JourRemplacementSemaineMon, group1Emballage, empClaudineRochefort);
        assertAssignment(1, tasks, events2JourRemplacementSemaineTue, group1Emballage, empClaudineRochefort);
        assertAssignment(1, tasks, events2JourRemplacementSemaineWed, group1Emballage, empClaudineRochefort);
        assertAssignment(1, tasks, events2JourRemplacementSemaineThu, group1Emballage, empClaudineRochefort);
        assertAssignment(1, tasks, events6JoursRemplacementFinDeSemaineSun, group1Emballage, empSuzanneCouturier);
        assertAssignment(1, tasks, events6JoursRemplacementFinDeSemaineFri, group1Emballage, empSuzanneCouturier);
        assertAssignment(1, tasks, events6JoursRemplacementFinDeSemaineSat, group1Emballage, empSuzanneCouturier);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuSun, posPreposeSalubrite, empMichelTougas);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuMon, posPreposeSalubrite, empMichelTougas);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuTue, posPreposeSalubrite, empMichelTougas);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuWed, posPreposeSalubrite, empMichelTougas);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuThu, posPreposeSalubrite, empMichelTougas);
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empDanielleBeaudry);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empDanielleBeaudry);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empDanielleBeaudry);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empDanielleBeaudry);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empLucieGuay);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empLucieGuay);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empLucieGuay);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empLucieGuay);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurLigneAPain, empRogerDagenais);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurLigneAPain, empRogerDagenais);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurLigneAPain, empRogerDagenais);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurLigneAPain, empRogerDagenais);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurRemplacementBoul, empMichelJeanneau);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurRemplacementBoul, empMichelJeanneau);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurRemplacementBoul, empMichelJeanneau);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurRemplacementBoul, empMichelJeanneau);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuSun, posPreposeSalubrite, empDenisPilon);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuMon, posPreposeSalubrite, empDenisPilon);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuTue, posPreposeSalubrite, empDenisPilon);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuWed, posPreposeSalubrite, empDenisPilon);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuThu, posPreposeSalubrite, empDenisPilon);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurDeRemplacementSnack, empRachelBergevin);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurDeRemplacementSnack, empRachelBergevin);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empRachelBergevin);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurDeRemplacementSnack, empRachelBergevin);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurGerbeuseVerification, empRejeanRoy);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurGerbeuseVerification, empRejeanRoy);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurGerbeuseVerification, empRejeanRoy);
        assertAssignment(1, tasks, events3SoirSemaineMon, posOperateurLigneFourMelba, empRolandJrBoucher);
        assertAssignment(1, tasks, events3SoirSemaineTue, posOperateurLigneFourMelba, empRolandJrBoucher);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurLigneFourMelba, empRolandJrBoucher);
        assertAssignment(1, tasks, events3SoirSemaineThu, posOperateurLigneFourMelba, empRolandJrBoucher);
        assertAssignment(1, tasks, events3SoirSemaineMon, posOperateurRemplacementBoul, empBernardJolin);
        assertAssignment(1, tasks, events3SoirSemaineTue, posOperateurRemplacementBoul, empBernardJolin);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurRemplacementBoul, empBernardJolin);
        assertAssignment(1, tasks, events3SoirSemaineThu, posOperateurRemplacementBoul, empBernardJolin);
        assertAssignment(1, tasks, events3SoirSemaineMon, posOperateurEmballageTriangle, empSartoTremblay);
        assertAssignment(1, tasks, events3SoirSemaineTue, posOperateurEmballageTriangle, empSartoTremblay);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurEmballageTriangle, empSartoTremblay);
        assertAssignment(1, tasks, events3SoirSemaineThu, posOperateurEmballageTriangle, empSartoTremblay);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, posOperateurEmballageTriangle, empDianeGirard);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, group1Emballage, empDianeGirard);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, group1Emballage, empDianeGirard);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurRemplacementBaton, empMarioPaille);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurRemplacementBaton, empMarioPaille);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurRemplacementBaton, empMarioPaille);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurRemplacementBaton, empMarioPaille);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurGerbeuseVerification, empGuylaineGuy);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group8Formation, empGuylaineGuy);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group8Formation, empGuylaineGuy);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group8Formation, empGuylaineGuy);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empPierretteLamothe);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empPierretteLamothe);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empPierretteLamothe);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empPierretteLamothe);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posMecanicien, empMarcBellemare);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posMecanicien, empMarcBellemare);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posMecanicien, empMarcBellemare);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posMecanicien, empMarcBellemare);
        assertAssignment(1, tasks, events3SoirSemaineMon, posRecuperateurEmballage, empMichelineLegault);
        assertAssignment(1, tasks, events3SoirSemaineTue, posRecuperateurEmballage, empMichelineLegault);
        assertAssignment(1, tasks, events3SoirSemaineWed, posRecuperateurEmballage, empMichelineLegault);
        assertAssignment(1, tasks, events3SoirSemaineThu, posRecuperateurEmballage, empMichelineLegault);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group8Formation, empJoseeConstantineau);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group8Formation, empJoseeConstantineau);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group8Formation, empJoseeConstantineau);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group8Formation, empJoseeConstantineau);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posPreposeAuMelange, empMadelaineMarleau);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posPreposeAuMelange, empMadelaineMarleau);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posPreposeAuMelange, empMadelaineMarleau);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posPreposeAuMelange, empMadelaineMarleau);
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empManonPoissant);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empManonPoissant);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurEmballageFmc, empManonPoissant);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empManonPoissant);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group1Emballage, empFrancineLemieux);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group1Emballage, empFrancineLemieux);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empFrancineLemieux);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empFrancineLemieux);
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empFrancoiseTrudeau);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empFrancoiseTrudeau);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empFrancoiseTrudeau);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empFrancoiseTrudeau);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group8Formation, empNancyTheoret);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group8Formation, empNancyTheoret);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group8Formation, empNancyTheoret);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group8Formation, empNancyTheoret);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineMon, group1Emballage, empLiseCampeau);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineTue, group1Emballage, empLiseCampeau);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineWed, group1Emballage, empLiseCampeau);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineThu, group1Emballage, empLiseCampeau);
        assertAssignment(1, tasks, events3SoirSemaineMon, posOperateurDeLigneBiscottebagHorsDoeuvre, empLucieLeavey);
        assertAssignment(1, tasks, events3SoirSemaineTue, posOperateurDeLigneBiscottebagHorsDoeuvre, empLucieLeavey);
        assertAssignment(1, tasks, events3SoirSemaineWed, posOperateurDeLigneBiscottebagHorsDoeuvre, empLucieLeavey);
        assertAssignment(1, tasks, events3SoirSemaineThu, posOperateurDeLigneBiscottebagHorsDoeuvre, empLucieLeavey);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurPetrisseurSnack, empLyndaLajoie);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurPetrisseurSnack, empLyndaLajoie);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posOperateurPetrisseurSnack, empLyndaLajoie);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posOperateurPetrisseurSnack, empLyndaLajoie);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, group1Emballage, empJeanfrancoisBreton);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posPetrisseurAPain, empJeanfrancoisBreton);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, posPetrisseurAPain, empJeanfrancoisBreton);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, posRecuperateurEmballage, empStephaneJacques);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posRecuperateurEmballage, empStephaneJacques);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, posRecuperateurEmballage, empStephaneJacques);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group4Baton, empBrigitteBouchard);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group4Baton, empBrigitteBouchard);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empBrigitteBouchard);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empBrigitteBouchard);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, posOperateurLigneFourMelba, empJoseeLapierre);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posOperateurLigneFourMelba, empJoseeLapierre);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, posOperateurLigneFourMelba, empJoseeLapierre);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posPreposeSalubrite, empHachezGabriel);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posPreposeSalubrite, empHachezGabriel);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posPreposeSalubrite, empHachezGabriel);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posPreposeSalubrite, empHachezGabriel);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, group1Emballage, empSandraDupuis);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, group1Emballage, empSandraDupuis);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, group1Emballage, empSandraDupuis);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posMecanicien, empLucRoy);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posMecanicien, empLucRoy);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posMecanicien, empLucRoy);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posMecanicien, empLucRoy);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuSun, posPreposeSalubrite, empLucieCaron);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuMon, posPreposeSalubrite, empLucieCaron);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuTue, posPreposeSalubrite, empLucieCaron);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuWed, posPreposeSalubrite, empLucieCaron);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuThu, posPreposeSalubrite, empLucieCaron);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group4Baton, empRachelMoise);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group4Baton, empRachelMoise);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empRachelMoise);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group1Emballage, empRachelMoise);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group2Snack, empCatherinePiette);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group2Snack, empCatherinePiette);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empCatherinePiette);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group2Snack, empCatherinePiette);
        assertAssignment(1, tasks, events2JourRemplacementSemaineMon, group1Emballage, empChantalXavier);
        assertAssignment(1, tasks, events2JourRemplacementSemaineTue, group1Emballage, empChantalXavier);
        assertAssignment(1, tasks, events2JourRemplacementSemaineWed, group1Emballage, empChantalXavier);
        assertAssignment(1, tasks, events2JourRemplacementSemaineThu, group1Emballage, empChantalXavier);
        assertAssignment(1, tasks, events3SoirSemaineMon, posPrefarinePremelange, empDominicHoude);
        assertAssignment(1, tasks, events3SoirSemaineTue, posPrefarinePremelange, empDominicHoude);
        assertAssignment(1, tasks, events3SoirSemaineWed, posPrefarinePremelange, empDominicHoude);
        assertAssignment(1, tasks, events3SoirSemaineThu, posPrefarinePremelange, empDominicHoude);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group3Boulangerie, empFrancoisParent);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group3Boulangerie, empFrancoisParent);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group3Boulangerie, empFrancoisParent);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group3Boulangerie, empFrancoisParent);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group4Baton, empMartinLina);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group4Baton, empMartinLina);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group4Baton, empMartinLina);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group4Baton, empMartinLina);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posOperateurGrispac, empLiseJoncas);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posOperateurGrispac, empLiseJoncas);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group4Baton, empLiseJoncas);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posPreposeAuMelange, empLiseJoncas);
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empNathalieReid);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empNathalieReid);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empNathalieReid);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empNathalieReid);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posOperateurEmballageChap25lb, empCecileCouillard);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, group1Emballage, empCecileCouillard);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, posElectrotechniciens, empSylvainJulien);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, posElectrotechniciens, empSylvainJulien);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, posElectrotechniciens, empSylvainJulien);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, posElectrotechniciens, empSylvainJulien);
        assertAssignment(1, tasks, events3SoirSemaineMon, posMecanicien, empSylvainCarriere);
        assertAssignment(1, tasks, events3SoirSemaineTue, posMecanicien, empSylvainCarriere);
        assertAssignment(1, tasks, events3SoirSemaineWed, posMecanicien, empSylvainCarriere);
        assertAssignment(1, tasks, events3SoirSemaineThu, posMecanicien, empSylvainCarriere);
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empFranceBoyer);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empFranceBoyer);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empFranceBoyer);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empFranceBoyer);
        assertAssignment(1, tasks, events7SoirDeFinDeSemaineSun, posPreposeSalubrite, empIsabelleLeclerc);
        assertAssignment(1, tasks, events7SoirDeFinDeSemaineFri, posPreposeSalubrite, empIsabelleLeclerc);
        assertAssignment(1, tasks, events7SoirDeFinDeSemaineSat, posPreposeSalubrite, empIsabelleLeclerc);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, group1Emballage, empFrancoisArcoite);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, group1Emballage, empFrancoisArcoite);
        assertAssignment(1, tasks, events1JoursDeSemaineMon, group4Baton, empSabrinaDupuis);
        assertAssignment(1, tasks, events1JoursDeSemaineTue, group4Baton, empSabrinaDupuis);
        assertAssignment(1, tasks, events1JoursDeSemaineWed, group1Emballage, empSabrinaDupuis);
        assertAssignment(1, tasks, events1JoursDeSemaineThu, group2Snack, empSabrinaDupuis);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, posElectrotechniciens, empIvanhoeMaisonneuve);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posElectrotechniciens, empIvanhoeMaisonneuve);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, posElectrotechniciens, empIvanhoeMaisonneuve);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, posOperateurGerbeuseVerification, empMathieuGuy);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posOperateurGerbeuseVerification, empMathieuGuy);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, posOperateurGerbeuseVerification, empMathieuGuy);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, posMecanicien, empMathewBellemare);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, posMecanicien, empMathewBellemare);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, posMecanicien, empMathewBellemare);
        assertAssignment(1, tasks, events7SoirDeFinDeSemaineSun, posPreposeSalubrite, empAlexandreDube);
        assertAssignment(1, tasks, events7SoirDeFinDeSemaineFri, posPreposeSalubrite, empAlexandreDube);
        assertAssignment(1, tasks, events7SoirDeFinDeSemaineSat, posPreposeSalubrite, empAlexandreDube);
        assertAssignment(1, tasks, events3SoirSemaineMon, group3Boulangerie, empDanielDuquette);
        assertAssignment(1, tasks, events3SoirSemaineTue, group3Boulangerie, empDanielDuquette);
        assertAssignment(1, tasks, events3SoirSemaineWed, group3Boulangerie, empDanielDuquette);
        assertAssignment(1, tasks, events3SoirSemaineThu, group3Boulangerie, empDanielDuquette);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuSun, group8Formation, empJohanneDuval);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuMon, group8Formation, empJohanneDuval);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuTue, group8Formation, empJohanneDuval);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuWed, group8Formation, empJohanneDuval);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuThu, group8Formation, empJohanneDuval);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, group1Emballage, empMarcGrondin);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, group1Emballage, empMarcGrondin);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, group1Emballage, Arrays.asList(empMarcGrondin, empFrancoisArcoite));
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empMarcelLegault);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empMarcelLegault);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empMarcelLegault);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empMarcelLegault);
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empNicolasLegault);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empNicolasLegault);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empNicolasLegault);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empNicolasLegault);
        assertAssignment(1, tasks, events5JourFinDeSemaineSun, group1Emballage, empStefanieReynolds);
        assertAssignment(1, tasks, events5JourFinDeSemaineFri, group1Emballage, empStefanieReynolds);
        assertAssignment(1, tasks, events5JourFinDeSemaineSat, group1Emballage, empStefanieReynolds);
        assertAssignment(1, tasks, events3SoirSemaineMon, posElectrotechniciens, empGinoLemoine);
        assertAssignment(1, tasks, events3SoirSemaineTue, posElectrotechniciens, empGinoLemoine);
        assertAssignment(1, tasks, events3SoirSemaineWed, posElectrotechniciens, empGinoLemoine);
        assertAssignment(1, tasks, events3SoirSemaineThu, posElectrotechniciens, empGinoLemoine);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empMarioLongtin);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empRobertAllen);
        assertAssignment(1, tasks, events3SoirSemaineMon, group1Emballage, empChristopheDeneault);
        assertAssignment(1, tasks, events3SoirSemaineTue, group1Emballage, empChristopheDeneault);
        assertAssignment(1, tasks, events3SoirSemaineWed, group1Emballage, empChristopheDeneault);
        assertAssignment(1, tasks, events3SoirSemaineThu, group1Emballage, empChristopheDeneault);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuSun, group5Salubrite, empPierreLemelin);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuMon, group5Salubrite, empPierreLemelin);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuTue, group5Salubrite, empPierreLemelin);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuWed, group5Salubrite, empPierreLemelin);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuThu, group5Salubrite, empPierreLemelin);
        assertAssignment(1, tasks, events10NuitSemaineLunVenMon, group5Salubrite, empRobertShonfield);
        assertAssignment(1, tasks, events10NuitSemaineLunVenTue, group5Salubrite, empRobertShonfield);
        assertAssignment(1, tasks, events10NuitSemaineLunVenWed, group5Salubrite, empRobertShonfield);
        assertAssignment(1, tasks, events10NuitSemaineLunVenThu, group5Salubrite, empRobertShonfield);
        assertAssignment(1, tasks, events10NuitSemaineLunVenFri, group5Salubrite, empRobertShonfield);
        assertAssignment(1, tasks, events10NuitSemaineLunVenMon, group5Salubrite, empLouisCarlPage);
        assertAssignment(1, tasks, events10NuitSemaineLunVenTue, group5Salubrite, empLouisCarlPage);
        assertAssignment(1, tasks, events10NuitSemaineLunVenWed, group5Salubrite, empLouisCarlPage);
        assertAssignment(1, tasks, events10NuitSemaineLunVenThu, group5Salubrite, empLouisCarlPage);
        assertAssignment(1, tasks, events10NuitSemaineLunVenFri, group5Salubrite, empLouisCarlPage);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuSun, group8Formation, empDanielGuillemette);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuMon, group8Formation, empDanielGuillemette);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuTue, group8Formation, empDanielGuillemette);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuWed, group8Formation, empDanielGuillemette);
        assertAssignment(1, tasks, events9NuitSemaineDimJeuThu, group8Formation, empDanielGuillemette);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineMon, group1Emballage, empNathalieBedard);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineTue, group1Emballage, empNathalieBedard);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineWed, group1Emballage, empNathalieBedard);
        assertAssignment(1, tasks, events4SoirRemplacementSemaineThu, group1Emballage, empNathalieBedard);
        assertAssignment(1, tasks, events2JourRemplacementSemaineWed, group1Emballage, empPierreGagne);
        assertAssignment(1, tasks, events2JourRemplacementSemaineThu, group1Emballage, empPierreGagne);
    }
}
