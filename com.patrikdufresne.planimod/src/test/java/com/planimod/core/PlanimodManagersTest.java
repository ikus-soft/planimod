/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import static org.junit.Assert.*;

import java.net.MalformedURLException;

import org.junit.Test;

import com.patrikdufresne.managers.H2DBConfigurations;
import com.patrikdufresne.managers.ManagerException;

public class PlanimodManagersTest {

    @Test
    public void testOpenPageStoreDatabase() throws MalformedURLException, ManagerException {
        PlanimodManagers manager = new PlanimodManagers(H2DBConfigurations.create("./test.h2.db", true, false));
    }

    @Test
    public void testOpenMVStoreDatabase() throws MalformedURLException, ManagerException {
        PlanimodManagers manager = new PlanimodManagers(H2DBConfigurations.create("./test.h2.mv", true, false));
    }

}
