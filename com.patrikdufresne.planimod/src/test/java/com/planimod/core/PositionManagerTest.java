/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.patrikdufresne.managers.ManagerEvent;
import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.Position;
import com.planimod.core.Section;

/**
 * This class contains test to cover operation done using position objects. SR102 : permettre d'ajouter et modifier les
 * postes devant être pourvus lors de la production du produit.
 * 
 * @author patapouf
 * 
 */
public class PositionManagerTest extends ManagerTestCase {
    /**
     * Add the object.
     */
    @Test
    public void AddPosition() {

        addManagerObserver(ManagerEvent.ADD, Position.class);

        // Add a section
        Section section = addSection(managers, "Section");

        // Add a position
        Position p = new Position();
        p.setSection(section);
        try {
            managers.getPositionManager().add(Arrays.asList(p));
        } catch (ManagerException e) {
            fail("Error adding position", e);
        }

        assertEquals("Wrong number of manager events.", 1, getManagerEvents().size());
        assertManagerEvent(1, getManagerEvents(), ManagerEvent.ADD, p);
    }

    /**
     * Update the object.
     */
    @Test
    public void UpdatePosition() {

        addManagerObserver(ManagerEvent.UPDATE, Position.class);

        // Add a section
        Section section = addSection(managers, "Section");

        // Add a position
        Position p = new Position();
        p.setSection(section);
        try {
            managers.getPositionManager().add(Arrays.asList(p));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }
        p.setName("Position 1");
        p.setRefId("refid1");

        try {
            managers.getPositionManager().update(Arrays.asList(p));
        } catch (ManagerException e) {
            fail("Error updating object", e);
        }
        assertEquals("Wrong number of manager events", 1, getManagerEvents().size());
        assertManagerEvent(1, getManagerEvents(), ManagerEvent.UPDATE, p);

        // Reload the object
        Position p2;
        try {
            p2 = managers.getPositionManager().get(p.getId());
        } catch (ManagerException e) {
            fail("Error loading the object", e);
            return;
        }

        Assert.assertEquals("Object not equals", p.getName(), p2.getName());
        Assert.assertEquals("Object not equals", p.getRefId(), p2.getRefId());
    }

    /**
     * Create and then remove the object.
     */
    @Test
    public void RemovePosition() {

        addManagerObserver(ManagerEvent.REMOVE, Position.class);

        // Add a section
        Section section = addSection(managers, "Section");

        // Add a position
        Position p = new Position();
        p.setSection(section);
        try {
            managers.getPositionManager().add(Arrays.asList(p));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }

        int id = p.getId();
        try {
            managers.getPositionManager().remove(Arrays.asList(p));
        } catch (ManagerException e) {
            fail("Error removing object", e);
        }

        assertEquals("Wrong number of events.", 1, getManagerEvents().size());
        assertManagerEvent(1, getManagerEvents(), ManagerEvent.REMOVE, p);

        try {
            List<Position> list = managers.getPositionManager().list();
            for (Position e : list) {
                if (e.getId() == id) {
                    Assert.fail("Object still exists");
                }
            }
        } catch (ManagerException e) {
            fail("Error listing object", e);
        }
    }

    /**
     * Test the manager function <code>listBySection</code>.
     */
    @Test
    public void ListBySection() {

        Section s1 = addSection(managers, "Section");
        Section s2 = addSection(managers, "Section");

        Position pos1 = addPosition(managers, s1, "pos1", false);
        Position pos2 = addPosition(managers, s1, "pos2", false);
        Position pos3 = addPosition(managers, s1, "pos3", false);
        Position pos4 = addPosition(managers, s2, "pos4", false);

        List<Position> list;
        try {
            list = managers.getPositionManager().listBySection(s1);
        } catch (ManagerException e) {
            fail("Error listing position by section", e);
            return;
        }
        assertEquals("Wrong number of position", 3, list.size());

        assertTrue("Position 1 not found", list.contains(pos1));
        assertTrue("Position 2 not found", list.contains(pos2));
        assertTrue("Position 3 not found", list.contains(pos3));

    }
}
