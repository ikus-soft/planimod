/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import static org.junit.Assert.*;
import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.Position;
import com.planimod.core.Product;
import com.planimod.core.ProductPosition;
import com.planimod.core.Section;

/**
 * This class intent to test all functionalities related to product-position.
 * 
 * @author patapouf
 * 
 */
public class ProductPositionManagersTest extends ManagerTestCase {

    @Test
    public void AddProductPosition() {
        Product product = addProduct(managers, "product1");
        Position position = addPosition(managers, null, null, false);

        ProductPosition pp = new ProductPosition();
        pp.setProduct(product);
        pp.setPosition(position);
        pp.setNumber(2);
        try {
            managers.getProductPositionManager().add(Arrays.asList(pp));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }
    }

    @Test
    public void Update() {
        Product product = addProduct(managers, "product1");
        Position position = addPosition(managers, null, null, false);

        ProductPosition pp = new ProductPosition();
        pp.setProduct(product);
        pp.setPosition(position);
        pp.setNumber(1);
        try {
            managers.getProductPositionManager().add(Arrays.asList(pp));
        } catch (ManagerException e) {
            fail("Error adding object", e);
        }

        // Save modification
        try {
            managers.getProductPositionManager().update(Arrays.asList(pp));
        } catch (ManagerException e) {
            fail("Error updating object", e);
        }

        // Reload employee
        ProductPosition pp2;
        try {
            pp2 = managers.getProductPositionManager().get(pp.getId());
        } catch (ManagerException e) {
            fail("Error loading object", e);
            return;
        }

        Assert.assertEquals("Objects not equals", pp.getNumber(), pp2.getNumber());

    }

    @Test
    public void ListByProduct() {

        Product product1 = addProduct(managers, "product1");
        Product product2 = addProduct(managers, "product2");
        Product product3 = addProduct(managers, "product3");

        Section section = addSection(managers, "Section");
        Position position1 = addPosition(managers, section, "position1", false);
        Position position2 = addPosition(managers, section, "position2", false);
        Position position3 = addPosition(managers, section, "position3", false);
        Position position4 = addPosition(managers, section, "position4", false);
        Position position5 = addPosition(managers, section, "position5", false);

        ProductPosition pp11 = addProductPosition(managers, product1, position1, 1);
        ProductPosition pp12 = addProductPosition(managers, product1, position2, 1);
        ProductPosition pp13 = addProductPosition(managers, product1, position3, 1);

        ProductPosition pp21 = addProductPosition(managers, product2, position1, 1);
        ProductPosition pp22 = addProductPosition(managers, product2, position2, 1);

        ProductPosition pp33 = addProductPosition(managers, product3, position3, 1);
        ProductPosition pp34 = addProductPosition(managers, product3, position4, 1);
        ProductPosition pp35 = addProductPosition(managers, product3, position5, 1);

        /*
         * Query list 1
         */
        List<ProductPosition> results;
        try {
            results = managers.getProductPositionManager().listByProduct(product1);
        } catch (ManagerException e) {
            fail("Error listing product-position", e);
            return;
        }
        assertEquals("Wrong number of element in the list", 3, results.size());

        assertTrue(results.contains(pp11));
        assertTrue(results.contains(pp12));
        assertTrue(results.contains(pp13));

        /*
         * Query list 2
         */
        try {
            results = managers.getProductPositionManager().listByProduct(product2);
        } catch (ManagerException e) {
            fail("Error listing product-position", e);
            return;
        }
        assertEquals("Wrong number of element in the list", 2, results.size());

        assertTrue(results.contains(pp21));
        assertTrue(results.contains(pp22));

        /*
         * Query list 3
         */
        try {
            results = managers.getProductPositionManager().listByProduct(product3);
        } catch (ManagerException e) {
            fail("Error listing product-position", e);
            return;
        }
        assertEquals("Wrong number of element in the list", 3, results.size());
        assertTrue(results.contains(pp33));
        assertTrue(results.contains(pp34));
        assertTrue(results.contains(pp35));
    }
}
