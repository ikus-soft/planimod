/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import static org.junit.Assert.*;

import java.util.Calendar;
import java.util.Date;

import org.junit.Test;

import com.patrikdufresne.managers.ManagerEvent;
import com.patrikdufresne.managers.ManagerException;

/**
 * This class intend to test the functionnalities provided by the {@link ApplicationSettingManager}.
 * 
 * @author ikus060
 * 
 */
public class ApplicationSettingManagerTest extends ManagerTestCase {

    @Test
    public void testSetByName() {
        addManagerObserver(ManagerEvent.ADD, ApplicationSetting.class);
        try {
            managers.getApplicationSettingManager().setByName("settings_name", "new_value");
        } catch (ManagerException e) {
            fail("Fail to sets the application settings.", e);
        }
        assertEquals("Wrong number of events.", 1, getManagerEvents().size());
        // assertManagerEvent(1, getManagerEvents(), ManagerEvent.ADD, entity);

        try {
            assertEquals("new_value", managers.getApplicationSettingManager().getByName("settings_name"));
        } catch (ManagerException e) {
            fail("Fail to get the application settings.", e);
        }

    }

    @Test
    public void testSetFirstDayOfWeek() throws ManagerException {
        addManagerObserver(ManagerEvent.ADD, ApplicationSetting.class);
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        managers.getApplicationSettingManager().setFirstDayOfWeek(cal.getFirstDayOfWeek());
        assertEquals("Wrong number of events.", 1, getManagerEvents().size());
        assertEquals(cal.getFirstDayOfWeek(), managers.getApplicationSettingManager().getFirstDayOfWeek());
    }

}
