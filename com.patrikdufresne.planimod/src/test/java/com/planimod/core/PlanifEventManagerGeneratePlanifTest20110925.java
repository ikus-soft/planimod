/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import static com.planimod.test.TimeUtils.date;
import static com.planimod.test.TimeUtils.dateTime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.comparators.TimeRangeComparators;

/**
 * This test case check the result of a generate planif for a database.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanifEventManagerGeneratePlanifTest20110925 extends ManagerTestCase {

    public PlanifEventManagerGeneratePlanifTest20110925() {
        super("./unittest001", true, false);
    }

    Employee alexandreDube;
    Employee annickPigeon;
    Employee bernardBerube;
    Employee bernardJolin;
    Employee brigitteBouchard;
    Employee carmenBrais;
    Employee caroleMorand;
    Employee caroleRaymond;
    Employee catherinePiette;
    Employee cecileCouillard;
    Employee celineVadnais;
    Employee chantalXavier;
    Employee claudineRochefort;
    Employee daisyBourget;
    Employee danielDuquette;
    Employee danielleBeaudry;
    Employee danielNault;
    Employee denisBerube;
    Employee denisDulude;
    Employee deniseDaigneault;
    Employee denisPilon;
    Employee dianeDugas;
    Employee dianeGirard;
    Employee dominicHoude;
    Employee donaldTheriault;
    Position electrotechniciens;
    Position equarisseur;
    Employee ericRichard;
    private ProductionEvent[] events;
    Position formation;
    Position fournier;
    Employee franceBoyer;
    Employee francineDurivage;
    Employee francineGuerin;
    Employee francineLabbe;
    Employee francineLemieux;
    Employee francoisArcoite;
    Employee francoisBeaulne;
    Employee francoiseTrudeau;
    Employee francoisParent;
    Employee fxCotebrunet;
    Position generaleDemouleuse;
    Position generaleEmballageBaguettine;
    Position generaleEmballageBaton;
    Position generaleEmballageBiscotte;
    Position generaleEmballageFmc;
    Position generaleFourAideFournier;
    Position generaleEmballageMelba;
    Position generaleEmballageCroutonsVrac;
    Position generaleEmballagePainsMinces;
    private List<Position> generalEmballagePositions;
    Position generalEmballageSnackBoite;
    Position generaleTrancheuseBaguettine;
    Position generaleTrancheuseBiscotte;
    Position generaleSalubrite;
    private List<Position> generalSnackPositions;
    Position generalTrancheuseMelba;
    Employee gerardLanteigne;
    Employee gillesGosselin;
    Employee ginetteLevesque;
    Employee ginetteOuellette;
    Employee ginoLemoine;
    Employee guylaineGuy;
    Employee hachezGabriel;
    Employee huguesDenault;
    Employee huguetteLebel;
    Position huileurGraisseurEntretienPreventif;
    Employee isabelleLeclerc;
    Employee ivanhoeMaisonneuve;
    Employee jeanfrancoisBreton;
    Employee jeanguyRicher;
    Employee jeanLatour;
    Employee jeanpierreAuger;
    Employee johanneDuval;
    Employee johanneLemieux;
    Employee joseeConstantineau;
    Employee joseeLapierre;
    List<ProductionEvent> jourFinDeSemaineEvents;
    List<ProductionEvent> jourRemplacementSemaineEvents;
    List<ProductionEvent> jourSemaineEvents;
    List<ProductionEvent> joursRemplacementFinDeSemaineEvents;
    Employee lindaBoisvert;
    Employee liseCampeau;
    Employee liseHebert;
    Employee liseJoncas;
    Employee louiseVeillette;
    Employee lucieCaron;
    Employee lucieGarceau;
    Employee lucieGuay;
    Employee lucieLacoste;
    Employee lucieLeavey;
    Employee lucRoy;
    Employee lyndaLajoie;
    Employee madelaineMarleau;
    Employee manonPoissant;
    Employee manonTremblay;
    Employee marcBellemare;
    Employee marcelDalphond;
    Employee marcelLegault;
    Employee marcGrondin;
    Employee marioLongtin;
    Employee marioPaille;
    Employee martinDube;
    Employee martinLina;
    Employee mathewBellemare;
    Employee mathieuGuy;
    Position mecanicien;
    Employee michelDaniel;
    Employee michelineDemers;
    Employee michelineLegault;
    Employee michelJeanneau;
    Employee michelMeunier;
    Employee michelTougas;
    Employee moniqueLeblond;
    Employee nancyTheoret;
    Employee nathalieReid;
    Employee nicolasLegault;
    Employee nicoleFortin;
    Employee normandArsenault;
    List<ProductionEvent> nuitSemaineSalubriteEvents;
    Position operateurBatonCompteuse;
    Position operateurDeLigne;
    Position operateurLigneBiscottebagHorsDoeuvre;
    Position operateurRemplacementSnack;
    Position operateurEmballageChap25lb;
    Position operateurEmballageFmc;
    Position operateurEmballageMelba;
    Position operateurEmballageTriangle;
    Position operateurEnsacheuseVerticalSnack;
    Position operateurGerbeuseVerification;
    Position operateurGrispac;
    Position operateurLigneAPain;
    Position operateurLigneFourMelba;
    Position operateurPetrisseurSnack;
    Position operateurRemplacementBaton;
    Position operateurRemplacementBoul;
    Position petrisseurAPain;
    Employee philippeLegault;
    Employee pierreLamarque;
    Employee pierretteDupras;
    Employee pierretteLamothe;
    Position prefarinePremelange;
    Position preposeAuMelange;
    Position preposeAuxEpicesEtReparation;
    Position preposeSalubrite;
    Employee rachelBergevin;
    Employee rachelMoise;
    Employee raynaldStarnaud;
    Employee realGosselin;
    Position recuperateurEmballage;
    Employee rejeanBrabant;
    Employee rejeanRoy;
    Employee richardVaillant;
    Employee robertAllen;
    Employee robertLazure;
    Employee rogerDagenais;
    Employee rolandJrBoucher;
    Employee sabrinaDupuis;
    Employee sandraDupuis;
    Employee sartoTremblay;
    Employee sergeRobidoux;
    List<ProductionEvent> soirDeFinDeSemaineEvents;
    List<ProductionEvent> soirRemplacementFinDeSemaineEvents;
    List<ProductionEvent> soirRemplacementSemaineEvents;
    List<ProductionEvent> soirSemaineEvents;
    Employee solangeGirard;
    Employee stefanieReynolds;
    Employee stephaneJacques;
    Employee suzanneCouturier;
    Employee suzanneGagnon;
    Employee sylvainCarriere;
    Employee sylvainJulien;
    Employee sylviePineault;
    Position tolier;

    @Before
    public void fillDatabase() throws ManagerException {

        setFirstDayOfWeek(managers, Calendar.SUNDAY);

        /*
         * Create Sections
         */
        // SELECT 'Section ' || camel(SECTION.NAME) || ' = addSection(managers,
        // "' || SECTION.NAME || '");' FROM SECTION
        Section boulangerie = addSection(managers, "Boulangerie");
        Section emballage = addSection(managers, "Emballage");
        Section snack = addSection(managers, "Snack");
        Section baton = addSection(managers, "Bâton");
        Section salubrite = addSection(managers, "Salubrité");
        Section expedition = addSection(managers, "Expédition");
        Section maintenance = addSection(managers, "Maintenance");
        Section autre = addSection(managers, "Autre");

        /*
         * Create Position
         */
        // SELECT 'Position ' || camel(POSITION.NAME) || ' =
        // addPosition(managers, ' || camel(SECTION.NAME)|| ',
        // "' || POSITION.NAME || '", ' || CASEWHEN ( POSITION.CLASSIFIED ,
        // 'true' , 'false' ) || ');' FROM POSITION, SECTION WHERE
        // POSITION.SECTION_ID = SECTION.ID
        generaleEmballageFmc = addPosition(managers, emballage, "Générale emballage FMC", false);
        tolier = addPosition(managers, baton, "Tôlier", false);
        generaleFourAideFournier = addPosition(managers, baton, "Générale four (Aide fournier)", false);
        generaleEmballageBaton = addPosition(managers, emballage, "Générale emballage bâton", false);
        generaleDemouleuse = addPosition(managers, boulangerie, "Générale démouleuse", false);
        generaleTrancheuseBaguettine = addPosition(managers, emballage, "Générale trancheuse baguettine", false);
        generaleEmballageBaguettine = addPosition(managers, emballage, "Générale emballage baguettine", false);
        generaleTrancheuseBiscotte = addPosition(managers, emballage, "Générale trancheuse biscotte", false);
        generaleEmballageBiscotte = addPosition(managers, emballage, "Générale emballage biscotte", false);
        equarisseur = addPosition(managers, emballage, "Équarisseur", false);
        generalTrancheuseMelba = addPosition(managers, emballage, "Général trancheuse Melba", false);
        generaleEmballageMelba = addPosition(managers, emballage, "Général emballage Melba", false);
        generaleEmballagePainsMinces = addPosition(managers, snack, "Général emballage pains minces", false);
        generaleEmballageCroutonsVrac = addPosition(managers, snack, "Général emballage croutons vrac", false);
        generalEmballageSnackBoite = addPosition(managers, snack, "Général emballage Snack boîte", false);
        generaleSalubrite = addPosition(managers, salubrite, "Général salubrité", false);
        operateurLigneAPain = addPosition(managers, boulangerie, "Opérateur ligne à pain", true);
        prefarinePremelange = addPosition(managers, boulangerie, "Préfarine / Pré-mélange", true);
        petrisseurAPain = addPosition(managers, boulangerie, "Pétrisseur à pain", true);
        operateurRemplacementBoul = addPosition(managers, boulangerie, "Opérateur remplacement", true);
        operateurEmballageFmc = addPosition(managers, emballage, "Opérateur emballage FMC", true);
        operateurBatonCompteuse = addPosition(managers, emballage, "Opérateur bâton compteuse", true);
        operateurLigneBiscottebagHorsDoeuvre = addPosition(managers, emballage, "Opérateur de ligne biscotte,bag., hors d'oeuvre", true);
        operateurEmballageTriangle = addPosition(managers, emballage, "Opérateur emballage Triangle", true);
        operateurLigneFourMelba = addPosition(managers, emballage, "Opérateur ligne four melba", true);
        operateurEmballageMelba = addPosition(managers, emballage, "Opérateur emballage melba", true);
        operateurEmballageChap25lb = addPosition(managers, emballage, "Opérateur emballage chap 25lb", true);
        recuperateurEmballage = addPosition(managers, emballage, "Récupérateur emballage", true);
        operateurRemplacementSnack = addPosition(managers, snack, "Opérateur de remplacement Snack", true);
        operateurEnsacheuseVerticalSnack = addPosition(managers, snack, "Opérateur ensacheuse vertical Snack", true);
        operateurPetrisseurSnack = addPosition(managers, snack, "Opérateur pétrisseur Snack", true);
        operateurDeLigne = addPosition(managers, baton, "Opérateur de ligne", true);
        operateurRemplacementBaton = addPosition(managers, baton, "Opérateur remplacement", true);
        preposeAuMelange = addPosition(managers, baton, "Préposé au mélange", true);
        operateurGrispac = addPosition(managers, baton, "Opérateur Grispac", true);
        fournier = addPosition(managers, baton, "Fournier", true);
        preposeSalubrite = addPosition(managers, salubrite, "Préposé salubrité", true);
        operateurGerbeuseVerification = addPosition(managers, expedition, "Opérateur gerbeuse vérification", true);
        preposeAuxEpicesEtReparation = addPosition(managers, expedition, "Préposé aux épices et réparation", true);
        electrotechniciens = addPosition(managers, maintenance, "Électrotechniciens", true);
        mecanicien = addPosition(managers, maintenance, "Mécanicien", true);
        huileurGraisseurEntretienPreventif = addPosition(managers, maintenance, "Huileur graisseur entretien préventif", true);
        formation = addPosition(managers, autre, "Formation", false);

        /*
         * Create product
         */
        // SELECT 'Product ' || camel('prod ' || CASEWHEN(PRODUCT.REFID IS NOT
        // NULL, PRODUCT.REFID, PRODUCT.NAME)) || ' = addProduct(managers,
        // "' || CASEWHEN(PRODUCT.REFID IS NOT NULL, PRODUCT.REFID , '') || '",
        // "' || PRODUCT.NAME || '",
        // "' || CASEWHEN(PRODUCT.FAMILY IS NOT NULL, PRODUCT.FAMILY, '') || '");'
        // FROM PRODUCT
        Product prod56 = addProduct(managers, "56", "Melba multi fibre - 12x175g", "Melba");
        Product prod69 = addProduct(managers, "69", "Melba multi fibre - 12x350g", "Melba");
        Product prod600 = addProduct(managers, "600", "Bât nature emballage - 250x2", "Pain bâton 250x2");
        Product prod650 = addProduct(managers, "650", "Melba nature - 400x2", "Melba");
        Product prod660 = addProduct(managers, "660", "Cr. nature - 4.5 Kg", "Croûton Vrac");
        Product prod663 = addProduct(managers, "663", "Cr. Nature Tournesol - 4.5 Kg", "Croûton Vrac");
        Product prod670 = addProduct(managers, "670", "Cr. assaisonné - 4.5 Kg", "Croûton Vrac");
        Product prod680 = addProduct(managers, "680", "Chap. rég. vrac - 25lbs", "Chapelure");
        Product prod2162 = addProduct(managers, "2162", "Melba Lunch Costco - 180x1kg", "Melba");
        Product prod24610 = addProduct(managers, "24610", "Bât sésame emballage - 250x2", "Pain bâton 250x2");
        Product prod24665 = addProduct(managers, "24665", "Cr ail - 4.5 Kg", "Croûton Vrac");
        Product prod41028 = addProduct(managers, "41028", "Bât. Sésame emballage - lunchpack", "Pain bâton 250x2");
        Product prod41200 = addProduct(managers, "41200", "Bât. nature - 12x200g", "Pain bâton 200gr");
        Product prod41205 = addProduct(managers, "41205", "Bât. sésame  - 12x200g", "Pain bâton 200gr");
        Product prod41210 = addProduct(managers, "41210", "Bât. blé entier - 12x200g", "Pain bâton 200gr");
        Product prod41280 = addProduct(managers, "41280", "Bag bouché ail  & parmesan - 12x130g", "Baguettine");
        Product prod41281 = addProduct(managers, "41281", "Bag sachet ail  & parmesan - 100x22g", "Baguettine");
        Product prod41290 = addProduct(managers, "41290", "Bag bouché Bruschetta - 12x130g", "Baguettine");
        Product prod41340 = addProduct(managers, "41340", "Bag. Tomate/feta - 12x135g", "Baguettine");
        Product prod41350 = addProduct(managers, "41350", "Bag. herbes du jardin - 12x135g", "Baguettine");
        Product prod41352 = addProduct(managers, "41352", "Bag. Tom. & basilic - 12x135g", "Baguettine");
        Product prod41354 = addProduct(managers, "41354", "Bag. sésame & ail - 12x135g", "Baguettine");
        Product prod41606 = addProduct(managers, "41606", "Bât. sésame - 12x160g", "Bâtonnets");
        Product prod42039 = addProduct(managers, "42039", "Canapé soya & sesame - 12x100g", "Canapé");
        Product prod42040 = addProduct(managers, "42040", "Melba réguliere - 12x200g", "Melba");
        Product prod42042 = addProduct(managers, "42042", "Melba légume - 12x200g", "Melba");
        Product prod42044 = addProduct(managers, "42044", "Melba blé - 12x200g", "Melba");
        Product prod42046 = addProduct(managers, "42046", "Melba sésame - 12x200g", "Melba");
        Product prod42048 = addProduct(managers, "42048", "Melba sans sel - 12x200g", "Melba");
        Product prod42049 = addProduct(managers, "42049", "Canapé multi-grain - 12x150g", "Canapé");
        Product prod42050 = addProduct(managers, "42050", "canapé germe blé - 12x150g", "Canapé");
        Product prod42051 = addProduct(managers, "42051", "canapé régulier - 12x150g", "Canapé");
        Product prod42052 = addProduct(managers, "42052", "canapé ail - 12x150g", "Canapé");
        Product prod42054 = addProduct(managers, "42054", "Canapé sésame - 12x150g", "Canapé");
        Product prod42056 = addProduct(managers, "42056", "Melba lunch pack - 12x200g", "Melba");
        Product prod42060 = addProduct(managers, "42060", "Melba régulière - 12x400g", "Melba");
        Product prod42066 = addProduct(managers, "42066", "Melba multi-grain - 12x400g", "Melba");
        Product prod42067 = addProduct(managers, "42067", "Melba seigle/sésame - 12x400g", "Melba");
        Product prod42068 = addProduct(managers, "42068", "Melba sans sel - 12x400g", "Melba");
        Product prod42081 = addProduct(managers, "42081", "Canapé bruschetta - 12x125g", "Canapé");
        Product prod42083 = addProduct(managers, "42083", "Canapé légume - 12x150g", "Canapé");
        Product prod42140 = addProduct(managers, "42140", "Melba blé - 12x400g", "Melba");
        Product prod42142 = addProduct(managers, "42142", "Melba Loblaws - 6x1kg", "Melba");
        Product prod42570 = addProduct(managers, "42570", "H.O. nature 2.27Kg - 2.27", "H.O.");
        Product prod44900 = addProduct(managers, "44900", "F/B multigrain - 12x150g", "Flat bread");
        Product prod44910 = addProduct(managers, "44910", "F/B oignons - 12x150g", "Flat bread");
        Product prod44920 = addProduct(managers, "44920", "F/B tomate - 12x150g", "Flat bread");
        Product prod44930 = addProduct(managers, "44930", "F/B sésame/romarin - 12x150g", "Flat bread");
        Product prod45000 = addProduct(managers, "45000", "Bisc régulière - 12x250g", "Biscotte");
        Product prod45010 = addProduct(managers, "45010", "Bisc sans sel - 12x250g", "Biscotte");
        Product prod45020 = addProduct(managers, "45020", "Bisc blé entier - 12x250g", "Biscotte");
        Product prod45030 = addProduct(managers, "45030", "Bisc musli - 12x250g", "Biscotte");
        Product prod45050 = addProduct(managers, "45050", "HO nature - 12x125g", "H.O.");
        Product prod45052 = addProduct(managers, "45052", "Baguet. Croust. 3 fromages - 12x135g", "Baguettine");
        Product prod45054 = addProduct(managers, "45054", "Baguet. Croust. Romarin/olive - 12x135g", "Baguettine");
        Product prod45100 = addProduct(managers, "45100", "Chapelure rég. - 12x500g", "Chapelure");
        Product prod45111 = addProduct(managers, "45111", "Cr césar - 12x150g", "Croûton");
        Product prod45130 = addProduct(managers, "45130", "Cr césar boni - 12x175g", "Croûton");
        Product prod45200 = addProduct(managers, "45200", "Chapelure rég. - 12x250g", "Chapelure");
        Product prod45210 = addProduct(managers, "45210", "Chapelure ital. - 12x250g", "Chapelure");
        Product prod45250 = addProduct(managers, "45250", "Farce - 12x185g", "Farce");
        Product prod45575 = addProduct(managers, "45575", "Bisc lunchpack - 12x200g", "Biscotte");
        Product prod45600 = addProduct(managers, "45600", "3 pains régulier - 12x150g", "Croûton");
        Product prod45602 = addProduct(managers, "45602", "3 pains césar - 12x150g", "Croûton");
        Product prod45615 = addProduct(managers, "45615", "3 pains ail - 12x150g", "Croûton");
        Product prod45618 = addProduct(managers, "45618", "Cr.cesar inter - 12x150g", "Croûton");
        Product prod45620 = addProduct(managers, "45620", "Cr. nature Intern. - 12x150g", "Croûton");
        Product prod45622 = addProduct(managers, "45622", "Cr. ail Intern. - 12x150g", "Croûton");
        Product prod45720 = addProduct(managers, "45720", "Melba réguliere Espagne - 12x200g", "Melba");
        Product prod45730 = addProduct(managers, "45730", "Melba sans sel Espagne - 12x200g", "Melba");
        Product prod45740 = addProduct(managers, "45740", "Melba sésame Espagne - 12x200g", "Melba");
        Product prod45900 = addProduct(managers, "45900", "Cr. nature - 12x150g", "Croûton");
        Product prod45950 = addProduct(managers, "45950", "Cr. ail - 12x150g", "Croûton");
        Product prod45952 = addProduct(managers, "45952", "Cr. Ail boni - 12x175g", "Croûton");
        Product prod45992 = addProduct(managers, "45992", "Cr. Old London ail - 12x195g", "Croûton");
        Product prod45993 = addProduct(managers, "45993", "Cr. Old London césar - 12x195g", "Croûton");
        Product prod45994 = addProduct(managers, "45994", "Cr. Old London nature - 12x195g", "Croûton");
        Product prod50400 = addProduct(managers, "50400", "Cr. Gourmet ail prime - 5kg", "Croûton Vrac");
        Product prod50402 = addProduct(managers, "50402", "Cr. LeMarquis ail - 5kg", "Croûton Vrac");
        Product prod50404 = addProduct(managers, "50404", "CR LeMarquis césar ital - 5kg", "Croûton Vrac");
        Product prod50420 = addProduct(managers, "50420", "Cr. Trois pains césar - 4.5kg", "Croûton Vrac");
        Product prod50430 = addProduct(managers, "50430", "Cr. Gourmet ail sans sel - 5kg", "Croûton Vrac");
        Product prod92419 = addProduct(managers, "92419", "Pain bât. Rama. rég.", "Pain bâton");
        Product prod92421 = addProduct(managers, "92421", "Pain bât. Rama. sés.", "Pain bâton");
        Product prod92431 = addProduct(managers, "92431", "Séchage", "Cuber");
        Product prod93020 = addProduct(managers, "93020", "Pain mini bouchée", "Baguette");
        Product prodRecup = addProduct(managers, "RECUP", "Récupérateur", "Embalalge");
        Product prodBoul = addProduct(managers, "BOUL", "fabrication", "Pains");
        Product prodBoulbag = addProduct(managers, "BOULBAG", "fabrication", "Pains baguet");
        Product prodBoulmusli = addProduct(managers, "BOULMUSLI", "fabrication", "Pains musli");
        Product prodPs = addProduct(managers, "PS", "Préposé Salubrité", "");
        Product prodGs = addProduct(managers, "GS", "Général Salubrité", "");
        Product prodEpices = addProduct(managers, "EPICES", "Préposé aux épices", "");
        Product prodGerbeuse = addProduct(managers, "GERBEUSE", "Opérateur gerbeuse", "");
        Product prodMec = addProduct(managers, "MEC", "Mécaniciens", "");
        Product prodEl = addProduct(managers, "EL", "Electro", "");
        Product prodHuileur = addProduct(managers, "HUILEUR", "Huileur", "");
        Product prodRp = addProduct(managers, "RP", "Remplacent(e)", "");
        Product prodSalubrite = addProduct(managers, "SALUBRITE", "Salubrité Nuit", "");
        Product prodFormation = addProduct(managers, "", "Formation", "");

        /*
         * Create product-position
         */
        // SELECT 'addProductPosition(managers, ' || camel('prod ' ||
        // CASEWHEN(PRODUCT.REFID IS NOT NULL, PRODUCT.REFID, PRODUCT.NAME))
        // ||', ' || camel(POSITION.NAME)|| ', ' || PRODUCTPOSITION.NUMBER ||
        // ');' FROM PRODUCTPOSITION, POSITION, PRODUCT WHERE
        // PRODUCTPOSITION.PRODUCT_ID = PRODUCT.ID AND
        // PRODUCTPOSITION.POSITION_ID = POSITION.ID:
        addProductPosition(managers, prod24610, generaleEmballageFmc, 1);
        addProductPosition(managers, prod41028, generaleEmballageFmc, 1);
        addProductPosition(managers, prod600, generaleEmballageFmc, 1);
        addProductPosition(managers, prodSalubrite, generaleEmballageFmc, 1);
        addProductPosition(managers, prod41200, tolier, 1);
        addProductPosition(managers, prod41205, tolier, 1);
        addProductPosition(managers, prod41210, tolier, 1);
        addProductPosition(managers, prod41606, tolier, 1);
        addProductPosition(managers, prod92419, tolier, 1);
        addProductPosition(managers, prod92421, tolier, 1);
        addProductPosition(managers, prod93020, tolier, 1);
        addProductPosition(managers, prod41200, generaleFourAideFournier, 1);
        addProductPosition(managers, prod41205, generaleFourAideFournier, 1);
        addProductPosition(managers, prod41210, generaleFourAideFournier, 1);
        addProductPosition(managers, prod41606, generaleFourAideFournier, 1);
        addProductPosition(managers, prod92419, generaleFourAideFournier, 1);
        addProductPosition(managers, prod92421, generaleFourAideFournier, 1);
        addProductPosition(managers, prod93020, generaleFourAideFournier, 1);
        addProductPosition(managers, prod41200, generaleEmballageBaton, 2);
        addProductPosition(managers, prod41205, generaleEmballageBaton, 2);
        addProductPosition(managers, prod41210, generaleEmballageBaton, 2);
        addProductPosition(managers, prod41606, generaleEmballageBaton, 2);
        addProductPosition(managers, prod92419, generaleEmballageBaton, 4);
        addProductPosition(managers, prod92421, generaleEmballageBaton, 4);
        addProductPosition(managers, prod93020, generaleEmballageBaton, 2);
        addProductPosition(managers, prodBoul, generaleDemouleuse, 2);
        addProductPosition(managers, prodBoulbag, generaleDemouleuse, 2);
        addProductPosition(managers, prodBoulmusli, generaleDemouleuse, 2);
        addProductPosition(managers, prod41280, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod41290, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod41340, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod41350, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod41352, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod41354, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod42570, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod45050, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod45052, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod45054, generaleTrancheuseBaguettine, 1);
        addProductPosition(managers, prod41280, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod41290, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod41340, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod41350, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod41352, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod41354, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod42570, generaleEmballageBaguettine, 3);
        addProductPosition(managers, prod45050, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod45052, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod45054, generaleEmballageBaguettine, 2);
        addProductPosition(managers, prod45000, generaleTrancheuseBiscotte, 2);
        addProductPosition(managers, prod45010, generaleTrancheuseBiscotte, 2);
        addProductPosition(managers, prod45020, generaleTrancheuseBiscotte, 2);
        addProductPosition(managers, prod45030, generaleTrancheuseBiscotte, 2);
        addProductPosition(managers, prod45575, generaleTrancheuseBiscotte, 2);
        addProductPosition(managers, prod41606, generaleEmballageBiscotte, 2);
        addProductPosition(managers, prod45000, generaleEmballageBiscotte, 6);
        addProductPosition(managers, prod45010, generaleEmballageBiscotte, 6);
        addProductPosition(managers, prod45020, generaleEmballageBiscotte, 6);
        addProductPosition(managers, prod45030, generaleEmballageBiscotte, 6);
        addProductPosition(managers, prod45575, generaleEmballageBiscotte, 9);
        addProductPosition(managers, prod56, equarisseur, 1);
        addProductPosition(managers, prod69, equarisseur, 1);
        addProductPosition(managers, prod650, equarisseur, 1);
        addProductPosition(managers, prod2162, equarisseur, 1);
        addProductPosition(managers, prod42039, equarisseur, 1);
        addProductPosition(managers, prod42040, equarisseur, 1);
        addProductPosition(managers, prod42042, equarisseur, 1);
        addProductPosition(managers, prod42044, equarisseur, 1);
        addProductPosition(managers, prod42046, equarisseur, 1);
        addProductPosition(managers, prod42048, equarisseur, 1);
        addProductPosition(managers, prod42049, equarisseur, 1);
        addProductPosition(managers, prod42050, equarisseur, 1);
        addProductPosition(managers, prod42051, equarisseur, 1);
        addProductPosition(managers, prod42052, equarisseur, 1);
        addProductPosition(managers, prod42054, equarisseur, 1);
        addProductPosition(managers, prod42056, equarisseur, 1);
        addProductPosition(managers, prod42060, equarisseur, 1);
        addProductPosition(managers, prod42066, equarisseur, 1);
        addProductPosition(managers, prod42067, equarisseur, 1);
        addProductPosition(managers, prod42068, equarisseur, 1);
        addProductPosition(managers, prod42081, equarisseur, 1);
        addProductPosition(managers, prod42083, equarisseur, 1);
        addProductPosition(managers, prod42140, equarisseur, 1);
        addProductPosition(managers, prod42142, equarisseur, 1);
        addProductPosition(managers, prod56, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod69, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod650, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod2162, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42039, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42040, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42042, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42044, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42046, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42048, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42049, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42050, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42051, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42052, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42054, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42056, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42060, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42066, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42067, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42068, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42081, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42083, generalTrancheuseMelba, 2);
        addProductPosition(managers, prod42140, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod42142, generalTrancheuseMelba, 1);
        addProductPosition(managers, prod56, generaleEmballageMelba, 3);
        addProductPosition(managers, prod69, generaleEmballageMelba, 3);
        addProductPosition(managers, prod650, generaleEmballageMelba, 3);
        addProductPosition(managers, prod2162, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42039, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42040, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42042, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42044, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42046, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42048, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42049, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42050, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42051, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42052, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42054, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42056, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42060, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42066, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42067, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42068, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42081, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42083, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42140, generaleEmballageMelba, 3);
        addProductPosition(managers, prod42142, generaleEmballageMelba, 3);
        addProductPosition(managers, prod45720, generaleEmballageMelba, 5);
        addProductPosition(managers, prod45730, generaleEmballageMelba, 5);
        addProductPosition(managers, prod45740, generaleEmballageMelba, 5);
        addProductPosition(managers, prodRp, generaleEmballageMelba, 1);
        addProductPosition(managers, prod44900, generaleEmballagePainsMinces, 7);
        addProductPosition(managers, prod44910, generaleEmballagePainsMinces, 7);
        addProductPosition(managers, prod44920, generaleEmballagePainsMinces, 7);
        addProductPosition(managers, prod44930, generaleEmballagePainsMinces, 7);
        addProductPosition(managers, prod660, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod663, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod670, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod24665, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod41281, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod50400, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod50402, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod50404, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod50420, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod50430, generaleEmballageCroutonsVrac, 2);
        addProductPosition(managers, prod92431, generaleEmballageCroutonsVrac, 1);
        addProductPosition(managers, prod45100, generalEmballageSnackBoite, 2);
        addProductPosition(managers, prod45111, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45130, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45200, generalEmballageSnackBoite, 2);
        addProductPosition(managers, prod45210, generalEmballageSnackBoite, 2);
        addProductPosition(managers, prod45250, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45600, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45602, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45615, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45618, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45620, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45622, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45900, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45950, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45952, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45992, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45993, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prod45994, generalEmballageSnackBoite, 3);
        addProductPosition(managers, prodGs, generaleSalubrite, 1);
        addProductPosition(managers, prodSalubrite, generaleSalubrite, 1);
        addProductPosition(managers, prodSalubrite, generaleSalubrite, 1);
        addProductPosition(managers, prodSalubrite, generaleSalubrite, 1);
        addProductPosition(managers, prodSalubrite, generaleSalubrite, 1);
        addProductPosition(managers, prodBoul, operateurLigneAPain, 1);
        addProductPosition(managers, prodBoulbag, operateurLigneAPain, 1);
        addProductPosition(managers, prodBoulmusli, operateurLigneAPain, 1);
        addProductPosition(managers, prodBoul, prefarinePremelange, 1);
        addProductPosition(managers, prodBoulmusli, prefarinePremelange, 2);
        addProductPosition(managers, prodBoul, petrisseurAPain, 1);
        addProductPosition(managers, prodBoulbag, petrisseurAPain, 1);
        addProductPosition(managers, prodBoulmusli, petrisseurAPain, 1);
        addProductPosition(managers, prodBoul, operateurRemplacementBoul, 1);
        addProductPosition(managers, prodBoulbag, operateurRemplacementBoul, 1);
        addProductPosition(managers, prodBoulmusli, operateurRemplacementBoul, 1);
        addProductPosition(managers, prod24610, operateurEmballageFmc, 1);
        addProductPosition(managers, prod41028, operateurEmballageFmc, 1);
        addProductPosition(managers, prod600, operateurEmballageFmc, 1);
        addProductPosition(managers, prod41200, operateurBatonCompteuse, 2);
        addProductPosition(managers, prod41205, operateurBatonCompteuse, 2);
        addProductPosition(managers, prod41210, operateurBatonCompteuse, 2);
        addProductPosition(managers, prod41280, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod41290, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod41340, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod41350, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod41352, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod41354, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod42570, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45000, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45010, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45020, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45030, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45050, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45052, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45054, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod45575, operateurLigneBiscottebagHorsDoeuvre, 1);
        addProductPosition(managers, prod41280, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod41290, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod41340, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod41350, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod41352, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod41354, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod41606, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42039, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42049, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42050, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42051, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42052, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42054, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42081, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod42083, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod45050, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod45052, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod45054, operateurEmballageTriangle, 1);
        addProductPosition(managers, prod56, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod69, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod650, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod2162, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42039, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42040, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42042, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42044, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42046, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42048, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42049, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42050, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42051, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42052, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42054, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42056, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42060, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42066, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42067, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42068, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42081, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42083, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42140, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod42142, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod45720, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod45730, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod45740, operateurLigneFourMelba, 1);
        addProductPosition(managers, prod56, operateurEmballageMelba, 1);
        addProductPosition(managers, prod69, operateurEmballageMelba, 1);
        addProductPosition(managers, prod650, operateurEmballageMelba, 1);
        addProductPosition(managers, prod2162, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42040, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42042, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42044, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42046, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42048, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42056, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42060, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42066, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42067, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42068, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42140, operateurEmballageMelba, 1);
        addProductPosition(managers, prod42142, operateurEmballageMelba, 1);
        addProductPosition(managers, prod45720, operateurEmballageMelba, 1);
        addProductPosition(managers, prod45730, operateurEmballageMelba, 1);
        addProductPosition(managers, prod45740, operateurEmballageMelba, 1);
        addProductPosition(managers, prod680, operateurEmballageChap25lb, 1);
        addProductPosition(managers, prodRecup, recuperateurEmballage, 1);
        addProductPosition(managers, prod660, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod663, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod670, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod24665, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod44900, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod44910, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod44920, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod44930, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45111, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45130, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45250, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45600, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45602, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45615, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45618, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45620, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45622, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45900, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45950, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45952, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45992, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45993, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod45994, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod50400, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod50402, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod50404, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod50420, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod50430, operateurRemplacementSnack, 1);
        addProductPosition(managers, prod41281, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod44900, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod44910, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod44920, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod44930, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45100, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45111, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45130, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45200, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45210, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45250, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45600, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45602, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45615, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45618, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45620, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45622, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45900, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45950, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45952, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45992, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45993, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod45994, operateurEnsacheuseVerticalSnack, 1);
        addProductPosition(managers, prod660, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod663, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod670, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod24665, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod44900, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod44910, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod44920, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod44930, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45111, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45130, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45250, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45600, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45602, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45615, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45618, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45620, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45622, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45900, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45950, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45952, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45992, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45993, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod45994, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod50400, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod50402, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod50404, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod50420, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod50430, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod92431, operateurPetrisseurSnack, 1);
        addProductPosition(managers, prod41200, operateurDeLigne, 1);
        addProductPosition(managers, prod41205, operateurDeLigne, 1);
        addProductPosition(managers, prod41210, operateurDeLigne, 1);
        addProductPosition(managers, prod41606, operateurDeLigne, 1);
        addProductPosition(managers, prod92419, operateurDeLigne, 1);
        addProductPosition(managers, prod92421, operateurDeLigne, 1);
        addProductPosition(managers, prod93020, operateurDeLigne, 1);
        addProductPosition(managers, prod41200, operateurRemplacementBaton, 1);
        addProductPosition(managers, prod41205, operateurRemplacementBaton, 1);
        addProductPosition(managers, prod41210, operateurRemplacementBaton, 1);
        addProductPosition(managers, prod41606, operateurRemplacementBaton, 1);
        addProductPosition(managers, prod92419, operateurRemplacementBaton, 1);
        addProductPosition(managers, prod92421, operateurRemplacementBaton, 1);
        addProductPosition(managers, prod93020, operateurRemplacementBaton, 1);
        addProductPosition(managers, prod41200, preposeAuMelange, 1);
        addProductPosition(managers, prod41205, preposeAuMelange, 1);
        addProductPosition(managers, prod41210, preposeAuMelange, 1);
        addProductPosition(managers, prod41606, preposeAuMelange, 2);
        addProductPosition(managers, prod92419, preposeAuMelange, 1);
        addProductPosition(managers, prod92421, preposeAuMelange, 1);
        addProductPosition(managers, prod93020, preposeAuMelange, 1);
        addProductPosition(managers, prod41200, operateurGrispac, 1);
        addProductPosition(managers, prod41205, operateurGrispac, 1);
        addProductPosition(managers, prod41210, operateurGrispac, 1);
        addProductPosition(managers, prod41606, operateurGrispac, 1);
        addProductPosition(managers, prod92419, operateurGrispac, 1);
        addProductPosition(managers, prod92421, operateurGrispac, 1);
        addProductPosition(managers, prod93020, operateurGrispac, 2);
        addProductPosition(managers, prod41200, fournier, 1);
        addProductPosition(managers, prod41205, fournier, 1);
        addProductPosition(managers, prod41210, fournier, 1);
        addProductPosition(managers, prod41606, fournier, 1);
        addProductPosition(managers, prod92419, fournier, 1);
        addProductPosition(managers, prod92421, fournier, 1);
        addProductPosition(managers, prod93020, fournier, 1);
        addProductPosition(managers, prodPs, preposeSalubrite, 1);
        addProductPosition(managers, prodSalubrite, preposeSalubrite, 1);
        addProductPosition(managers, prodSalubrite, preposeSalubrite, 1);
        addProductPosition(managers, prodGerbeuse, operateurGerbeuseVerification, 1);
        addProductPosition(managers, prodEpices, preposeAuxEpicesEtReparation, 1);
        addProductPosition(managers, prodEl, electrotechniciens, 1);
        addProductPosition(managers, prodMec, mecanicien, 1);
        addProductPosition(managers, prodHuileur, huileurGraisseurEntretienPreventif, 1);
        addProductPosition(managers, prodFormation, formation, 1);

        /*
         * Create Shifts
         */
        // SELECT 'Shift ' || camel(SHIFT.NAME) || ' = addShift(managers,
        // "' || SHIFT.NAME || '");' FROM SHIFT;
        Team joursDeSemaine = addTeam(managers, "Jours de semaine");
        Team jourRemplacementSemaine = addTeam(managers, "Jour remplacement semaine");
        Team soirSemaine = addTeam(managers, "Soir semaine");
        Team soirRemplacementSemaine = addTeam(managers, "Soir remplacement semaine");
        Team nuitSemaineSalubrite = addTeam(managers, "Nuit semaine - Salubrité");
        Team jourFinDeSemaine = addTeam(managers, "Jour fin de semaine");
        Team joursRemplacementFinDeSemaine = addTeam(managers, "Jours remplacement fin de semaine");
        Team soirFinDeSemaine = addTeam(managers, "Soir de fin de semaine");
        Team soirRemplacementFinDeSemaine = addTeam(managers, "Soir remplacement fin de semaine");

        /*
         * Create Shift event
         */
        // SELECT 'ShiftEvent ' || camel(SHIFT.NAME || ' ' ||
        // FORMATDATETIME(CALENDAREVENT.STARTDATE,'EEE')) || ' =
        // addShiftEvent(managers, ' || camel(SHIFT.NAME) || ',
        // dateTime("' || FORMATDATETIME(CALENDAREVENT.STARTDATE,'yyyy-MM-dd EEE HH:mm') || '"),
        // dateTime("' || FORMATDATETIME(CALENDAREVENT.ENDDATE,'yyyy-MM-dd EEE HH:mm') || '"));'
        // FROM SHIFT, CALENDARENTRY, CALENDAREVENT WHERE SHIFT.ID =
        // CALENDARENTRY.ID AND CALENDARENTRY.ID =
        // CALENDAREVENT.CALENDARENTRY_ID
        Shift joursDeSemaineMon = addShift(managers, joursDeSemaine, dateTime("2011-09-26 Mon 05:00"), dateTime("2011-09-26 Mon 15:00"));
        Shift joursDeSemaineTue = addShift(managers, joursDeSemaine, dateTime("2011-09-27 Tue 05:00"), dateTime("2011-09-27 Tue 15:00"));
        Shift joursDeSemaineWed = addShift(managers, joursDeSemaine, dateTime("2011-09-28 Wed 05:00"), dateTime("2011-09-28 Wed 15:00"));
        Shift joursDeSemaineThu = addShift(managers, joursDeSemaine, dateTime("2011-09-29 Thu 05:00"), dateTime("2011-09-29 Thu 15:00"));
        Shift jourRemplacementSemaineMon = addShift(managers, jourRemplacementSemaine, dateTime("2011-09-26 Mon 08:15"), dateTime("2011-09-26 Mon 14:45"));
        Shift jourRemplacementSemaineTue = addShift(managers, jourRemplacementSemaine, dateTime("2011-09-27 Tue 08:15"), dateTime("2011-09-27 Tue 14:45"));
        Shift jourRemplacementSemaineWed = addShift(managers, jourRemplacementSemaine, dateTime("2011-09-28 Wed 08:15"), dateTime("2011-09-28 Wed 14:45"));
        Shift jourRemplacementSemaineThu = addShift(managers, jourRemplacementSemaine, dateTime("2011-09-29 Thu 08:15"), dateTime("2011-09-29 Thu 14:45"));
        Shift soirSemaineMon = addShift(managers, soirSemaine, dateTime("2011-09-26 Mon 15:00"), dateTime("2011-09-27 Tue 01:00"));
        Shift soirSemaineTue = addShift(managers, soirSemaine, dateTime("2011-09-27 Tue 15:00"), dateTime("2011-09-28 Wed 01:00"));
        Shift soirSemaineWed = addShift(managers, soirSemaine, dateTime("2011-09-28 Wed 15:00"), dateTime("2011-09-29 Thu 01:00"));
        Shift soirSemaineThu = addShift(managers, soirSemaine, dateTime("2011-09-29 Thu 15:00"), dateTime("2011-09-30 Fri 01:00"));
        Shift soirRemplacementSemaineMon = addShift(managers, soirRemplacementSemaine, dateTime("2011-09-26 Mon 18:15"), dateTime("2011-09-27 Tue 00:45"));
        Shift soirRemplacementSemaineTue = addShift(managers, soirRemplacementSemaine, dateTime("2011-09-27 Tue 18:15"), dateTime("2011-09-28 Wed 00:45"));
        Shift soirRemplacementSemaineWed = addShift(managers, soirRemplacementSemaine, dateTime("2011-09-28 Wed 18:15"), dateTime("2011-09-29 Thu 00:45"));
        Shift soirRemplacementSemaineThu = addShift(managers, soirRemplacementSemaine, dateTime("2011-09-29 Thu 18:15"), dateTime("2011-09-30 Fri 00:45"));
        Shift nuitSemaineSalubriteMon = addShift(managers, nuitSemaineSalubrite, dateTime("2011-09-26 Mon 22:00"), dateTime("2011-09-27 Tue 06:00"));
        Shift nuitSemaineSalubriteTue = addShift(managers, nuitSemaineSalubrite, dateTime("2011-09-27 Tue 22:00"), dateTime("2011-09-28 Wed 06:00"));
        Shift nuitSemaineSalubriteWed = addShift(managers, nuitSemaineSalubrite, dateTime("2011-09-28 Wed 22:00"), dateTime("2011-09-29 Thu 06:00"));
        Shift nuitSemaineSalubriteThu = addShift(managers, nuitSemaineSalubrite, dateTime("2011-09-29 Thu 22:00"), dateTime("2011-09-30 Fri 06:00"));
        Shift nuitSemaineSalubriteFri = addShift(managers, nuitSemaineSalubrite, dateTime("2011-09-30 Fri 22:00"), dateTime("2011-10-01 Sat 06:00"));
        Shift jourFinDeSemaineSun = addShift(managers, jourFinDeSemaine, dateTime("2011-09-25 Sun 06:00"), dateTime("2011-09-25 Sun 18:00"));
        Shift jourFinDeSemaineFri = addShift(managers, jourFinDeSemaine, dateTime("2011-09-30 Fri 06:00"), dateTime("2011-09-30 Fri 18:00"));
        Shift jourFinDeSemaineSat = addShift(managers, jourFinDeSemaine, dateTime("2011-10-01 Sat 06:00"), dateTime("2011-10-01 Sat 18:00"));
        Shift joursRemplacementFinDeSemaineSun = addShift(
                managers,
                joursRemplacementFinDeSemaine,
                dateTime("2011-09-25 Sun 08:15"),
                dateTime("2011-09-25 Sun 16:15"));
        Shift joursRemplacementFinDeSemaineFri = addShift(
                managers,
                joursRemplacementFinDeSemaine,
                dateTime("2011-09-30 Fri 08:15"),
                dateTime("2011-09-30 Fri 16:15"));
        Shift joursRemplacementFinDeSemaineSat = addShift(
                managers,
                joursRemplacementFinDeSemaine,
                dateTime("2011-10-01 Sat 08:15"),
                dateTime("2011-10-01 Sat 16:15"));
        Shift soirDeFinDeSemaineSun = addShift(managers, soirFinDeSemaine, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-09-25 Sun 06:00"));
        // ShiftEvent soirDeFinDeSemaineSun = addShiftEvent(managers,
        // soirDeFinDeSemaine, dateTime("2011-09-25 Sun 18:00"),
        // dateTime("2011-09-26 Mon 06:00"));
        Shift soirDeFinDeSemaineFri = addShift(managers, soirFinDeSemaine, dateTime("2011-09-30 Fri 18:00"), dateTime("2011-10-01 Sat 06:00"));
        Shift soirDeFinDeSemaineSat = addShift(managers, soirFinDeSemaine, dateTime("2011-10-01 Sat 18:00"), dateTime("2011-10-01 Sat 23:59"));
        Shift soirRemplacementFinDeSemaineSun = addShift(
                managers,
                soirRemplacementFinDeSemaine,
                dateTime("2011-09-25 Sun 00:00"),
                dateTime("2011-09-25 Sun 03:45"));
        // ShiftEvent soirRemplacementFinDeSemaineSun = addShiftEvent(managers,
        // soirRemplacementFinDeSemaine, dateTime("2011-09-25 Sun 20:15"),
        // dateTime("2011-09-26 Mon 03:45"));
        Shift soirRemplacementFinDeSemaineFri = addShift(
                managers,
                soirRemplacementFinDeSemaine,
                dateTime("2011-09-30 Fri 20:15"),
                dateTime("2011-10-01 Sat 03:45"));
        Shift soirRemplacementFinDeSemaineSat = addShift(
                managers,
                soirRemplacementFinDeSemaine,
                dateTime("2011-10-01 Sat 20:15"),
                dateTime("2011-10-01 Sat 23:59"));

        /*
         * Create Employees
         */
        // SELECT 'Employee ' || camel(EMPLOYEE.FIRSTNAME || ' ' ||
        // EMPLOYEE.LASTNAME) || ' = addEmployee(managers,
        // "' || EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME || '",
        // dateTime("' || FORMATDATETIME(EMPLOYEE.HIREDATE,'yyyy-MM-dd EEE HH:mm') || '"));'
        // FROM EMPLOYEE
        donaldTheriault = addEmployee(managers, "Donald Theriault", dateTime("1970-08-26 Wed 00:00"));
        louiseVeillette = addEmployee(managers, "Louise Veillette", dateTime("1970-10-05 Mon 00:00"));
        caroleMorand = addEmployee(managers, "Carole Morand", dateTime("1971-06-01 Tue 00:00"));
        lucieGarceau = addEmployee(managers, "Lucie Garceau", dateTime("1972-08-28 Mon 00:00"));
        huguetteLebel = addEmployee(managers, "Huguette Lebel", dateTime("1972-09-26 Tue 00:00"));
        michelineDemers = addEmployee(managers, "Micheline Demers", dateTime("1973-02-05 Mon 00:00"));
        jeanpierreAuger = addEmployee(managers, "Jean-pierre Auger", dateTime("1973-06-19 Tue 00:00"));
        dianeDugas = addEmployee(managers, "Diane Dugas", dateTime("1973-06-19 Tue 00:00"));
        rejeanBrabant = addEmployee(managers, "Rejean Brabant", dateTime("1973-11-07 Wed 00:00"));
        moniqueLeblond = addEmployee(managers, "Monique Leblond", dateTime("1973-11-07 Wed 00:00"));
        realGosselin = addEmployee(managers, "Real Gosselin", dateTime("1973-11-30 Fri 00:00"));
        lucieLacoste = addEmployee(managers, "Lucie Lacoste", dateTime("1974-07-05 Fri 00:00"));
        johanneLemieux = addEmployee(managers, "Johanne Lemieux", dateTime("1974-08-15 Thu 00:00"));
        manonTremblay = addEmployee(managers, "Manon Tremblay", dateTime("1974-11-04 Mon 00:00"));
        bernardBerube = addEmployee(managers, "Bernard Berube", dateTime("1975-09-17 Wed 00:00"));
        robertLazure = addEmployee(managers, "Robert Lazure", dateTime("1976-02-11 Wed 00:00"));
        lindaBoisvert = addEmployee(managers, "Linda Boisvert", dateTime("1976-02-23 Mon 00:00"));
        sergeRobidoux = addEmployee(managers, "Serge Robidoux", dateTime("1976-02-24 Tue 00:00"));
        michelDaniel = addEmployee(managers, "Michel Daniel", dateTime("1976-04-16 Fri 00:00"));
        caroleRaymond = addEmployee(managers, "Carole Raymond", dateTime("1976-04-28 Wed 00:00"));
        gillesGosselin = addEmployee(managers, "Gilles Gosselin", dateTime("1976-08-03 Tue 00:00"));
        francoisBeaulne = addEmployee(managers, "Francois Beaulne", dateTime("1977-02-14 Mon 00:00"));
        francineGuerin = addEmployee(managers, "Francine Guerin", dateTime("1977-02-28 Mon 00:00"));
        jeanguyRicher = addEmployee(managers, "Jean-guy Richer", dateTime("1977-06-01 Wed 00:00"));
        marcelDalphond = addEmployee(managers, "Marcel Dalphond", dateTime("1977-07-07 Thu 00:00"));
        michelMeunier = addEmployee(managers, "Michel Meunier", dateTime("1978-02-16 Thu 00:00"));
        pierreLamarque = addEmployee(managers, "Pierre Lamarque", dateTime("1979-05-29 Tue 00:00"));
        gerardLanteigne = addEmployee(managers, "Gerard Lanteigne", dateTime("1979-08-07 Tue 00:00"));
        francineDurivage = addEmployee(managers, "Francine Durivage", dateTime("1979-09-11 Tue 00:00"));
        jeanLatour = addEmployee(managers, "Jean Latour", dateTime("1979-09-13 Thu 00:00"));
        pierretteDupras = addEmployee(managers, "Pierrette Dupras", dateTime("1980-09-04 Thu 00:00"));
        danielNault = addEmployee(managers, "Daniel Nault", dateTime("1980-09-23 Tue 00:00"));
        raynaldStarnaud = addEmployee(managers, "Raynald St-Arnaud", dateTime("1980-11-26 Wed 00:00"));
        nicoleFortin = addEmployee(managers, "Nicole Fortin", dateTime("1981-01-23 Fri 00:00"));
        normandArsenault = addEmployee(managers, "Normand Arsenault", dateTime("1981-07-13 Mon 00:00"));
        liseHebert = addEmployee(managers, "Lise Hebert", dateTime("1981-07-13 Mon 00:00"));
        deniseDaigneault = addEmployee(managers, "Denise Daigneault", dateTime("1981-07-13 Mon 00:00"));
        francineLabbe = addEmployee(managers, "Francine Labbe", dateTime("1981-07-30 Thu 00:00"));
        claudineRochefort = addEmployee(managers, "Claudine Rochefort", dateTime("1981-08-05 Wed 00:00"));
        suzanneCouturier = addEmployee(managers, "Suzanne Couturier", dateTime("1981-08-07 Fri 00:00"));
        michelTougas = addEmployee(managers, "Michel Tougas", dateTime("1981-08-18 Tue 00:00"));
        danielleBeaudry = addEmployee(managers, "Danielle Beaudry", dateTime("1981-08-19 Wed 00:00"));
        lucieGuay = addEmployee(managers, "Lucie Guay", dateTime("1981-08-27 Thu 00:00"));
        rogerDagenais = addEmployee(managers, "Roger Dagenais", dateTime("1981-09-28 Mon 00:00"));
        michelJeanneau = addEmployee(managers, "Michel Jeanneau", dateTime("1981-10-08 Thu 00:00"));
        denisPilon = addEmployee(managers, "Denis Pilon", dateTime("1981-10-13 Tue 00:00"));
        suzanneGagnon = addEmployee(managers, "Suzanne Gagnon", dateTime("1981-10-13 Tue 00:00"));
        rachelBergevin = addEmployee(managers, "Rachel Bergevin", dateTime("1981-10-13 Tue 00:00"));
        rejeanRoy = addEmployee(managers, "Rejean Roy", dateTime("1985-02-11 Mon 00:00"));
        huguesDenault = addEmployee(managers, "Hugues Denault", dateTime("1985-02-12 Tue 00:00"));
        rolandJrBoucher = addEmployee(managers, "Roland jr. Boucher", dateTime("1985-04-30 Tue 00:00"));
        bernardJolin = addEmployee(managers, "Bernard Jolin", dateTime("1985-09-03 Tue 00:00"));
        sartoTremblay = addEmployee(managers, "Sarto Tremblay", dateTime("1988-05-16 Mon 00:00"));
        dianeGirard = addEmployee(managers, "Diane Girard", dateTime("1988-05-20 Fri 00:00"));
        marioPaille = addEmployee(managers, "Mario Paille", dateTime("1988-05-20 Fri 00:00"));
        ginetteOuellette = addEmployee(managers, "Ginette Ouellette", dateTime("1988-05-20 Fri 00:00"));
        guylaineGuy = addEmployee(managers, "Guylaine Guy", dateTime("1989-05-05 Fri 00:00"));
        pierretteLamothe = addEmployee(managers, "Pierrette Lamothe", dateTime("1989-05-19 Fri 00:00"));
        marcBellemare = addEmployee(managers, "Marc Bellemare", dateTime("1989-11-06 Mon 00:00"));
        michelineLegault = addEmployee(managers, "Micheline Legault", dateTime("1990-06-14 Thu 00:00"));
        joseeConstantineau = addEmployee(managers, "Josee Constantineau", dateTime("1993-07-06 Tue 00:00"));
        madelaineMarleau = addEmployee(managers, "Madelaine  Marleau,", dateTime("1995-02-21 Tue 00:00"));
        manonPoissant = addEmployee(managers, "Manon Poissant,", dateTime("1995-02-22 Wed 00:00"));
        francineLemieux = addEmployee(managers, "Francine Lemieux,", dateTime("1995-06-27 Tue 00:00"));
        carmenBrais = addEmployee(managers, "Carmen Brais", dateTime("1995-09-05 Tue 00:00"));
        francoiseTrudeau = addEmployee(managers, "Francoise Trudeau", dateTime("1995-09-05 Tue 00:00"));
        ericRichard = addEmployee(managers, "Eric Richard", dateTime("1995-09-12 Tue 00:00"));
        nancyTheoret = addEmployee(managers, "Nancy Theoret", dateTime("1995-12-04 Mon 00:00"));
        liseCampeau = addEmployee(managers, "Lise Campeau", dateTime("1997-11-10 Mon 00:00"));
        lucieLeavey = addEmployee(managers, "Lucie Leavey", dateTime("1997-11-19 Wed 00:00"));
        lyndaLajoie = addEmployee(managers, "Lynda Lajoie", dateTime("1997-11-20 Thu 00:00"));
        jeanfrancoisBreton = addEmployee(managers, "Jean-francois Breton", dateTime("1998-01-26 Mon 00:00"));
        stephaneJacques = addEmployee(managers, "Stephane Jacques", dateTime("1998-01-27 Tue 00:00"));
        brigitteBouchard = addEmployee(managers, "Brigitte Bouchard", dateTime("1998-01-27 Tue 00:00"));
        martinDube = addEmployee(managers, "Martin Dube", dateTime("1998-03-17 Tue 00:00"));
        sylviePineault = addEmployee(managers, "Sylvie Pineault", dateTime("1998-09-17 Thu 00:00"));
        joseeLapierre = addEmployee(managers, "Josée Lapierre", dateTime("1998-10-07 Wed 00:00"));
        hachezGabriel = addEmployee(managers, "Hachez Gabriel", dateTime("1998-10-08 Thu 00:00"));
        sandraDupuis = addEmployee(managers, "Sandra Dupuis", dateTime("1998-12-08 Tue 00:00"));
        lucRoy = addEmployee(managers, "Luc Roy", dateTime("1999-02-10 Wed 00:00"));
        lucieCaron = addEmployee(managers, "Lucie Caron", dateTime("1999-08-14 Sat 00:00"));
        rachelMoise = addEmployee(managers, "Rachel Moise", dateTime("2000-08-22 Tue 00:00"));
        catherinePiette = addEmployee(managers, " catherine Piette", dateTime("2001-04-24 Tue 00:00"));
        chantalXavier = addEmployee(managers, " chantal Xavier", dateTime("2001-05-26 Sat 00:00"));
        dominicHoude = addEmployee(managers, "Dominic Houde", dateTime("2001-08-21 Tue 00:00"));
        francoisParent = addEmployee(managers, "Francois Parent", dateTime("2001-08-26 Sun 00:00"));
        solangeGirard = addEmployee(managers, "Solange Girard", dateTime("2001-09-01 Sat 00:00"));
        martinLina = addEmployee(managers, " martin Lina", dateTime("2002-10-21 Mon 00:00"));
        liseJoncas = addEmployee(managers, " lise Joncas", dateTime("2003-01-21 Tue 00:00"));
        nathalieReid = addEmployee(managers, "Nathalie Reid", dateTime("2004-03-14 Sun 00:00"));
        cecileCouillard = addEmployee(managers, "Cecile Couillard", dateTime("2004-10-15 Fri 00:00"));
        sylvainJulien = addEmployee(managers, "Sylvain Julien", dateTime("2004-10-18 Mon 00:00"));
        sylvainCarriere = addEmployee(managers, "Sylvain  Carriere", dateTime("2005-05-03 Tue 00:00"));
        richardVaillant = addEmployee(managers, "Richard Vaillant", dateTime("2005-06-22 Wed 00:00"));
        franceBoyer = addEmployee(managers, "France  Boyer", dateTime("2005-08-22 Mon 00:00"));
        fxCotebrunet = addEmployee(managers, "F.x. Cote-brunet,", dateTime("2005-11-28 Mon 00:00"));
        isabelleLeclerc = addEmployee(managers, "Isabelle Leclerc", dateTime("2006-09-05 Tue 00:00"));
        francoisArcoite = addEmployee(managers, "Francois Arcoite", dateTime("2006-09-08 Fri 00:00"));
        sabrinaDupuis = addEmployee(managers, "Sabrina Dupuis", dateTime("2006-11-27 Mon 00:00"));
        ivanhoeMaisonneuve = addEmployee(managers, "Ivanhoe Maisonneuve", dateTime("2007-02-19 Mon 00:00"));
        mathieuGuy = addEmployee(managers, "Mathieu Guy", dateTime("2007-04-20 Fri 00:00"));
        denisBerube = addEmployee(managers, "Denis Berube", dateTime("2007-06-11 Mon 00:00"));
        daisyBourget = addEmployee(managers, "Daisy Bourget", dateTime("2007-09-18 Tue 00:00"));
        mathewBellemare = addEmployee(managers, "Mathew Bellemare", dateTime("2007-09-23 Sun 00:00"));
        alexandreDube = addEmployee(managers, "Alexandre Dube", dateTime("2008-05-29 Thu 00:00"));
        annickPigeon = addEmployee(managers, "Annick Pigeon", dateTime("2008-09-02 Tue 00:00"));
        danielDuquette = addEmployee(managers, "Daniel Duquette", dateTime("2008-10-17 Fri 00:00"));
        johanneDuval = addEmployee(managers, "Johanne Duval", dateTime("2011-07-29 Fri 08:30"));
        philippeLegault = addEmployee(managers, "Philippe Legault", dateTime("2009-09-08 Tue 00:00"));
        celineVadnais = addEmployee(managers, "Celine Vadnais", dateTime("2009-09-08 Tue 00:00"));
        marcGrondin = addEmployee(managers, "Marc Grondin", dateTime("2009-09-14 Mon 00:00"));
        marcelLegault = addEmployee(managers, "Marcel  Legault", dateTime("2011-03-10 Thu 00:00"));
        nicolasLegault = addEmployee(managers, "Nicolas Legault", dateTime("2011-03-10 Thu 00:00"));
        ginoLemoine = addEmployee(managers, "Gino Lemoine", dateTime("2011-04-04 Mon 00:00"));
        ginetteLevesque = addEmployee(managers, "Ginette Levesque", dateTime("2011-03-09 Wed 00:00"));
        stefanieReynolds = addEmployee(managers, "Stefanie Reynolds", dateTime("2011-03-15 Tue 00:00"));
        marioLongtin = addEmployee(managers, "Mario Longtin", dateTime("2011-07-29 Fri 08:25"));
        robertAllen = addEmployee(managers, "Robert Allen", dateTime("2011-07-29 Fri 16:39"));
        denisDulude = addEmployee(managers, "Denis Dulude", dateTime("1976-10-29 Fri 09:05"));

        // SELECT 'addQualification(managers, ' || camel(EMPLOYEE.FIRSTNAME ||
        // ' ' || EMPLOYEE.LASTNAME) || ', ' || (SELECT GROUP_CONCAT
        // (camel(POSITION.NAME)) FROM QUALIFICATION, POSITION WHERE
        // QUALIFICATION.EMPLOYEE_ID = EMPLOYEE.ID AND QUALIFICATION.POSITION_ID
        // = POSITION.ID ) || ');' FROM EMPLOYEE;
        addQualification(managers, denisDulude, generaleDemouleuse);
        addQualification(managers, francineGuerin, operateurDeLigne);
        addQualification(managers, michelMeunier, operateurDeLigne);
        addQualification(managers, francineLabbe, operateurDeLigne);
        addQualification(managers, suzanneGagnon, operateurDeLigne);
        addQualification(managers, francineLemieux, operateurDeLigne);
        addQualification(managers, liseJoncas, operateurDeLigne);
        addQualification(managers, jeanpierreAuger, operateurRemplacementBaton);
        addQualification(managers, rejeanBrabant, operateurRemplacementBaton);
        addQualification(managers, marcelDalphond, operateurRemplacementBaton);
        addQualification(managers, michelMeunier, operateurRemplacementBaton);
        addQualification(managers, jeanLatour, operateurRemplacementBaton);
        addQualification(managers, rogerDagenais, operateurRemplacementBaton);
        addQualification(managers, michelJeanneau, operateurRemplacementBaton);
        addQualification(managers, marioPaille, operateurRemplacementBaton);
        addQualification(managers, carmenBrais, operateurRemplacementBaton);
        addQualification(managers, liseJoncas, operateurRemplacementBaton);
        addQualification(managers, francoisArcoite, operateurRemplacementBaton);
        addQualification(managers, danielDuquette, operateurRemplacementBaton);
        addQualification(managers, nicolasLegault, operateurRemplacementBaton);
        addQualification(managers, jeanpierreAuger, preposeAuMelange);
        addQualification(managers, marcelDalphond, preposeAuMelange);
        addQualification(managers, michelMeunier, preposeAuMelange);
        addQualification(managers, jeanLatour, preposeAuMelange);
        addQualification(managers, danielNault, preposeAuMelange);
        addQualification(managers, normandArsenault, preposeAuMelange);
        addQualification(managers, rogerDagenais, preposeAuMelange);
        addQualification(managers, michelJeanneau, preposeAuMelange);
        addQualification(managers, marioPaille, preposeAuMelange);
        addQualification(managers, carmenBrais, preposeAuMelange);
        addQualification(managers, ericRichard, preposeAuMelange);
        addQualification(managers, liseJoncas, preposeAuMelange);
        addQualification(managers, francoisArcoite, preposeAuMelange);
        addQualification(managers, danielDuquette, preposeAuMelange);
        addQualification(managers, jeanpierreAuger, operateurGrispac);
        addQualification(managers, francoisBeaulne, operateurGrispac);
        addQualification(managers, marcelDalphond, operateurGrispac);
        addQualification(managers, michelMeunier, operateurGrispac);
        addQualification(managers, jeanLatour, operateurGrispac);
        addQualification(managers, rogerDagenais, operateurGrispac);
        addQualification(managers, michelJeanneau, operateurGrispac);
        addQualification(managers, marioPaille, operateurGrispac);
        addQualification(managers, carmenBrais, operateurGrispac);
        addQualification(managers, liseJoncas, operateurGrispac);
        addQualification(managers, francoisArcoite, operateurGrispac);
        addQualification(managers, danielDuquette, operateurGrispac);
        addQualification(managers, nicolasLegault, operateurGrispac);
        addQualification(managers, michelineDemers, operateurEmballageFmc);
        addQualification(managers, francineGuerin, operateurEmballageFmc);
        addQualification(managers, michelMeunier, operateurEmballageFmc);
        addQualification(managers, francineDurivage, operateurEmballageFmc);
        addQualification(managers, liseHebert, operateurEmballageFmc);
        addQualification(managers, francineLabbe, operateurEmballageFmc);
        addQualification(managers, dianeGirard, operateurEmballageFmc);
        addQualification(managers, michelineLegault, operateurEmballageFmc);
        addQualification(managers, joseeConstantineau, operateurEmballageFmc);
        addQualification(managers, madelaineMarleau, operateurEmballageFmc);
        addQualification(managers, lucieLeavey, operateurEmballageFmc);
        addQualification(managers, sylviePineault, operateurEmballageFmc);
        addQualification(managers, caroleMorand, generaleEmballageFmc);
        addQualification(managers, lucieGarceau, generaleEmballageFmc);
        addQualification(managers, huguetteLebel, generaleEmballageFmc);
        addQualification(managers, michelineDemers, generaleEmballageFmc);
        addQualification(managers, dianeDugas, generaleEmballageFmc);
        addQualification(managers, moniqueLeblond, generaleEmballageFmc);
        addQualification(managers, realGosselin, generaleEmballageFmc);
        addQualification(managers, lucieLacoste, generaleEmballageFmc);
        addQualification(managers, johanneLemieux, generaleEmballageFmc);
        addQualification(managers, manonTremblay, generaleEmballageFmc);
        addQualification(managers, bernardBerube, generaleEmballageFmc);
        addQualification(managers, robertLazure, generaleEmballageFmc);
        addQualification(managers, lindaBoisvert, generaleEmballageFmc);
        addQualification(managers, sergeRobidoux, generaleEmballageFmc);
        addQualification(managers, michelDaniel, generaleEmballageFmc);
        addQualification(managers, caroleRaymond, generaleEmballageFmc);
        addQualification(managers, gillesGosselin, generaleEmballageFmc);
        addQualification(managers, francoisBeaulne, generaleEmballageFmc);
        addQualification(managers, francineGuerin, generaleEmballageFmc);
        addQualification(managers, jeanguyRicher, generaleEmballageFmc);
        addQualification(managers, marcelDalphond, generaleEmballageFmc);
        addQualification(managers, michelMeunier, generaleEmballageFmc);
        addQualification(managers, pierreLamarque, generaleEmballageFmc);
        addQualification(managers, gerardLanteigne, generaleEmballageFmc);
        addQualification(managers, francineDurivage, generaleEmballageFmc);
        addQualification(managers, jeanLatour, generaleEmballageFmc);
        addQualification(managers, pierretteDupras, generaleEmballageFmc);
        addQualification(managers, danielNault, generaleEmballageFmc);
        addQualification(managers, raynaldStarnaud, generaleEmballageFmc);
        addQualification(managers, nicoleFortin, generaleEmballageFmc);
        addQualification(managers, normandArsenault, generaleEmballageFmc);
        addQualification(managers, liseHebert, generaleEmballageFmc);
        addQualification(managers, deniseDaigneault, generaleEmballageFmc);
        addQualification(managers, francineLabbe, generaleEmballageFmc);
        addQualification(managers, claudineRochefort, generaleEmballageFmc);
        addQualification(managers, suzanneCouturier, generaleEmballageFmc);
        addQualification(managers, michelTougas, generaleEmballageFmc);
        addQualification(managers, danielleBeaudry, generaleEmballageFmc);
        addQualification(managers, lucieGuay, generaleEmballageFmc);
        addQualification(managers, rogerDagenais, generaleEmballageFmc);
        addQualification(managers, michelJeanneau, generaleEmballageFmc);
        addQualification(managers, denisPilon, generaleEmballageFmc);
        addQualification(managers, suzanneGagnon, generaleEmballageFmc);
        addQualification(managers, rachelBergevin, generaleEmballageFmc);
        addQualification(managers, rejeanRoy, generaleEmballageFmc);
        addQualification(managers, huguesDenault, generaleEmballageFmc);
        addQualification(managers, rolandJrBoucher, generaleEmballageFmc);
        addQualification(managers, bernardJolin, generaleEmballageFmc);
        addQualification(managers, sartoTremblay, generaleEmballageFmc);
        addQualification(managers, dianeGirard, generaleEmballageFmc);
        addQualification(managers, marioPaille, generaleEmballageFmc);
        addQualification(managers, ginetteOuellette, generaleEmballageFmc);
        addQualification(managers, guylaineGuy, generaleEmballageFmc);
        addQualification(managers, pierretteLamothe, generaleEmballageFmc);
        addQualification(managers, michelineLegault, generaleEmballageFmc);
        addQualification(managers, joseeConstantineau, generaleEmballageFmc);
        addQualification(managers, madelaineMarleau, generaleEmballageFmc);
        addQualification(managers, manonPoissant, generaleEmballageFmc);
        addQualification(managers, francineLemieux, generaleEmballageFmc);
        addQualification(managers, carmenBrais, generaleEmballageFmc);
        addQualification(managers, francoiseTrudeau, generaleEmballageFmc);
        addQualification(managers, ericRichard, generaleEmballageFmc);
        addQualification(managers, nancyTheoret, generaleEmballageFmc);
        addQualification(managers, liseCampeau, generaleEmballageFmc);
        addQualification(managers, lucieLeavey, generaleEmballageFmc);
        addQualification(managers, lyndaLajoie, generaleEmballageFmc);
        addQualification(managers, jeanfrancoisBreton, generaleEmballageFmc);
        addQualification(managers, stephaneJacques, generaleEmballageFmc);
        addQualification(managers, brigitteBouchard, generaleEmballageFmc);
        addQualification(managers, sylviePineault, generaleEmballageFmc);
        addQualification(managers, joseeLapierre, generaleEmballageFmc);
        addQualification(managers, hachezGabriel, generaleEmballageFmc);
        addQualification(managers, sandraDupuis, generaleEmballageFmc);
        addQualification(managers, lucieCaron, generaleEmballageFmc);
        addQualification(managers, rachelMoise, generaleEmballageFmc);
        addQualification(managers, catherinePiette, generaleEmballageFmc);
        addQualification(managers, chantalXavier, generaleEmballageFmc);
        addQualification(managers, dominicHoude, generaleEmballageFmc);
        addQualification(managers, francoisParent, generaleEmballageFmc);
        addQualification(managers, solangeGirard, generaleEmballageFmc);
        addQualification(managers, martinLina, generaleEmballageFmc);
        addQualification(managers, liseJoncas, generaleEmballageFmc);
        addQualification(managers, nathalieReid, generaleEmballageFmc);
        addQualification(managers, cecileCouillard, generaleEmballageFmc);
        addQualification(managers, richardVaillant, generaleEmballageFmc);
        addQualification(managers, franceBoyer, generaleEmballageFmc);
        addQualification(managers, fxCotebrunet, generaleEmballageFmc);
        addQualification(managers, isabelleLeclerc, generaleEmballageFmc);
        addQualification(managers, francoisArcoite, generaleEmballageFmc);
        addQualification(managers, sabrinaDupuis, generaleEmballageFmc);
        addQualification(managers, mathieuGuy, generaleEmballageFmc);
        addQualification(managers, daisyBourget, generaleEmballageFmc);
        addQualification(managers, alexandreDube, generaleEmballageFmc);
        addQualification(managers, annickPigeon, generaleEmballageFmc);
        addQualification(managers, danielDuquette, generaleEmballageFmc);
        addQualification(managers, johanneDuval, generaleEmballageFmc);
        addQualification(managers, philippeLegault, generaleEmballageFmc);
        addQualification(managers, celineVadnais, generaleEmballageFmc);
        addQualification(managers, marcGrondin, generaleEmballageFmc);
        addQualification(managers, ginetteLevesque, generaleEmballageFmc);
        addQualification(managers, marcelLegault, generaleEmballageFmc);
        addQualification(managers, nicolasLegault, generaleEmballageFmc);
        addQualification(managers, stefanieReynolds, generaleEmballageFmc);
        addQualification(managers, marioLongtin, generaleEmballageFmc);
        addQualification(managers, robertAllen, generaleEmballageFmc);
        addQualification(managers, michelineDemers, operateurBatonCompteuse);
        addQualification(managers, francineGuerin, operateurBatonCompteuse);
        addQualification(managers, michelMeunier, operateurBatonCompteuse);
        addQualification(managers, francineLabbe, operateurBatonCompteuse);
        addQualification(managers, madelaineMarleau, operateurBatonCompteuse);
        addQualification(managers, francineLemieux, operateurBatonCompteuse);
        addQualification(managers, nancyTheoret, operateurBatonCompteuse);
        addQualification(managers, rachelMoise, operateurBatonCompteuse);
        addQualification(managers, jeanpierreAuger, fournier);
        addQualification(managers, rejeanBrabant, fournier);
        addQualification(managers, bernardBerube, fournier);
        addQualification(managers, marcelDalphond, fournier);
        addQualification(managers, michelMeunier, fournier);
        addQualification(managers, pierreLamarque, fournier);
        addQualification(managers, jeanLatour, fournier);
        addQualification(managers, raynaldStarnaud, fournier);
        addQualification(managers, rogerDagenais, fournier);
        addQualification(managers, michelJeanneau, fournier);
        addQualification(managers, marioPaille, fournier);
        addQualification(managers, stephaneJacques, fournier);
        addQualification(managers, alexandreDube, fournier);
        addQualification(managers, marcelLegault, fournier);
        addQualification(managers, nicolasLegault, fournier);
        addQualification(managers, jeanpierreAuger, tolier);
        addQualification(managers, rejeanBrabant, tolier);
        addQualification(managers, realGosselin, tolier);
        addQualification(managers, bernardBerube, tolier);
        addQualification(managers, robertLazure, tolier);
        addQualification(managers, sergeRobidoux, tolier);
        addQualification(managers, michelDaniel, tolier);
        addQualification(managers, gillesGosselin, tolier);
        addQualification(managers, francoisBeaulne, tolier);
        addQualification(managers, jeanguyRicher, tolier);
        addQualification(managers, marcelDalphond, tolier);
        addQualification(managers, michelMeunier, tolier);
        addQualification(managers, pierreLamarque, tolier);
        addQualification(managers, gerardLanteigne, tolier);
        addQualification(managers, jeanLatour, tolier);
        addQualification(managers, danielNault, tolier);
        addQualification(managers, normandArsenault, tolier);
        addQualification(managers, michelTougas, tolier);
        addQualification(managers, rogerDagenais, tolier);
        addQualification(managers, michelJeanneau, tolier);
        addQualification(managers, denisPilon, tolier);
        addQualification(managers, rejeanRoy, tolier);
        addQualification(managers, huguesDenault, tolier);
        addQualification(managers, rolandJrBoucher, tolier);
        addQualification(managers, bernardJolin, tolier);
        addQualification(managers, sartoTremblay, tolier);
        addQualification(managers, marioPaille, tolier);
        addQualification(managers, carmenBrais, tolier);
        addQualification(managers, ericRichard, tolier);
        addQualification(managers, jeanfrancoisBreton, tolier);
        addQualification(managers, stephaneJacques, tolier);
        addQualification(managers, martinDube, tolier);
        addQualification(managers, dominicHoude, tolier);
        addQualification(managers, francoisParent, tolier);
        addQualification(managers, martinLina, tolier);
        addQualification(managers, liseJoncas, tolier);
        addQualification(managers, richardVaillant, tolier);
        addQualification(managers, fxCotebrunet, tolier);
        addQualification(managers, francoisArcoite, tolier);
        addQualification(managers, mathieuGuy, tolier);
        addQualification(managers, alexandreDube, tolier);
        addQualification(managers, danielDuquette, tolier);
        addQualification(managers, philippeLegault, tolier);
        addQualification(managers, marcGrondin, tolier);
        addQualification(managers, marcelLegault, tolier);
        addQualification(managers, nicolasLegault, tolier);
        addQualification(managers, marioLongtin, tolier);
        addQualification(managers, robertAllen, tolier);
        addQualification(managers, jeanpierreAuger, generaleFourAideFournier);
        addQualification(managers, rejeanBrabant, generaleFourAideFournier);
        addQualification(managers, realGosselin, generaleFourAideFournier);
        addQualification(managers, bernardBerube, generaleFourAideFournier);
        addQualification(managers, robertLazure, generaleFourAideFournier);
        addQualification(managers, sergeRobidoux, generaleFourAideFournier);
        addQualification(managers, michelDaniel, generaleFourAideFournier);
        addQualification(managers, gillesGosselin, generaleFourAideFournier);
        addQualification(managers, francoisBeaulne, generaleFourAideFournier);
        addQualification(managers, jeanguyRicher, generaleFourAideFournier);
        addQualification(managers, marcelDalphond, generaleFourAideFournier);
        addQualification(managers, michelMeunier, generaleFourAideFournier);
        addQualification(managers, pierreLamarque, generaleFourAideFournier);
        addQualification(managers, gerardLanteigne, generaleFourAideFournier);
        addQualification(managers, jeanLatour, generaleFourAideFournier);
        addQualification(managers, danielNault, generaleFourAideFournier);
        addQualification(managers, normandArsenault, generaleFourAideFournier);
        addQualification(managers, michelTougas, generaleFourAideFournier);
        addQualification(managers, rogerDagenais, generaleFourAideFournier);
        addQualification(managers, michelJeanneau, generaleFourAideFournier);
        addQualification(managers, denisPilon, generaleFourAideFournier);
        addQualification(managers, rejeanRoy, generaleFourAideFournier);
        addQualification(managers, huguesDenault, generaleFourAideFournier);
        addQualification(managers, rolandJrBoucher, generaleFourAideFournier);
        addQualification(managers, bernardJolin, generaleFourAideFournier);
        addQualification(managers, sartoTremblay, generaleFourAideFournier);
        addQualification(managers, marioPaille, generaleFourAideFournier);
        addQualification(managers, carmenBrais, generaleFourAideFournier);
        addQualification(managers, ericRichard, generaleFourAideFournier);
        addQualification(managers, jeanfrancoisBreton, generaleFourAideFournier);
        addQualification(managers, stephaneJacques, generaleFourAideFournier);
        addQualification(managers, martinDube, generaleFourAideFournier);
        addQualification(managers, dominicHoude, generaleFourAideFournier);
        addQualification(managers, francoisParent, generaleFourAideFournier);
        addQualification(managers, martinLina, generaleFourAideFournier);
        addQualification(managers, liseJoncas, generaleFourAideFournier);
        addQualification(managers, richardVaillant, generaleFourAideFournier);
        addQualification(managers, fxCotebrunet, generaleFourAideFournier);
        addQualification(managers, francoisArcoite, generaleFourAideFournier);
        addQualification(managers, mathieuGuy, generaleFourAideFournier);
        addQualification(managers, alexandreDube, generaleFourAideFournier);
        addQualification(managers, danielDuquette, generaleFourAideFournier);
        addQualification(managers, philippeLegault, generaleFourAideFournier);
        addQualification(managers, marcGrondin, generaleFourAideFournier);
        addQualification(managers, marcelLegault, generaleFourAideFournier);
        addQualification(managers, nicolasLegault, generaleFourAideFournier);
        addQualification(managers, marioLongtin, generaleFourAideFournier);
        addQualification(managers, robertAllen, generaleFourAideFournier);
        addQualification(managers, louiseVeillette, generaleEmballageBaton);
        addQualification(managers, caroleMorand, generaleEmballageBaton);
        addQualification(managers, lucieGarceau, generaleEmballageBaton);
        addQualification(managers, huguetteLebel, generaleEmballageBaton);
        addQualification(managers, michelineDemers, generaleEmballageBaton);
        addQualification(managers, jeanpierreAuger, generaleEmballageBaton);
        addQualification(managers, dianeDugas, generaleEmballageBaton);
        addQualification(managers, moniqueLeblond, generaleEmballageBaton);
        addQualification(managers, realGosselin, generaleEmballageBaton);
        addQualification(managers, lucieLacoste, generaleEmballageBaton);
        addQualification(managers, johanneLemieux, generaleEmballageBaton);
        addQualification(managers, manonTremblay, generaleEmballageBaton);
        addQualification(managers, bernardBerube, generaleEmballageBaton);
        addQualification(managers, robertLazure, generaleEmballageBaton);
        addQualification(managers, lindaBoisvert, generaleEmballageBaton);
        addQualification(managers, sergeRobidoux, generaleEmballageBaton);
        addQualification(managers, michelDaniel, generaleEmballageBaton);
        addQualification(managers, caroleRaymond, generaleEmballageBaton);
        addQualification(managers, gillesGosselin, generaleEmballageBaton);
        addQualification(managers, francoisBeaulne, generaleEmballageBaton);
        addQualification(managers, francineGuerin, generaleEmballageBaton);
        addQualification(managers, jeanguyRicher, generaleEmballageBaton);
        addQualification(managers, michelMeunier, generaleEmballageBaton);
        addQualification(managers, pierreLamarque, generaleEmballageBaton);
        addQualification(managers, gerardLanteigne, generaleEmballageBaton);
        addQualification(managers, francineDurivage, generaleEmballageBaton);
        addQualification(managers, jeanLatour, generaleEmballageBaton);
        addQualification(managers, pierretteDupras, generaleEmballageBaton);
        addQualification(managers, danielNault, generaleEmballageBaton);
        addQualification(managers, raynaldStarnaud, generaleEmballageBaton);
        addQualification(managers, nicoleFortin, generaleEmballageBaton);
        addQualification(managers, normandArsenault, generaleEmballageBaton);
        addQualification(managers, liseHebert, generaleEmballageBaton);
        addQualification(managers, deniseDaigneault, generaleEmballageBaton);
        addQualification(managers, francineLabbe, generaleEmballageBaton);
        addQualification(managers, claudineRochefort, generaleEmballageBaton);
        addQualification(managers, suzanneCouturier, generaleEmballageBaton);
        addQualification(managers, michelTougas, generaleEmballageBaton);
        addQualification(managers, danielleBeaudry, generaleEmballageBaton);
        addQualification(managers, lucieGuay, generaleEmballageBaton);
        addQualification(managers, rogerDagenais, generaleEmballageBaton);
        addQualification(managers, michelJeanneau, generaleEmballageBaton);
        addQualification(managers, denisPilon, generaleEmballageBaton);
        addQualification(managers, rachelBergevin, generaleEmballageBaton);
        addQualification(managers, rejeanRoy, generaleEmballageBaton);
        addQualification(managers, huguesDenault, generaleEmballageBaton);
        addQualification(managers, rolandJrBoucher, generaleEmballageBaton);
        addQualification(managers, bernardJolin, generaleEmballageBaton);
        addQualification(managers, sartoTremblay, generaleEmballageBaton);
        addQualification(managers, dianeGirard, generaleEmballageBaton);
        addQualification(managers, marioPaille, generaleEmballageBaton);
        addQualification(managers, ginetteOuellette, generaleEmballageBaton);
        addQualification(managers, guylaineGuy, generaleEmballageBaton);
        addQualification(managers, pierretteLamothe, generaleEmballageBaton);
        addQualification(managers, michelineLegault, generaleEmballageBaton);
        addQualification(managers, joseeConstantineau, generaleEmballageBaton);
        addQualification(managers, madelaineMarleau, generaleEmballageBaton);
        addQualification(managers, manonPoissant, generaleEmballageBaton);
        addQualification(managers, francineLemieux, generaleEmballageBaton);
        addQualification(managers, carmenBrais, generaleEmballageBaton);
        addQualification(managers, francoiseTrudeau, generaleEmballageBaton);
        addQualification(managers, ericRichard, generaleEmballageBaton);
        addQualification(managers, nancyTheoret, generaleEmballageBaton);
        addQualification(managers, liseCampeau, generaleEmballageBaton);
        addQualification(managers, lucieLeavey, generaleEmballageBaton);
        addQualification(managers, lyndaLajoie, generaleEmballageBaton);
        addQualification(managers, jeanfrancoisBreton, generaleEmballageBaton);
        addQualification(managers, stephaneJacques, generaleEmballageBaton);
        addQualification(managers, brigitteBouchard, generaleEmballageBaton);
        addQualification(managers, sylviePineault, generaleEmballageBaton);
        addQualification(managers, joseeLapierre, generaleEmballageBaton);
        addQualification(managers, hachezGabriel, generaleEmballageBaton);
        addQualification(managers, sandraDupuis, generaleEmballageBaton);
        addQualification(managers, lucieCaron, generaleEmballageBaton);
        addQualification(managers, rachelMoise, generaleEmballageBaton);
        addQualification(managers, catherinePiette, generaleEmballageBaton);
        addQualification(managers, chantalXavier, generaleEmballageBaton);
        addQualification(managers, dominicHoude, generaleEmballageBaton);
        addQualification(managers, francoisParent, generaleEmballageBaton);
        addQualification(managers, solangeGirard, generaleEmballageBaton);
        addQualification(managers, martinLina, generaleEmballageBaton);
        addQualification(managers, liseJoncas, generaleEmballageBaton);
        addQualification(managers, nathalieReid, generaleEmballageBaton);
        addQualification(managers, cecileCouillard, generaleEmballageBaton);
        addQualification(managers, richardVaillant, generaleEmballageBaton);
        addQualification(managers, franceBoyer, generaleEmballageBaton);
        addQualification(managers, fxCotebrunet, generaleEmballageBaton);
        addQualification(managers, isabelleLeclerc, generaleEmballageBaton);
        addQualification(managers, francoisArcoite, generaleEmballageBaton);
        addQualification(managers, sabrinaDupuis, generaleEmballageBaton);
        addQualification(managers, daisyBourget, generaleEmballageBaton);
        addQualification(managers, alexandreDube, generaleEmballageBaton);
        addQualification(managers, annickPigeon, generaleEmballageBaton);
        addQualification(managers, danielDuquette, generaleEmballageBaton);
        addQualification(managers, johanneDuval, generaleEmballageBaton);
        addQualification(managers, philippeLegault, generaleEmballageBaton);
        addQualification(managers, celineVadnais, generaleEmballageBaton);
        addQualification(managers, marcGrondin, generaleEmballageBaton);
        addQualification(managers, ginetteLevesque, generaleEmballageBaton);
        addQualification(managers, marcelLegault, generaleEmballageBaton);
        addQualification(managers, nicolasLegault, generaleEmballageBaton);
        addQualification(managers, stefanieReynolds, generaleEmballageBaton);
        addQualification(managers, marioLongtin, generaleEmballageBaton);
        addQualification(managers, robertAllen, generaleEmballageBaton);
        addQualification(managers, raynaldStarnaud, operateurLigneAPain);
        addQualification(managers, rogerDagenais, operateurLigneAPain);
        addQualification(managers, fxCotebrunet, operateurLigneAPain);
        addQualification(managers, francoisArcoite, operateurLigneAPain);
        addQualification(managers, mathieuGuy, operateurLigneAPain);
        addQualification(managers, danielDuquette, operateurLigneAPain);
        addQualification(managers, pierreLamarque, prefarinePremelange);
        addQualification(managers, jeanLatour, prefarinePremelange);
        addQualification(managers, raynaldStarnaud, prefarinePremelange);
        addQualification(managers, normandArsenault, prefarinePremelange);
        addQualification(managers, rogerDagenais, prefarinePremelange);
        addQualification(managers, michelJeanneau, prefarinePremelange);
        addQualification(managers, rolandJrBoucher, prefarinePremelange);
        addQualification(managers, bernardJolin, prefarinePremelange);
        addQualification(managers, marioPaille, prefarinePremelange);
        addQualification(managers, carmenBrais, prefarinePremelange);
        addQualification(managers, ericRichard, prefarinePremelange);
        addQualification(managers, jeanfrancoisBreton, prefarinePremelange);
        addQualification(managers, martinDube, prefarinePremelange);
        addQualification(managers, dominicHoude, prefarinePremelange);
        addQualification(managers, francoisParent, prefarinePremelange);
        addQualification(managers, richardVaillant, prefarinePremelange);
        addQualification(managers, fxCotebrunet, prefarinePremelange);
        addQualification(managers, francoisArcoite, prefarinePremelange);
        addQualification(managers, mathieuGuy, prefarinePremelange);
        addQualification(managers, danielDuquette, prefarinePremelange);
        addQualification(managers, philippeLegault, prefarinePremelange);
        addQualification(managers, gerardLanteigne, petrisseurAPain);
        addQualification(managers, jeanLatour, petrisseurAPain);
        addQualification(managers, rogerDagenais, petrisseurAPain);
        addQualification(managers, marioPaille, petrisseurAPain);
        addQualification(managers, ericRichard, petrisseurAPain);
        addQualification(managers, jeanfrancoisBreton, petrisseurAPain);
        addQualification(managers, francoisParent, petrisseurAPain);
        addQualification(managers, richardVaillant, petrisseurAPain);
        addQualification(managers, fxCotebrunet, petrisseurAPain);
        addQualification(managers, mathieuGuy, petrisseurAPain);
        addQualification(managers, philippeLegault, petrisseurAPain);
        addQualification(managers, jeanpierreAuger, operateurRemplacementBoul);
        addQualification(managers, jeanLatour, operateurRemplacementBoul);
        addQualification(managers, rogerDagenais, operateurRemplacementBoul);
        addQualification(managers, michelJeanneau, operateurRemplacementBoul);
        addQualification(managers, rolandJrBoucher, operateurRemplacementBoul);
        addQualification(managers, bernardJolin, operateurRemplacementBoul);
        addQualification(managers, marioPaille, operateurRemplacementBoul);
        addQualification(managers, ericRichard, operateurRemplacementBoul);
        addQualification(managers, francoisParent, operateurRemplacementBoul);
        addQualification(managers, martinLina, operateurRemplacementBoul);
        addQualification(managers, fxCotebrunet, operateurRemplacementBoul);
        addQualification(managers, mathieuGuy, operateurRemplacementBoul);
        addQualification(managers, philippeLegault, operateurRemplacementBoul);
        addQualification(managers, jeanpierreAuger, generaleDemouleuse);
        addQualification(managers, rejeanBrabant, generaleDemouleuse);
        addQualification(managers, realGosselin, generaleDemouleuse);
        addQualification(managers, bernardBerube, generaleDemouleuse);
        addQualification(managers, robertLazure, generaleDemouleuse);
        addQualification(managers, sergeRobidoux, generaleDemouleuse);
        addQualification(managers, michelDaniel, generaleDemouleuse);
        addQualification(managers, gillesGosselin, generaleDemouleuse);
        addQualification(managers, francoisBeaulne, generaleDemouleuse);
        addQualification(managers, francineGuerin, generaleDemouleuse);
        addQualification(managers, jeanguyRicher, generaleDemouleuse);
        addQualification(managers, marcelDalphond, generaleDemouleuse);
        addQualification(managers, michelMeunier, generaleDemouleuse);
        addQualification(managers, pierreLamarque, generaleDemouleuse);
        addQualification(managers, gerardLanteigne, generaleDemouleuse);
        addQualification(managers, jeanLatour, generaleDemouleuse);
        addQualification(managers, danielNault, generaleDemouleuse);
        addQualification(managers, raynaldStarnaud, generaleDemouleuse);
        addQualification(managers, normandArsenault, generaleDemouleuse);
        addQualification(managers, michelTougas, generaleDemouleuse);
        addQualification(managers, rogerDagenais, generaleDemouleuse);
        addQualification(managers, michelJeanneau, generaleDemouleuse);
        addQualification(managers, denisPilon, generaleDemouleuse);
        addQualification(managers, rejeanRoy, generaleDemouleuse);
        addQualification(managers, huguesDenault, generaleDemouleuse);
        addQualification(managers, rolandJrBoucher, generaleDemouleuse);
        addQualification(managers, bernardJolin, generaleDemouleuse);
        addQualification(managers, marioPaille, generaleDemouleuse);
        addQualification(managers, carmenBrais, generaleDemouleuse);
        addQualification(managers, ericRichard, generaleDemouleuse);
        addQualification(managers, jeanfrancoisBreton, generaleDemouleuse);
        addQualification(managers, stephaneJacques, generaleDemouleuse);
        addQualification(managers, martinDube, generaleDemouleuse);
        addQualification(managers, hachezGabriel, generaleDemouleuse);
        addQualification(managers, dominicHoude, generaleDemouleuse);
        addQualification(managers, francoisParent, generaleDemouleuse);
        addQualification(managers, martinLina, generaleDemouleuse);
        addQualification(managers, richardVaillant, generaleDemouleuse);
        addQualification(managers, fxCotebrunet, generaleDemouleuse);
        addQualification(managers, francoisArcoite, generaleDemouleuse);
        addQualification(managers, mathieuGuy, generaleDemouleuse);
        addQualification(managers, alexandreDube, generaleDemouleuse);
        addQualification(managers, danielDuquette, generaleDemouleuse);
        addQualification(managers, philippeLegault, generaleDemouleuse);
        addQualification(managers, marcGrondin, generaleDemouleuse);
        addQualification(managers, marcelLegault, generaleDemouleuse);
        addQualification(managers, nicolasLegault, generaleDemouleuse);
        addQualification(managers, marioLongtin, generaleDemouleuse);
        addQualification(managers, robertAllen, generaleDemouleuse);
        addQualification(managers, rejeanBrabant, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, lindaBoisvert, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, sergeRobidoux, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, michelMeunier, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, pierreLamarque, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, deniseDaigneault, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, francineLabbe, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, suzanneGagnon, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, rolandJrBoucher, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, ginetteOuellette, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, lucieLeavey, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, lyndaLajoie, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, cecileCouillard, operateurLigneBiscottebagHorsDoeuvre);
        addQualification(managers, lindaBoisvert, operateurEmballageTriangle);
        addQualification(managers, michelMeunier, operateurEmballageTriangle);
        addQualification(managers, rachelBergevin, operateurEmballageTriangle);
        addQualification(managers, rolandJrBoucher, operateurEmballageTriangle);
        addQualification(managers, sartoTremblay, operateurEmballageTriangle);
        addQualification(managers, dianeGirard, operateurEmballageTriangle);
        addQualification(managers, joseeConstantineau, operateurEmballageTriangle);
        addQualification(managers, liseCampeau, operateurEmballageTriangle);
        addQualification(managers, lucieLeavey, operateurEmballageTriangle);
        addQualification(managers, lyndaLajoie, operateurEmballageTriangle);
        addQualification(managers, joseeLapierre, operateurEmballageTriangle);
        addQualification(managers, rachelMoise, operateurEmballageTriangle);
        addQualification(managers, caroleMorand, generaleTrancheuseBaguettine);
        addQualification(managers, lucieGarceau, generaleTrancheuseBaguettine);
        addQualification(managers, huguetteLebel, generaleTrancheuseBaguettine);
        addQualification(managers, michelineDemers, generaleTrancheuseBaguettine);
        addQualification(managers, jeanpierreAuger, generaleTrancheuseBaguettine);
        addQualification(managers, dianeDugas, generaleTrancheuseBaguettine);
        addQualification(managers, rejeanBrabant, generaleTrancheuseBaguettine);
        addQualification(managers, moniqueLeblond, generaleTrancheuseBaguettine);
        addQualification(managers, realGosselin, generaleTrancheuseBaguettine);
        addQualification(managers, lucieLacoste, generaleTrancheuseBaguettine);
        addQualification(managers, johanneLemieux, generaleTrancheuseBaguettine);
        addQualification(managers, manonTremblay, generaleTrancheuseBaguettine);
        addQualification(managers, bernardBerube, generaleTrancheuseBaguettine);
        addQualification(managers, robertLazure, generaleTrancheuseBaguettine);
        addQualification(managers, lindaBoisvert, generaleTrancheuseBaguettine);
        addQualification(managers, sergeRobidoux, generaleTrancheuseBaguettine);
        addQualification(managers, michelDaniel, generaleTrancheuseBaguettine);
        addQualification(managers, caroleRaymond, generaleTrancheuseBaguettine);
        addQualification(managers, gillesGosselin, generaleTrancheuseBaguettine);
        addQualification(managers, francoisBeaulne, generaleTrancheuseBaguettine);
        addQualification(managers, francineGuerin, generaleTrancheuseBaguettine);
        addQualification(managers, jeanguyRicher, generaleTrancheuseBaguettine);
        addQualification(managers, marcelDalphond, generaleTrancheuseBaguettine);
        addQualification(managers, michelMeunier, generaleTrancheuseBaguettine);
        addQualification(managers, pierreLamarque, generaleTrancheuseBaguettine);
        addQualification(managers, gerardLanteigne, generaleTrancheuseBaguettine);
        addQualification(managers, francineDurivage, generaleTrancheuseBaguettine);
        addQualification(managers, jeanLatour, generaleTrancheuseBaguettine);
        addQualification(managers, pierretteDupras, generaleTrancheuseBaguettine);
        addQualification(managers, danielNault, generaleTrancheuseBaguettine);
        addQualification(managers, raynaldStarnaud, generaleTrancheuseBaguettine);
        addQualification(managers, nicoleFortin, generaleTrancheuseBaguettine);
        addQualification(managers, normandArsenault, generaleTrancheuseBaguettine);
        addQualification(managers, liseHebert, generaleTrancheuseBaguettine);
        addQualification(managers, deniseDaigneault, generaleTrancheuseBaguettine);
        addQualification(managers, francineLabbe, generaleTrancheuseBaguettine);
        addQualification(managers, claudineRochefort, generaleTrancheuseBaguettine);
        addQualification(managers, suzanneCouturier, generaleTrancheuseBaguettine);
        addQualification(managers, michelTougas, generaleTrancheuseBaguettine);
        addQualification(managers, danielleBeaudry, generaleTrancheuseBaguettine);
        addQualification(managers, lucieGuay, generaleTrancheuseBaguettine);
        addQualification(managers, rogerDagenais, generaleTrancheuseBaguettine);
        addQualification(managers, denisPilon, generaleTrancheuseBaguettine);
        addQualification(managers, rachelBergevin, generaleTrancheuseBaguettine);
        addQualification(managers, rejeanRoy, generaleTrancheuseBaguettine);
        addQualification(managers, huguesDenault, generaleTrancheuseBaguettine);
        addQualification(managers, rolandJrBoucher, generaleTrancheuseBaguettine);
        addQualification(managers, bernardJolin, generaleTrancheuseBaguettine);
        addQualification(managers, sartoTremblay, generaleTrancheuseBaguettine);
        addQualification(managers, dianeGirard, generaleTrancheuseBaguettine);
        addQualification(managers, ginetteOuellette, generaleTrancheuseBaguettine);
        addQualification(managers, guylaineGuy, generaleTrancheuseBaguettine);
        addQualification(managers, pierretteLamothe, generaleTrancheuseBaguettine);
        addQualification(managers, michelineLegault, generaleTrancheuseBaguettine);
        addQualification(managers, joseeConstantineau, generaleTrancheuseBaguettine);
        addQualification(managers, madelaineMarleau, generaleTrancheuseBaguettine);
        addQualification(managers, manonPoissant, generaleTrancheuseBaguettine);
        addQualification(managers, francineLemieux, generaleTrancheuseBaguettine);
        addQualification(managers, carmenBrais, generaleTrancheuseBaguettine);
        addQualification(managers, francoiseTrudeau, generaleTrancheuseBaguettine);
        addQualification(managers, ericRichard, generaleTrancheuseBaguettine);
        addQualification(managers, nancyTheoret, generaleTrancheuseBaguettine);
        addQualification(managers, liseCampeau, generaleTrancheuseBaguettine);
        addQualification(managers, lucieLeavey, generaleTrancheuseBaguettine);
        addQualification(managers, lyndaLajoie, generaleTrancheuseBaguettine);
        addQualification(managers, jeanfrancoisBreton, generaleTrancheuseBaguettine);
        addQualification(managers, stephaneJacques, generaleTrancheuseBaguettine);
        addQualification(managers, brigitteBouchard, generaleTrancheuseBaguettine);
        addQualification(managers, sylviePineault, generaleTrancheuseBaguettine);
        addQualification(managers, joseeLapierre, generaleTrancheuseBaguettine);
        addQualification(managers, hachezGabriel, generaleTrancheuseBaguettine);
        addQualification(managers, sandraDupuis, generaleTrancheuseBaguettine);
        addQualification(managers, lucieCaron, generaleTrancheuseBaguettine);
        addQualification(managers, rachelMoise, generaleTrancheuseBaguettine);
        addQualification(managers, catherinePiette, generaleTrancheuseBaguettine);
        addQualification(managers, chantalXavier, generaleTrancheuseBaguettine);
        addQualification(managers, dominicHoude, generaleTrancheuseBaguettine);
        addQualification(managers, francoisParent, generaleTrancheuseBaguettine);
        addQualification(managers, solangeGirard, generaleTrancheuseBaguettine);
        addQualification(managers, martinLina, generaleTrancheuseBaguettine);
        addQualification(managers, liseJoncas, generaleTrancheuseBaguettine);
        addQualification(managers, nathalieReid, generaleTrancheuseBaguettine);
        addQualification(managers, cecileCouillard, generaleTrancheuseBaguettine);
        addQualification(managers, richardVaillant, generaleTrancheuseBaguettine);
        addQualification(managers, franceBoyer, generaleTrancheuseBaguettine);
        addQualification(managers, fxCotebrunet, generaleTrancheuseBaguettine);
        addQualification(managers, isabelleLeclerc, generaleTrancheuseBaguettine);
        addQualification(managers, francoisArcoite, generaleTrancheuseBaguettine);
        addQualification(managers, sabrinaDupuis, generaleTrancheuseBaguettine);
        addQualification(managers, mathieuGuy, generaleTrancheuseBaguettine);
        addQualification(managers, daisyBourget, generaleTrancheuseBaguettine);
        addQualification(managers, alexandreDube, generaleTrancheuseBaguettine);
        addQualification(managers, annickPigeon, generaleTrancheuseBaguettine);
        addQualification(managers, danielDuquette, generaleTrancheuseBaguettine);
        addQualification(managers, johanneDuval, generaleTrancheuseBaguettine);
        addQualification(managers, philippeLegault, generaleTrancheuseBaguettine);
        addQualification(managers, celineVadnais, generaleTrancheuseBaguettine);
        addQualification(managers, marcGrondin, generaleTrancheuseBaguettine);
        addQualification(managers, ginetteLevesque, generaleTrancheuseBaguettine);
        addQualification(managers, marcelLegault, generaleTrancheuseBaguettine);
        addQualification(managers, nicolasLegault, generaleTrancheuseBaguettine);
        addQualification(managers, stefanieReynolds, generaleTrancheuseBaguettine);
        addQualification(managers, marioLongtin, generaleTrancheuseBaguettine);
        addQualification(managers, robertAllen, generaleTrancheuseBaguettine);
        addQualification(managers, louiseVeillette, generaleEmballageBaguettine);
        addQualification(managers, caroleMorand, generaleEmballageBaguettine);
        addQualification(managers, lucieGarceau, generaleEmballageBaguettine);
        addQualification(managers, huguetteLebel, generaleEmballageBaguettine);
        addQualification(managers, michelineDemers, generaleEmballageBaguettine);
        addQualification(managers, jeanpierreAuger, generaleEmballageBaguettine);
        addQualification(managers, dianeDugas, generaleEmballageBaguettine);
        addQualification(managers, moniqueLeblond, generaleEmballageBaguettine);
        addQualification(managers, realGosselin, generaleEmballageBaguettine);
        addQualification(managers, lucieLacoste, generaleEmballageBaguettine);
        addQualification(managers, johanneLemieux, generaleEmballageBaguettine);
        addQualification(managers, manonTremblay, generaleEmballageBaguettine);
        addQualification(managers, bernardBerube, generaleEmballageBaguettine);
        addQualification(managers, robertLazure, generaleEmballageBaguettine);
        addQualification(managers, lindaBoisvert, generaleEmballageBaguettine);
        addQualification(managers, sergeRobidoux, generaleEmballageBaguettine);
        addQualification(managers, michelDaniel, generaleEmballageBaguettine);
        addQualification(managers, caroleRaymond, generaleEmballageBaguettine);
        addQualification(managers, gillesGosselin, generaleEmballageBaguettine);
        addQualification(managers, francoisBeaulne, generaleEmballageBaguettine);
        addQualification(managers, francineGuerin, generaleEmballageBaguettine);
        addQualification(managers, jeanguyRicher, generaleEmballageBaguettine);
        addQualification(managers, marcelDalphond, generaleEmballageBaguettine);
        addQualification(managers, michelMeunier, generaleEmballageBaguettine);
        addQualification(managers, pierreLamarque, generaleEmballageBaguettine);
        addQualification(managers, gerardLanteigne, generaleEmballageBaguettine);
        addQualification(managers, francineDurivage, generaleEmballageBaguettine);
        addQualification(managers, jeanLatour, generaleEmballageBaguettine);
        addQualification(managers, pierretteDupras, generaleEmballageBaguettine);
        addQualification(managers, danielNault, generaleEmballageBaguettine);
        addQualification(managers, raynaldStarnaud, generaleEmballageBaguettine);
        addQualification(managers, nicoleFortin, generaleEmballageBaguettine);
        addQualification(managers, normandArsenault, generaleEmballageBaguettine);
        addQualification(managers, liseHebert, generaleEmballageBaguettine);
        addQualification(managers, deniseDaigneault, generaleEmballageBaguettine);
        addQualification(managers, francineLabbe, generaleEmballageBaguettine);
        addQualification(managers, claudineRochefort, generaleEmballageBaguettine);
        addQualification(managers, suzanneCouturier, generaleEmballageBaguettine);
        addQualification(managers, michelTougas, generaleEmballageBaguettine);
        addQualification(managers, danielleBeaudry, generaleEmballageBaguettine);
        addQualification(managers, lucieGuay, generaleEmballageBaguettine);
        addQualification(managers, rogerDagenais, generaleEmballageBaguettine);
        addQualification(managers, michelJeanneau, generaleEmballageBaguettine);
        addQualification(managers, denisPilon, generaleEmballageBaguettine);
        addQualification(managers, suzanneGagnon, generaleEmballageBaguettine);
        addQualification(managers, rachelBergevin, generaleEmballageBaguettine);
        addQualification(managers, rejeanRoy, generaleEmballageBaguettine);
        addQualification(managers, huguesDenault, generaleEmballageBaguettine);
        addQualification(managers, rolandJrBoucher, generaleEmballageBaguettine);
        addQualification(managers, bernardJolin, generaleEmballageBaguettine);
        addQualification(managers, sartoTremblay, generaleEmballageBaguettine);
        addQualification(managers, dianeGirard, generaleEmballageBaguettine);
        addQualification(managers, marioPaille, generaleEmballageBaguettine);
        addQualification(managers, ginetteOuellette, generaleEmballageBaguettine);
        addQualification(managers, guylaineGuy, generaleEmballageBaguettine);
        addQualification(managers, pierretteLamothe, generaleEmballageBaguettine);
        addQualification(managers, michelineLegault, generaleEmballageBaguettine);
        addQualification(managers, joseeConstantineau, generaleEmballageBaguettine);
        addQualification(managers, madelaineMarleau, generaleEmballageBaguettine);
        addQualification(managers, manonPoissant, generaleEmballageBaguettine);
        addQualification(managers, francineLemieux, generaleEmballageBaguettine);
        addQualification(managers, carmenBrais, generaleEmballageBaguettine);
        addQualification(managers, francoiseTrudeau, generaleEmballageBaguettine);
        addQualification(managers, ericRichard, generaleEmballageBaguettine);
        addQualification(managers, nancyTheoret, generaleEmballageBaguettine);
        addQualification(managers, liseCampeau, generaleEmballageBaguettine);
        addQualification(managers, lucieLeavey, generaleEmballageBaguettine);
        addQualification(managers, lyndaLajoie, generaleEmballageBaguettine);
        addQualification(managers, jeanfrancoisBreton, generaleEmballageBaguettine);
        addQualification(managers, stephaneJacques, generaleEmballageBaguettine);
        addQualification(managers, brigitteBouchard, generaleEmballageBaguettine);
        addQualification(managers, sylviePineault, generaleEmballageBaguettine);
        addQualification(managers, joseeLapierre, generaleEmballageBaguettine);
        addQualification(managers, hachezGabriel, generaleEmballageBaguettine);
        addQualification(managers, sandraDupuis, generaleEmballageBaguettine);
        addQualification(managers, lucieCaron, generaleEmballageBaguettine);
        addQualification(managers, rachelMoise, generaleEmballageBaguettine);
        addQualification(managers, catherinePiette, generaleEmballageBaguettine);
        addQualification(managers, chantalXavier, generaleEmballageBaguettine);
        addQualification(managers, dominicHoude, generaleEmballageBaguettine);
        addQualification(managers, francoisParent, generaleEmballageBaguettine);
        addQualification(managers, solangeGirard, generaleEmballageBaguettine);
        addQualification(managers, martinLina, generaleEmballageBaguettine);
        addQualification(managers, liseJoncas, generaleEmballageBaguettine);
        addQualification(managers, nathalieReid, generaleEmballageBaguettine);
        addQualification(managers, cecileCouillard, generaleEmballageBaguettine);
        addQualification(managers, richardVaillant, generaleEmballageBaguettine);
        addQualification(managers, franceBoyer, generaleEmballageBaguettine);
        addQualification(managers, fxCotebrunet, generaleEmballageBaguettine);
        addQualification(managers, isabelleLeclerc, generaleEmballageBaguettine);
        addQualification(managers, francoisArcoite, generaleEmballageBaguettine);
        addQualification(managers, sabrinaDupuis, generaleEmballageBaguettine);
        addQualification(managers, mathieuGuy, generaleEmballageBaguettine);
        addQualification(managers, daisyBourget, generaleEmballageBaguettine);
        addQualification(managers, alexandreDube, generaleEmballageBaguettine);
        addQualification(managers, annickPigeon, generaleEmballageBaguettine);
        addQualification(managers, danielDuquette, generaleEmballageBaguettine);
        addQualification(managers, johanneDuval, generaleEmballageBaguettine);
        addQualification(managers, philippeLegault, generaleEmballageBaguettine);
        addQualification(managers, celineVadnais, generaleEmballageBaguettine);
        addQualification(managers, marcGrondin, generaleEmballageBaguettine);
        addQualification(managers, ginetteLevesque, generaleEmballageBaguettine);
        addQualification(managers, marcelLegault, generaleEmballageBaguettine);
        addQualification(managers, nicolasLegault, generaleEmballageBaguettine);
        addQualification(managers, stefanieReynolds, generaleEmballageBaguettine);
        addQualification(managers, marioLongtin, generaleEmballageBaguettine);
        addQualification(managers, robertAllen, generaleEmballageBaguettine);
        addQualification(managers, caroleMorand, generaleTrancheuseBiscotte);
        addQualification(managers, lucieGarceau, generaleTrancheuseBiscotte);
        addQualification(managers, huguetteLebel, generaleTrancheuseBiscotte);
        addQualification(managers, michelineDemers, generaleTrancheuseBiscotte);
        addQualification(managers, jeanpierreAuger, generaleTrancheuseBiscotte);
        addQualification(managers, dianeDugas, generaleTrancheuseBiscotte);
        addQualification(managers, rejeanBrabant, generaleTrancheuseBiscotte);
        addQualification(managers, moniqueLeblond, generaleTrancheuseBiscotte);
        addQualification(managers, realGosselin, generaleTrancheuseBiscotte);
        addQualification(managers, lucieLacoste, generaleTrancheuseBiscotte);
        addQualification(managers, johanneLemieux, generaleTrancheuseBiscotte);
        addQualification(managers, manonTremblay, generaleTrancheuseBiscotte);
        addQualification(managers, bernardBerube, generaleTrancheuseBiscotte);
        addQualification(managers, robertLazure, generaleTrancheuseBiscotte);
        addQualification(managers, lindaBoisvert, generaleTrancheuseBiscotte);
        addQualification(managers, sergeRobidoux, generaleTrancheuseBiscotte);
        addQualification(managers, michelDaniel, generaleTrancheuseBiscotte);
        addQualification(managers, caroleRaymond, generaleTrancheuseBiscotte);
        addQualification(managers, gillesGosselin, generaleTrancheuseBiscotte);
        addQualification(managers, francoisBeaulne, generaleTrancheuseBiscotte);
        addQualification(managers, francineGuerin, generaleTrancheuseBiscotte);
        addQualification(managers, jeanguyRicher, generaleTrancheuseBiscotte);
        addQualification(managers, marcelDalphond, generaleTrancheuseBiscotte);
        addQualification(managers, michelMeunier, generaleTrancheuseBiscotte);
        addQualification(managers, pierreLamarque, generaleTrancheuseBiscotte);
        addQualification(managers, gerardLanteigne, generaleTrancheuseBiscotte);
        addQualification(managers, francineDurivage, generaleTrancheuseBiscotte);
        addQualification(managers, jeanLatour, generaleTrancheuseBiscotte);
        addQualification(managers, pierretteDupras, generaleTrancheuseBiscotte);
        addQualification(managers, danielNault, generaleTrancheuseBiscotte);
        addQualification(managers, raynaldStarnaud, generaleTrancheuseBiscotte);
        addQualification(managers, nicoleFortin, generaleTrancheuseBiscotte);
        addQualification(managers, normandArsenault, generaleTrancheuseBiscotte);
        addQualification(managers, liseHebert, generaleTrancheuseBiscotte);
        addQualification(managers, deniseDaigneault, generaleTrancheuseBiscotte);
        addQualification(managers, francineLabbe, generaleTrancheuseBiscotte);
        addQualification(managers, claudineRochefort, generaleTrancheuseBiscotte);
        addQualification(managers, suzanneCouturier, generaleTrancheuseBiscotte);
        addQualification(managers, michelTougas, generaleTrancheuseBiscotte);
        addQualification(managers, danielleBeaudry, generaleTrancheuseBiscotte);
        addQualification(managers, lucieGuay, generaleTrancheuseBiscotte);
        addQualification(managers, rogerDagenais, generaleTrancheuseBiscotte);
        addQualification(managers, michelJeanneau, generaleTrancheuseBiscotte);
        addQualification(managers, denisPilon, generaleTrancheuseBiscotte);
        addQualification(managers, rachelBergevin, generaleTrancheuseBiscotte);
        addQualification(managers, rejeanRoy, generaleTrancheuseBiscotte);
        addQualification(managers, huguesDenault, generaleTrancheuseBiscotte);
        addQualification(managers, rolandJrBoucher, generaleTrancheuseBiscotte);
        addQualification(managers, bernardJolin, generaleTrancheuseBiscotte);
        addQualification(managers, sartoTremblay, generaleTrancheuseBiscotte);
        addQualification(managers, dianeGirard, generaleTrancheuseBiscotte);
        addQualification(managers, marioPaille, generaleTrancheuseBiscotte);
        addQualification(managers, ginetteOuellette, generaleTrancheuseBiscotte);
        addQualification(managers, guylaineGuy, generaleTrancheuseBiscotte);
        addQualification(managers, pierretteLamothe, generaleTrancheuseBiscotte);
        addQualification(managers, michelineLegault, generaleTrancheuseBiscotte);
        addQualification(managers, joseeConstantineau, generaleTrancheuseBiscotte);
        addQualification(managers, madelaineMarleau, generaleTrancheuseBiscotte);
        addQualification(managers, manonPoissant, generaleTrancheuseBiscotte);
        addQualification(managers, francineLemieux, generaleTrancheuseBiscotte);
        addQualification(managers, carmenBrais, generaleTrancheuseBiscotte);
        addQualification(managers, francoiseTrudeau, generaleTrancheuseBiscotte);
        addQualification(managers, ericRichard, generaleTrancheuseBiscotte);
        addQualification(managers, nancyTheoret, generaleTrancheuseBiscotte);
        addQualification(managers, liseCampeau, generaleTrancheuseBiscotte);
        addQualification(managers, lucieLeavey, generaleTrancheuseBiscotte);
        addQualification(managers, lyndaLajoie, generaleTrancheuseBiscotte);
        addQualification(managers, jeanfrancoisBreton, generaleTrancheuseBiscotte);
        addQualification(managers, stephaneJacques, generaleTrancheuseBiscotte);
        addQualification(managers, brigitteBouchard, generaleTrancheuseBiscotte);
        addQualification(managers, sylviePineault, generaleTrancheuseBiscotte);
        addQualification(managers, joseeLapierre, generaleTrancheuseBiscotte);
        addQualification(managers, hachezGabriel, generaleTrancheuseBiscotte);
        addQualification(managers, sandraDupuis, generaleTrancheuseBiscotte);
        addQualification(managers, lucieCaron, generaleTrancheuseBiscotte);
        addQualification(managers, rachelMoise, generaleTrancheuseBiscotte);
        addQualification(managers, catherinePiette, generaleTrancheuseBiscotte);
        addQualification(managers, chantalXavier, generaleTrancheuseBiscotte);
        addQualification(managers, dominicHoude, generaleTrancheuseBiscotte);
        addQualification(managers, francoisParent, generaleTrancheuseBiscotte);
        addQualification(managers, solangeGirard, generaleTrancheuseBiscotte);
        addQualification(managers, martinLina, generaleTrancheuseBiscotte);
        addQualification(managers, liseJoncas, generaleTrancheuseBiscotte);
        addQualification(managers, nathalieReid, generaleTrancheuseBiscotte);
        addQualification(managers, cecileCouillard, generaleTrancheuseBiscotte);
        addQualification(managers, richardVaillant, generaleTrancheuseBiscotte);
        addQualification(managers, franceBoyer, generaleTrancheuseBiscotte);
        addQualification(managers, fxCotebrunet, generaleTrancheuseBiscotte);
        addQualification(managers, isabelleLeclerc, generaleTrancheuseBiscotte);
        addQualification(managers, francoisArcoite, generaleTrancheuseBiscotte);
        addQualification(managers, sabrinaDupuis, generaleTrancheuseBiscotte);
        addQualification(managers, mathieuGuy, generaleTrancheuseBiscotte);
        addQualification(managers, daisyBourget, generaleTrancheuseBiscotte);
        addQualification(managers, alexandreDube, generaleTrancheuseBiscotte);
        addQualification(managers, annickPigeon, generaleTrancheuseBiscotte);
        addQualification(managers, danielDuquette, generaleTrancheuseBiscotte);
        addQualification(managers, johanneDuval, generaleTrancheuseBiscotte);
        addQualification(managers, philippeLegault, generaleTrancheuseBiscotte);
        addQualification(managers, celineVadnais, generaleTrancheuseBiscotte);
        addQualification(managers, marcGrondin, generaleTrancheuseBiscotte);
        addQualification(managers, ginetteLevesque, generaleTrancheuseBiscotte);
        addQualification(managers, marcelLegault, generaleTrancheuseBiscotte);
        addQualification(managers, nicolasLegault, generaleTrancheuseBiscotte);
        addQualification(managers, stefanieReynolds, generaleTrancheuseBiscotte);
        addQualification(managers, marioLongtin, generaleTrancheuseBiscotte);
        addQualification(managers, robertAllen, generaleTrancheuseBiscotte);
        addQualification(managers, louiseVeillette, generaleEmballageBiscotte);
        addQualification(managers, caroleMorand, generaleEmballageBiscotte);
        addQualification(managers, lucieGarceau, generaleEmballageBiscotte);
        addQualification(managers, huguetteLebel, generaleEmballageBiscotte);
        addQualification(managers, michelineDemers, generaleEmballageBiscotte);
        addQualification(managers, jeanpierreAuger, generaleEmballageBiscotte);
        addQualification(managers, dianeDugas, generaleEmballageBiscotte);
        addQualification(managers, rejeanBrabant, generaleEmballageBiscotte);
        addQualification(managers, moniqueLeblond, generaleEmballageBiscotte);
        addQualification(managers, realGosselin, generaleEmballageBiscotte);
        addQualification(managers, lucieLacoste, generaleEmballageBiscotte);
        addQualification(managers, johanneLemieux, generaleEmballageBiscotte);
        addQualification(managers, manonTremblay, generaleEmballageBiscotte);
        addQualification(managers, bernardBerube, generaleEmballageBiscotte);
        addQualification(managers, robertLazure, generaleEmballageBiscotte);
        addQualification(managers, lindaBoisvert, generaleEmballageBiscotte);
        addQualification(managers, sergeRobidoux, generaleEmballageBiscotte);
        addQualification(managers, michelDaniel, generaleEmballageBiscotte);
        addQualification(managers, caroleRaymond, generaleEmballageBiscotte);
        addQualification(managers, gillesGosselin, generaleEmballageBiscotte);
        addQualification(managers, francoisBeaulne, generaleEmballageBiscotte);
        addQualification(managers, francineGuerin, generaleEmballageBiscotte);
        addQualification(managers, jeanguyRicher, generaleEmballageBiscotte);
        addQualification(managers, marcelDalphond, generaleEmballageBiscotte);
        addQualification(managers, michelMeunier, generaleEmballageBiscotte);
        addQualification(managers, pierreLamarque, generaleEmballageBiscotte);
        addQualification(managers, gerardLanteigne, generaleEmballageBiscotte);
        addQualification(managers, francineDurivage, generaleEmballageBiscotte);
        addQualification(managers, jeanLatour, generaleEmballageBiscotte);
        addQualification(managers, pierretteDupras, generaleEmballageBiscotte);
        addQualification(managers, danielNault, generaleEmballageBiscotte);
        addQualification(managers, raynaldStarnaud, generaleEmballageBiscotte);
        addQualification(managers, nicoleFortin, generaleEmballageBiscotte);
        addQualification(managers, normandArsenault, generaleEmballageBiscotte);
        addQualification(managers, liseHebert, generaleEmballageBiscotte);
        addQualification(managers, deniseDaigneault, generaleEmballageBiscotte);
        addQualification(managers, francineLabbe, generaleEmballageBiscotte);
        addQualification(managers, claudineRochefort, generaleEmballageBiscotte);
        addQualification(managers, suzanneCouturier, generaleEmballageBiscotte);
        addQualification(managers, michelTougas, generaleEmballageBiscotte);
        addQualification(managers, danielleBeaudry, generaleEmballageBiscotte);
        addQualification(managers, lucieGuay, generaleEmballageBiscotte);
        addQualification(managers, rogerDagenais, generaleEmballageBiscotte);
        addQualification(managers, michelJeanneau, generaleEmballageBiscotte);
        addQualification(managers, denisPilon, generaleEmballageBiscotte);
        addQualification(managers, suzanneGagnon, generaleEmballageBiscotte);
        addQualification(managers, rachelBergevin, generaleEmballageBiscotte);
        addQualification(managers, rejeanRoy, generaleEmballageBiscotte);
        addQualification(managers, huguesDenault, generaleEmballageBiscotte);
        addQualification(managers, rolandJrBoucher, generaleEmballageBiscotte);
        addQualification(managers, bernardJolin, generaleEmballageBiscotte);
        addQualification(managers, sartoTremblay, generaleEmballageBiscotte);
        addQualification(managers, dianeGirard, generaleEmballageBiscotte);
        addQualification(managers, marioPaille, generaleEmballageBiscotte);
        addQualification(managers, ginetteOuellette, generaleEmballageBiscotte);
        addQualification(managers, guylaineGuy, generaleEmballageBiscotte);
        addQualification(managers, pierretteLamothe, generaleEmballageBiscotte);
        addQualification(managers, michelineLegault, generaleEmballageBiscotte);
        addQualification(managers, joseeConstantineau, generaleEmballageBiscotte);
        addQualification(managers, madelaineMarleau, generaleEmballageBiscotte);
        addQualification(managers, manonPoissant, generaleEmballageBiscotte);
        addQualification(managers, francineLemieux, generaleEmballageBiscotte);
        addQualification(managers, carmenBrais, generaleEmballageBiscotte);
        addQualification(managers, francoiseTrudeau, generaleEmballageBiscotte);
        addQualification(managers, ericRichard, generaleEmballageBiscotte);
        addQualification(managers, nancyTheoret, generaleEmballageBiscotte);
        addQualification(managers, liseCampeau, generaleEmballageBiscotte);
        addQualification(managers, lucieLeavey, generaleEmballageBiscotte);
        addQualification(managers, lyndaLajoie, generaleEmballageBiscotte);
        addQualification(managers, jeanfrancoisBreton, generaleEmballageBiscotte);
        addQualification(managers, stephaneJacques, generaleEmballageBiscotte);
        addQualification(managers, brigitteBouchard, generaleEmballageBiscotte);
        addQualification(managers, sylviePineault, generaleEmballageBiscotte);
        addQualification(managers, joseeLapierre, generaleEmballageBiscotte);
        addQualification(managers, hachezGabriel, generaleEmballageBiscotte);
        addQualification(managers, sandraDupuis, generaleEmballageBiscotte);
        addQualification(managers, lucieCaron, generaleEmballageBiscotte);
        addQualification(managers, rachelMoise, generaleEmballageBiscotte);
        addQualification(managers, catherinePiette, generaleEmballageBiscotte);
        addQualification(managers, chantalXavier, generaleEmballageBiscotte);
        addQualification(managers, dominicHoude, generaleEmballageBiscotte);
        addQualification(managers, francoisParent, generaleEmballageBiscotte);
        addQualification(managers, solangeGirard, generaleEmballageBiscotte);
        addQualification(managers, martinLina, generaleEmballageBiscotte);
        addQualification(managers, liseJoncas, generaleEmballageBiscotte);
        addQualification(managers, nathalieReid, generaleEmballageBiscotte);
        addQualification(managers, cecileCouillard, generaleEmballageBiscotte);
        addQualification(managers, richardVaillant, generaleEmballageBiscotte);
        addQualification(managers, franceBoyer, generaleEmballageBiscotte);
        addQualification(managers, fxCotebrunet, generaleEmballageBiscotte);
        addQualification(managers, isabelleLeclerc, generaleEmballageBiscotte);
        addQualification(managers, francoisArcoite, generaleEmballageBiscotte);
        addQualification(managers, sabrinaDupuis, generaleEmballageBiscotte);
        addQualification(managers, mathieuGuy, generaleEmballageBiscotte);
        addQualification(managers, daisyBourget, generaleEmballageBiscotte);
        addQualification(managers, alexandreDube, generaleEmballageBiscotte);
        addQualification(managers, annickPigeon, generaleEmballageBiscotte);
        addQualification(managers, danielDuquette, generaleEmballageBiscotte);
        addQualification(managers, johanneDuval, generaleEmballageBiscotte);
        addQualification(managers, philippeLegault, generaleEmballageBiscotte);
        addQualification(managers, celineVadnais, generaleEmballageBiscotte);
        addQualification(managers, marcGrondin, generaleEmballageBiscotte);
        addQualification(managers, ginetteLevesque, generaleEmballageBiscotte);
        addQualification(managers, marcelLegault, generaleEmballageBiscotte);
        addQualification(managers, nicolasLegault, generaleEmballageBiscotte);
        addQualification(managers, stefanieReynolds, generaleEmballageBiscotte);
        addQualification(managers, marioLongtin, generaleEmballageBiscotte);
        addQualification(managers, robertAllen, generaleEmballageBiscotte);
        addQualification(managers, marcelDalphond, recuperateurEmballage);
        addQualification(managers, danielNault, recuperateurEmballage);
        addQualification(managers, rejeanRoy, recuperateurEmballage);
        addQualification(managers, huguesDenault, recuperateurEmballage);
        addQualification(managers, dianeGirard, recuperateurEmballage);
        addQualification(managers, ericRichard, recuperateurEmballage);
        addQualification(managers, lucieLeavey, recuperateurEmballage);
        addQualification(managers, stephaneJacques, recuperateurEmballage);
        addQualification(managers, joseeLapierre, recuperateurEmballage);
        addQualification(managers, rachelMoise, recuperateurEmballage);
        addQualification(managers, francoisParent, recuperateurEmballage);
        addQualification(managers, philippeLegault, recuperateurEmballage);
        addQualification(managers, sergeRobidoux, operateurLigneFourMelba);
        addQualification(managers, marcelDalphond, operateurLigneFourMelba);
        addQualification(managers, gerardLanteigne, operateurLigneFourMelba);
        addQualification(managers, suzanneGagnon, operateurLigneFourMelba);
        addQualification(managers, rolandJrBoucher, operateurLigneFourMelba);
        addQualification(managers, dianeGirard, operateurLigneFourMelba);
        addQualification(managers, ginetteOuellette, operateurLigneFourMelba);
        addQualification(managers, joseeConstantineau, operateurLigneFourMelba);
        addQualification(managers, lucieLeavey, operateurLigneFourMelba);
        addQualification(managers, lyndaLajoie, operateurLigneFourMelba);
        addQualification(managers, joseeLapierre, operateurLigneFourMelba);
        addQualification(managers, alexandreDube, operateurLigneFourMelba);
        addQualification(managers, moniqueLeblond, operateurEmballageMelba);
        addQualification(managers, marcelDalphond, operateurEmballageMelba);
        addQualification(managers, nicoleFortin, operateurEmballageMelba);
        addQualification(managers, francineLabbe, operateurEmballageMelba);
        addQualification(managers, dianeGirard, operateurEmballageMelba);
        addQualification(managers, ginetteOuellette, operateurEmballageMelba);
        addQualification(managers, madelaineMarleau, operateurEmballageMelba);
        addQualification(managers, lucieLeavey, operateurEmballageMelba);
        addQualification(managers, joseeLapierre, operateurEmballageMelba);
        addQualification(managers, rachelMoise, operateurEmballageMelba);
        addQualification(managers, caroleMorand, equarisseur);
        addQualification(managers, lucieGarceau, equarisseur);
        addQualification(managers, huguetteLebel, equarisseur);
        addQualification(managers, michelineDemers, equarisseur);
        addQualification(managers, jeanpierreAuger, equarisseur);
        addQualification(managers, dianeDugas, equarisseur);
        addQualification(managers, rejeanBrabant, equarisseur);
        addQualification(managers, moniqueLeblond, equarisseur);
        addQualification(managers, realGosselin, equarisseur);
        addQualification(managers, johanneLemieux, equarisseur);
        addQualification(managers, manonTremblay, equarisseur);
        addQualification(managers, bernardBerube, equarisseur);
        addQualification(managers, robertLazure, equarisseur);
        addQualification(managers, lindaBoisvert, equarisseur);
        addQualification(managers, sergeRobidoux, equarisseur);
        addQualification(managers, michelDaniel, equarisseur);
        addQualification(managers, caroleRaymond, equarisseur);
        addQualification(managers, gillesGosselin, equarisseur);
        addQualification(managers, francoisBeaulne, equarisseur);
        addQualification(managers, francineGuerin, equarisseur);
        addQualification(managers, jeanguyRicher, equarisseur);
        addQualification(managers, marcelDalphond, equarisseur);
        addQualification(managers, michelMeunier, equarisseur);
        addQualification(managers, pierreLamarque, equarisseur);
        addQualification(managers, gerardLanteigne, equarisseur);
        addQualification(managers, francineDurivage, equarisseur);
        addQualification(managers, jeanLatour, equarisseur);
        addQualification(managers, pierretteDupras, equarisseur);
        addQualification(managers, danielNault, equarisseur);
        addQualification(managers, raynaldStarnaud, equarisseur);
        addQualification(managers, nicoleFortin, equarisseur);
        addQualification(managers, normandArsenault, equarisseur);
        addQualification(managers, liseHebert, equarisseur);
        addQualification(managers, deniseDaigneault, equarisseur);
        addQualification(managers, francineLabbe, equarisseur);
        addQualification(managers, claudineRochefort, equarisseur);
        addQualification(managers, suzanneCouturier, equarisseur);
        addQualification(managers, michelTougas, equarisseur);
        addQualification(managers, danielleBeaudry, equarisseur);
        addQualification(managers, lucieGuay, equarisseur);
        addQualification(managers, rogerDagenais, equarisseur);
        addQualification(managers, michelJeanneau, equarisseur);
        addQualification(managers, denisPilon, equarisseur);
        addQualification(managers, rachelBergevin, equarisseur);
        addQualification(managers, rejeanRoy, equarisseur);
        addQualification(managers, huguesDenault, equarisseur);
        addQualification(managers, rolandJrBoucher, equarisseur);
        addQualification(managers, bernardJolin, equarisseur);
        addQualification(managers, sartoTremblay, equarisseur);
        addQualification(managers, dianeGirard, equarisseur);
        addQualification(managers, marioPaille, equarisseur);
        addQualification(managers, ginetteOuellette, equarisseur);
        addQualification(managers, guylaineGuy, equarisseur);
        addQualification(managers, pierretteLamothe, equarisseur);
        addQualification(managers, michelineLegault, equarisseur);
        addQualification(managers, joseeConstantineau, equarisseur);
        addQualification(managers, madelaineMarleau, equarisseur);
        addQualification(managers, manonPoissant, equarisseur);
        addQualification(managers, francineLemieux, equarisseur);
        addQualification(managers, carmenBrais, equarisseur);
        addQualification(managers, francoiseTrudeau, equarisseur);
        addQualification(managers, ericRichard, equarisseur);
        addQualification(managers, nancyTheoret, equarisseur);
        addQualification(managers, liseCampeau, equarisseur);
        addQualification(managers, lucieLeavey, equarisseur);
        addQualification(managers, lyndaLajoie, equarisseur);
        addQualification(managers, jeanfrancoisBreton, equarisseur);
        addQualification(managers, stephaneJacques, equarisseur);
        addQualification(managers, brigitteBouchard, equarisseur);
        addQualification(managers, sylviePineault, equarisseur);
        addQualification(managers, joseeLapierre, equarisseur);
        addQualification(managers, hachezGabriel, equarisseur);
        addQualification(managers, sandraDupuis, equarisseur);
        addQualification(managers, lucieCaron, equarisseur);
        addQualification(managers, rachelMoise, equarisseur);
        addQualification(managers, catherinePiette, equarisseur);
        addQualification(managers, chantalXavier, equarisseur);
        addQualification(managers, dominicHoude, equarisseur);
        addQualification(managers, francoisParent, equarisseur);
        addQualification(managers, solangeGirard, equarisseur);
        addQualification(managers, martinLina, equarisseur);
        addQualification(managers, liseJoncas, equarisseur);
        addQualification(managers, nathalieReid, equarisseur);
        addQualification(managers, cecileCouillard, equarisseur);
        addQualification(managers, sylvainJulien, equarisseur);
        addQualification(managers, richardVaillant, equarisseur);
        addQualification(managers, franceBoyer, equarisseur);
        addQualification(managers, fxCotebrunet, equarisseur);
        addQualification(managers, isabelleLeclerc, equarisseur);
        addQualification(managers, francoisArcoite, equarisseur);
        addQualification(managers, sabrinaDupuis, equarisseur);
        addQualification(managers, mathieuGuy, equarisseur);
        addQualification(managers, daisyBourget, equarisseur);
        addQualification(managers, alexandreDube, equarisseur);
        addQualification(managers, annickPigeon, equarisseur);
        addQualification(managers, danielDuquette, equarisseur);
        addQualification(managers, johanneDuval, equarisseur);
        addQualification(managers, philippeLegault, equarisseur);
        addQualification(managers, celineVadnais, equarisseur);
        addQualification(managers, marcGrondin, equarisseur);
        addQualification(managers, ginetteLevesque, equarisseur);
        addQualification(managers, marcelLegault, equarisseur);
        addQualification(managers, nicolasLegault, equarisseur);
        addQualification(managers, stefanieReynolds, equarisseur);
        addQualification(managers, marioLongtin, equarisseur);
        addQualification(managers, robertAllen, equarisseur);
        addQualification(managers, louiseVeillette, generalTrancheuseMelba);
        addQualification(managers, caroleMorand, generalTrancheuseMelba);
        addQualification(managers, lucieGarceau, generalTrancheuseMelba);
        addQualification(managers, huguetteLebel, generalTrancheuseMelba);
        addQualification(managers, michelineDemers, generalTrancheuseMelba);
        addQualification(managers, jeanpierreAuger, generalTrancheuseMelba);
        addQualification(managers, dianeDugas, generalTrancheuseMelba);
        addQualification(managers, moniqueLeblond, generalTrancheuseMelba);
        addQualification(managers, realGosselin, generalTrancheuseMelba);
        addQualification(managers, lucieLacoste, generalTrancheuseMelba);
        addQualification(managers, johanneLemieux, generalTrancheuseMelba);
        addQualification(managers, manonTremblay, generalTrancheuseMelba);
        addQualification(managers, bernardBerube, generalTrancheuseMelba);
        addQualification(managers, robertLazure, generalTrancheuseMelba);
        addQualification(managers, lindaBoisvert, generalTrancheuseMelba);
        addQualification(managers, sergeRobidoux, generalTrancheuseMelba);
        addQualification(managers, michelDaniel, generalTrancheuseMelba);
        addQualification(managers, caroleRaymond, generalTrancheuseMelba);
        addQualification(managers, gillesGosselin, generalTrancheuseMelba);
        addQualification(managers, francoisBeaulne, generalTrancheuseMelba);
        addQualification(managers, francineGuerin, generalTrancheuseMelba);
        addQualification(managers, jeanguyRicher, generalTrancheuseMelba);
        addQualification(managers, marcelDalphond, generalTrancheuseMelba);
        addQualification(managers, michelMeunier, generalTrancheuseMelba);
        addQualification(managers, pierreLamarque, generalTrancheuseMelba);
        addQualification(managers, gerardLanteigne, generalTrancheuseMelba);
        addQualification(managers, francineDurivage, generalTrancheuseMelba);
        addQualification(managers, jeanLatour, generalTrancheuseMelba);
        addQualification(managers, pierretteDupras, generalTrancheuseMelba);
        addQualification(managers, danielNault, generalTrancheuseMelba);
        addQualification(managers, raynaldStarnaud, generalTrancheuseMelba);
        addQualification(managers, nicoleFortin, generalTrancheuseMelba);
        addQualification(managers, normandArsenault, generalTrancheuseMelba);
        addQualification(managers, liseHebert, generalTrancheuseMelba);
        addQualification(managers, deniseDaigneault, generalTrancheuseMelba);
        addQualification(managers, francineLabbe, generalTrancheuseMelba);
        addQualification(managers, claudineRochefort, generalTrancheuseMelba);
        addQualification(managers, suzanneCouturier, generalTrancheuseMelba);
        addQualification(managers, michelTougas, generalTrancheuseMelba);
        addQualification(managers, danielleBeaudry, generalTrancheuseMelba);
        addQualification(managers, lucieGuay, generalTrancheuseMelba);
        addQualification(managers, rogerDagenais, generalTrancheuseMelba);
        addQualification(managers, michelJeanneau, generalTrancheuseMelba);
        addQualification(managers, denisPilon, generalTrancheuseMelba);
        addQualification(managers, rachelBergevin, generalTrancheuseMelba);
        addQualification(managers, rejeanRoy, generalTrancheuseMelba);
        addQualification(managers, huguesDenault, generalTrancheuseMelba);
        addQualification(managers, rolandJrBoucher, generalTrancheuseMelba);
        addQualification(managers, bernardJolin, generalTrancheuseMelba);
        addQualification(managers, sartoTremblay, generalTrancheuseMelba);
        addQualification(managers, dianeGirard, generalTrancheuseMelba);
        addQualification(managers, marioPaille, generalTrancheuseMelba);
        addQualification(managers, ginetteOuellette, generalTrancheuseMelba);
        addQualification(managers, guylaineGuy, generalTrancheuseMelba);
        addQualification(managers, pierretteLamothe, generalTrancheuseMelba);
        addQualification(managers, michelineLegault, generalTrancheuseMelba);
        addQualification(managers, joseeConstantineau, generalTrancheuseMelba);
        addQualification(managers, madelaineMarleau, generalTrancheuseMelba);
        addQualification(managers, manonPoissant, generalTrancheuseMelba);
        addQualification(managers, francineLemieux, generalTrancheuseMelba);
        addQualification(managers, carmenBrais, generalTrancheuseMelba);
        addQualification(managers, francoiseTrudeau, generalTrancheuseMelba);
        addQualification(managers, ericRichard, generalTrancheuseMelba);
        addQualification(managers, nancyTheoret, generalTrancheuseMelba);
        addQualification(managers, liseCampeau, generalTrancheuseMelba);
        addQualification(managers, lucieLeavey, generalTrancheuseMelba);
        addQualification(managers, lyndaLajoie, generalTrancheuseMelba);
        addQualification(managers, jeanfrancoisBreton, generalTrancheuseMelba);
        addQualification(managers, stephaneJacques, generalTrancheuseMelba);
        addQualification(managers, brigitteBouchard, generalTrancheuseMelba);
        addQualification(managers, sylviePineault, generalTrancheuseMelba);
        addQualification(managers, joseeLapierre, generalTrancheuseMelba);
        addQualification(managers, hachezGabriel, generalTrancheuseMelba);
        addQualification(managers, sandraDupuis, generalTrancheuseMelba);
        addQualification(managers, lucieCaron, generalTrancheuseMelba);
        addQualification(managers, rachelMoise, generalTrancheuseMelba);
        addQualification(managers, catherinePiette, generalTrancheuseMelba);
        addQualification(managers, chantalXavier, generalTrancheuseMelba);
        addQualification(managers, dominicHoude, generalTrancheuseMelba);
        addQualification(managers, francoisParent, generalTrancheuseMelba);
        addQualification(managers, solangeGirard, generalTrancheuseMelba);
        addQualification(managers, martinLina, generalTrancheuseMelba);
        addQualification(managers, liseJoncas, generalTrancheuseMelba);
        addQualification(managers, nathalieReid, generalTrancheuseMelba);
        addQualification(managers, cecileCouillard, generalTrancheuseMelba);
        addQualification(managers, sylvainJulien, generalTrancheuseMelba);
        addQualification(managers, richardVaillant, generalTrancheuseMelba);
        addQualification(managers, franceBoyer, generalTrancheuseMelba);
        addQualification(managers, fxCotebrunet, generalTrancheuseMelba);
        addQualification(managers, isabelleLeclerc, generalTrancheuseMelba);
        addQualification(managers, francoisArcoite, generalTrancheuseMelba);
        addQualification(managers, sabrinaDupuis, generalTrancheuseMelba);
        addQualification(managers, mathieuGuy, generalTrancheuseMelba);
        addQualification(managers, daisyBourget, generalTrancheuseMelba);
        addQualification(managers, alexandreDube, generalTrancheuseMelba);
        addQualification(managers, annickPigeon, generalTrancheuseMelba);
        addQualification(managers, danielDuquette, generalTrancheuseMelba);
        addQualification(managers, johanneDuval, generalTrancheuseMelba);
        addQualification(managers, philippeLegault, generalTrancheuseMelba);
        addQualification(managers, celineVadnais, generalTrancheuseMelba);
        addQualification(managers, marcGrondin, generalTrancheuseMelba);
        addQualification(managers, ginetteLevesque, generalTrancheuseMelba);
        addQualification(managers, marcelLegault, generalTrancheuseMelba);
        addQualification(managers, nicolasLegault, generalTrancheuseMelba);
        addQualification(managers, stefanieReynolds, generalTrancheuseMelba);
        addQualification(managers, marioLongtin, generalTrancheuseMelba);
        addQualification(managers, robertAllen, generalTrancheuseMelba);
        addQualification(managers, louiseVeillette, generaleEmballageMelba);
        addQualification(managers, caroleMorand, generaleEmballageMelba);
        addQualification(managers, lucieGarceau, generaleEmballageMelba);
        addQualification(managers, huguetteLebel, generaleEmballageMelba);
        addQualification(managers, michelineDemers, generaleEmballageMelba);
        addQualification(managers, jeanpierreAuger, generaleEmballageMelba);
        addQualification(managers, dianeDugas, generaleEmballageMelba);
        addQualification(managers, moniqueLeblond, generaleEmballageMelba);
        addQualification(managers, realGosselin, generaleEmballageMelba);
        addQualification(managers, lucieLacoste, generaleEmballageMelba);
        addQualification(managers, johanneLemieux, generaleEmballageMelba);
        addQualification(managers, manonTremblay, generaleEmballageMelba);
        addQualification(managers, bernardBerube, generaleEmballageMelba);
        addQualification(managers, robertLazure, generaleEmballageMelba);
        addQualification(managers, lindaBoisvert, generaleEmballageMelba);
        addQualification(managers, sergeRobidoux, generaleEmballageMelba);
        addQualification(managers, michelDaniel, generaleEmballageMelba);
        addQualification(managers, caroleRaymond, generaleEmballageMelba);
        addQualification(managers, gillesGosselin, generaleEmballageMelba);
        addQualification(managers, francoisBeaulne, generaleEmballageMelba);
        addQualification(managers, francineGuerin, generaleEmballageMelba);
        addQualification(managers, jeanguyRicher, generaleEmballageMelba);
        addQualification(managers, marcelDalphond, generaleEmballageMelba);
        addQualification(managers, michelMeunier, generaleEmballageMelba);
        addQualification(managers, pierreLamarque, generaleEmballageMelba);
        addQualification(managers, gerardLanteigne, generaleEmballageMelba);
        addQualification(managers, francineDurivage, generaleEmballageMelba);
        addQualification(managers, jeanLatour, generaleEmballageMelba);
        addQualification(managers, pierretteDupras, generaleEmballageMelba);
        addQualification(managers, danielNault, generaleEmballageMelba);
        addQualification(managers, raynaldStarnaud, generaleEmballageMelba);
        addQualification(managers, nicoleFortin, generaleEmballageMelba);
        addQualification(managers, normandArsenault, generaleEmballageMelba);
        addQualification(managers, liseHebert, generaleEmballageMelba);
        addQualification(managers, deniseDaigneault, generaleEmballageMelba);
        addQualification(managers, francineLabbe, generaleEmballageMelba);
        addQualification(managers, claudineRochefort, generaleEmballageMelba);
        addQualification(managers, suzanneCouturier, generaleEmballageMelba);
        addQualification(managers, michelTougas, generaleEmballageMelba);
        addQualification(managers, danielleBeaudry, generaleEmballageMelba);
        addQualification(managers, lucieGuay, generaleEmballageMelba);
        addQualification(managers, rogerDagenais, generaleEmballageMelba);
        addQualification(managers, michelJeanneau, generaleEmballageMelba);
        addQualification(managers, denisPilon, generaleEmballageMelba);
        addQualification(managers, suzanneGagnon, generaleEmballageMelba);
        addQualification(managers, rachelBergevin, generaleEmballageMelba);
        addQualification(managers, rejeanRoy, generaleEmballageMelba);
        addQualification(managers, huguesDenault, generaleEmballageMelba);
        addQualification(managers, rolandJrBoucher, generaleEmballageMelba);
        addQualification(managers, bernardJolin, generaleEmballageMelba);
        addQualification(managers, sartoTremblay, generaleEmballageMelba);
        addQualification(managers, dianeGirard, generaleEmballageMelba);
        addQualification(managers, marioPaille, generaleEmballageMelba);
        addQualification(managers, ginetteOuellette, generaleEmballageMelba);
        addQualification(managers, guylaineGuy, generaleEmballageMelba);
        addQualification(managers, pierretteLamothe, generaleEmballageMelba);
        addQualification(managers, michelineLegault, generaleEmballageMelba);
        addQualification(managers, joseeConstantineau, generaleEmballageMelba);
        addQualification(managers, madelaineMarleau, generaleEmballageMelba);
        addQualification(managers, manonPoissant, generaleEmballageMelba);
        addQualification(managers, francineLemieux, generaleEmballageMelba);
        addQualification(managers, carmenBrais, generaleEmballageMelba);
        addQualification(managers, francoiseTrudeau, generaleEmballageMelba);
        addQualification(managers, ericRichard, generaleEmballageMelba);
        addQualification(managers, nancyTheoret, generaleEmballageMelba);
        addQualification(managers, liseCampeau, generaleEmballageMelba);
        addQualification(managers, lucieLeavey, generaleEmballageMelba);
        addQualification(managers, lyndaLajoie, generaleEmballageMelba);
        addQualification(managers, jeanfrancoisBreton, generaleEmballageMelba);
        addQualification(managers, stephaneJacques, generaleEmballageMelba);
        addQualification(managers, brigitteBouchard, generaleEmballageMelba);
        addQualification(managers, sylviePineault, generaleEmballageMelba);
        addQualification(managers, joseeLapierre, generaleEmballageMelba);
        addQualification(managers, hachezGabriel, generaleEmballageMelba);
        addQualification(managers, sandraDupuis, generaleEmballageMelba);
        addQualification(managers, lucieCaron, generaleEmballageMelba);
        addQualification(managers, rachelMoise, generaleEmballageMelba);
        addQualification(managers, catherinePiette, generaleEmballageMelba);
        addQualification(managers, chantalXavier, generaleEmballageMelba);
        addQualification(managers, dominicHoude, generaleEmballageMelba);
        addQualification(managers, francoisParent, generaleEmballageMelba);
        addQualification(managers, solangeGirard, generaleEmballageMelba);
        addQualification(managers, martinLina, generaleEmballageMelba);
        addQualification(managers, liseJoncas, generaleEmballageMelba);
        addQualification(managers, nathalieReid, generaleEmballageMelba);
        addQualification(managers, cecileCouillard, generaleEmballageMelba);
        addQualification(managers, sylvainJulien, generaleEmballageMelba);
        addQualification(managers, richardVaillant, generaleEmballageMelba);
        addQualification(managers, franceBoyer, generaleEmballageMelba);
        addQualification(managers, fxCotebrunet, generaleEmballageMelba);
        addQualification(managers, isabelleLeclerc, generaleEmballageMelba);
        addQualification(managers, francoisArcoite, generaleEmballageMelba);
        addQualification(managers, sabrinaDupuis, generaleEmballageMelba);
        addQualification(managers, mathieuGuy, generaleEmballageMelba);
        addQualification(managers, daisyBourget, generaleEmballageMelba);
        addQualification(managers, alexandreDube, generaleEmballageMelba);
        addQualification(managers, annickPigeon, generaleEmballageMelba);
        addQualification(managers, danielDuquette, generaleEmballageMelba);
        addQualification(managers, johanneDuval, generaleEmballageMelba);
        addQualification(managers, philippeLegault, generaleEmballageMelba);
        addQualification(managers, celineVadnais, generaleEmballageMelba);
        addQualification(managers, marcGrondin, generaleEmballageMelba);
        addQualification(managers, ginetteLevesque, generaleEmballageMelba);
        addQualification(managers, marcelLegault, generaleEmballageMelba);
        addQualification(managers, nicolasLegault, generaleEmballageMelba);
        addQualification(managers, stefanieReynolds, generaleEmballageMelba);
        addQualification(managers, marioLongtin, generaleEmballageMelba);
        addQualification(managers, robertAllen, generaleEmballageMelba);
        addQualification(managers, caroleMorand, operateurEmballageChap25lb);
        addQualification(managers, lucieGarceau, operateurEmballageChap25lb);
        addQualification(managers, pierreLamarque, operateurEmballageChap25lb);
        addQualification(managers, liseHebert, operateurEmballageChap25lb);
        addQualification(managers, francineLabbe, operateurEmballageChap25lb);
        addQualification(managers, rolandJrBoucher, operateurEmballageChap25lb);
        addQualification(managers, michelineLegault, operateurEmballageChap25lb);
        addQualification(managers, francineLemieux, operateurEmballageChap25lb);
        addQualification(managers, liseCampeau, operateurEmballageChap25lb);
        addQualification(managers, lucieLeavey, operateurEmballageChap25lb);
        addQualification(managers, joseeLapierre, operateurEmballageChap25lb);
        addQualification(managers, rachelMoise, operateurEmballageChap25lb);
        addQualification(managers, cecileCouillard, operateurEmballageChap25lb);
        addQualification(managers, francineLabbe, operateurRemplacementSnack);
        addQualification(managers, rachelBergevin, operateurRemplacementSnack);
        addQualification(managers, marioPaille, operateurRemplacementSnack);
        addQualification(managers, madelaineMarleau, operateurRemplacementSnack);
        addQualification(managers, ericRichard, operateurRemplacementSnack);
        addQualification(managers, liseCampeau, operateurRemplacementSnack);
        addQualification(managers, lucieLeavey, operateurRemplacementSnack);
        addQualification(managers, lyndaLajoie, operateurRemplacementSnack);
        addQualification(managers, joseeLapierre, operateurRemplacementSnack);
        addQualification(managers, francoisParent, operateurRemplacementSnack);
        addQualification(managers, alexandreDube, operateurRemplacementSnack);
        addQualification(managers, danielDuquette, operateurRemplacementSnack);
        addQualification(managers, caroleMorand, generaleEmballagePainsMinces);
        addQualification(managers, lucieGarceau, generaleEmballagePainsMinces);
        addQualification(managers, huguetteLebel, generaleEmballagePainsMinces);
        addQualification(managers, michelineDemers, generaleEmballagePainsMinces);
        addQualification(managers, jeanpierreAuger, generaleEmballagePainsMinces);
        addQualification(managers, dianeDugas, generaleEmballagePainsMinces);
        addQualification(managers, rejeanBrabant, generaleEmballagePainsMinces);
        addQualification(managers, moniqueLeblond, generaleEmballagePainsMinces);
        addQualification(managers, realGosselin, generaleEmballagePainsMinces);
        addQualification(managers, johanneLemieux, generaleEmballagePainsMinces);
        addQualification(managers, manonTremblay, generaleEmballagePainsMinces);
        addQualification(managers, bernardBerube, generaleEmballagePainsMinces);
        addQualification(managers, robertLazure, generaleEmballagePainsMinces);
        addQualification(managers, lindaBoisvert, generaleEmballagePainsMinces);
        addQualification(managers, sergeRobidoux, generaleEmballagePainsMinces);
        addQualification(managers, michelDaniel, generaleEmballagePainsMinces);
        addQualification(managers, caroleRaymond, generaleEmballagePainsMinces);
        addQualification(managers, gillesGosselin, generaleEmballagePainsMinces);
        addQualification(managers, francoisBeaulne, generaleEmballagePainsMinces);
        addQualification(managers, francineGuerin, generaleEmballagePainsMinces);
        addQualification(managers, jeanguyRicher, generaleEmballagePainsMinces);
        addQualification(managers, marcelDalphond, generaleEmballagePainsMinces);
        addQualification(managers, pierreLamarque, generaleEmballagePainsMinces);
        addQualification(managers, gerardLanteigne, generaleEmballagePainsMinces);
        addQualification(managers, francineDurivage, generaleEmballagePainsMinces);
        addQualification(managers, jeanLatour, generaleEmballagePainsMinces);
        addQualification(managers, pierretteDupras, generaleEmballagePainsMinces);
        addQualification(managers, danielNault, generaleEmballagePainsMinces);
        addQualification(managers, raynaldStarnaud, generaleEmballagePainsMinces);
        addQualification(managers, nicoleFortin, generaleEmballagePainsMinces);
        addQualification(managers, normandArsenault, generaleEmballagePainsMinces);
        addQualification(managers, liseHebert, generaleEmballagePainsMinces);
        addQualification(managers, deniseDaigneault, generaleEmballagePainsMinces);
        addQualification(managers, francineLabbe, generaleEmballagePainsMinces);
        addQualification(managers, claudineRochefort, generaleEmballagePainsMinces);
        addQualification(managers, suzanneCouturier, generaleEmballagePainsMinces);
        addQualification(managers, michelTougas, generaleEmballagePainsMinces);
        addQualification(managers, danielleBeaudry, generaleEmballagePainsMinces);
        addQualification(managers, lucieGuay, generaleEmballagePainsMinces);
        addQualification(managers, rogerDagenais, generaleEmballagePainsMinces);
        addQualification(managers, michelJeanneau, generaleEmballagePainsMinces);
        addQualification(managers, denisPilon, generaleEmballagePainsMinces);
        addQualification(managers, rachelBergevin, generaleEmballagePainsMinces);
        addQualification(managers, rejeanRoy, generaleEmballagePainsMinces);
        addQualification(managers, huguesDenault, generaleEmballagePainsMinces);
        addQualification(managers, rolandJrBoucher, generaleEmballagePainsMinces);
        addQualification(managers, bernardJolin, generaleEmballagePainsMinces);
        addQualification(managers, sartoTremblay, generaleEmballagePainsMinces);
        addQualification(managers, dianeGirard, generaleEmballagePainsMinces);
        addQualification(managers, marioPaille, generaleEmballagePainsMinces);
        addQualification(managers, ginetteOuellette, generaleEmballagePainsMinces);
        addQualification(managers, guylaineGuy, generaleEmballagePainsMinces);
        addQualification(managers, pierretteLamothe, generaleEmballagePainsMinces);
        addQualification(managers, michelineLegault, generaleEmballagePainsMinces);
        addQualification(managers, joseeConstantineau, generaleEmballagePainsMinces);
        addQualification(managers, madelaineMarleau, generaleEmballagePainsMinces);
        addQualification(managers, manonPoissant, generaleEmballagePainsMinces);
        addQualification(managers, francineLemieux, generaleEmballagePainsMinces);
        addQualification(managers, carmenBrais, generaleEmballagePainsMinces);
        addQualification(managers, francoiseTrudeau, generaleEmballagePainsMinces);
        addQualification(managers, ericRichard, generaleEmballagePainsMinces);
        addQualification(managers, nancyTheoret, generaleEmballagePainsMinces);
        addQualification(managers, liseCampeau, generaleEmballagePainsMinces);
        addQualification(managers, lucieLeavey, generaleEmballagePainsMinces);
        addQualification(managers, lyndaLajoie, generaleEmballagePainsMinces);
        addQualification(managers, jeanfrancoisBreton, generaleEmballagePainsMinces);
        addQualification(managers, stephaneJacques, generaleEmballagePainsMinces);
        addQualification(managers, brigitteBouchard, generaleEmballagePainsMinces);
        addQualification(managers, sylviePineault, generaleEmballagePainsMinces);
        addQualification(managers, joseeLapierre, generaleEmballagePainsMinces);
        addQualification(managers, hachezGabriel, generaleEmballagePainsMinces);
        addQualification(managers, sandraDupuis, generaleEmballagePainsMinces);
        addQualification(managers, lucieCaron, generaleEmballagePainsMinces);
        addQualification(managers, rachelMoise, generaleEmballagePainsMinces);
        addQualification(managers, catherinePiette, generaleEmballagePainsMinces);
        addQualification(managers, chantalXavier, generaleEmballagePainsMinces);
        addQualification(managers, dominicHoude, generaleEmballagePainsMinces);
        addQualification(managers, francoisParent, generaleEmballagePainsMinces);
        addQualification(managers, solangeGirard, generaleEmballagePainsMinces);
        addQualification(managers, martinLina, generaleEmballagePainsMinces);
        addQualification(managers, liseJoncas, generaleEmballagePainsMinces);
        addQualification(managers, nathalieReid, generaleEmballagePainsMinces);
        addQualification(managers, cecileCouillard, generaleEmballagePainsMinces);
        addQualification(managers, sylvainJulien, generaleEmballagePainsMinces);
        addQualification(managers, richardVaillant, generaleEmballagePainsMinces);
        addQualification(managers, franceBoyer, generaleEmballagePainsMinces);
        addQualification(managers, fxCotebrunet, generaleEmballagePainsMinces);
        addQualification(managers, isabelleLeclerc, generaleEmballagePainsMinces);
        addQualification(managers, francoisArcoite, generaleEmballagePainsMinces);
        addQualification(managers, sabrinaDupuis, generaleEmballagePainsMinces);
        addQualification(managers, mathieuGuy, generaleEmballagePainsMinces);
        addQualification(managers, daisyBourget, generaleEmballagePainsMinces);
        addQualification(managers, alexandreDube, generaleEmballagePainsMinces);
        addQualification(managers, annickPigeon, generaleEmballagePainsMinces);
        addQualification(managers, danielDuquette, generaleEmballagePainsMinces);
        addQualification(managers, johanneDuval, generaleEmballagePainsMinces);
        addQualification(managers, philippeLegault, generaleEmballagePainsMinces);
        addQualification(managers, celineVadnais, generaleEmballagePainsMinces);
        addQualification(managers, marcGrondin, generaleEmballagePainsMinces);
        addQualification(managers, ginetteLevesque, generaleEmballagePainsMinces);
        addQualification(managers, marcelLegault, generaleEmballagePainsMinces);
        addQualification(managers, nicolasLegault, generaleEmballagePainsMinces);
        addQualification(managers, stefanieReynolds, generaleEmballagePainsMinces);
        addQualification(managers, marioLongtin, generaleEmballagePainsMinces);
        addQualification(managers, robertAllen, generaleEmballagePainsMinces);
        addQualification(managers, caroleMorand, generaleEmballageCroutonsVrac);
        addQualification(managers, lucieGarceau, generaleEmballageCroutonsVrac);
        addQualification(managers, huguetteLebel, generaleEmballageCroutonsVrac);
        addQualification(managers, michelineDemers, generaleEmballageCroutonsVrac);
        addQualification(managers, jeanpierreAuger, generaleEmballageCroutonsVrac);
        addQualification(managers, dianeDugas, generaleEmballageCroutonsVrac);
        addQualification(managers, rejeanBrabant, generaleEmballageCroutonsVrac);
        addQualification(managers, moniqueLeblond, generaleEmballageCroutonsVrac);
        addQualification(managers, realGosselin, generaleEmballageCroutonsVrac);
        addQualification(managers, johanneLemieux, generaleEmballageCroutonsVrac);
        addQualification(managers, manonTremblay, generaleEmballageCroutonsVrac);
        addQualification(managers, bernardBerube, generaleEmballageCroutonsVrac);
        addQualification(managers, robertLazure, generaleEmballageCroutonsVrac);
        addQualification(managers, lindaBoisvert, generaleEmballageCroutonsVrac);
        addQualification(managers, sergeRobidoux, generaleEmballageCroutonsVrac);
        addQualification(managers, michelDaniel, generaleEmballageCroutonsVrac);
        addQualification(managers, caroleRaymond, generaleEmballageCroutonsVrac);
        addQualification(managers, gillesGosselin, generaleEmballageCroutonsVrac);
        addQualification(managers, francoisBeaulne, generaleEmballageCroutonsVrac);
        addQualification(managers, francineGuerin, generaleEmballageCroutonsVrac);
        addQualification(managers, jeanguyRicher, generaleEmballageCroutonsVrac);
        addQualification(managers, marcelDalphond, generaleEmballageCroutonsVrac);
        addQualification(managers, pierreLamarque, generaleEmballageCroutonsVrac);
        addQualification(managers, gerardLanteigne, generaleEmballageCroutonsVrac);
        addQualification(managers, francineDurivage, generaleEmballageCroutonsVrac);
        addQualification(managers, jeanLatour, generaleEmballageCroutonsVrac);
        addQualification(managers, pierretteDupras, generaleEmballageCroutonsVrac);
        addQualification(managers, danielNault, generaleEmballageCroutonsVrac);
        addQualification(managers, raynaldStarnaud, generaleEmballageCroutonsVrac);
        addQualification(managers, nicoleFortin, generaleEmballageCroutonsVrac);
        addQualification(managers, normandArsenault, generaleEmballageCroutonsVrac);
        addQualification(managers, liseHebert, generaleEmballageCroutonsVrac);
        addQualification(managers, deniseDaigneault, generaleEmballageCroutonsVrac);
        addQualification(managers, francineLabbe, generaleEmballageCroutonsVrac);
        addQualification(managers, claudineRochefort, generaleEmballageCroutonsVrac);
        addQualification(managers, suzanneCouturier, generaleEmballageCroutonsVrac);
        addQualification(managers, michelTougas, generaleEmballageCroutonsVrac);
        addQualification(managers, danielleBeaudry, generaleEmballageCroutonsVrac);
        addQualification(managers, lucieGuay, generaleEmballageCroutonsVrac);
        addQualification(managers, rogerDagenais, generaleEmballageCroutonsVrac);
        addQualification(managers, michelJeanneau, generaleEmballageCroutonsVrac);
        addQualification(managers, denisPilon, generaleEmballageCroutonsVrac);
        addQualification(managers, rachelBergevin, generaleEmballageCroutonsVrac);
        addQualification(managers, rejeanRoy, generaleEmballageCroutonsVrac);
        addQualification(managers, huguesDenault, generaleEmballageCroutonsVrac);
        addQualification(managers, rolandJrBoucher, generaleEmballageCroutonsVrac);
        addQualification(managers, bernardJolin, generaleEmballageCroutonsVrac);
        addQualification(managers, sartoTremblay, generaleEmballageCroutonsVrac);
        addQualification(managers, dianeGirard, generaleEmballageCroutonsVrac);
        addQualification(managers, marioPaille, generaleEmballageCroutonsVrac);
        addQualification(managers, ginetteOuellette, generaleEmballageCroutonsVrac);
        addQualification(managers, guylaineGuy, generaleEmballageCroutonsVrac);
        addQualification(managers, pierretteLamothe, generaleEmballageCroutonsVrac);
        addQualification(managers, michelineLegault, generaleEmballageCroutonsVrac);
        addQualification(managers, joseeConstantineau, generaleEmballageCroutonsVrac);
        addQualification(managers, madelaineMarleau, generaleEmballageCroutonsVrac);
        addQualification(managers, manonPoissant, generaleEmballageCroutonsVrac);
        addQualification(managers, francineLemieux, generaleEmballageCroutonsVrac);
        addQualification(managers, carmenBrais, generaleEmballageCroutonsVrac);
        addQualification(managers, francoiseTrudeau, generaleEmballageCroutonsVrac);
        addQualification(managers, ericRichard, generaleEmballageCroutonsVrac);
        addQualification(managers, nancyTheoret, generaleEmballageCroutonsVrac);
        addQualification(managers, liseCampeau, generaleEmballageCroutonsVrac);
        addQualification(managers, lucieLeavey, generaleEmballageCroutonsVrac);
        addQualification(managers, lyndaLajoie, generaleEmballageCroutonsVrac);
        addQualification(managers, jeanfrancoisBreton, generaleEmballageCroutonsVrac);
        addQualification(managers, stephaneJacques, generaleEmballageCroutonsVrac);
        addQualification(managers, brigitteBouchard, generaleEmballageCroutonsVrac);
        addQualification(managers, sylviePineault, generaleEmballageCroutonsVrac);
        addQualification(managers, joseeLapierre, generaleEmballageCroutonsVrac);
        addQualification(managers, hachezGabriel, generaleEmballageCroutonsVrac);
        addQualification(managers, sandraDupuis, generaleEmballageCroutonsVrac);
        addQualification(managers, lucieCaron, generaleEmballageCroutonsVrac);
        addQualification(managers, rachelMoise, generaleEmballageCroutonsVrac);
        addQualification(managers, catherinePiette, generaleEmballageCroutonsVrac);
        addQualification(managers, chantalXavier, generaleEmballageCroutonsVrac);
        addQualification(managers, dominicHoude, generaleEmballageCroutonsVrac);
        addQualification(managers, francoisParent, generaleEmballageCroutonsVrac);
        addQualification(managers, solangeGirard, generaleEmballageCroutonsVrac);
        addQualification(managers, martinLina, generaleEmballageCroutonsVrac);
        addQualification(managers, liseJoncas, generaleEmballageCroutonsVrac);
        addQualification(managers, nathalieReid, generaleEmballageCroutonsVrac);
        addQualification(managers, cecileCouillard, generaleEmballageCroutonsVrac);
        addQualification(managers, sylvainJulien, generaleEmballageCroutonsVrac);
        addQualification(managers, richardVaillant, generaleEmballageCroutonsVrac);
        addQualification(managers, franceBoyer, generaleEmballageCroutonsVrac);
        addQualification(managers, fxCotebrunet, generaleEmballageCroutonsVrac);
        addQualification(managers, isabelleLeclerc, generaleEmballageCroutonsVrac);
        addQualification(managers, francoisArcoite, generaleEmballageCroutonsVrac);
        addQualification(managers, sabrinaDupuis, generaleEmballageCroutonsVrac);
        addQualification(managers, mathieuGuy, generaleEmballageCroutonsVrac);
        addQualification(managers, daisyBourget, generaleEmballageCroutonsVrac);
        addQualification(managers, alexandreDube, generaleEmballageCroutonsVrac);
        addQualification(managers, annickPigeon, generaleEmballageCroutonsVrac);
        addQualification(managers, danielDuquette, generaleEmballageCroutonsVrac);
        addQualification(managers, johanneDuval, generaleEmballageCroutonsVrac);
        addQualification(managers, philippeLegault, generaleEmballageCroutonsVrac);
        addQualification(managers, celineVadnais, generaleEmballageCroutonsVrac);
        addQualification(managers, marcGrondin, generaleEmballageCroutonsVrac);
        addQualification(managers, ginetteLevesque, generaleEmballageCroutonsVrac);
        addQualification(managers, marcelLegault, generaleEmballageCroutonsVrac);
        addQualification(managers, nicolasLegault, generaleEmballageCroutonsVrac);
        addQualification(managers, stefanieReynolds, generaleEmballageCroutonsVrac);
        addQualification(managers, marioLongtin, generaleEmballageCroutonsVrac);
        addQualification(managers, robertAllen, generaleEmballageCroutonsVrac);
        addQualification(managers, caroleMorand, generalEmballageSnackBoite);
        addQualification(managers, lucieGarceau, generalEmballageSnackBoite);
        addQualification(managers, huguetteLebel, generalEmballageSnackBoite);
        addQualification(managers, michelineDemers, generalEmballageSnackBoite);
        addQualification(managers, jeanpierreAuger, generalEmballageSnackBoite);
        addQualification(managers, dianeDugas, generalEmballageSnackBoite);
        addQualification(managers, rejeanBrabant, generalEmballageSnackBoite);
        addQualification(managers, moniqueLeblond, generalEmballageSnackBoite);
        addQualification(managers, realGosselin, generalEmballageSnackBoite);
        addQualification(managers, johanneLemieux, generalEmballageSnackBoite);
        addQualification(managers, manonTremblay, generalEmballageSnackBoite);
        addQualification(managers, bernardBerube, generalEmballageSnackBoite);
        addQualification(managers, robertLazure, generalEmballageSnackBoite);
        addQualification(managers, lindaBoisvert, generalEmballageSnackBoite);
        addQualification(managers, sergeRobidoux, generalEmballageSnackBoite);
        addQualification(managers, michelDaniel, generalEmballageSnackBoite);
        addQualification(managers, caroleRaymond, generalEmballageSnackBoite);
        addQualification(managers, gillesGosselin, generalEmballageSnackBoite);
        addQualification(managers, francoisBeaulne, generalEmballageSnackBoite);
        addQualification(managers, francineGuerin, generalEmballageSnackBoite);
        addQualification(managers, jeanguyRicher, generalEmballageSnackBoite);
        addQualification(managers, marcelDalphond, generalEmballageSnackBoite);
        addQualification(managers, pierreLamarque, generalEmballageSnackBoite);
        addQualification(managers, gerardLanteigne, generalEmballageSnackBoite);
        addQualification(managers, francineDurivage, generalEmballageSnackBoite);
        addQualification(managers, jeanLatour, generalEmballageSnackBoite);
        addQualification(managers, pierretteDupras, generalEmballageSnackBoite);
        addQualification(managers, danielNault, generalEmballageSnackBoite);
        addQualification(managers, raynaldStarnaud, generalEmballageSnackBoite);
        addQualification(managers, nicoleFortin, generalEmballageSnackBoite);
        addQualification(managers, normandArsenault, generalEmballageSnackBoite);
        addQualification(managers, liseHebert, generalEmballageSnackBoite);
        addQualification(managers, deniseDaigneault, generalEmballageSnackBoite);
        addQualification(managers, francineLabbe, generalEmballageSnackBoite);
        addQualification(managers, claudineRochefort, generalEmballageSnackBoite);
        addQualification(managers, suzanneCouturier, generalEmballageSnackBoite);
        addQualification(managers, michelTougas, generalEmballageSnackBoite);
        addQualification(managers, danielleBeaudry, generalEmballageSnackBoite);
        addQualification(managers, lucieGuay, generalEmballageSnackBoite);
        addQualification(managers, rogerDagenais, generalEmballageSnackBoite);
        addQualification(managers, michelJeanneau, generalEmballageSnackBoite);
        addQualification(managers, denisPilon, generalEmballageSnackBoite);
        addQualification(managers, suzanneGagnon, generalEmballageSnackBoite);
        addQualification(managers, rachelBergevin, generalEmballageSnackBoite);
        addQualification(managers, rejeanRoy, generalEmballageSnackBoite);
        addQualification(managers, huguesDenault, generalEmballageSnackBoite);
        addQualification(managers, rolandJrBoucher, generalEmballageSnackBoite);
        addQualification(managers, bernardJolin, generalEmballageSnackBoite);
        addQualification(managers, sartoTremblay, generalEmballageSnackBoite);
        addQualification(managers, dianeGirard, generalEmballageSnackBoite);
        addQualification(managers, marioPaille, generalEmballageSnackBoite);
        addQualification(managers, ginetteOuellette, generalEmballageSnackBoite);
        addQualification(managers, guylaineGuy, generalEmballageSnackBoite);
        addQualification(managers, pierretteLamothe, generalEmballageSnackBoite);
        addQualification(managers, michelineLegault, generalEmballageSnackBoite);
        addQualification(managers, joseeConstantineau, generalEmballageSnackBoite);
        addQualification(managers, madelaineMarleau, generalEmballageSnackBoite);
        addQualification(managers, manonPoissant, generalEmballageSnackBoite);
        addQualification(managers, francineLemieux, generalEmballageSnackBoite);
        addQualification(managers, carmenBrais, generalEmballageSnackBoite);
        addQualification(managers, francoiseTrudeau, generalEmballageSnackBoite);
        addQualification(managers, ericRichard, generalEmballageSnackBoite);
        addQualification(managers, nancyTheoret, generalEmballageSnackBoite);
        addQualification(managers, liseCampeau, generalEmballageSnackBoite);
        addQualification(managers, lucieLeavey, generalEmballageSnackBoite);
        addQualification(managers, lyndaLajoie, generalEmballageSnackBoite);
        addQualification(managers, jeanfrancoisBreton, generalEmballageSnackBoite);
        addQualification(managers, stephaneJacques, generalEmballageSnackBoite);
        addQualification(managers, brigitteBouchard, generalEmballageSnackBoite);
        addQualification(managers, sylviePineault, generalEmballageSnackBoite);
        addQualification(managers, joseeLapierre, generalEmballageSnackBoite);
        addQualification(managers, hachezGabriel, generalEmballageSnackBoite);
        addQualification(managers, sandraDupuis, generalEmballageSnackBoite);
        addQualification(managers, lucieCaron, generalEmballageSnackBoite);
        addQualification(managers, rachelMoise, generalEmballageSnackBoite);
        addQualification(managers, catherinePiette, generalEmballageSnackBoite);
        addQualification(managers, chantalXavier, generalEmballageSnackBoite);
        addQualification(managers, dominicHoude, generalEmballageSnackBoite);
        addQualification(managers, francoisParent, generalEmballageSnackBoite);
        addQualification(managers, solangeGirard, generalEmballageSnackBoite);
        addQualification(managers, martinLina, generalEmballageSnackBoite);
        addQualification(managers, liseJoncas, generalEmballageSnackBoite);
        addQualification(managers, nathalieReid, generalEmballageSnackBoite);
        addQualification(managers, cecileCouillard, generalEmballageSnackBoite);
        addQualification(managers, sylvainJulien, generalEmballageSnackBoite);
        addQualification(managers, richardVaillant, generalEmballageSnackBoite);
        addQualification(managers, franceBoyer, generalEmballageSnackBoite);
        addQualification(managers, fxCotebrunet, generalEmballageSnackBoite);
        addQualification(managers, isabelleLeclerc, generalEmballageSnackBoite);
        addQualification(managers, francoisArcoite, generalEmballageSnackBoite);
        addQualification(managers, sabrinaDupuis, generalEmballageSnackBoite);
        addQualification(managers, mathieuGuy, generalEmballageSnackBoite);
        addQualification(managers, daisyBourget, generalEmballageSnackBoite);
        addQualification(managers, alexandreDube, generalEmballageSnackBoite);
        addQualification(managers, annickPigeon, generalEmballageSnackBoite);
        addQualification(managers, danielDuquette, generalEmballageSnackBoite);
        addQualification(managers, johanneDuval, generalEmballageSnackBoite);
        addQualification(managers, philippeLegault, generalEmballageSnackBoite);
        addQualification(managers, celineVadnais, generalEmballageSnackBoite);
        addQualification(managers, marcGrondin, generalEmballageSnackBoite);
        addQualification(managers, ginetteLevesque, generalEmballageSnackBoite);
        addQualification(managers, marcelLegault, generalEmballageSnackBoite);
        addQualification(managers, nicolasLegault, generalEmballageSnackBoite);
        addQualification(managers, stefanieReynolds, generalEmballageSnackBoite);
        addQualification(managers, marioLongtin, generalEmballageSnackBoite);
        addQualification(managers, robertAllen, generalEmballageSnackBoite);
        addQualification(managers, michelMeunier, operateurEnsacheuseVerticalSnack);
        addQualification(managers, pierreLamarque, operateurEnsacheuseVerticalSnack);
        addQualification(managers, rolandJrBoucher, operateurEnsacheuseVerticalSnack);
        addQualification(managers, liseCampeau, operateurEnsacheuseVerticalSnack);
        addQualification(managers, lucieLeavey, operateurEnsacheuseVerticalSnack);
        addQualification(managers, joseeLapierre, operateurEnsacheuseVerticalSnack);
        addQualification(managers, danielDuquette, operateurEnsacheuseVerticalSnack);
        addQualification(managers, rejeanBrabant, operateurPetrisseurSnack);
        addQualification(managers, marcelDalphond, operateurPetrisseurSnack);
        addQualification(managers, francineLabbe, operateurPetrisseurSnack);
        addQualification(managers, suzanneGagnon, operateurPetrisseurSnack);
        addQualification(managers, rachelBergevin, operateurPetrisseurSnack);
        addQualification(managers, rolandJrBoucher, operateurPetrisseurSnack);
        addQualification(managers, ericRichard, operateurPetrisseurSnack);
        addQualification(managers, lucieLeavey, operateurPetrisseurSnack);
        addQualification(managers, lyndaLajoie, operateurPetrisseurSnack);
        addQualification(managers, richardVaillant, operateurPetrisseurSnack);
        addQualification(managers, francoisArcoite, operateurPetrisseurSnack);
        addQualification(managers, alexandreDube, operateurPetrisseurSnack);
        addQualification(managers, michelDaniel, preposeSalubrite);
        addQualification(managers, michelTougas, preposeSalubrite);
        addQualification(managers, denisPilon, preposeSalubrite);
        addQualification(managers, guylaineGuy, preposeSalubrite);
        addQualification(managers, joseeConstantineau, preposeSalubrite);
        addQualification(managers, ericRichard, preposeSalubrite);
        addQualification(managers, liseCampeau, preposeSalubrite);
        addQualification(managers, lyndaLajoie, preposeSalubrite);
        addQualification(managers, hachezGabriel, preposeSalubrite);
        addQualification(managers, rachelMoise, preposeSalubrite);
        addQualification(managers, solangeGirard, preposeSalubrite);
        addQualification(managers, isabelleLeclerc, preposeSalubrite);
        addQualification(managers, alexandreDube, preposeSalubrite);
        addQualification(managers, caroleMorand, generaleSalubrite);
        addQualification(managers, lucieGarceau, generaleSalubrite);
        addQualification(managers, huguetteLebel, generaleSalubrite);
        addQualification(managers, michelineDemers, generaleSalubrite);
        addQualification(managers, jeanpierreAuger, generaleSalubrite);
        addQualification(managers, dianeDugas, generaleSalubrite);
        addQualification(managers, moniqueLeblond, generaleSalubrite);
        addQualification(managers, realGosselin, generaleSalubrite);
        addQualification(managers, johanneLemieux, generaleSalubrite);
        addQualification(managers, manonTremblay, generaleSalubrite);
        addQualification(managers, bernardBerube, generaleSalubrite);
        addQualification(managers, robertLazure, generaleSalubrite);
        addQualification(managers, lindaBoisvert, generaleSalubrite);
        addQualification(managers, sergeRobidoux, generaleSalubrite);
        addQualification(managers, michelDaniel, generaleSalubrite);
        addQualification(managers, caroleRaymond, generaleSalubrite);
        addQualification(managers, gillesGosselin, generaleSalubrite);
        addQualification(managers, francoisBeaulne, generaleSalubrite);
        addQualification(managers, francineGuerin, generaleSalubrite);
        addQualification(managers, jeanguyRicher, generaleSalubrite);
        addQualification(managers, marcelDalphond, generaleSalubrite);
        addQualification(managers, pierreLamarque, generaleSalubrite);
        addQualification(managers, gerardLanteigne, generaleSalubrite);
        addQualification(managers, francineDurivage, generaleSalubrite);
        addQualification(managers, jeanLatour, generaleSalubrite);
        addQualification(managers, pierretteDupras, generaleSalubrite);
        addQualification(managers, danielNault, generaleSalubrite);
        addQualification(managers, raynaldStarnaud, generaleSalubrite);
        addQualification(managers, nicoleFortin, generaleSalubrite);
        addQualification(managers, normandArsenault, generaleSalubrite);
        addQualification(managers, liseHebert, generaleSalubrite);
        addQualification(managers, deniseDaigneault, generaleSalubrite);
        addQualification(managers, francineLabbe, generaleSalubrite);
        addQualification(managers, claudineRochefort, generaleSalubrite);
        addQualification(managers, suzanneCouturier, generaleSalubrite);
        addQualification(managers, michelTougas, generaleSalubrite);
        addQualification(managers, danielleBeaudry, generaleSalubrite);
        addQualification(managers, lucieGuay, generaleSalubrite);
        addQualification(managers, rogerDagenais, generaleSalubrite);
        addQualification(managers, michelJeanneau, generaleSalubrite);
        addQualification(managers, denisPilon, generaleSalubrite);
        addQualification(managers, rachelBergevin, generaleSalubrite);
        addQualification(managers, rejeanRoy, generaleSalubrite);
        addQualification(managers, huguesDenault, generaleSalubrite);
        addQualification(managers, rolandJrBoucher, generaleSalubrite);
        addQualification(managers, bernardJolin, generaleSalubrite);
        addQualification(managers, sartoTremblay, generaleSalubrite);
        addQualification(managers, dianeGirard, generaleSalubrite);
        addQualification(managers, marioPaille, generaleSalubrite);
        addQualification(managers, ginetteOuellette, generaleSalubrite);
        addQualification(managers, guylaineGuy, generaleSalubrite);
        addQualification(managers, pierretteLamothe, generaleSalubrite);
        addQualification(managers, michelineLegault, generaleSalubrite);
        addQualification(managers, joseeConstantineau, generaleSalubrite);
        addQualification(managers, madelaineMarleau, generaleSalubrite);
        addQualification(managers, manonPoissant, generaleSalubrite);
        addQualification(managers, francineLemieux, generaleSalubrite);
        addQualification(managers, carmenBrais, generaleSalubrite);
        addQualification(managers, francoiseTrudeau, generaleSalubrite);
        addQualification(managers, ericRichard, generaleSalubrite);
        addQualification(managers, nancyTheoret, generaleSalubrite);
        addQualification(managers, liseCampeau, generaleSalubrite);
        addQualification(managers, lucieLeavey, generaleSalubrite);
        addQualification(managers, lyndaLajoie, generaleSalubrite);
        addQualification(managers, jeanfrancoisBreton, generaleSalubrite);
        addQualification(managers, stephaneJacques, generaleSalubrite);
        addQualification(managers, brigitteBouchard, generaleSalubrite);
        addQualification(managers, sylviePineault, generaleSalubrite);
        addQualification(managers, joseeLapierre, generaleSalubrite);
        addQualification(managers, hachezGabriel, generaleSalubrite);
        addQualification(managers, sandraDupuis, generaleSalubrite);
        addQualification(managers, lucieCaron, generaleSalubrite);
        addQualification(managers, rachelMoise, generaleSalubrite);
        addQualification(managers, catherinePiette, generaleSalubrite);
        addQualification(managers, chantalXavier, generaleSalubrite);
        addQualification(managers, dominicHoude, generaleSalubrite);
        addQualification(managers, francoisParent, generaleSalubrite);
        addQualification(managers, solangeGirard, generaleSalubrite);
        addQualification(managers, martinLina, generaleSalubrite);
        addQualification(managers, liseJoncas, generaleSalubrite);
        addQualification(managers, nathalieReid, generaleSalubrite);
        addQualification(managers, cecileCouillard, generaleSalubrite);
        addQualification(managers, sylvainJulien, generaleSalubrite);
        addQualification(managers, richardVaillant, generaleSalubrite);
        addQualification(managers, franceBoyer, generaleSalubrite);
        addQualification(managers, fxCotebrunet, generaleSalubrite);
        addQualification(managers, isabelleLeclerc, generaleSalubrite);
        addQualification(managers, francoisArcoite, generaleSalubrite);
        addQualification(managers, sabrinaDupuis, generaleSalubrite);
        addQualification(managers, mathieuGuy, generaleSalubrite);
        addQualification(managers, daisyBourget, generaleSalubrite);
        addQualification(managers, alexandreDube, generaleSalubrite);
        addQualification(managers, annickPigeon, generaleSalubrite);
        addQualification(managers, danielDuquette, generaleSalubrite);
        addQualification(managers, johanneDuval, generaleSalubrite);
        addQualification(managers, philippeLegault, generaleSalubrite);
        addQualification(managers, celineVadnais, generaleSalubrite);
        addQualification(managers, marcGrondin, generaleSalubrite);
        addQualification(managers, ginetteLevesque, generaleSalubrite);
        addQualification(managers, marcelLegault, generaleSalubrite);
        addQualification(managers, nicolasLegault, generaleSalubrite);
        addQualification(managers, stefanieReynolds, generaleSalubrite);
        addQualification(managers, marioLongtin, generaleSalubrite);
        addQualification(managers, robertAllen, generaleSalubrite);
        addQualification(managers, realGosselin, operateurGerbeuseVerification);
        addQualification(managers, robertLazure, operateurGerbeuseVerification);
        addQualification(managers, gillesGosselin, operateurGerbeuseVerification);
        addQualification(managers, jeanLatour, operateurGerbeuseVerification);
        addQualification(managers, raynaldStarnaud, operateurGerbeuseVerification);
        addQualification(managers, rejeanRoy, operateurGerbeuseVerification);
        addQualification(managers, rolandJrBoucher, operateurGerbeuseVerification);
        addQualification(managers, marioPaille, operateurGerbeuseVerification);
        addQualification(managers, ericRichard, operateurGerbeuseVerification);
        addQualification(managers, stephaneJacques, operateurGerbeuseVerification);
        addQualification(managers, martinDube, operateurGerbeuseVerification);
        addQualification(managers, francoisArcoite, operateurGerbeuseVerification);
        addQualification(managers, mathieuGuy, operateurGerbeuseVerification);
        addQualification(managers, marcGrondin, operateurGerbeuseVerification);
        addQualification(managers, lucieGarceau, preposeAuxEpicesEtReparation);
        addQualification(managers, huguetteLebel, preposeAuxEpicesEtReparation);
        addQualification(managers, lindaBoisvert, preposeAuxEpicesEtReparation);
        addQualification(managers, francineGuerin, preposeAuxEpicesEtReparation);
        addQualification(managers, francineDurivage, preposeAuxEpicesEtReparation);
        addQualification(managers, sylvainJulien, electrotechniciens);
        addQualification(managers, ivanhoeMaisonneuve, electrotechniciens);
        addQualification(managers, ginoLemoine, electrotechniciens);
        addQualification(managers, donaldTheriault, mecanicien);
        addQualification(managers, marcBellemare, mecanicien);
        addQualification(managers, lucRoy, mecanicien);
        addQualification(managers, sylvainCarriere, mecanicien);
        addQualification(managers, denisBerube, mecanicien);
        addQualification(managers, mathewBellemare, mecanicien);
        addQualification(managers, michelMeunier, huileurGraisseurEntretienPreventif);
        addQualification(managers, donaldTheriault, formation);
        addQualification(managers, louiseVeillette, formation);
        addQualification(managers, caroleMorand, formation);
        addQualification(managers, lucieGarceau, formation);
        addQualification(managers, huguetteLebel, formation);
        addQualification(managers, michelineDemers, formation);
        addQualification(managers, jeanpierreAuger, formation);
        addQualification(managers, dianeDugas, formation);
        addQualification(managers, rejeanBrabant, formation);
        addQualification(managers, moniqueLeblond, formation);
        addQualification(managers, realGosselin, formation);
        addQualification(managers, lucieLacoste, formation);
        addQualification(managers, johanneLemieux, formation);
        addQualification(managers, manonTremblay, formation);
        addQualification(managers, bernardBerube, formation);
        addQualification(managers, robertLazure, formation);
        addQualification(managers, lindaBoisvert, formation);
        addQualification(managers, sergeRobidoux, formation);
        addQualification(managers, michelDaniel, formation);
        addQualification(managers, caroleRaymond, formation);
        addQualification(managers, gillesGosselin, formation);
        addQualification(managers, francoisBeaulne, formation);
        addQualification(managers, francineGuerin, formation);
        addQualification(managers, jeanguyRicher, formation);
        addQualification(managers, marcelDalphond, formation);
        addQualification(managers, michelMeunier, formation);
        addQualification(managers, pierreLamarque, formation);
        addQualification(managers, gerardLanteigne, formation);
        addQualification(managers, francineDurivage, formation);
        addQualification(managers, jeanLatour, formation);
        addQualification(managers, pierretteDupras, formation);
        addQualification(managers, danielNault, formation);
        addQualification(managers, raynaldStarnaud, formation);
        addQualification(managers, nicoleFortin, formation);
        addQualification(managers, normandArsenault, formation);
        addQualification(managers, liseHebert, formation);
        addQualification(managers, deniseDaigneault, formation);
        addQualification(managers, francineLabbe, formation);
        addQualification(managers, claudineRochefort, formation);
        addQualification(managers, suzanneCouturier, formation);
        addQualification(managers, michelTougas, formation);
        addQualification(managers, danielleBeaudry, formation);
        addQualification(managers, lucieGuay, formation);
        addQualification(managers, rogerDagenais, formation);
        addQualification(managers, michelJeanneau, formation);
        addQualification(managers, denisPilon, formation);
        addQualification(managers, suzanneGagnon, formation);
        addQualification(managers, rachelBergevin, formation);
        addQualification(managers, rejeanRoy, formation);
        addQualification(managers, huguesDenault, formation);
        addQualification(managers, rolandJrBoucher, formation);
        addQualification(managers, bernardJolin, formation);
        addQualification(managers, sartoTremblay, formation);
        addQualification(managers, dianeGirard, formation);
        addQualification(managers, marioPaille, formation);
        addQualification(managers, ginetteOuellette, formation);
        addQualification(managers, guylaineGuy, formation);
        addQualification(managers, pierretteLamothe, formation);
        addQualification(managers, marcBellemare, formation);
        addQualification(managers, michelineLegault, formation);
        addQualification(managers, joseeConstantineau, formation);
        addQualification(managers, madelaineMarleau, formation);
        addQualification(managers, manonPoissant, formation);
        addQualification(managers, francineLemieux, formation);
        addQualification(managers, carmenBrais, formation);
        addQualification(managers, francoiseTrudeau, formation);
        addQualification(managers, ericRichard, formation);
        addQualification(managers, nancyTheoret, formation);
        addQualification(managers, liseCampeau, formation);
        addQualification(managers, lucieLeavey, formation);
        addQualification(managers, lyndaLajoie, formation);
        addQualification(managers, jeanfrancoisBreton, formation);
        addQualification(managers, stephaneJacques, formation);
        addQualification(managers, brigitteBouchard, formation);
        addQualification(managers, martinDube, formation);
        addQualification(managers, sylviePineault, formation);
        addQualification(managers, joseeLapierre, formation);
        addQualification(managers, hachezGabriel, formation);
        addQualification(managers, sandraDupuis, formation);
        addQualification(managers, lucRoy, formation);
        addQualification(managers, lucieCaron, formation);
        addQualification(managers, rachelMoise, formation);
        addQualification(managers, catherinePiette, formation);
        addQualification(managers, chantalXavier, formation);
        addQualification(managers, dominicHoude, formation);
        addQualification(managers, francoisParent, formation);
        addQualification(managers, solangeGirard, formation);
        addQualification(managers, martinLina, formation);
        addQualification(managers, liseJoncas, formation);
        addQualification(managers, nathalieReid, formation);
        addQualification(managers, cecileCouillard, formation);
        addQualification(managers, sylvainJulien, formation);
        addQualification(managers, sylvainCarriere, formation);
        addQualification(managers, richardVaillant, formation);
        addQualification(managers, franceBoyer, formation);
        addQualification(managers, fxCotebrunet, formation);
        addQualification(managers, isabelleLeclerc, formation);
        addQualification(managers, francoisArcoite, formation);
        addQualification(managers, sabrinaDupuis, formation);
        addQualification(managers, ivanhoeMaisonneuve, formation);
        addQualification(managers, mathieuGuy, formation);
        addQualification(managers, denisBerube, formation);
        addQualification(managers, daisyBourget, formation);
        addQualification(managers, mathewBellemare, formation);
        addQualification(managers, alexandreDube, formation);
        addQualification(managers, annickPigeon, formation);
        addQualification(managers, danielDuquette, formation);
        addQualification(managers, johanneDuval, formation);
        addQualification(managers, philippeLegault, formation);
        addQualification(managers, celineVadnais, formation);
        addQualification(managers, marcGrondin, formation);
        addQualification(managers, ginetteLevesque, formation);
        addQualification(managers, marcelLegault, formation);
        addQualification(managers, nicolasLegault, formation);
        addQualification(managers, stefanieReynolds, formation);
        addQualification(managers, ginoLemoine, formation);
        addQualification(managers, marioLongtin, formation);
        addQualification(managers, robertAllen, formation);

        /*
         * Create Employee preferences
         */
        // SELECT 'setEmployeePreference(managers, ' || camel(EMPLOYEE.FIRSTNAME
        // || ' ' || EMPLOYEE.LASTNAME) || ', ' ||
        // CASEWHEN(EMPLOYEEPREFERENCE.PREFERREDPOSITION_ID IS NOT NULL,
        // camel(SELECT NAME FROM POSITION WHERE POSITION.ID =
        // EMPLOYEEPREFERENCE.PREFERREDPOSITION_ID),'null') || ', ' ||
        // CASEWHEN(PREFERREDPOSITIONSHIFT_ID IS NOT NULL, camel(SELECT NAME
        // FROM SHIFT WHERE SHIFT.ID = PREFERREDPOSITIONSHIFT_ID),'null') || ',
        // ' || CASEWHEN(PREFERREDSECTION_ID IS NOT NULL, camel(SELECT NAME FROM
        // SECTION WHERE SECTION.ID=PREFERREDSECTION_ID), 'null') || ', ' ||
        // CASEWHEN( SELECT COUNT(*) FROM EMPLOYEEPREFERENCE_SHIFT WHERE
        // EMPLOYEEPREFERENCE.ID =
        // EMPLOYEEPREFERENCE_SHIFT.EMPLOYEEPREFERENCE_ID > 0, 'Arrays.asList('
        // || (SELECT GROUP_CONCAT (camel(SHIFT.NAME)) FROM
        // EMPLOYEEPREFERENCE_SHIFT, SHIFT WHERE EMPLOYEEPREFERENCE.ID =
        // EMPLOYEEPREFERENCE_SHIFT.EMPLOYEEPREFERENCE_ID AND
        // EMPLOYEEPREFERENCE_SHIFT.PREFERREDSHIFT_ID = SHIFT.ID) || ')','null')
        // || ');' FROM EMPLOYEE, EMPLOYEEPREFERENCE WHERE EMPLOYEE.ID =
        // EMPLOYEEPREFERENCE.EMPLOYEE_ID;
        setEmployeePreference(managers, donaldTheriault, mecanicien, joursDeSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, louiseVeillette, null, null, null, null);
        setEmployeePreference(managers, caroleMorand, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, lucieGarceau, null, null, snack, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, huguetteLebel, preposeAuxEpicesEtReparation, joursDeSemaine, expedition, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, michelineDemers, operateurEmballageFmc, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, jeanpierreAuger, null, null, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, dianeDugas, null, null, snack, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, rejeanBrabant, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine));
        setEmployeePreference(managers, moniqueLeblond, operateurEmballageMelba, soirSemaine, emballage, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, realGosselin, operateurGerbeuseVerification, joursDeSemaine, expedition, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, lucieLacoste, null, null, null, null);
        setEmployeePreference(managers, johanneLemieux, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, manonTremblay, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, bernardBerube, fournier, joursDeSemaine, baton, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, robertLazure, operateurGerbeuseVerification, soirSemaine, expedition, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                jourRemplacementSemaine,
                nuitSemaineSalubrite,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, lindaBoisvert, operateurEmballageTriangle, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, sergeRobidoux, operateurLigneFourMelba, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, michelDaniel, preposeSalubrite, nuitSemaineSalubrite, salubrite, Arrays.asList(
                nuitSemaineSalubrite,
                soirSemaine,
                joursDeSemaine,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, caroleRaymond, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, gillesGosselin, operateurGerbeuseVerification, jourFinDeSemaine, expedition, Arrays.asList(
                jourFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, francoisBeaulne, operateurGrispac, joursDeSemaine, baton, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, francineGuerin, operateurDeLigne, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirRemplacementSemaine));
        setEmployeePreference(managers, jeanguyRicher, null, null, boulangerie, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, marcelDalphond, operateurEmballageMelba, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                nuitSemaineSalubrite,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, michelMeunier, huileurGraisseurEntretienPreventif, joursDeSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, pierreLamarque, operateurEnsacheuseVerticalSnack, joursDeSemaine, snack, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, gerardLanteigne, petrisseurAPain, soirSemaine, emballage, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, francineDurivage, null, null, snack, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, jeanLatour, petrisseurAPain, joursDeSemaine, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine));
        setEmployeePreference(managers, pierretteDupras, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, danielNault, recuperateurEmballage, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, raynaldStarnaud, operateurLigneAPain, soirSemaine, boulangerie, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, nicoleFortin, operateurEmballageMelba, jourFinDeSemaine, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, normandArsenault, preposeAuMelange, joursDeSemaine, baton, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, liseHebert, null, null, salubrite, Arrays.asList(
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine));
        setEmployeePreference(managers, deniseDaigneault, operateurLigneBiscottebagHorsDoeuvre, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                soirSemaine));
        setEmployeePreference(managers, francineLabbe, operateurRemplacementSnack, joursDeSemaine, snack, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, claudineRochefort, null, null, emballage, Arrays.asList(
                jourRemplacementSemaine,
                joursDeSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, suzanneCouturier, null, null, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                joursDeSemaine));
        setEmployeePreference(managers, michelTougas, preposeSalubrite, nuitSemaineSalubrite, salubrite, Arrays.asList(
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                soirSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                joursDeSemaine));
        setEmployeePreference(managers, danielleBeaudry, null, null, salubrite, Arrays.asList(
                soirSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, lucieGuay, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                soirRemplacementSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, rogerDagenais, operateurLigneAPain, joursDeSemaine, boulangerie, Arrays.asList(
                joursDeSemaine,
                nuitSemaineSalubrite,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, michelJeanneau, operateurRemplacementBoul, joursDeSemaine, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, denisPilon, preposeSalubrite, nuitSemaineSalubrite, salubrite, Arrays.asList(
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                soirSemaine,
                joursDeSemaine,
                jourFinDeSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, suzanneGagnon, null, null, null, null);
        setEmployeePreference(managers, rachelBergevin, operateurPetrisseurSnack, joursDeSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                soirSemaine,
                joursRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, rejeanRoy, operateurGerbeuseVerification, joursDeSemaine, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, huguesDenault, recuperateurEmballage, soirSemaine, emballage, Arrays.asList(
                soirSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, rolandJrBoucher, operateurLigneFourMelba, soirSemaine, emballage, Arrays.asList(
                soirSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, bernardJolin, operateurRemplacementBoul, soirSemaine, boulangerie, Arrays.asList(
                soirSemaine,
                nuitSemaineSalubrite,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                joursDeSemaine,
                jourRemplacementSemaine));
        setEmployeePreference(managers, sartoTremblay, operateurEmballageTriangle, soirSemaine, emballage, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, dianeGirard, operateurEmballageTriangle, jourFinDeSemaine, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine));
        setEmployeePreference(managers, marioPaille, operateurRemplacementBaton, joursDeSemaine, baton, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, ginetteOuellette, null, null, emballage, Arrays.asList(
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                soirRemplacementSemaine,
                soirSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, guylaineGuy, null, null, salubrite, Arrays.asList(
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, pierretteLamothe, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, marcBellemare, mecanicien, joursDeSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, michelineLegault, null, null, emballage, Arrays.asList(
                soirSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursDeSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, joseeConstantineau, operateurLigneFourMelba, jourFinDeSemaine, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, madelaineMarleau, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                soirSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, manonPoissant, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine));
        setEmployeePreference(managers, francineLemieux, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, carmenBrais, prefarinePremelange, joursDeSemaine, boulangerie, Arrays.asList(
                joursDeSemaine,
                nuitSemaineSalubrite,
                soirSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, francoiseTrudeau, null, null, emballage, Arrays.asList(
                jourFinDeSemaine,
                soirSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                jourRemplacementSemaine));
        setEmployeePreference(managers, ericRichard, preposeSalubrite, joursDeSemaine, salubrite, Arrays.asList(
                joursDeSemaine,
                nuitSemaineSalubrite,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, nancyTheoret, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, liseCampeau, null, null, salubrite, Arrays.asList(
                soirSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                joursDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, lucieLeavey, null, null, emballage, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, lyndaLajoie, operateurPetrisseurSnack, soirSemaine, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, jeanfrancoisBreton, petrisseurAPain, jourFinDeSemaine, boulangerie, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite,
                soirSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, stephaneJacques, recuperateurEmballage, jourFinDeSemaine, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirSemaine,
                soirRemplacementSemaine));
        setEmployeePreference(managers, brigitteBouchard, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, martinDube, prefarinePremelange, jourFinDeSemaine, boulangerie, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, sylviePineault, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, joseeLapierre, null, null, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, hachezGabriel, preposeAuMelange, joursDeSemaine, boulangerie, Arrays.asList(
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, sandraDupuis, null, null, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                joursDeSemaine,
                jourRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                soirSemaine));
        setEmployeePreference(managers, lucRoy, mecanicien, joursDeSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, lucieCaron, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, rachelMoise, null, null, emballage, Arrays.asList(
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                soirFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, catherinePiette, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                soirSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, chantalXavier, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, dominicHoude, prefarinePremelange, soirSemaine, boulangerie, Arrays.asList(
                soirSemaine,
                jourFinDeSemaine,
                joursDeSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, francoisParent, petrisseurAPain, joursDeSemaine, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, solangeGirard, preposeSalubrite, nuitSemaineSalubrite, salubrite, Arrays.asList(
                nuitSemaineSalubrite,
                soirSemaine,
                joursDeSemaine,
                soirFinDeSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine));
        setEmployeePreference(managers, martinLina, operateurRemplacementBoul, jourFinDeSemaine, boulangerie, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, liseJoncas, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, nathalieReid, null, null, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, cecileCouillard, null, null, emballage, Arrays.asList(
                jourFinDeSemaine,
                soirSemaine,
                joursDeSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine));
        setEmployeePreference(managers, sylvainJulien, electrotechniciens, joursDeSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, sylvainCarriere, mecanicien, joursDeSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, richardVaillant, null, null, boulangerie, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine));
        setEmployeePreference(managers, franceBoyer, null, null, emballage, Arrays.asList(
                soirSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                joursDeSemaine,
                jourRemplacementSemaine));
        setEmployeePreference(managers, fxCotebrunet, null, null, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, isabelleLeclerc, preposeSalubrite, soirFinDeSemaine, salubrite, Arrays.asList(
                jourFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirRemplacementSemaine));
        setEmployeePreference(managers, francoisArcoite, null, null, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                nuitSemaineSalubrite,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                joursRemplacementFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, sabrinaDupuis, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine));
        setEmployeePreference(managers, ivanhoeMaisonneuve, electrotechniciens, jourFinDeSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                soirSemaine));
        setEmployeePreference(managers, mathieuGuy, operateurLigneAPain, jourFinDeSemaine, boulangerie, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, denisBerube, mecanicien, jourFinDeSemaine, maintenance, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, daisyBourget, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, mathewBellemare, mecanicien, soirSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, alexandreDube, preposeSalubrite, soirFinDeSemaine, salubrite, Arrays.asList(
                soirFinDeSemaine,
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, annickPigeon, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, danielDuquette, null, null, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, johanneDuval, null, null, emballage, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, philippeLegault, petrisseurAPain, soirSemaine, boulangerie, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, celineVadnais, null, null, emballage, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                jourFinDeSemaine,
                soirRemplacementSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, marcGrondin, null, null, boulangerie, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                nuitSemaineSalubrite,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, marcelLegault, null, null, baton, Arrays.asList(
                joursDeSemaine,
                soirSemaine,
                jourFinDeSemaine,
                soirFinDeSemaine,
                nuitSemaineSalubrite,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementFinDeSemaine));
        setEmployeePreference(managers, nicolasLegault, null, null, boulangerie, Arrays.asList(
                soirSemaine,
                joursDeSemaine,
                nuitSemaineSalubrite,
                jourFinDeSemaine,
                soirFinDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                soirRemplacementFinDeSemaine,
                joursRemplacementFinDeSemaine));
        setEmployeePreference(managers, ginoLemoine, electrotechniciens, soirSemaine, maintenance, Arrays.asList(
                joursDeSemaine,
                jourFinDeSemaine,
                soirSemaine,
                soirFinDeSemaine));
        setEmployeePreference(managers, ginetteLevesque, null, null, emballage, Arrays.asList(
                joursDeSemaine,
                jourRemplacementSemaine,
                soirRemplacementSemaine,
                jourFinDeSemaine,
                joursRemplacementFinDeSemaine,
                nuitSemaineSalubrite,
                soirRemplacementFinDeSemaine,
                soirFinDeSemaine,
                soirSemaine));
        setEmployeePreference(managers, stefanieReynolds, null, null, emballage, Arrays.asList(
                jourFinDeSemaine,
                joursDeSemaine,
                soirSemaine,
                jourRemplacementSemaine,
                joursRemplacementFinDeSemaine,
                soirRemplacementSemaine,
                soirFinDeSemaine,
                soirRemplacementFinDeSemaine,
                nuitSemaineSalubrite));
        setEmployeePreference(managers, marioLongtin, null, null, null, null);
        setEmployeePreference(managers, robertAllen, null, null, null, null);
        setEmployeePreference(managers, denisDulude, null, null, boulangerie, Arrays.asList(joursDeSemaine));

        /*
         * Sets preferred seniority
         */
        // SELECT 'setEmployeePreferredSeniority(managers, ' ||
        // camel(EMPLOYEE.FIRSTNAME || ' ' || EMPLOYEE.LASTNAME) || ', true);'
        // FROM EMPLOYEE WHERE EMPLOYEE.PREFERENCIALSENIORITY = TRUE
        setEmployeePreferredSeniority(managers, richardVaillant, true);

        /*
         * Create production event.
         */
        // SELECT 'addProductionEvent(managers, ' || camel('prod ' ||
        // CASEWHEN(PRODUCT.REFID IS NOT NULL, PRODUCT.REFID, PRODUCT.NAME)) ||
        // ', ' || camel( (SELECT SHIFT.NAME FROM SHIFT WHERE SHIFT.ID =
        // CALENDAREVENT.CALENDARENTRY_ID) || ' ' ||
        // FORMATDATETIME(CALENDAREVENT.STARTDATE,'EEE')) || ');' FROM
        // PRODUCTIONEVENT, PRODUCT, CALENDAREVENT WHERE PLANIF.ID =
        // PRODUCTIONEVENT.PLANIF_ID AND PRODUCTIONEVENT.PRODUCT_ID = PRODUCT.ID
        // AND PRODUCTIONEVENT.SHIFTEVENT_ID = CALENDAREVENT.ID
        events = new ProductionEvent[185];
        events[1] = addProductionEvent(managers, prodGs, jourFinDeSemaineSun);
        // events[2] = addProductionEvent(managers, prodPs,
        // jourFinDeSemaineSun);
        events[3] = addProductionEvent(managers, prodRecup, jourFinDeSemaineSun);
        events[4] = addProductionEvent(managers, prodGerbeuse, jourFinDeSemaineSun);
        events[5] = addProductionEvent(managers, prodEl, jourFinDeSemaineSun);
        events[6] = addProductionEvent(managers, prodMec, jourFinDeSemaineSun);
        events[7] = addProductionEvent(managers, prod42040, jourFinDeSemaineSun);
        events[8] = addProductionEvent(managers, prodRp, joursRemplacementFinDeSemaineSun);
        events[9] = addProductionEvent(managers, prodPs, joursDeSemaineMon);
        events[10] = addProductionEvent(managers, prodEpices, joursDeSemaineMon);
        events[11] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineMon);
        events[12] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineMon);
        events[13] = addProductionEvent(managers, prodHuileur, joursDeSemaineMon);
        events[14] = addProductionEvent(managers, prodEl, joursDeSemaineMon);
        events[15] = addProductionEvent(managers, prodMec, joursDeSemaineMon);
        events[16] = addProductionEvent(managers, prodMec, joursDeSemaineMon);
        events[17] = addProductionEvent(managers, prodMec, joursDeSemaineMon);
        events[18] = addProductionEvent(managers, prodMec, joursDeSemaineMon);
        events[19] = addProductionEvent(managers, prod45615, joursDeSemaineMon);
        events[20] = addProductionEvent(managers, prod45050, joursDeSemaineMon);
        events[21] = addProductionEvent(managers, prod56, joursDeSemaineMon);
        events[22] = addProductionEvent(managers, prodBoulbag, joursDeSemaineMon);
        events[23] = addProductionEvent(managers, prodRp, jourRemplacementSemaineMon);
        events[24] = addProductionEvent(managers, prodRp, jourRemplacementSemaineMon);
        events[25] = addProductionEvent(managers, prodRp, jourRemplacementSemaineMon);
        events[26] = addProductionEvent(managers, prodRp, jourRemplacementSemaineMon);
        events[27] = addProductionEvent(managers, prod45111, joursDeSemaineTue);
        events[28] = addProductionEvent(managers, prod41350, joursDeSemaineTue);
        events[29] = addProductionEvent(managers, prod42040, joursDeSemaineTue);
        events[30] = addProductionEvent(managers, prodBoulbag, joursDeSemaineTue);
        events[31] = addProductionEvent(managers, prodRecup, joursDeSemaineMon);
        events[32] = addProductionEvent(managers, prodPs, joursDeSemaineTue);
        events[33] = addProductionEvent(managers, prodEpices, joursDeSemaineTue);
        events[34] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineTue);
        events[35] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineTue);
        events[36] = addProductionEvent(managers, prodHuileur, joursDeSemaineTue);
        events[37] = addProductionEvent(managers, prodEl, joursDeSemaineTue);
        events[38] = addProductionEvent(managers, prodMec, joursDeSemaineTue);
        events[39] = addProductionEvent(managers, prodMec, joursDeSemaineTue);
        events[40] = addProductionEvent(managers, prodMec, joursDeSemaineTue);
        events[41] = addProductionEvent(managers, prodMec, joursDeSemaineTue);
        events[42] = addProductionEvent(managers, prodRecup, joursDeSemaineTue);
        events[43] = addProductionEvent(managers, prodRp, jourRemplacementSemaineTue);
        events[44] = addProductionEvent(managers, prodRp, jourRemplacementSemaineTue);
        events[45] = addProductionEvent(managers, prodPs, joursDeSemaineWed);
        events[46] = addProductionEvent(managers, prodEpices, joursDeSemaineWed);
        events[47] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineWed);
        events[48] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineWed);
        events[49] = addProductionEvent(managers, prodHuileur, joursDeSemaineWed);
        events[50] = addProductionEvent(managers, prodEl, joursDeSemaineWed);
        events[51] = addProductionEvent(managers, prodMec, joursDeSemaineWed);
        events[52] = addProductionEvent(managers, prodMec, joursDeSemaineWed);
        events[53] = addProductionEvent(managers, prodMec, joursDeSemaineWed);
        events[54] = addProductionEvent(managers, prodMec, joursDeSemaineWed);
        events[55] = addProductionEvent(managers, prodRecup, joursDeSemaineWed);
        events[56] = addProductionEvent(managers, prod45111, joursDeSemaineWed);
        events[57] = addProductionEvent(managers, prod41350, joursDeSemaineWed);
        events[58] = addProductionEvent(managers, prod42040, joursDeSemaineWed);
        events[59] = addProductionEvent(managers, prodBoul, joursDeSemaineWed);
        events[60] = addProductionEvent(managers, prodRp, jourRemplacementSemaineWed);
        events[61] = addProductionEvent(managers, prodRp, jourRemplacementSemaineWed);
        events[62] = addProductionEvent(managers, prodRp, jourRemplacementSemaineWed);
        events[63] = addProductionEvent(managers, prodRp, jourRemplacementSemaineWed);
        events[64] = addProductionEvent(managers, prodPs, joursDeSemaineThu);
        events[65] = addProductionEvent(managers, prodEpices, joursDeSemaineThu);
        events[66] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineThu);
        events[67] = addProductionEvent(managers, prodGerbeuse, joursDeSemaineThu);
        events[68] = addProductionEvent(managers, prodHuileur, joursDeSemaineThu);
        events[69] = addProductionEvent(managers, prodEl, joursDeSemaineThu);
        events[70] = addProductionEvent(managers, prodMec, joursDeSemaineThu);
        events[71] = addProductionEvent(managers, prodMec, joursDeSemaineThu);
        events[72] = addProductionEvent(managers, prodMec, joursDeSemaineThu);
        events[73] = addProductionEvent(managers, prodMec, joursDeSemaineThu);
        events[74] = addProductionEvent(managers, prodRecup, joursDeSemaineThu);
        events[75] = addProductionEvent(managers, prod663, joursDeSemaineThu);
        events[76] = addProductionEvent(managers, prod41354, joursDeSemaineThu);
        events[77] = addProductionEvent(managers, prod2162, joursDeSemaineThu);
        events[78] = addProductionEvent(managers, prodBoul, joursDeSemaineThu);
        events[79] = addProductionEvent(managers, prodRp, jourRemplacementSemaineThu);
        events[80] = addProductionEvent(managers, prodRp, jourRemplacementSemaineThu);
        events[81] = addProductionEvent(managers, prodRp, jourRemplacementSemaineThu);
        events[82] = addProductionEvent(managers, prodRp, jourRemplacementSemaineThu);
        events[83] = addProductionEvent(managers, prodGs, jourFinDeSemaineFri);
        // events[84] = addProductionEvent(managers, prodPs,
        // jourFinDeSemaineFri);
        events[85] = addProductionEvent(managers, prodRecup, jourFinDeSemaineFri);
        events[86] = addProductionEvent(managers, prodGerbeuse, jourFinDeSemaineFri);
        events[87] = addProductionEvent(managers, prodEl, jourFinDeSemaineFri);
        events[88] = addProductionEvent(managers, prodMec, jourFinDeSemaineFri);
        events[89] = addProductionEvent(managers, prod2162, jourFinDeSemaineFri);
        events[90] = addProductionEvent(managers, prodRp, joursRemplacementFinDeSemaineFri);
        events[91] = addProductionEvent(managers, prodGs, jourFinDeSemaineSat);
        // events[92] = addProductionEvent(managers, prodPs,
        // jourFinDeSemaineSat);
        events[93] = addProductionEvent(managers, prodRecup, jourFinDeSemaineSat);
        events[94] = addProductionEvent(managers, prodGerbeuse, jourFinDeSemaineSat);
        events[95] = addProductionEvent(managers, prodEl, jourFinDeSemaineSat);
        events[96] = addProductionEvent(managers, prodMec, jourFinDeSemaineSat);
        events[97] = addProductionEvent(managers, prod2162, jourFinDeSemaineSat);
        events[98] = addProductionEvent(managers, prodRp, joursRemplacementFinDeSemaineSat);
        events[99] = addProductionEvent(managers, prodRecup, soirSemaineMon);
        events[100] = addProductionEvent(managers, prodGerbeuse, soirSemaineMon);
        events[101] = addProductionEvent(managers, prodEl, soirSemaineMon);
        events[102] = addProductionEvent(managers, prodMec, soirSemaineMon);
        events[103] = addProductionEvent(managers, prod45052, soirSemaineMon);
        events[104] = addProductionEvent(managers, prod42044, soirSemaineMon);
        events[105] = addProductionEvent(managers, prodBoulbag, soirSemaineMon);
        events[106] = addProductionEvent(managers, prodRp, soirRemplacementSemaineMon);
        events[107] = addProductionEvent(managers, prodRp, soirRemplacementSemaineMon);
        events[108] = addProductionEvent(managers, prodRecup, soirSemaineTue);
        events[109] = addProductionEvent(managers, prodGerbeuse, soirSemaineTue);
        events[110] = addProductionEvent(managers, prodEl, soirSemaineTue);
        events[111] = addProductionEvent(managers, prodMec, soirSemaineTue);
        events[112] = addProductionEvent(managers, prod41352, soirSemaineTue);
        events[113] = addProductionEvent(managers, prod42040, soirSemaineTue);
        events[114] = addProductionEvent(managers, prodBoulbag, soirSemaineTue);
        events[115] = addProductionEvent(managers, prodRp, soirRemplacementSemaineTue);
        events[116] = addProductionEvent(managers, prodRp, soirRemplacementSemaineTue);
        events[117] = addProductionEvent(managers, prodRecup, soirSemaineWed);
        events[118] = addProductionEvent(managers, prodGerbeuse, soirSemaineWed);
        events[119] = addProductionEvent(managers, prodEl, soirSemaineWed);
        events[120] = addProductionEvent(managers, prodMec, soirSemaineWed);
        events[121] = addProductionEvent(managers, prod50404, soirSemaineWed);
        events[122] = addProductionEvent(managers, prodBoul, soirSemaineWed);
        events[123] = addProductionEvent(managers, prod42056, soirSemaineWed);
        events[124] = addProductionEvent(managers, prodRp, soirRemplacementSemaineWed);
        events[125] = addProductionEvent(managers, prodRp, soirRemplacementSemaineWed);
        events[126] = addProductionEvent(managers, prodRecup, soirSemaineThu);
        events[127] = addProductionEvent(managers, prodGerbeuse, soirSemaineThu);
        events[128] = addProductionEvent(managers, prodEl, soirSemaineThu);
        events[129] = addProductionEvent(managers, prodMec, soirSemaineThu);
        events[130] = addProductionEvent(managers, prod45200, soirSemaineThu);
        events[131] = addProductionEvent(managers, prod2162, soirSemaineThu);
        events[132] = addProductionEvent(managers, prodBoul, soirSemaineThu);
        events[133] = addProductionEvent(managers, prodRp, soirRemplacementSemaineThu);
        events[134] = addProductionEvent(managers, prodRp, soirRemplacementSemaineThu);
        events[135] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteMon);
        events[136] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteMon);
        events[137] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteMon);
        events[138] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteMon);
        events[139] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteMon);
        events[140] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteMon);
        events[141] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteTue);
        events[142] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteTue);
        events[143] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteTue);
        events[144] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteTue);
        events[145] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteTue);
        events[146] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteTue);
        events[147] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteWed);
        events[148] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteWed);
        events[149] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteWed);
        events[150] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteWed);
        events[151] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteWed);
        events[152] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteWed);
        events[153] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteThu);
        events[154] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteThu);
        events[155] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteThu);
        events[156] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteThu);
        events[157] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteThu);
        events[158] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteThu);
        events[159] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteFri);
        events[160] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteFri);
        events[161] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteFri);
        events[162] = addProductionEvent(managers, prodPs, nuitSemaineSalubriteFri);
        events[163] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteFri);
        events[164] = addProductionEvent(managers, prodGs, nuitSemaineSalubriteFri);
        events[165] = addProductionEvent(managers, prodRp, jourRemplacementSemaineTue);
        events[166] = addProductionEvent(managers, prodRp, jourRemplacementSemaineTue);
        events[167] = addProductionEvent(managers, prodGs, soirSemaineMon);
        events[168] = addProductionEvent(managers, prodGs, soirSemaineTue);
        events[169] = addProductionEvent(managers, prodGs, soirSemaineThu);
        events[170] = addProductionEvent(managers, prodFormation, joursDeSemaineMon);
        events[171] = addProductionEvent(managers, prodFormation, joursDeSemaineTue);
        events[172] = addProductionEvent(managers, prodFormation, joursDeSemaineWed);
        events[173] = addProductionEvent(managers, prodFormation, joursDeSemaineThu);
        events[174] = addProductionEvent(managers, prodPs, soirDeFinDeSemaineSun);
        events[175] = addProductionEvent(managers, prodGs, soirDeFinDeSemaineSun);
        events[176] = addProductionEvent(managers, prodGs, soirDeFinDeSemaineFri);
        events[177] = addProductionEvent(managers, prodPs, soirDeFinDeSemaineFri);
        events[178] = addProductionEvent(managers, prodPs, soirDeFinDeSemaineSat);
        events[179] = addProductionEvent(managers, prodGs, soirDeFinDeSemaineSat);
        events[180] = addProductionEvent(managers, prodFormation, nuitSemaineSalubriteMon);
        events[181] = addProductionEvent(managers, prodFormation, nuitSemaineSalubriteTue);
        events[182] = addProductionEvent(managers, prodFormation, nuitSemaineSalubriteWed);
        events[183] = addProductionEvent(managers, prodFormation, nuitSemaineSalubriteThu);
        events[184] = addProductionEvent(managers, prodFormation, nuitSemaineSalubriteFri);

        /*
         * Non-availabilities
         */
        addNonAvailability(managers, louiseVeillette, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, rejeanBrabant, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, lucieLacoste, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, gerardLanteigne, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, jeanLatour, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, suzanneGagnon, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, huguesDenault, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, huguesDenault, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, bernardJolin, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, ginetteOuellette, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, pierretteLamothe, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, carmenBrais, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, ericRichard, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, jeanfrancoisBreton, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, martinDube, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, sylviePineault, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, sandraDupuis, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, chantalXavier, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, francoisParent, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));
        addNonAvailability(managers, francoisArcoite, dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));

        addNonAvailability(managers, richardVaillant, joursDeSemaineThu.getStartDate(), joursDeSemaineThu.getEndDate());
        addNonAvailability(managers, pierreLamarque, joursDeSemaineThu.getStartDate(), joursDeSemaineThu.getEndDate());
        addNonAvailability(managers, liseCampeau, soirSemaineThu.getStartDate(), soirSemaineThu.getEndDate());

        // Regroup the event by Team to make it easier for verification
        jourSemaineEvents = select(Arrays.asList(events), joursDeSemaine);
        jourRemplacementSemaineEvents = select(Arrays.asList(events), jourRemplacementSemaine);
        soirSemaineEvents = select(Arrays.asList(events), soirSemaine);
        soirRemplacementSemaineEvents = select(Arrays.asList(events), soirRemplacementSemaine);
        nuitSemaineSalubriteEvents = select(Arrays.asList(events), nuitSemaineSalubrite);
        jourFinDeSemaineEvents = select(Arrays.asList(events), jourFinDeSemaine);
        joursRemplacementFinDeSemaineEvents = select(Arrays.asList(events), joursRemplacementFinDeSemaine);
        soirDeFinDeSemaineEvents = select(Arrays.asList(events), soirFinDeSemaine);
        soirRemplacementFinDeSemaineEvents = select(Arrays.asList(events), soirRemplacementFinDeSemaine);

        // Regroup some position to make it easier for verification
        generalEmballagePositions = Arrays.asList(
                generaleFourAideFournier,
                generaleEmballageBaton,
                generaleDemouleuse,
                generaleTrancheuseBaguettine,
                generaleEmballageBaguettine,
                generaleTrancheuseBiscotte,
                generaleEmballageBiscotte,
                equarisseur,
                generalTrancheuseMelba,
                generaleEmballageMelba);
        generalSnackPositions = Arrays.asList(generalEmballageSnackBoite, generaleEmballageCroutonsVrac, generaleEmballagePainsMinces);

    }

    private List<ProductionEvent> select(List<ProductionEvent> asList, Team joursDeSemaine) {
        List<ProductionEvent> list = new ArrayList<ProductionEvent>();
        for (ProductionEvent e : asList) {
            if (e != null && e.getShift().getTeam().equals(joursDeSemaine)) {
                list.add(e);
            }
        }
        return list;
    }

    @Before
    public void setupLog() {

    }

    /**
     * This unit test verify the result of the generate planif.
     * 
     * @throws ManagerException
     * @throws IOException
     */
    @Test
    public void testGeneratePlanif() throws ManagerException, IOException {

        Task lockedTask;
        /*
         * Lock Jean-Pierre Auger on Opérateur de Remplacement - Boul
         */
        lockedTask = addTask(managers, events[22], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, jeanpierreAuger);
        lockedTask = addTask(managers, events[30], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, jeanpierreAuger);
        lockedTask = addTask(managers, events[59], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, jeanpierreAuger);
        lockedTask = addTask(managers, events[78], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, jeanpierreAuger);

        /*
         * Lock Normand Arsenault Pré-farine / Pré-mélange wednesday And Thrusday
         */
        lockedTask = addTask(managers, events[59], prefarinePremelange, null, null);
        setTaskLocked(managers, lockedTask, normandArsenault);
        lockedTask = addTask(managers, events[78], prefarinePremelange, null, null);
        setTaskLocked(managers, lockedTask, normandArsenault);

        /*
         * Lock Michel Jeanneau Opérateur Remplacement Boul Soir
         */
        lockedTask = addTask(managers, events[105], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, michelJeanneau);
        lockedTask = addTask(managers, events[114], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, michelJeanneau);
        lockedTask = addTask(managers, events[122], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, michelJeanneau);
        lockedTask = addTask(managers, events[132], operateurRemplacementBoul, null, null);
        setTaskLocked(managers, lockedTask, michelJeanneau);

        /*
         * Lock Lucie Leavey - Lun et Mardi Opérateur bag, Mer Opérateur remplacement snack, Jeudi Ensacheuse snack
         */
        lockedTask = addTask(managers, events[103], operateurLigneBiscottebagHorsDoeuvre, null, null);
        setTaskLocked(managers, lockedTask, lucieLeavey);
        lockedTask = addTask(managers, events[112], operateurLigneBiscottebagHorsDoeuvre, null, null);
        setTaskLocked(managers, lockedTask, lucieLeavey);
        lockedTask = addTask(managers, events[121], operateurRemplacementSnack, null, null);
        setTaskLocked(managers, lockedTask, lucieLeavey);
        lockedTask = addTask(managers, events[130], operateurEnsacheuseVerticalSnack, null, null);
        setTaskLocked(managers, lockedTask, lucieLeavey);

        /*
         * Lock Josée Lapierre - Lun, mar, mer, jeu Formation
         */
        lockedTask = addTask(managers, events[170], formation, null, null);
        setTaskLocked(managers, lockedTask, joseeLapierre);
        lockedTask = addTask(managers, events[171], formation, null, null);
        setTaskLocked(managers, lockedTask, joseeLapierre);
        lockedTask = addTask(managers, events[172], formation, null, null);
        setTaskLocked(managers, lockedTask, joseeLapierre);
        lockedTask = addTask(managers, events[173], formation, null, null);
        setTaskLocked(managers, lockedTask, joseeLapierre);

        /*
         * Lock Hachez Gabriel - Lun, Mar, Mer et Jeu Préposé Salibrité
         */
        lockedTask = addTask(managers, events[9], preposeSalubrite, null, null);
        setTaskLocked(managers, lockedTask, hachezGabriel);
        lockedTask = addTask(managers, events[32], preposeSalubrite, null, null);
        setTaskLocked(managers, lockedTask, hachezGabriel);
        lockedTask = addTask(managers, events[45], preposeSalubrite, null, null);
        setTaskLocked(managers, lockedTask, hachezGabriel);
        lockedTask = addTask(managers, events[64], preposeSalubrite, null, null);
        setTaskLocked(managers, lockedTask, hachezGabriel);

        /*
         * Rachel Moise - Lun, Mar, Mer et Jeu Récupérateur
         */
        lockedTask = addTask(managers, events[99], recuperateurEmballage, null, null);
        setTaskLocked(managers, lockedTask, rachelMoise);
        lockedTask = addTask(managers, events[108], recuperateurEmballage, null, null);
        setTaskLocked(managers, lockedTask, rachelMoise);
        lockedTask = addTask(managers, events[117], recuperateurEmballage, null, null);
        setTaskLocked(managers, lockedTask, rachelMoise);
        lockedTask = addTask(managers, events[126], recuperateurEmballage, null, null);
        setTaskLocked(managers, lockedTask, rachelMoise);

        /*
         * Richard Vaillant - Lun, mar, mer Pétrisseur à Pain
         */
        lockedTask = addTask(managers, events[22], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, richardVaillant);
        lockedTask = addTask(managers, events[30], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, richardVaillant);
        lockedTask = addTask(managers, events[59], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, richardVaillant);

        /*
         * F.x. Cote-Brunet - Jeu Pétrisseur
         */
        lockedTask = addTask(managers, events[78], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, fxCotebrunet);

        /*
         * Isabelle Leclerc - Formation nuit
         */
        lockedTask = addTask(managers, events[180], formation, null, null);
        setTaskLocked(managers, lockedTask, isabelleLeclerc);
        lockedTask = addTask(managers, events[181], formation, null, null);
        setTaskLocked(managers, lockedTask, isabelleLeclerc);
        lockedTask = addTask(managers, events[182], formation, null, null);
        setTaskLocked(managers, lockedTask, isabelleLeclerc);
        lockedTask = addTask(managers, events[183], formation, null, null);
        setTaskLocked(managers, lockedTask, isabelleLeclerc);
        lockedTask = addTask(managers, events[184], formation, null, null);
        setTaskLocked(managers, lockedTask, isabelleLeclerc);

        /*
         * Mathieu Guy - Lun, mar, mer et Jeu Pétrisseu à pain soir
         */
        lockedTask = addTask(managers, events[105], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, mathieuGuy);
        lockedTask = addTask(managers, events[114], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, mathieuGuy);
        lockedTask = addTask(managers, events[122], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, mathieuGuy);
        lockedTask = addTask(managers, events[132], petrisseurAPain, null, null);
        setTaskLocked(managers, lockedTask, mathieuGuy);

        /*
         * Alexandre Dube- Sun, Fri, Sat Préposé salubrite
         */
        lockedTask = addTask(managers, events[174], preposeSalubrite, null, null);
        setTaskLocked(managers, lockedTask, alexandreDube);
        lockedTask = addTask(managers, events[177], preposeSalubrite, null, null);
        setTaskLocked(managers, lockedTask, alexandreDube);
        lockedTask = addTask(managers, events[178], preposeSalubrite, null, null);
        setTaskLocked(managers, lockedTask, alexandreDube);

        /*
         * Generate planif
         */
        generatePlanif(managers, date("2011-09-25"));

        /*
         * Get the result
         */
        List<Task> tasks = managers.getTaskManager().list(dateTime("2011-09-25 Sun 00:00"), dateTime("2011-10-01 Sat 23:59"));

        Collections.sort(tasks, TimeRangeComparators.byDate());

        /*
         * Validate the result
         */
        assertAssignment(4, tasks, jourSemaineEvents, mecanicien, donaldTheriault);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, louiseVeillette);
        assertAssignment(4, tasks, jourSemaineEvents, generalEmballagePositions, caroleMorand);
        assertAssignment(4, tasks, jourSemaineEvents, generalSnackPositions, lucieGarceau);
        assertAssignment(4, tasks, jourSemaineEvents, preposeAuxEpicesEtReparation, huguetteLebel);
        assertAssignment(4, tasks, jourSemaineEvents, generalEmballagePositions, michelineDemers);
        assertAssignment(4, tasks, jourSemaineEvents, operateurRemplacementBoul, jeanpierreAuger);
        assertAssignment(4, tasks, jourSemaineEvents, generalSnackPositions, dianeDugas);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, rejeanBrabant);
        assertAssignment(4, tasks, soirSemaineEvents, operateurEmballageMelba, moniqueLeblond);
        assertAssignment(4, tasks, jourSemaineEvents, operateurGerbeuseVerification, realGosselin);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, lucieLacoste);
        assertAssignment(4, tasks, jourSemaineEvents, generalEmballagePositions, johanneLemieux);
        assertAssignment(4, tasks, jourSemaineEvents, generalEmballagePositions, manonTremblay);
        // Lun, mar,mer
        assertAssignment(2, 4, tasks, jourSemaineEvents, generalEmballagePositions, bernardBerube);
        // Jeu
        assertAssignment(0, 1, tasks, jourSemaineEvents, generaleDemouleuse, bernardBerube);
        assertAssignment(4, tasks, soirSemaineEvents, operateurGerbeuseVerification, robertLazure);
        assertAssignment(4, tasks, jourSemaineEvents, operateurEmballageTriangle, lindaBoisvert);
        assertAssignment(4, tasks, jourSemaineEvents, operateurLigneFourMelba, sergeRobidoux);
        assertAssignment(5, tasks, nuitSemaineSalubriteEvents, preposeSalubrite, michelDaniel);
        assertAssignment(4, tasks, jourSemaineEvents, generalEmballagePositions, caroleRaymond);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, operateurGerbeuseVerification, gillesGosselin);
        assertAssignment(3, 4, tasks, jourSemaineEvents, generalEmballagePositions, francoisBeaulne);
        assertAssignment(4, tasks, jourSemaineEvents, generalEmballagePositions, francineGuerin);
        assertAssignment(4, tasks, soirSemaineEvents, generaleDemouleuse, jeanguyRicher);
        assertAssignment(4, tasks, jourSemaineEvents, operateurEmballageMelba, marcelDalphond);
        assertAssignment(4, tasks, jourSemaineEvents, huileurGraisseurEntretienPreventif, michelMeunier);
        assertAssignment(3, tasks, jourSemaineEvents, operateurEnsacheuseVerticalSnack, pierreLamarque);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, gerardLanteigne);
        assertAssignment(3, tasks, jourSemaineEvents, generalEmballageSnackBoite, francineDurivage);
        assertAssignment(1, tasks, jourSemaineEvents, generalEmballagePositions, francineDurivage);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, jeanLatour);
        assertAssignment(4, tasks, soirSemaineEvents, generalEmballagePositions, pierretteDupras);
        assertAssignment(4, tasks, jourSemaineEvents, recuperateurEmballage, danielNault);
        assertAssignment(4, tasks, soirSemaineEvents, operateurLigneAPain, raynaldStarnaud);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, operateurEmballageMelba, nicoleFortin);
        assertAssignment(4, tasks, soirRemplacementSemaineEvents, generalEmballagePositions, liseHebert);
        assertAssignment(4, tasks, jourSemaineEvents, operateurLigneBiscottebagHorsDoeuvre, deniseDaigneault);
        // Lun, mar
        assertAssignment(0, 2, tasks, jourSemaineEvents, generalEmballagePositions, normandArsenault);
        // Mer, Jeu
        assertAssignment(2, tasks, jourSemaineEvents, prefarinePremelange, normandArsenault);
        assertAssignment(4, tasks, jourSemaineEvents, operateurRemplacementSnack, francineLabbe);
        assertAssignment(4, tasks, jourRemplacementSemaineEvents, generalEmballagePositions, claudineRochefort);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, generalEmballagePositions, suzanneCouturier);
        assertAssignment(5, tasks, nuitSemaineSalubriteEvents, preposeSalubrite, michelTougas);
        // Lun, Mar, Jeu
        assertAssignment(3, tasks, soirSemaineEvents, generaleSalubrite, danielleBeaudry);
        assertAssignment(1, tasks, soirSemaineEvents, generalSnackPositions, danielleBeaudry);
        assertAssignment(4, tasks, jourRemplacementSemaineEvents, generalEmballagePositions, lucieGuay);
        assertAssignment(4, tasks, jourSemaineEvents, operateurLigneAPain, rogerDagenais);
        assertAssignment(4, tasks, soirSemaineEvents, operateurRemplacementBoul, michelJeanneau);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, suzanneGagnon);
        assertAssignment(5, tasks, nuitSemaineSalubriteEvents, preposeSalubrite, denisPilon);
        assertAssignment(4, tasks, jourSemaineEvents, operateurPetrisseurSnack, rachelBergevin);
        assertAssignment(4, tasks, jourSemaineEvents, operateurGerbeuseVerification, rejeanRoy);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, huguesDenault);
        assertAssignment(4, tasks, soirSemaineEvents, operateurLigneFourMelba, rolandJrBoucher);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, bernardJolin);
        // Lun, Mar
        assertAssignment(2, tasks, soirSemaineEvents, operateurEmballageTriangle, sartoTremblay);
        // Mer, Jeu
        assertAssignment(2, tasks, soirSemaineEvents, generalEmballagePositions, sartoTremblay);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, generalEmballagePositions, marioPaille);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, generalEmballagePositions, dianeGirard);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, ginetteOuellette);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, generaleSalubrite, guylaineGuy);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, pierretteLamothe);
        assertAssignment(4, tasks, jourSemaineEvents, mecanicien, marcBellemare);
        assertAssignment(4, tasks, soirSemaineEvents, generalEmballagePositions, michelineLegault);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, operateurLigneFourMelba, joseeConstantineau);
        assertAssignment(4, tasks, jourRemplacementSemaineEvents, generalEmballagePositions, madelaineMarleau);
        assertAssignment(4, tasks, soirSemaineEvents, generalEmballagePositions, manonPoissant);
        assertAssignment(4, tasks, soirSemaineEvents, generalEmballagePositions, francineLemieux);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, carmenBrais);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, generalEmballagePositions, francoiseTrudeau);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, ericRichard);
        // Mon, Tue
        assertAssignment(2, tasks, soirSemaineEvents, generalEmballagePositions, nancyTheoret);
        // Wed Thu
        assertAssignment(2, tasks, soirSemaineEvents, generalSnackPositions, nancyTheoret);
        // Mon, Tue
        assertAssignment(2, tasks, soirSemaineEvents, generalEmballagePositions, liseCampeau);
        // Mer
        assertAssignment(0, 1, tasks, soirSemaineEvents, generalSnackPositions, liseCampeau);
        assertAssignment(2, tasks, soirSemaineEvents, operateurLigneBiscottebagHorsDoeuvre, lucieLeavey);
        assertAssignment(1, tasks, soirSemaineEvents, operateurRemplacementSnack, lucieLeavey);
        assertAssignment(1, tasks, soirSemaineEvents, operateurEnsacheuseVerticalSnack, lucieLeavey);
        // Mon & Tue
        assertAssignment(2, tasks, soirSemaineEvents, generalEmballagePositions, lyndaLajoie);
        // Wed
        assertAssignment(1, tasks, soirSemaineEvents, operateurPetrisseurSnack, lyndaLajoie);
        // Thu
        assertAssignment(1, tasks, soirSemaineEvents, generalSnackPositions, lyndaLajoie);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, jeanfrancoisBreton);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, recuperateurEmballage, stephaneJacques);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, generalEmballagePositions, brigitteBouchard);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, martinDube);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, sylviePineault);
        assertAssignment(4, tasks, jourSemaineEvents, formation, joseeLapierre);
        assertAssignment(4, tasks, jourSemaineEvents, preposeSalubrite, hachezGabriel);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, sandraDupuis);
        assertAssignment(4, tasks, jourSemaineEvents, mecanicien, lucRoy);

        assertAssignment(5, tasks, nuitSemaineSalubriteEvents, generaleSalubrite, lucieCaron);
        assertAssignment(4, tasks, soirSemaineEvents, recuperateurEmballage, rachelMoise);
        assertAssignment(4, tasks, jourRemplacementSemaineEvents, generalEmballagePositions, catherinePiette);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, chantalXavier);
        assertAssignment(2, tasks, soirSemaineEvents, generaleDemouleuse, dominicHoude);
        assertAssignment(2, tasks, soirSemaineEvents, prefarinePremelange, dominicHoude);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, francoisParent);
        assertAssignment(5, tasks, nuitSemaineSalubriteEvents, preposeSalubrite, solangeGirard);
        assertAssignment(2, 4, tasks, soirSemaineEvents, generalEmballagePositions, martinLina);
        assertAssignment(2, tasks, soirSemaineEvents, generaleDemouleuse, martinLina);
        assertAssignment(4, tasks, soirRemplacementSemaineEvents, generalEmballagePositions, liseJoncas);
        assertAssignment(3, tasks, joursRemplacementFinDeSemaineEvents, generalEmballagePositions, nathalieReid);
        assertAssignment(5, tasks, nuitSemaineSalubriteEvents, generaleSalubrite, cecileCouillard);
        assertAssignment(4, tasks, jourSemaineEvents, electrotechniciens, sylvainJulien);
        // Lun, mar, mer
        assertAssignment(3, tasks, jourSemaineEvents, petrisseurAPain, richardVaillant);
        assertAssignment(3, tasks, soirDeFinDeSemaineEvents, generaleSalubrite, franceBoyer);
        assertAssignment(3, tasks, jourSemaineEvents, generaleDemouleuse, fxCotebrunet);
        assertAssignment(1, tasks, jourSemaineEvents, petrisseurAPain, fxCotebrunet);
        assertAssignment(5, tasks, nuitSemaineSalubriteEvents, formation, isabelleLeclerc);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, francoisArcoite);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, sabrinaDupuis);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, electrotechniciens, ivanhoeMaisonneuve);
        assertAssignment(4, tasks, soirSemaineEvents, petrisseurAPain, mathieuGuy);
        assertAssignment(3, tasks, jourFinDeSemaineEvents, mecanicien, denisBerube);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, daisyBourget);
        assertAssignment(4, tasks, soirSemaineEvents, mecanicien, mathewBellemare);
        assertAssignment(3, tasks, soirDeFinDeSemaineEvents, preposeSalubrite, alexandreDube);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, annickPigeon);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, danielDuquette);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, celineVadnais);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, philippeLegault);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, marcGrondin);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, ginetteLevesque);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, nicolasLegault);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, marcelLegault);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, stefanieReynolds);
        assertAssignment(4, tasks, soirSemaineEvents, electrotechniciens, ginoLemoine);
        assertAssignment(0, tasks, Arrays.asList(events), (Position) null, johanneDuval);

    }
}
