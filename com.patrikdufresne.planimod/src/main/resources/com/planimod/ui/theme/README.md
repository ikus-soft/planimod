# Command line to convert SVG files into png and ico.

    convert -background none planimod.svg -resize 16x16 planimod_16.png
    convert -background none -density 300 planimod.svg -resize 32x32 planimod_32.png
    convert -background none planimod.svg -resize 48x48 planimod_48.png
    convert -background none planimod.svg -resize 128x128 planimod_128.png
    icotool -c -o planimod.ico planimod_16.png planimod_32.png planimod_48.png planimod_128.png

    
    convert -background none logo.svg -resize 375x375 logo_375.png