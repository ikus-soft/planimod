/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.test;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

public class TimeUtils {

    public static Date date(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Calendar cal = new GregorianCalendar();
            cal.setTime(format.parse(date));
            return cal.getTime();
        } catch (ParseException e) {
            throw new IllegalStateException("Fail parsing time", e);
        }
    }

    public static Date dateTime(String date) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd EEE HH:mm", Locale.ENGLISH);
        try {
            Calendar cal = new GregorianCalendar();
            cal.setTime(format.parse(date));
            return cal.getTime();
        } catch (ParseException e) {
            throw new IllegalStateException("Fail parsing time", e);
        }
    }

    public static Date time(String time) {
        DateFormat format = new SimpleDateFormat("EEE HH:mm", Locale.ENGLISH);
        try {
            Calendar cal = new GregorianCalendar();
            int year = cal.get(Calendar.YEAR);
            int week = cal.get(Calendar.WEEK_OF_YEAR);
            cal.setTime(format.parse(time));
            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.WEEK_OF_YEAR, week);
            return cal.getTime();
        } catch (ParseException e) {
            throw new IllegalStateException("Fail parsing time", e);
        }
    }

}
