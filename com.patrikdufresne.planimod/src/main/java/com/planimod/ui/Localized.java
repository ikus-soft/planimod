/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui;

import java.util.Locale;

import org.slf4j.LoggerFactory;
import org.xnap.commons.i18n.I18n;
import org.xnap.commons.i18n.I18nFactory;

public class Localized {

    private static final String BUNDLE_NAME = "com.patrikdufresne.planimod.messages";

    private static I18n i18n;

    static {
        LoggerFactory.getLogger(Localized.class).info("using default locale {}", Locale.getDefault());
        i18n = I18nFactory.getI18n(Localized.class, BUNDLE_NAME, Locale.getDefault(), I18nFactory.FALLBACK);
    }

    /**
     * Localize the given text (a la gettext).
     * 
     * @param text
     * @return
     */
    public static String _(String text) {
        return i18n.tr(text);
    }

    /**
     * Localize the given text (a la gettext). The string may contains <code>%s</code> as place holder for
     * <code>args</code>.
     * 
     * @param text
     * @param args
     * @return
     */
    public static String _(String text, Object... args) {
        return String.format(i18n.tr(text), args);
    }
}
