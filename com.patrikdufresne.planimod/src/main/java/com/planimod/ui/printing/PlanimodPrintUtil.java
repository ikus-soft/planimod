/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.printing;

import static com.planimod.ui.Localized._;

import com.patrikdufresne.jface.databinding.conversion.AcronymConverter;
import com.patrikdufresne.util.DateFormatRegistry;
import com.planimod.core.Employee;
import com.planimod.core.EmployeePreference;
import com.planimod.core.Position;
import com.planimod.core.Section;
import com.planimod.core.Team;
import com.planimod.core.comparators.EmployeePreferenceComparators;
import com.planimod.ui.databinding.Converters;
import com.planimod.ui.views.employees.EmployeeBySectionPrintFactory;

/**
 * Utility class for printing.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanimodPrintUtil {

    public static final String ANNOTATION = " *";

    public static final String ANNOTATION_NO_SPACE = "*";

    public static final String EMPTY = "";

    public static final String SPACE = " ";

    public static final String X = "X";

    public static String getAcronym(String str) {
        return (String) AcronymConverter.getInstance().convert(str);
    }

    public static String getAcronym(Team t) {
        return getAcronym(getName(t));
    }

    public static String getFirstPreferredTeamAcronym(EmployeePreference employee) {
        if (employee.getPreferredTeam() != null && employee.getPreferredTeam().size() > 0) {
            return getAcronym(employee.getPreferredTeam().get(0));
        }
        return EMPTY;
    }

    /**
     * This function return the employee's comments if available. Otherwise, it return an empty string.
     * 
     * @param employee
     * @return
     */
    public static String getHireDate(Employee employee) {
        if (employee.getHireDate() != null) {
            return DateFormatRegistry.format(employee.getHireDate(), DateFormatRegistry.DATE | DateFormatRegistry.ISO);
        }
        return EMPTY;
    }

    /**
     * This function return the hire date of the employee if the employee has preferences.
     * 
     * @param pref
     *            the employee preferences.
     * @return the hiredate or empty string.
     */
    public static String getHireDate(EmployeePreference pref) {
        if (EmployeePreferenceComparators.hasPreferences(pref)) {
            return getHireDate(pref.getEmployee());
        }
        return EMPTY;
    }

    /**
     * This function return the employee's name if available. Otherwise, it return an empty string.
     * 
     * @param employee
     * @return
     */
    public static String getName(Employee employee) {
        String name = "";
        if (employee.getFirstname() != null) {
            name += employee.getFirstname();
        }
        if (employee.getLastname() != null) {
            if (employee.getFirstname() != null) {
                name += " ";
            }
            name += employee.getLastname();
        }
        return name;
    }

    public static String getName(Employee employee, boolean annotation) {
        return getName(employee) + (annotation ? ANNOTATION : EMPTY);
    }

    /**
     * This function return the position name.
     * 
     * @return a string
     */
    public static String getName(Position position) {
        if (position != null && position.getName() != null) {
            return position.getName();
        }
        return EMPTY;
    }

    /**
     * This function is called to return the section name.
     * 
     * @param section
     *            the section or null for "no section"
     * @return
     */
    public static String getName(Section section) {
        if (section != null) {
            return (String) Converters.removeFrontNumber().convert(section.getName());
        }
        return _("Aucune section");
    }

    /**
     * Return a string representing the Team.
     * 
     * @param team
     *            the team
     * @return the team's name or empty string.
     */
    public static String getName(Team team) {
        if (team.getName() != null) {
            return (String) Converters.removeFrontNumber().convert(team.getName());
        }
        return EMPTY;
    }

    /**
     * Return a string representing the Team.
     * 
     * @param team
     *            the team
     * @return the team's name or empty string.
     */
    public static String getName(Team team, int count) {
        if (team.getName() != null) {
            return String.format("%s (%d)", getName(team), Integer.valueOf(count));
        }
        return EMPTY;
    }

    /**
     * Returns the employee ref id.
     * 
     * @param employee
     * @return
     */
    public static String getRefId(Employee employee) {
        if (employee.getRefId() != null) {
            return employee.getRefId();
        }
        return "";
    }

    public static String getName(Employee e, Team team) {
        if (team != null) {
            return String.format("%s (%s)", getName(e), getName(team));
        }
        return getName(e);
    }

}
