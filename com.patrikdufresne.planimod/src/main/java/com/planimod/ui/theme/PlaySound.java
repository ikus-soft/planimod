/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.theme;

import java.io.IOException;
import java.net.URL;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;

public class PlaySound {

    /**
     * Play a "notify" sound.
     */
    public static void playNotify() {
        URL url = PlaySound.class.getResource("notify.wav");
        if (url != null) {
            play(url);
        }
    }

    /**
     * Play the given url file.
     * 
     * @param filename
     *            the name of the file that is going to be played
     */
    public static void play(final URL url) {
        (new Thread() {
            @Override
            public void run() {
                // Open the stream as an audio stream.
                AudioInputStream audioStream;
                SourceDataLine sourceLine;
                try {
                    audioStream = AudioSystem.getAudioInputStream(url);
                    AudioFormat audioFormat = audioStream.getFormat();
                    DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
                    sourceLine = (SourceDataLine) AudioSystem.getLine(info);
                    sourceLine.open(audioFormat);
                } catch (Exception e) {
                    return;
                }

                // Read the audio stream.
                final int BUFFER_SIZE = 128000;
                sourceLine.start();
                int nBytesRead = 0;
                byte[] abData = new byte[BUFFER_SIZE];
                while (nBytesRead != -1) {
                    try {
                        nBytesRead = audioStream.read(abData, 0, abData.length);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (nBytesRead >= 0) {
                        @SuppressWarnings("unused")
                        int nBytesWritten = sourceLine.write(abData, 0, nBytesRead);
                    }
                }
                // Clear the resources.
                sourceLine.drain();
                sourceLine.close();
                try {
                    audioStream.close();
                } catch (IOException e) {
                    return;
                }
            }
        }).start();
    }
}