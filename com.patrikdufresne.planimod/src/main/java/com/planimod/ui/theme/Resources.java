/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.theme;

import org.eclipse.jface.dialogs.TitleAreaDialog;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.resource.ImageRegistry;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.FontData;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Display;

import com.patrikdufresne.jface.resource.OverlayImageDescriptor;

/**
 * Utility class used to access resources for Planimod application. This class provide functions to avoid repeating
 * codes related to {@link JFaceResources}.
 * 
 * @author Patrik Dufresne
 * 
 */
public class Resources {

    public static final String APP_ICON_128 = "planimod_128.png";
    public static final String APP_ICON_48 = "planimod_48.png";
    public static final String LOGO_ICON_375 = "logo_375.png";

    /**
     * Resource id for edit title banner image.
     */
    public static final String DLG_EDIT_TITLE_IMAGE = "Resources.dlgEditTitleImage";
    public static final String ICON_CREATE_TASKS_16 = "Resources.iconCreateTask16";
    public static final String ICON_DUPLICATE_16 = "Resources.iconDuplicate16";
    public static final String ICON_DUPLICATE_48 = "Resources.iconDuplicate48";
    public static final String ICON_EDIT_16 = "Resources.iconEdit16";
    public static final String ICON_EDIT_48 = "Resources.iconEdit48";
    public static final String ICON_EMPLOYEES_16 = "employees-16.png";
    public static final String ICON_GENERATE_PLANIF_16 = "Resources.iconGeneratePlanif16";
    public static final String ICON_GO_DOWN_16 = "Resources.iconGoDown16";
    public static final String ICON_GO_HOME_16 = "Resources.iconGoHome16";
    public static final String ICON_GO_NEXT_16 = "Resources.iconGoNext16";
    public static final String ICON_GO_PREVIOUS_16 = "Resources.iconGoPrevious16";
    public static final String ICON_GO_UP_16 = "Resources.iconGoUp16";
    public static final String ICON_LIST_ADD_16 = "Resources.iconListAdd16";
    public static final String ICON_LIST_REMOVE_16 = "Resources.iconListRemove16";
    public static final String ICON_LIST_REMOVE_ALL_16 = "Resources.iconListRemoveAll16";
    public static final String ICON_PARAMETERS_16 = "Resources.parameters16";
    public static final String ICON_PRINT_16 = "Resources.iconPrint16";
    public static final String ICON_PRINT_48 = "Resources.iconPrint48";

    /**
     * Resource id used within the font registry.
     */
    private static final String WEEK_TITLE_FONT = "Resources.weekTitleFont";

    static {
        initializeDefaultImages();
    }

    /**
     * Return a font used to draw the application name in the about dialog.
     * 
     * @return the font
     */
    public static Font getApplicationNameFont() {
        return getWeekTitleFont();
    }

    /**
     * Returns the image associated with the given key, or null if none. The image returned by this function should not
     * be disposed.
     * 
     * @param key
     *            the key
     * @return the image, or null if none
     */
    public static Image getImage(String key) {
        return JFaceResources.getImage(key);
    }

    /**
     * Returns the descriptor associated with the given key , or null if none.
     * 
     * @param key
     *            the key
     * @return the image descriptor, or null if none
     */
    public static ImageDescriptor getImageDescriptor(String key) {
        return JFaceResources.getImageRegistry().getDescriptor(key);
    }

    /**
     * Return a font used to draw the week title. This method use the JFace font registry to avoid allocating the same
     * font date multiple time.
     * 
     * @return the font
     */
    public static Font getWeekTitleFont() {
        if (JFaceResources.getFontRegistry().hasValueFor(WEEK_TITLE_FONT)) {
            return JFaceResources.getFontRegistry().get(WEEK_TITLE_FONT);
        }
        FontData[] datas = Display.getCurrent().getSystemFont().getFontData();
        FontData[] dateLabelFontDatas = new FontData[datas.length];
        for (int i = 0; i < datas.length; i++) {
            dateLabelFontDatas[i] = new FontData(datas[i].getName(), (int) (datas[i].getHeight() * 1.5f), datas[i].getStyle() | SWT.BOLD);
        }
        JFaceResources.getFontRegistry().put(WEEK_TITLE_FONT, dateLabelFontDatas);
        return JFaceResources.getFontRegistry().get(WEEK_TITLE_FONT);
    }

    /**
     * Populate the image registry with Planimod icons.
     */
    private static void initializeDefaultImages() {
        ImageRegistry ir = JFaceResources.getImageRegistry();
        ir.put(APP_ICON_48, ImageDescriptor.createFromFile(Resources.class, APP_ICON_48));
        ir.put(APP_ICON_128, ImageDescriptor.createFromFile(Resources.class, APP_ICON_128));
        ir.put(LOGO_ICON_375, ImageDescriptor.createFromFile(Resources.class, LOGO_ICON_375));
        ir.put(ICON_CREATE_TASKS_16, ImageDescriptor.createFromFile(Resources.class, "insert-object-16.png"));
        ir.put(ICON_DUPLICATE_16, ImageDescriptor.createFromFile(Resources.class, "edit-copy-16.png"));
        ir.put(ICON_DUPLICATE_48, ImageDescriptor.createFromFile(Resources.class, "edit-copy-48.png"));
        ir.put(ICON_EDIT_16, ImageDescriptor.createFromFile(Resources.class, "accessories-text-editor-humanity-16.png"));
        ir.put(ICON_EDIT_48, ImageDescriptor.createFromFile(Resources.class, "accessories-text-editor-humanity-48.png"));
        ir.put(ICON_LIST_REMOVE_ALL_16, ImageDescriptor.createFromFile(Resources.class, "edit-delete-16.png"));
        ir.put(ICON_EMPLOYEES_16, ImageDescriptor.createFromFile(Resources.class, "employees-16.png"));
        ir.put(ICON_GENERATE_PLANIF_16, ImageDescriptor.createFromFile(Resources.class, "insert-object2-16.png"));
        ir.put(ICON_GO_HOME_16, ImageDescriptor.createFromFile(Resources.class, "go-home-16.png"));
        ir.put(ICON_GO_NEXT_16, ImageDescriptor.createFromFile(Resources.class, "go-next-16.png"));
        ir.put(ICON_GO_PREVIOUS_16, ImageDescriptor.createFromFile(Resources.class, "go-previous-16.png"));
        ir.put(ICON_GO_UP_16, ImageDescriptor.createFromFile(Resources.class, "go-up-16.png"));
        ir.put(ICON_GO_DOWN_16, ImageDescriptor.createFromFile(Resources.class, "go-down-16.png"));
        ir.put(ICON_LIST_ADD_16, ImageDescriptor.createFromFile(Resources.class, "list-add-16.png"));
        ir.put(ICON_LIST_REMOVE_16, ImageDescriptor.createFromFile(Resources.class, "list-remove-16.png"));
        ir.put(ICON_PARAMETERS_16, ImageDescriptor.createFromFile(Resources.class, "preferences-system-16.png"));
        ir.put(ICON_PRINT_16, ImageDescriptor.createFromFile(Resources.class, "printer-printing-16.png"));
        ir.put(ICON_PRINT_48, ImageDescriptor.createFromFile(Resources.class, "printer-printing-48.png"));
        // Composite images
        ir.put(DLG_EDIT_TITLE_IMAGE, new OverlayImageDescriptor(ir.getDescriptor(TitleAreaDialog.DLG_IMG_TITLE_BANNER), Resources
                .getImageDescriptor(Resources.ICON_EDIT_48), SWT.LEFT, SWT.CENTER));
    }

}
