/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.databinding.state;

import org.eclipse.core.commands.IStateListener;
import org.eclipse.core.commands.State;
import org.eclipse.core.databinding.observable.Diffs;
import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;

/**
 * Property used to observe State.
 * 
 * @author Patrik Dufresne
 * 
 */
public class StateValueProperty extends SimpleValueProperty {

    private class Listener extends NativePropertyListener implements IStateListener {

        public Listener(ISimplePropertyListener listener) {
            super(StateValueProperty.this, listener);
        }

        @Override
        protected void doAddTo(Object source) {
            ((State) source).addListener(this);
        }

        @Override
        protected void doRemoveFrom(Object source) {
            ((State) source).removeListener(this);
        }

        @Override
        public void handleStateChange(State state, Object oldValue) {
            fireChange(state, Diffs.createValueDiff(oldValue, state.getValue()));
        }

    }

    private Object type;

    /**
     * Create an untyped value property.
     */
    public StateValueProperty() {
        this(null);
    }

    public StateValueProperty(Object type) {
        this.type = type;
    }

    @Override
    public INativePropertyListener adaptListener(ISimplePropertyListener listener) {
        return new Listener(listener);
    }

    @Override
    protected Object doGetValue(Object source) {
        return ((State) source).getValue();
    }

    @Override
    protected void doSetValue(Object source, Object value) {
        ((State) source).setValue(value);
    }

    @Override
    public Object getValueType() {
        return this.type;
    }

}
