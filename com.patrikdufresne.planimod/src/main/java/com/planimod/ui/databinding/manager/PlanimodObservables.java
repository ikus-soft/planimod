/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.databinding.manager;

import org.eclipse.core.databinding.observable.set.IObservableSet;

import com.patrikdufresne.managers.databinding.ArchivableObjectComputedSet;
import com.patrikdufresne.managers.databinding.ManagedObjectComputedSet;
import com.planimod.core.Employee;
import com.planimod.core.EmployeePreference;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Position;
import com.planimod.core.Product;
import com.planimod.core.Section;
import com.planimod.core.Team;

/**
 * Utility class to create computed set for managed object.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanimodObservables {

    /**
     * List employees.
     * 
     * @param managers
     *            the managers
     * @return the observable computedset
     */
    public static ManagedObjectComputedSet listEmployees(PlanimodManagers managers) {
        return new ManagedObjectComputedSet(managers, Employee.class);
    }

    /**
     * List positions.
     * 
     * @param managers
     *            the managers
     * @return the observable computedset
     */
    public static ManagedObjectComputedSet listPosition(PlanimodManagers managers) {
        return new ManagedObjectComputedSet(managers, Position.class);
    }

    /**
     * List employees.
     * 
     * @param managers
     *            the managers
     * @return the observable computedset
     */
    public static ManagedObjectComputedSet listProduct(PlanimodManagers managers) {
        return new ManagedObjectComputedSet(managers, Product.class);
    }

    /**
     * List sections.
     * 
     * @param managers
     *            the managers
     * @return the observable computedset
     */
    public static ManagedObjectComputedSet listSection(PlanimodManagers managers) {
        return new ManagedObjectComputedSet(managers, Section.class);
    }

    /**
     * List teams.
     * 
     * @param managers
     *            the managers
     * @return the observable computedset
     */
    public static ManagedObjectComputedSet listTeam(PlanimodManagers managers) {
        return new ManagedObjectComputedSet(managers, Team.class);
    }

}
