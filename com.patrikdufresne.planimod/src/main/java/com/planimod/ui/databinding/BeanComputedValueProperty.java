/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.databinding;

import java.beans.PropertyChangeListener;
import java.beans.PropertyDescriptor;

import org.eclipse.core.databinding.property.INativePropertyListener;
import org.eclipse.core.databinding.property.ISimplePropertyListener;
import org.eclipse.core.databinding.property.NativePropertyListener;
import org.eclipse.core.databinding.property.value.SimpleValueProperty;
import org.eclipse.core.internal.databinding.beans.BeanPropertyListenerSupport;

public abstract class BeanComputedValueProperty extends SimpleValueProperty {

    protected final PropertyDescriptor[] propertyDescriptors;
    protected final Class valueType;

    @SuppressWarnings("unqualified-field-access")
    protected class Listener extends NativePropertyListener implements PropertyChangeListener {

        protected Listener(ISimplePropertyListener listener) {
            super(BeanComputedValueProperty.this, listener);
        }

        @Override
        public void propertyChange(java.beans.PropertyChangeEvent evt) {

            if (evt.getPropertyName() != null) {
                int i = 0;
                while (i < propertyDescriptors.length && !propertyDescriptors[i].getName().equals(evt.getPropertyName())) {
                    i++;
                }
                if (i >= propertyDescriptors.length) {
                    return;
                }
            }

            // Fire change
            fireChange(evt.getSource(), null);
        }

        @Override
        protected void doAddTo(Object source) {
            for (int i = 0; i < propertyDescriptors.length; i++) {
                BeanPropertyListenerSupport.hookListener(source, propertyDescriptors[i].getName(), this);
            }
        }

        @Override
        protected void doRemoveFrom(Object source) {
            for (int i = 0; i < propertyDescriptors.length; i++) {
                BeanPropertyListenerSupport.unhookListener(source, propertyDescriptors[i].getName(), this);
            }
        }
    }

    /**
     * @param propertyDescriptor
     * @param valueType
     */
    public BeanComputedValueProperty(PropertyDescriptor[] propertyDescriptors, Class valueType) {
        this.propertyDescriptors = propertyDescriptors;
        this.valueType = valueType;
    }

    @Override
    public Object getValueType() {
        return this.valueType;
    }

    @Override
    protected void doSetValue(Object source, Object value) {
        // Not supported
        throw new UnsupportedOperationException(toString() + ": Setter not supported on a converted value!");
    }

    public INativePropertyListener adaptListener(final ISimplePropertyListener listener) {
        return new Listener(listener);
    }
}
