/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.databinding;

import java.beans.PropertyDescriptor;

import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.internal.databinding.beans.BeanPropertyHelper;

import com.planimod.core.EmployeePreference;

public class EmployeePreferredPositionValueProperty extends BeanComputedValueProperty {

    private String noneString;

    /**
     * Create a new property value.
     * 
     * @param noneString
     *            define the string value when the employee doesn't have a preferred position.
     */
    public EmployeePreferredPositionValueProperty(String noneString) {
        super(properties(), String.class);
        this.noneString = noneString;
    }

    /**
     * Create a new property value.
     */
    public EmployeePreferredPositionValueProperty() {
        this("");
    }

    /**
     * Static function used to return the list of property descriptors for this computed value property.
     * 
     * @return
     */
    private static PropertyDescriptor[] properties() {
        return new PropertyDescriptor[] {
                BeanPropertyHelper.getPropertyDescriptor(EmployeePreference.class, EmployeePreference.PREFERRED_POSITION),
                BeanPropertyHelper.getPropertyDescriptor(EmployeePreference.class, EmployeePreference.PREFERRED_POSITION_TEAM) };
    }

    private IConverter converter = Converters.removeFrontNumber();

    @Override
    protected Object doGetValue(Object source) {
        EmployeePreference pref = (EmployeePreference) source;
        if (pref.getPreferredPosition() == null || pref.getPreferredPositionTeam() == null) {
            return this.noneString;
        }
        return this.converter.convert(pref.getPreferredPositionTeam().getName()) + " - " + pref.getPreferredPosition().getName();
    }
}
