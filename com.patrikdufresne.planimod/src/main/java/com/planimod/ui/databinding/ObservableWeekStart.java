/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.databinding;

import java.util.Date;

import org.eclipse.core.databinding.observable.value.ComputedValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.TimeRanges;

public class ObservableWeekStart extends ComputedValue {

    private IObservableValue value;

    private PlanimodManagers managers;

    public ObservableWeekStart(PlanimodManagers managers, IObservableValue value) {
        this.managers = managers;
        this.value = value;
    }

    @Override
    protected Object calculate() {
        if (!(this.value.getValue() instanceof Date)) {
            return null;
        }
        try {
            return TimeRanges.getWeekStart((Date) this.value.getValue(), this.managers.getApplicationSettingManager().getFirstDayOfWeek());
        } catch (ManagerException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

}
