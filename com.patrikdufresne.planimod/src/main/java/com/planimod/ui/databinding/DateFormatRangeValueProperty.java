/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.databinding;

import java.beans.PropertyDescriptor;

import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.core.internal.databinding.beans.BeanPropertyHelper;

import com.patrikdufresne.util.DateFormatRegistry;
import com.planimod.core.AbstractCalendarEvent;

/**
 * This value property return a formated date time range using the DateFormatRegistry.
 * 
 * @author Patrik Dufresne
 * 
 */
public class DateFormatRangeValueProperty extends BeanComputedValueProperty {

    /**
     * Create a new instance of this class with the style DATE | MEDIUM.
     * 
     * @return new instance of this class
     */
    public static DateFormatRangeValueProperty createDateMedium() {
        return new DateFormatRangeValueProperty(DateFormatRegistry.DATE | DateFormatRegistry.MEDIUM);
    }

    /**
     * Create a new instance of this class with the style DATE | SHORT.
     * 
     * @return
     */
    public static IValueProperty createDateShort() {
        return new DateFormatRangeValueProperty(DateFormatRegistry.DATE | DateFormatRegistry.SHORT);
    }

    /**
     * Create a new instance of this class with the style TIME | MEDIUM.
     * 
     * @return new instance of this class.
     */
    public static IValueProperty createTimeMedium() {
        return new DateFormatRangeValueProperty(DateFormatRegistry.TIME | DateFormatRegistry.MEDIUM);
    }

    /**
     * Static function used to return the list of property descriptors for this computed value property.
     * 
     * @return
     */
    private static PropertyDescriptor[] properties() {
        return new PropertyDescriptor[] {
                BeanPropertyHelper.getPropertyDescriptor(AbstractCalendarEvent.class, AbstractCalendarEvent.START_DATE),
                BeanPropertyHelper.getPropertyDescriptor(AbstractCalendarEvent.class, AbstractCalendarEvent.END_DATE) };
    }

    private int style;

    /**
     * Create a new property value.
     */
    public DateFormatRangeValueProperty(int style) {
        super(properties(), String.class);
        this.style = style;
    }

    @Override
    protected Object doGetValue(Object source) {
        AbstractCalendarEvent event = (AbstractCalendarEvent) source;
        return DateFormatRegistry.formatRange(event.getStartDate(), event.getEndDate(), this.style);
    }
}
