/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.viewers.planner;

import java.util.Date;

import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.widgets.Display;

import com.patrikdufresne.planner.DefaultPlannerLook;
import com.patrikdufresne.planner.Planner;
import com.patrikdufresne.ui.ColorUtil;
import com.patrikdufresne.util.DateFormatRegistry;
import com.planimod.ui.databinding.Converters;
import com.planimod.ui.theme.Resources;

/**
 * Class defining the planner look. This implementation overwrite some of the default look.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanimodPlannerLook extends DefaultPlannerLook {

    private static final String ITEM_COLOR = "PlanimodPlannerLook.itemColor";

    private static final String SELECTED_ITEM_BORDER_COLOR = "PlanimodPlannerLook.selectedItemBorderColor";

    private Color gridColor = Display.getDefault().getSystemColor(SWT.COLOR_WIDGET_NORMAL_SHADOW);

    private Color itemColor;

    private Color itemForeground = Display.getDefault().getSystemColor(SWT.COLOR_LIST_FOREGROUND);

    private Color selection = Display.getDefault().getSystemColor(SWT.COLOR_LIST_SELECTION);

    private Color selectedItemBorderColor;

    private Color selectionText = Display.getDefault().getSystemColor(SWT.COLOR_LIST_SELECTION_TEXT);

    private Font titleFont;

    @Override
    public String formatItemTimeRange(Planner planner, Date startTime, Date endTime, boolean small) {
        if (small) {
            return DateFormatRegistry.formatRange(startTime, endTime, DateFormatRegistry.TIME | DateFormatRegistry.MEDIUM);
        }
        return DateFormatRegistry.formatRange(startTime, endTime, DateFormatRegistry.TIME | DateFormatRegistry.SHORT);
    }

    private IConverter weekTitleConverter = Converters.weekTitleConverter();

    /**
     * This implementation display a title to include the starting week date.
     */
    @Override
    public String formatTitle(Planner planner, Date start, Date end, Date curDateSelection) {
        return (String) this.weekTitleConverter.convert(start);
    }

    @Override
    public Color getGridColor(Planner planner) {
        return this.gridColor;
    }

    @Override
    public Color getItemBorderColor(Planner planner) {
        return this.selection;
    }

    @Override
    public Color getItemColor(Planner planner) {
        if (this.itemColor == null) {
            if (!JFaceResources.getColorRegistry().hasValueFor(ITEM_COLOR)) {
                RGB rgb = ColorUtil.blend(Display.getDefault().getSystemColor(SWT.COLOR_LIST_BACKGROUND).getRGB(), Display.getDefault().getSystemColor(
                        SWT.COLOR_LIST_SELECTION).getRGB(), 75);
                JFaceResources.getColorRegistry().put(ITEM_COLOR, rgb);
            }
            this.itemColor = JFaceResources.getColorRegistry().get(ITEM_COLOR);
        }
        return this.itemColor;
    }

    @Override
    public Color getItemForeground(Planner planner) {
        return this.itemForeground;
    }

    @Override
    public Color getSelectedItemBorderColor(Planner planner) {
        if (this.selectedItemBorderColor == null) {
            if (!JFaceResources.getColorRegistry().hasValueFor(SELECTED_ITEM_BORDER_COLOR)) {
                RGB rgb = ColorUtil.blend(new RGB(0, 0, 0), this.selection.getRGB(), 50);
                JFaceResources.getColorRegistry().put(SELECTED_ITEM_BORDER_COLOR, rgb);
            }
            this.selectedItemBorderColor = JFaceResources.getColorRegistry().get(SELECTED_ITEM_BORDER_COLOR);
        }
        return this.selectedItemBorderColor;
    }

    @Override
    public Color getSelectedItemColor(Planner planner) {
        return this.selection;
    }

    @Override
    public Color getSelectedItemForeground(Planner planner) {
        return this.selectionText;
    }

    @Override
    public Font getTitleFont(Planner planner) {
        if (this.titleFont == null) {
            this.titleFont = Resources.getWeekTitleFont();
        }
        return this.titleFont;
    }

}
