/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.viewers;

import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.conversion.IConverter;

import com.planimod.core.ProductionEvent;
import com.planimod.ui.databinding.Converters;

//TODO The label provide should be deprecated.
public class ProductionEventViewerUtils {

    /**
     * Create a converter to display a Product object as string.
     * <p>
     * This converter return the product's name.
     * 
     * @return the converter
     */
    public static IConverter labelConverter() {
        return new Converter(ProductionEvent.class, String.class) {

            private IConverter shortDate = Converters.shortDateTimeConverter();

            @Override
            public Object convert(Object fromObject) {
                ProductionEvent event = (ProductionEvent) fromObject;

                return this.shortDate.convert(event.getShift().getStartDate())
                        + " - "
                        + this.shortDate.convert(event.getShift().getEndDate())
                        + " - "
                        + event.getProduct().getName();
            }
        };
    }

}
