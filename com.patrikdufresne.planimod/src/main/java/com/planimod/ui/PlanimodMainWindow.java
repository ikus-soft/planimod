/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui;

import static com.planimod.ui.Localized._;

import org.eclipse.jface.resource.ColorRegistry;
import org.eclipse.jface.resource.JFaceColors;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;

import com.patrikdufresne.ui.MainWindow;
import com.planimod.core.PlanimodManagers;
import com.planimod.ui.views.admin.AdminViewPart;
import com.planimod.ui.views.employees.EmployeeViewPart;
import com.planimod.ui.views.nonavailabilities.NonAvailabilitiesViewPart;
import com.planimod.ui.views.planif.PlanifViewPart;
import com.planimod.ui.views.production.ProductionViewPart;
import com.planimod.ui.views.qualifs.QualificationViewPart;
import com.planimod.ui.views.shifts.ShiftViewPart;

/**
 * This implementation of the {@link MainWindow} create default vies for Planimod and provide a {@link PlanimodManagers}
 * service.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanimodMainWindow extends MainWindow {

    /**
     * Managers used by this window.
     */
    private PlanimodManagers managers;

    /**
     * Default constructor.
     * 
     * @param parentShell
     * @see ApplicationWindow
     */
    public PlanimodMainWindow(Shell parentShell) {
        super(parentShell);
    }

    /**
     * This implementation add all the view required for Planimod.
     */
    @Override
    protected void addViews() {
        addView(new EmployeeViewPart());
        addView(new NonAvailabilitiesViewPart());
        addView(new QualificationViewPart());
        addView(new ShiftViewPart());
        addView(new ProductionViewPart());
        addView(new PlanifViewPart());
        addView(new AdminViewPart());
    }

    /**
     * This implementation add a shell title and icon.
     */
    @Override
    protected void configureShell(final Shell shell) {
        super.configureShell(shell);
        shell.setText(_("Planimod"));
        if (getManagers().isCopy()) {
            shell.setText(shell.getText() + _(" (COPIE)"));
        }
    }

    @Override
    protected Control createContents(Composite parent) {
        Control control = super.createContents(parent);

        // Set color
        if (getManagers().isCopy()) {
            getBook().getTabFolder().setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_DARK_RED));
        }

        return control;
    }

    /**
     * Return the planimod managers.
     * 
     * @return
     */
    public PlanimodManagers getManagers() {
        return this.managers;
    }

    /**
     * Sets the managers used by this windows.
     * 
     * @param managers
     */
    public void setManagers(PlanimodManagers managers) {
        this.managers = managers;
    }

    @Override
    public <T> T locateService(Class<T> serviceClass) {

        if (serviceClass.equals(PlanimodManagers.class)) {
            return (T) getManagers();
        }

        return null;

    }
}
