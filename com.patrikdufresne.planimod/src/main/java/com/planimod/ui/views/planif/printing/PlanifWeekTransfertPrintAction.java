/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif.printing;

import static com.planimod.ui.Localized._;

import java.util.Date;

import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.wizard.WizardDialog;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.printing.PrintAction;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.TimeRanges;
import com.planimod.ui.PlanimodPolicy;

/**
 * This action is used to duplicate the production events from one week to the other week.
 * 
 * @author Patrik Dufresne
 * 
 */

public class PlanifWeekTransfertPrintAction extends PrintAction {

    /**
     * Property name of an action's date (value <code>"date"</code>). The date property is used to know the time range to be
     * printed by this action.
     */
    public static final String DATE = "date";

    /**
     * The managers to use.
     */
    private PlanimodManagers managers;

    /**
     * Define the date value.
     */
    private Date date;

    private IShellProvider shellProvider;

    /**
     * Create a new action.
     * 
     * @param managers
     *            the managers to be used
     * @param shellProvider
     *            a shell provider used to open dialog
     * @param context
     *            a runnable context to run the operation
     */

    public PlanifWeekTransfertPrintAction(PlanimodManagers managers, IShellProvider shellProvider) {
        super(shellProvider, new PlanifWeekTransfertPrintFactory(), 0);
        if (shellProvider == null || managers == null) {
            throw new NullPointerException();
        }
        this.shellProvider = shellProvider;
        this.managers = managers;
        setText(_("Imprimer programmation des équipes de travail"));
    }

    /**
     * Returns True if the action can be run.
     * 
     * @return
     */
    protected boolean canRun() {
        return this.date != null;
    }

    /**
     * Returns the current date state or null if not define.
     * 
     * @return the date state or null
     */
    public Date getDate() {
        return this.date;
    }

    /**
     * This implementation run the generate planif in a runnable context.
     */
    @Override
    public void run() {
        int firstDayOfWeek;
        try {
            firstDayOfWeek = this.managers.getApplicationSettingManager().getFirstDayOfWeek();
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
            return;
        }
        PlanifWeekTransfertPrintWizard wizard = new PlanifWeekTransfertPrintWizard(this.managers);
        wizard.setDate(this.date);
        wizard.setPreviousDate(TimeRanges.previousWeek(this.date, firstDayOfWeek));
        WizardDialog dlg = new WizardDialog(shellProvider.getShell(), wizard);
        dlg.open();
    }

    /**
     * Sets the destination date (week).
     * 
     * @param date
     *            the new date or null
     */
    public void setDate(Date date) {
        firePropertyChange(DATE, this.date, this.date = date);
        setEnabled(canRun());
    }

}
