/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.employees;

import static com.planimod.ui.Localized._;

import java.util.Date;

import org.eclipse.core.databinding.BindingProperties;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.ListToSetAdapter;
import org.eclipse.core.databinding.observable.value.DateAndTimeObservableValue;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.property.value.IValueProperty;
import org.eclipse.core.internal.databinding.property.value.SelfValueProperty;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.databinding.swt.SWTObservables;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.databinding.viewers.IViewerObservableList;
import org.eclipse.jface.databinding.viewers.ObservableListContentProvider;
import org.eclipse.jface.databinding.viewers.ObservableSetContentProvider;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.databinding.viewers.ViewerSupport;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.DateTime;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.patrikdufresne.jface.action.ActionMenuManager;
import com.patrikdufresne.jface.databinding.util.JFaceProperties;
import com.patrikdufresne.jface.databinding.viewers.ColumnSupport;
import com.patrikdufresne.jface.databinding.viewers.PropertyViewerComparator;
import com.patrikdufresne.jface.viewers.ColumnViewerPreferences;
import com.patrikdufresne.managers.databinding.ManagerUpdateValueStrategy;
import com.patrikdufresne.managers.jface.RemoveArchiveAction;
import com.patrikdufresne.printing.PrintAction;
import com.patrikdufresne.ui.SashFactory;
import com.planimod.core.Employee;
import com.planimod.core.EmployeePreference;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Section;
import com.planimod.core.Team;
import com.planimod.core.comparators.EmployeePreferenceComparators;
import com.planimod.core.comparators.SectionComparators;
import com.planimod.ui.databinding.Converters;
import com.planimod.ui.databinding.EmployeePreferredPositionValueProperty;
import com.planimod.ui.databinding.ListObservableValue;
import com.planimod.ui.databinding.manager.PlanimodObservables;
import com.planimod.ui.preference.PlanimodPreferences;
import com.planimod.ui.printing.PrintActionMenuManager;
import com.planimod.ui.viewers.EmployeeViewerUtils;
import com.planimod.ui.views.AbstractViewPart;

/**
 * This view display the list of employee.
 * 
 * @author Patrik Dufresne
 */
public class EmployeeViewPart extends AbstractViewPart {

    /**
     * Converter used for text filtering.
     */
    private final static IConverter CONVERTER = new Converter(EmployeePreference.class, String.class) {

        @Override
        public Object convert(Object fromObject) {
            return EmployeeViewerUtils.getName(((EmployeePreference) fromObject).getEmployee());
        }
    };

    private static final String DOT = ".";

    /**
     * The part id.
     */
    public static final String ID = "com.planimod.ui.views.employees.EmployeeViewPart";

    /**
     * Preference key to access the column's width.
     */
    private static final String PREFERENCE_COLUMNS = "EmployeeViewPart.columns";

    /**
     * Preference key to access the column's width.
     */
    private static final String PREFERENCE_SASH_WEIGHTS = "EmployeeViewPart.sash.weights";

    /**
     * Used to edit the employee's firstname.
     */
    private Text firstnameText;

    /**
     * Used to select the employee's hire date.
     */
    private DateTime hireDate;

    /**
     * Used to select the employee's hire time.
     */
    private DateTime hireTime;

    /**
     * Used to edit the employee's lastname.
     */
    private Text lastnameText;

    private EmployeeEditPreferredPositionAction preferredPositionAction;

    /**
     * Preferred section.
     */
    private ComboViewer preferredSectionViewer;

    /**
     * Toggle the preferred seniority.
     */
    private Button preferredSeniorityButton;

    private EmployeePreferredTeamAddAllAction preferredTeamAddAllAction;

    private EmployeePreferredTeamMoveAction preferredTeamMoveDownAction;

    private EmployeePreferredTeamMoveAction preferredTeamMoveUpAction;

    private EmployeePreferredTeamRemoveAction preferredTeamRemoveAction;

    /**
     * Viewer for preferred shift.
     */
    private TableViewer preferredTeamViewer;

    /**
     * Remove or archive action
     */
    private RemoveArchiveAction removeAction;

    /**
     * Viewer displayed on the left.
     */
    private TableViewer viewer;

    /**
     * Create a new view part.
     */
    public EmployeeViewPart() {
        super(ID);
        setTitle(_("Employés"));
    }

    /**
     * This implementation create the content of the employee view.
     */
    @Override
    public void activate(Composite parent) {
        // Create table viewer on the left
        this.viewer = createTableViewer(parent, SWT.MULTI);
        this.viewer.getTable().setLinesVisible(PlanimodPreferences.getPreferenceStore().getBoolean(PlanimodPreferences.SHOW_LINES));
        // Create editor
        createEditorControls(parent);
        SashFactory.createRightPane(parent, false, PlanimodPreferences.getPreferenceStore(), PREFERENCE_SASH_WEIGHTS);
        // Populate tool bar.
        fillToolBar();
        // Create binding.
        bind();
    }

    /**
     * This implementation bind the viewer and the editor.
     */
    @Override
    protected void bindValues() {
        DataBindingContext dbc = getDbc();
        PlanimodManagers managers = getSite().getService(PlanimodManagers.class);
        /*
         * Bind the viewer
         */
        IObservableSet employeePreferences = new EmployeePreferenceComputedSet(managers);
        createViewerColumns(this.viewer, employeePreferences);
        this.viewer.setContentProvider(new ObservableSetContentProvider());
        this.viewer.setInput(filter(employeePreferences, CONVERTER));
        /*
         * Bind Actions
         */
        getDbc().bindList(
                JFaceProperties.list(RemoveArchiveAction.class, RemoveArchiveAction.OBJECTS, RemoveArchiveAction.OBJECTS).observe(this.removeAction),
                ViewerProperties.multipleSelection().values(BeanProperties.value(EmployeePreference.class, EmployeePreference.EMPLOYEE)).observe(this.viewer));
        /*
         * Bind right editor.
         */
        IObservableValue selection = ViewerProperties.singleSelection().observe(this.viewer);
        IObservableValue employee = BeanProperties.value(EmployeePreference.class, EmployeePreference.EMPLOYEE).observeDetail(selection);
        // Bind firstname
        IObservableValue firstname = BeanProperties.value(Employee.class, Employee.FIRSTNAME, String.class).observeDetail(employee);
        dbc.bindValue(WidgetProperties.text(SWT.FocusOut).observe(this.firstnameText), firstname, new ManagerUpdateValueStrategy(managers), null);
        // Bind lastname
        IObservableValue lastname = BeanProperties.value(Employee.class, Employee.LASTNAME, String.class).observeDetail(employee);
        dbc.bindValue(SWTObservables.observeText(this.lastnameText, SWT.FocusOut), lastname, new ManagerUpdateValueStrategy(managers), null);
        // Bind Hire date
        IObservableValue hireDate = BeanProperties.value(Employee.class, Employee.HIRE_DATE, Date.class).observeDetail(employee);
        dbc.bindValue(
                new DateAndTimeObservableValue(WidgetProperties.selection().observe(this.hireDate), WidgetProperties.selection().observe(this.hireTime)),
                hireDate,
                new ManagerUpdateValueStrategy(managers),
                null);
        // Bind preferred seniority
        IObservableValue preferencialSeniority = BeanProperties.value(Employee.class, Employee.PREFERENCIAL_SENIORITY, Boolean.TYPE).observeDetail(employee);
        dbc.bindValue(SWTObservables.observeSelection(this.preferredSeniorityButton), preferencialSeniority, new ManagerUpdateValueStrategy(managers), null);
        // Bind preferred section viewer.
        IObservableSet proposedSections = PlanimodObservables.listSection(managers);
        IValueProperty sectionLabel = BeanProperties.value(Section.class, Section.NAME).value(BindingProperties.convertedValue(Converters.removeFrontNumber()));
        this.preferredSectionViewer.setComparator(new PropertyViewerComparator(SectionComparators.byName()));
        ViewerSupport.bind(this.preferredSectionViewer, proposedSections, sectionLabel);
        IObservableValue preferredSection = BeanProperties.value(EmployeePreference.class, EmployeePreference.PREFERRED_SECTION).observeDetail(selection);
        dbc
                .bindValue(
                        ViewerProperties.singleSelection().observe(this.preferredSectionViewer),
                        preferredSection,
                        new ManagerUpdateValueStrategy(managers),
                        null);
        // Bind preferred position
        IObservableValue preferredPositionLabel = (new EmployeePreferredPositionValueProperty(_("Aucune préférence"))).observeDetail(selection);
        dbc.bindValue(JFaceProperties.value(
                EmployeeEditPreferredPositionAction.class,
                EmployeeEditPreferredPositionAction.TEXT,
                EmployeeEditPreferredPositionAction.TEXT).observe(this.preferredPositionAction), preferredPositionLabel);
        dbc.bindValue(JFaceProperties.value(
                EmployeeEditPreferredPositionAction.class,
                EmployeeEditPreferredPositionAction.TOOL_TIP_TEXT,
                EmployeeEditPreferredPositionAction.TOOL_TIP_TEXT).observe(this.preferredPositionAction), preferredPositionLabel);
        dbc.bindValue(JFaceProperties.value(
                EmployeeEditPreferredPositionAction.class,
                EmployeeEditPreferredPositionAction.EMPLOYEE_PREFERENCE,
                EmployeeEditPreferredPositionAction.EMPLOYEE_PREFERENCE).observe(this.preferredPositionAction), selection);
        // Bind preferred team viewer
        // Avoid creating columns for this viewer Since the data will not
        // display properly. See bug #176.
        IObservableList preferredTeams = BeanProperties.list(EmployeePreference.class, EmployeePreference.PREFERRED_TEAM, Team.class).observeDetail(selection);
        this.preferredTeamViewer.setContentProvider(new ObservableListContentProvider());
        this.preferredTeamViewer.setLabelProvider(new EmployeePreferredTeamLabelProvider(BeanProperties.value(Team.class, Team.NAME).value(
                BindingProperties.convertedValue(Converters.removeFrontNumber())).observeDetail(new ListToSetAdapter(preferredTeams)), preferredTeams));
        this.preferredTeamViewer.setInput(preferredTeams);
        IObservableValue preferredTeamSelection = ViewerProperties.singleSelection().observe(this.preferredTeamViewer);
        IViewerObservableList preferredTeamSelections = ViewerProperties.multipleSelection().observe(this.preferredTeamViewer);
        // Add all
        dbc.bindValue(JFaceProperties.value(
                EmployeePreferredTeamAddAllAction.class,
                EmployeePreferredTeamAddAllAction.EMPLOYEE_PREFERENCE,
                EmployeePreferredTeamAddAllAction.EMPLOYEE_PREFERENCE).observe(this.preferredTeamAddAllAction), selection);
        // Remove action
        dbc.bindValue(JFaceProperties.value(
                EmployeePreferredTeamRemoveAction.class,
                EmployeePreferredTeamRemoveAction.EMPLOYEE_PREFERENCE,
                EmployeePreferredTeamRemoveAction.EMPLOYEE_PREFERENCE).observe(this.preferredTeamRemoveAction), selection);
        dbc
                .bindValue(JFaceProperties.value(
                        EmployeePreferredTeamRemoveAction.class,
                        EmployeePreferredTeamRemoveAction.SHIFTS,
                        EmployeePreferredTeamRemoveAction.SHIFTS).observe(this.preferredTeamRemoveAction), new ListObservableValue(
                        preferredTeamSelections,
                        Team.class));
        // Move up
        dbc.bindValue(JFaceProperties.value(
                EmployeePreferredTeamMoveAction.class,
                EmployeePreferredTeamMoveAction.EMPLOYEE_PREFERENCE,
                EmployeePreferredTeamMoveAction.EMPLOYEE_PREFERENCE).observe(this.preferredTeamMoveUpAction), selection);
        dbc.bindValue(JFaceProperties
                .value(EmployeePreferredTeamMoveAction.class, EmployeePreferredTeamMoveAction.SHIFT, EmployeePreferredTeamMoveAction.SHIFT)
                .observe(this.preferredTeamMoveUpAction), preferredTeamSelection);
        // Move down
        dbc.bindValue(JFaceProperties.value(
                EmployeePreferredTeamMoveAction.class,
                EmployeePreferredTeamMoveAction.EMPLOYEE_PREFERENCE,
                EmployeePreferredTeamMoveAction.EMPLOYEE_PREFERENCE).observe(this.preferredTeamMoveDownAction), selection);
        dbc.bindValue(JFaceProperties
                .value(EmployeePreferredTeamMoveAction.class, EmployeePreferredTeamMoveAction.SHIFT, EmployeePreferredTeamMoveAction.SHIFT)
                .observe(this.preferredTeamMoveDownAction), preferredTeamSelection);
    }

    /**
     * Use to create widgets.
     * 
     * @param parent
     */
    private void createEditorControls(Composite parent) {
        PlanimodManagers managers = getSite().getService(PlanimodManagers.class);
        Composite comp = new Composite(parent, SWT.NONE);
        comp.setLayout(new GridLayout(2, false));
        /*
         * Firstname
         */
        Label label = new Label(comp, SWT.NONE);
        label.setText(_("Prénom :"));
        this.firstnameText = new Text(comp, SWT.BORDER);
        this.firstnameText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        /*
         * Lastname
         */
        label = new Label(comp, SWT.NONE);
        label.setText(_("Nom:"));
        this.lastnameText = new Text(comp, SWT.BORDER);
        this.lastnameText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        /*
         * Hiredate
         */
        label = new Label(comp, SWT.NONE);
        label.setText(_("Date d'ancienneté :"));
        GridLayout layout = new GridLayout(2, false);
        layout.horizontalSpacing = 2;
        layout.verticalSpacing = 0;
        layout.marginHeight = 0;
        layout.marginWidth = 0;
        Composite hireDateTimeComp = new Composite(comp, SWT.NONE);
        hireDateTimeComp.setLayout(layout);
        this.hireDate = new DateTime(hireDateTimeComp, SWT.DROP_DOWN | SWT.BORDER | SWT.DATE);
        this.hireDate.setLayoutData(new GridData(SWT.FILL, SWT.FILL, false, false));
        this.hireTime = new DateTime(hireDateTimeComp, SWT.BORDER | SWT.TIME | SWT.SHORT);
        this.hireTime.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        /*
         * Preferred Seniority
         */
        label = new Label(comp, SWT.NONE);
        label.setText("");
        this.preferredSeniorityButton = new Button(comp, SWT.CHECK);
        this.preferredSeniorityButton.setText(_("Ancienneté privilégiée"));
        /*
         * Preferred Position
         */
        Group group = new Group(comp, SWT.NONE);
        group.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
        group.setLayout(new GridLayout(2, false));
        group.setText(_("Préférences"));
        label = new Label(group, SWT.NONE);
        label.setText(_("Poste attitré :"));
        ActionContributionItem preferredPositionContributionItem = new ActionContributionItem(
                this.preferredPositionAction = new EmployeeEditPreferredPositionAction(managers, getSite()));
        preferredPositionContributionItem.fill(group);
        ((Control) preferredPositionContributionItem.getWidget()).setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        /*
         * Preferred section
         */
        label = new Label(group, SWT.NONE);
        label.setText(_("Section préférée :"));
        this.preferredSectionViewer = new ComboViewer(group, SWT.BORDER | SWT.READ_ONLY | SWT.DROP_DOWN);
        /*
         * Preferred shifts
         */
        GridLayout comp2Layout = new GridLayout(4, false);
        comp2Layout.marginWidth = 0;
        comp2Layout.marginHeight = 0;
        Composite comp2 = new Composite(group, SWT.NONE);
        comp2.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
        comp2.setLayout(comp2Layout);
        label = new Label(comp2, SWT.NONE);
        label.setText(_("Préférence d'équipe de travail :"));
        label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 4, 1));
        this.preferredTeamViewer = new TableViewer(comp2);
        this.preferredTeamViewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 4, 1));
        // Add all shift action
        ActionContributionItem aci;
        aci = new ActionContributionItem(this.preferredTeamAddAllAction = new EmployeePreferredTeamAddAllAction(managers));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);
        aci.fill(comp2);
        // Remove action
        aci = new ActionContributionItem(this.preferredTeamRemoveAction = new EmployeePreferredTeamRemoveAction(managers));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);
        aci.fill(comp2);
        // Move Up action
        aci = new ActionContributionItem(this.preferredTeamMoveUpAction = new EmployeePreferredTeamMoveAction(
                managers,
                EmployeePreferredTeamMoveAction.DIRECTION_UP));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);
        aci.fill(comp2);
        // Move down Action
        aci = new ActionContributionItem(this.preferredTeamMoveDownAction = new EmployeePreferredTeamMoveAction(
                managers,
                EmployeePreferredTeamMoveAction.DIRECTION_DOWN));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);
        aci.fill(comp2);
        /*
         * Spacer
         */
        label = new Label(comp, SWT.NONE);
        label.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true, 2, 1));
    }

    /**
     * Create the required column to display the preferred shifts and the associated order.
     * 
     * @param viewer
     *            the parent viewer
     */
    private void createPreferredTeamColumns(final TableViewer viewer, IObservableSet knownElements) {
        // Team
        IValueProperty teamLabel = BeanProperties.value(Team.class, Team.NAME).value(BindingProperties.convertedValue(Converters.removeFrontNumber()));
        ColumnSupport.create(viewer, _("équipe de travail"), knownElements, teamLabel);
    }

    /**
     * Create columns in the table viewer.
     * 
     * @param viewer
     *            the table viewer
     * @param employeePreferences
     *            the known elements
     */
    private void createViewerColumns(TableViewer viewer, IObservableSet employeePreferences) {
        // RedId
        IValueProperty refIdProperty = BeanProperties.value(EmployeePreference.class, EmployeePreference.EMPLOYEE + DOT + Employee.REFID);
        ColumnSupport.create(viewer, _("No."), employeePreferences, refIdProperty).addPropertySorting().addTextEditingSupport(
                getDbc(),
                new ManagerUpdateValueStrategy(getSite().getService(PlanimodManagers.class), UpdateValueStrategy.POLICY_CONVERT),
                null);
        // FirstName
        IValueProperty firstnameProperty = BeanProperties.value(EmployeePreference.class, EmployeePreference.EMPLOYEE + DOT + Employee.FIRSTNAME);
        ColumnSupport.create(viewer, ("Prénom"), employeePreferences, firstnameProperty).setWidth(150).addPropertySorting().addTextEditingSupport(
                getDbc(),
                new ManagerUpdateValueStrategy(getSite().getService(PlanimodManagers.class), UpdateValueStrategy.POLICY_CONVERT),
                null);
        // Lastname
        IValueProperty lastnameProperty = BeanProperties.value(EmployeePreference.class, EmployeePreference.EMPLOYEE + DOT + Employee.LASTNAME);
        ColumnSupport.create(viewer, _("Nom"), employeePreferences, lastnameProperty).setWidth(150).addPropertySorting().addTextEditingSupport(
                getDbc(),
                new ManagerUpdateValueStrategy(getSite().getService(PlanimodManagers.class), UpdateValueStrategy.POLICY_CONVERT),
                null);
        // HireDate
        IValueProperty hireDateLabelProperty = BeanProperties.value(EmployeePreference.class, EmployeePreference.EMPLOYEE + DOT + Employee.HIRE_DATE).value(
                BindingProperties.convertedValue(Converters.shortDateConverter()));
        ColumnSupport.create(viewer, _("Date d'ancienneté"), employeePreferences, hireDateLabelProperty).setWidth(150).addPropertySorting(
                new SelfValueProperty(EmployeePreference.class),
                EmployeePreferenceComparators.bySeniority()).activateSorting();
        // Preferred Seniority
        IValueProperty preferredSeniorityProperty = BeanProperties.value(EmployeePreference.class, EmployeePreference.EMPLOYEE
                + DOT
                + Employee.PREFERENCIAL_SENIORITY, Boolean.TYPE);
        IValueProperty preferredSeniorityLabel = preferredSeniorityProperty.value(BindingProperties.convertedValue(Converters.booleanConverter()));
        ColumnSupport.create(viewer, _("Ancienneté privilégiée"), employeePreferences, preferredSeniorityLabel).addPropertySorting();
        // Preferred Section
        IValueProperty preferredSectionProperty = BeanProperties.value(EmployeePreference.class, EmployeePreference.PREFERRED_SECTION + DOT + Section.NAME);
        IValueProperty preferredSectionLabel = preferredSectionProperty.value(BindingProperties.convertedValue(Converters.removeFrontNumber()));
        ColumnSupport.create(viewer, _("Section préférée"), employeePreferences, preferredSectionLabel).setWidth(150).addPropertySorting();
        // Preferred Position
        IValueProperty preferredPositionLabel = new EmployeePreferredPositionValueProperty();
        ColumnSupport.create(viewer, _("Poste attitré"), employeePreferences, preferredPositionLabel).addPropertySorting();
        // Column preferences
        ColumnViewerPreferences.create(viewer, PlanimodPreferences.getPreferenceStore(), PREFERENCE_COLUMNS);
    }

    /**
     * This implementation dispose the editor.
     */
    @Override
    public void deactivate() {
        try {
            this.firstnameText = null;
            this.hireDate = null;
            this.hireTime = null;
            this.lastnameText = null;
            this.preferredSectionViewer = null;
            this.preferredSeniorityButton = null;
            this.preferredTeamAddAllAction = null;
            this.preferredTeamRemoveAction = null;
            this.preferredTeamViewer = null;
            if (this.preferredTeamMoveDownAction != null) {
                this.preferredTeamMoveDownAction.dispose();
                this.preferredTeamMoveDownAction = null;
            }
            if (this.preferredTeamMoveUpAction != null) {
                this.preferredTeamMoveUpAction.dispose();
                this.preferredTeamMoveUpAction = null;
            }
            this.viewer = null;
        } finally {
            super.deactivate();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    /**
     * Populate the tool bar.
     */
    void fillToolBar() {
        IToolBarManager toolbar = getSite().getToolBarManager();
        toolbar.removeAll();
        /*
         * Add action to tool-bar
         */
        // Add
        ActionContributionItem aci;
        toolbar.add(aci = new ActionContributionItem(new AddEmployeeAction(getSite().getService(PlanimodManagers.class), getSite(), this.viewer)));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);
        // Remove
        toolbar.add(aci = new ActionContributionItem(this.removeAction = new RemoveArchiveAction(getSite().getService(PlanimodManagers.class), getSite())));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);
        // Separator
        toolbar.add(new Separator());
        /*
         * Add Print actions
         */
        ActionMenuManager printMenu = new PrintActionMenuManager("EmployeeViewPart.printMenu");
        printMenu.add(new EmployeeBySeniorityPrintAction(getSite().getService(PlanimodManagers.class), getSite(), PrintAction.PRINT_ACTION, getSite()
                .getMainWindow()));
        printMenu.add(new EmployeeBySeniorityPrintAction(getSite().getService(PlanimodManagers.class), getSite(), PrintAction.PREVIEW_ACTION, getSite()
                .getMainWindow()));
        // Separator
        printMenu.add(new Separator());
        // Print employee by section
        printMenu.add(new EmployeeBySectionPrintAction(getSite().getService(PlanimodManagers.class), getSite(), PrintAction.PRINT_ACTION, getSite()
                .getMainWindow()));
        printMenu.add(new EmployeeBySectionPrintAction(getSite().getService(PlanimodManagers.class), getSite(), PrintAction.PREVIEW_ACTION, getSite()
                .getMainWindow()));
        toolbar.add(printMenu);
        // Filter
        fillToolbarWithTextFilter();
    }
}
