/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.employees;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.eclipse.core.databinding.observable.list.IListChangeListener;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.list.ListChangeEvent;
import org.eclipse.core.databinding.observable.list.ListDiffEntry;
import org.eclipse.core.databinding.observable.list.ListDiffVisitor;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;
import org.eclipse.swt.custom.CCombo;

import com.planimod.ui.views.planif.EmployeeLabelWithAnnotation;

/**
 * This implementation of label provider display the item order in front of the team name.
 * <p>
 * i.e.: 1. Jour semaine
 * 
 * 
 * @author Patrik Dufresne
 * 
 */
public class EmployeePreferredTeamLabelProvider extends ObservableMapLabelProvider {

    private IListChangeListener listChangeListener = new IListChangeListener() {

        @Override
        public void handleListChange(ListChangeEvent event) {

            int minIndex = teamList.size();
            for (ListDiffEntry e : event.diff.getDifferences()) {
                minIndex = Math.min(minIndex, e.getPosition());
            }

            fireLabelProviderChanged(new LabelProviderChangedEvent(EmployeePreferredTeamLabelProvider.this, teamList
                    .subList(minIndex, teamList.size())
                    .toArray()));
        }

    };
    private IObservableList teamList;

    /**
     * Create a new label provider.
     * 
     * @param teamLabelMap
     *            an observable map providing the team's label.
     * @param teamList
     *            an observable list defining the team order.
     */
    public EmployeePreferredTeamLabelProvider(IObservableMap teamLabelMap, IObservableList teamList) {
        super(teamLabelMap);
        if (teamList == null || teamLabelMap == null) {
            throw new IllegalArgumentException();
        }
        this.teamList = teamList;
        this.teamList.addListChangeListener(this.listChangeListener);
    }

    @Override
    public void dispose() {
        try {
            if (this.teamList != null) {
                this.teamList.removeListChangeListener(this.listChangeListener);
                this.teamList = null;
            }
        } finally {
            super.dispose();
        }
    }

    /**
     * This implementation prefix the team name with the order number.
     */
    @Override
    public String getColumnText(Object element, int columnIndex) {
        int index = this.teamList.indexOf(element);
        return index + 1 + ". " + super.getColumnText(element, columnIndex);
    }

}
