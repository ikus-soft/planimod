/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif;

import static com.planimod.ui.Localized._;

import java.util.Date;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.ObservablesManager;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.swt.widgets.Composite;

import com.patrikdufresne.jface.databinding.preference.PreferenceStoreProperties;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.ui.PageBookViewPart;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.planif.GeneratePlanifContext;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.preference.PlanimodPreferences;

/**
 * Page book to display planif views.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanifViewPart extends PageBookViewPart {

    /**
     * The part id.
     */
    public static final String ID = "com.planimod.ui.views.planif.PlanifViewPart";

    /**
     * The generate planif context use by the view parts.
     */
    private GeneratePlanifContext context;

    /**
     * Data binding context.
     */
    private DataBindingContext dbc;

    private ObservablesManager om;

    /**
     * Create a new view part.
     */
    public PlanifViewPart() {
        super(ID);
        setTitle(_("Planification"));
    }

    /**
     * This implementation creates a view book to hold multiple view part.
     */
    @Override
    public void activate(Composite parent) {

        // Create the context
        try {
            this.context = getSite().getService(PlanimodManagers.class).getTaskManager().createGeneratePlanifContext();
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }

        super.activate(parent);
        /*
         * Proceed with binding.
         */
        this.dbc = new DataBindingContext();
        this.om = new ObservablesManager();
        this.om.runAndCollect(new Runnable() {
            @Override
            public void run() {
                bindValues();
            }
        });
        this.om.addObservablesFromContext(this.dbc, true, true);
    }

    @Override
    protected void addViews() {
        // Add the views.
        addView(new TaskViewPartPage());
        addView(new ProposedTaskViewPartPage());
    }

    /**
     * This implementation bind the planimod state with the Generate planif context.
     */
    protected void bindValues() {
        /**
         * Bind the context to the date preference.
         */
        if (this.context != null) {
            WritableValue date = new WritableValue(null, Date.class);
            this.dbc.bindValue(date, PreferenceStoreProperties.value(PlanimodPreferences.DATE, String.class).observe(PlanimodPreferences.getPreferenceStore()));
            if (date.getValue() == null) {
                date.setValue(new Date());
            }
            this.dbc.bindValue(BeanProperties.value(GeneratePlanifContext.class, GeneratePlanifContext.WEEK).observe(this.context), date);
        }
    }

    /**
     * This implementation dispose the generate planif context.
     */
    @Override
    public void deactivate() {
        super.deactivate();
        if (this.context != null) {
            this.context.dispose();
            this.context = null;
        }

        /*
         * Dispose the Observable manager
         */
        if (this.om != null) {
            this.om.dispose();
            this.om = null;
        }
        if (this.dbc != null) {
            this.dbc.dispose();
            this.dbc = null;
        }
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    /**
     * Return the generate planif context of this view part. This function should be used by the pages to share the same
     * context.
     * 
     * @return the generate planif context.
     */
    public GeneratePlanifContext getContext() {
        return this.context;
    }

}
