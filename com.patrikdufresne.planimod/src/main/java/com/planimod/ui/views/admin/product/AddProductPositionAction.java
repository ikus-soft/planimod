/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
/**
 * 
 */
package com.planimod.ui.views.admin.product;

import static com.planimod.ui.Localized._;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.window.IShellProvider;

import com.patrikdufresne.managers.ManagedObject;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.jface.AbstractAddAction;
import com.planimod.core.IntegrityException;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Position;
import com.planimod.core.Product;
import com.planimod.core.ProductPosition;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * Action to create a new product-position object and add it to the database.
 * 
 * @author Patrik Dufresne
 * 
 */
public class AddProductPositionAction extends AbstractAddAction {

    public static final String PRODUCT = "product";

    /**
     * The product to associated with.
     */
    private Product product;

    /**
     * Create a new action.
     * 
     * @param shellProvider
     *            a shell provider in case the action need to display a message box.
     */
    public AddProductPositionAction(PlanimodManagers managers, IShellProvider shellProvider, ISelectionProvider selectionProvider) {
        super(managers, shellProvider, selectionProvider);
        setText(_("Ajouter"));
        setToolTipText(_("Ajouter un poste pour ce produit"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_LIST_ADD_16));
    }

    /**
     * This implementation always return true.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#canCreateObject()
     */
    @Override
    protected boolean canCreateObject() {
        return this.product != null;
    }

    /**
     * This implementation create rider object.
     * 
     * @throws IntegrityException
     *             if the objects can be created for integrity reason.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#createObjects()
     */
    @Override
    protected List<? extends ManagedObject> createObjects() throws IntegrityException {
        // Create product position object
        ProductPosition pp = new ProductPosition();
        pp.setProduct(this.product);
        pp.setNumber(Integer.valueOf(1));

        // Find a proper position object
        List<Position> list;
        try {
            list = ((PlanimodManagers) getManagers()).getPositionManager().list();
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
            return new ArrayList<ManagedObject>(0);
        }

        // Raise exception is not position is found.
        if (list.size() == 0) {
            throw new IntegrityException(_("Ajouter un nouveau poste avant d'exécuter cette opération."));
        }

        pp.setPosition(list.get(0));
        return Arrays.asList(pp);
    }

    /**
     * Returns the product to to create the product-position object.
     * 
     * @return
     */
    public Product getProduct() {
        return this.product;
    }

    /**
     * Sets the product used to create the product-position object.
     * 
     * @param product
     */
    public void setProduct(Product product) {
        firePropertyChange(PRODUCT, this.product, this.product = product);
        setEnabled(canCreateObject());
    }
}