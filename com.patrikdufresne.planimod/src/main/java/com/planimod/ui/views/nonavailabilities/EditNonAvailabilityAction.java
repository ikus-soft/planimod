/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.nonavailabilities;

import static com.planimod.ui.Localized._;

import java.util.Arrays;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.NonAvailability;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.TimeRanges;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * Action to edit a non-availability event.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EditNonAvailabilityAction extends Action {

    /**
     * Property name of this action non-availability event (value <code>"nonAvailability"</code>). The non-availability
     * property is used to define the object to edit.
     */
    public static final String NON_AVAILABILITY = "nonAvailability";

    public static void edit(PlanimodManagers managers, Shell shell, NonAvailability event) {

        boolean isArchived = event.getEmployee().getArchivedDate() != null;

        // Open the dialog
        NonAvailabilityDialog dlg = new NonAvailabilityDialog(shell, managers, NonAvailabilityDialog.EDIT);
        dlg.setStartDate(event.getStartDate());
        dlg.setEndDate(event.getEndDate());
        dlg.setSummary(event.getSummary());
        dlg.setEmployee(event.getEmployee());
        dlg.setVisible(event.isVisible());
        if (dlg.open() != Window.OK) {
            // Cancel by user
            return;
        }

        // Don't change the employee if it was archived.
        if (!isArchived) {
            event.setEmployee(dlg.getEmployee());
        }
        event.setStartDate(TimeRanges.removeSecond(dlg.getStartDate()));
        event.setEndDate(TimeRanges.removeSecond(dlg.getEndDate()));
        event.setSummary(dlg.getSummary());
        event.setVisible(dlg.getVisible());

        // Save the event
        try {
            managers.getNonAvailabilityManager().update(Arrays.asList(event));
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
            return;
        }
    }

    /**
     * Managers to use
     */
    private PlanimodManagers managers;

    /**
     * The event to edit.
     */
    private NonAvailability nonAvailability;

    /**
     * Shell provider.
     */
    private IShellProvider shellProvider;

    /**
     * Create a new action.
     * 
     * @param managers
     * @param provider
     */
    public EditNonAvailabilityAction(PlanimodManagers managers, IShellProvider provider) {
        if (managers == null || provider == null) {
            throw new NullPointerException();
        }
        this.managers = managers;
        this.shellProvider = provider;
        setText(_("Modifier"));
        setToolTipText(_("Modifie une absence"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_EDIT_16));
        setEnabled(canRun());

    }

    /**
     * Check if this action can be run.
     * 
     * @return True if this action can be run.
     */
    protected boolean canRun() {
        return this.nonAvailability != null;
    }

    /**
     * Returns the current non-availability even.
     * 
     * @return
     */
    public NonAvailability getNonAvailability() {
        return this.nonAvailability;
    }

    /**
     * This implementation open a dialog to edit the non-availability.
     */
    @Override
    public void run() {
        if (this.nonAvailability == null) {
            return;
        }
        edit(this.managers, this.shellProvider.getShell(), this.nonAvailability);
    }

    /**
     * Sets the non-availability to edit.
     * 
     * @param nonAvailability
     *            the non-availability or null
     */
    public void setNonAvailability(NonAvailability nonAvailability) {
        if ((this.nonAvailability == null && nonAvailability != null)
                || (this.nonAvailability != null && nonAvailability == null)
                || (this.nonAvailability != null && nonAvailability != null && !nonAvailability.equals(this.nonAvailability))) {
            NonAvailability oldValue = this.nonAvailability;
            this.nonAvailability = nonAvailability;
            firePropertyChange(NON_AVAILABILITY, oldValue, this.nonAvailability);
            setEnabled(canRun());
        }
    }
}