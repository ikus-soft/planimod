/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.employees;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.map.HashedMap;
import org.eclipse.core.databinding.observable.Diffs;

import com.patrikdufresne.managers.ManagerEvent;
import com.patrikdufresne.managers.databinding.ManagedObjectComputedSet;
import com.planimod.core.Employee;
import com.planimod.core.EmployeePreference;
import com.planimod.core.PlanimodManagers;

/**
 * A computed set to compute only the employee preferences for un-archived employee.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EmployeePreferenceComputedSet extends ManagedObjectComputedSet {

    /**
     * Create a map of event to listen to.
     * 
     * @return
     */
    private static Map<Class, Integer> events() {
        Map<Class, Integer> map = new HashedMap();
        map.put(EmployeePreference.class, ManagerEvent.ALL);
        map.put(Employee.class, ManagerEvent.UPDATE);
        return map;
    }

    /**
     * Create a new computed set for employee preferences
     * 
     * @param managers
     *            the planimod managers.
     */
    public EmployeePreferenceComputedSet(PlanimodManagers managers) {
        super(managers, EmployeePreference.class, events());
    }

    /**
     * This implementation is selecting un-archived elements.
     */
    @Override
    protected boolean doSelect(Object element) {
        return element instanceof EmployeePreference && ((EmployeePreference) element).getEmployee().getArchivedDate() == null;
    }

    /**
     * This implementation handle event to Employee class.
     */
    protected void notifyIfChanged(ManagerEvent event) {

        // Check if event is realted to Employee class
        if (!Employee.class.equals(event.clazz)) {
            super.notifyIfChanged(event);
            return;
        }

        // Handle event related to Employee class.
        Set<Object> additions = new HashSet<Object>();
        Set<Object> removals = new HashSet<Object>();
        if ((event.type & ManagerEvent.UPDATE) != 0) {
            for (Object element : event.objects) {

                // Check if the employee is archived.
                if (element instanceof Employee && ((Employee) element).getArchivedDate() != null) {

                    Iterator iter = this.cachedSet.iterator();
                    while (iter.hasNext()) {
                        Object obj = iter.next();
                        if (obj instanceof EmployeePreference && element.equals(((EmployeePreference) obj).getEmployee())) {
                            iter.remove();
                            removals.add(obj);
                            break;
                        }
                    }
                }

            }
        }

        // Fire change
        if (additions.size() != 0 || removals.size() != 0) {
            fireSetChange(Diffs.createSetDiff(additions, removals));
        }

    }
}
