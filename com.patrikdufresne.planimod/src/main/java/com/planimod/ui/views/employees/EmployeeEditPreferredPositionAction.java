/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.employees;

import java.util.Arrays;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.EmployeePreference;
import com.planimod.core.PlanimodManagers;
import com.planimod.ui.PlanimodPolicy;

/**
 * Action to edit the preferred position of an employee. This action open a new dialog to edit the preferred position.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EmployeeEditPreferredPositionAction extends Action {

    /**
     * The employee preference property.
     */
    public static final String EMPLOYEE_PREFERENCE = "employeePreference";

    /**
     * The employee preference.
     */
    private EmployeePreference employeePreference;
    /**
     * Managers to use.
     */
    private PlanimodManagers managers;

    private IShellProvider shell;

    /**
     * Create a new action.
     * 
     * @param direction
     *            the direction of the move
     */
    public EmployeeEditPreferredPositionAction(PlanimodManagers managers, IShellProvider shell) {
        if (managers == null || shell == null) {
            throw new IllegalArgumentException();
        }
        this.managers = managers;
        this.shell = shell;
        setEnabled(canRun());
    }

    protected boolean canRun() {
        return this.employeePreference != null;
    }

    /**
     * Returns the current employee preference.
     * 
     * @return
     */
    public EmployeePreference getEmployeePreference() {
        return this.employeePreference;
    }

    @Override
    public void run() {
        // Check if the observable is a employee-preference
        if (this.employeePreference == null) {
            return;
        }

        // Open the dialog
        PreferredPositionDialog dlg = new PreferredPositionDialog(this.shell.getShell(), this.managers);
        dlg.setShift(this.employeePreference.getPreferredPositionTeam());
        dlg.setPosition(this.employeePreference.getPreferredPosition());

        switch (dlg.open()) {
        case Window.OK:
            // Update the entity
            this.employeePreference.setPreferredPosition(dlg.getPosition());
            this.employeePreference.setPreferredPositionTeam(dlg.getShift());
            break;
        case PreferredPositionDialog.CLEAR_ID:
            // Update the entity
            this.employeePreference.setPreferredPosition(null);
            this.employeePreference.setPreferredPositionTeam(null);
            break;
        default:
            // Operation canceled by user
            return;
        }

        // Persists the modification
        try {
            this.managers.updateAll(Arrays.asList(this.employeePreference));
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }
    }

    /**
     * Sets the employee preference to update.
     * 
     * @param employeePreference
     */
    public void setEmployeePreference(EmployeePreference employeePreference) {
        firePropertyChange(EMPLOYEE_PREFERENCE, this.employeePreference, this.employeePreference = employeePreference);
        setEnabled(canRun());
    }

}
