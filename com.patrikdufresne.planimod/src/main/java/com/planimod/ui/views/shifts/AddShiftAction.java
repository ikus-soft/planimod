/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
/**
 * 
 */
package com.planimod.ui.views.shifts;

import static com.planimod.ui.Localized._;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;

import com.patrikdufresne.managers.ManagedObject;
import com.patrikdufresne.managers.jface.AbstractAddAction;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Shift;
import com.planimod.core.Team;
import com.planimod.core.TimeRanges;
import com.planimod.ui.theme.Resources;

/**
 * Action to create a new non-availability event and add it to the database.
 * 
 * @author Patrik Dufresne
 * 
 */
public class AddShiftAction extends AbstractAddAction {
    /**
     * Property name of an action's date (value <code>"date"</code>). The date property is used sets a default time range.
     */
    public static final String DATE = "date";
    /**
     * A date used as a hint to create the new shift event.
     */
    private Date date;
    /**
     * Temporary end date.
     */
    private Date end;

    /**
     * Temporary shift object, or null.
     */
    private Team shift;

    /**
     * Temporary start date.
     */
    private Date start;

    /**
     * Create a new action.
     * 
     * @param shellProvider
     *            a shell provider in case the action need to display a message box.
     */
    public AddShiftAction(PlanimodManagers managers, IShellProvider shellProvider, ISelectionProvider selectionProvider) {
        super(managers, shellProvider, selectionProvider);
        setText(_("Ajouter"));
        setToolTipText(_("Ajoute un quart de travail."));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_LIST_ADD_16));
    }

    /**
     * This implementation always return true.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#canCreateObject()
     */
    @Override
    protected boolean canCreateObject() {
        return true;
    }

    /**
     * This implementation create non-availability event object.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#createObjects()
     */
    @Override
    protected List<? extends ManagedObject> createObjects() {
        List<ManagedObject> list = new ArrayList<ManagedObject>();
        // Create the event
        Shift event = new Shift();
        event.setTeam(this.shift);
        event.setStartDate(this.start);
        event.setEndDate(this.end);
        list.add(event);
        return list;
    }

    /**
     * Returns the current date state of null if not define.
     * 
     * @return the date state or null.
     */
    public Date getDate() {
        return this.date;
    }

    @Override
    public void run() {
        ShiftDialog dlg = new ShiftDialog(getShellProvider(), (PlanimodManagers) getManagers(), ShiftDialog.NEW);
        Date curDate = this.date == null ? new Date() : this.date;
        dlg.setStart(curDate);
        dlg.setEnd(curDate);
        dlg.setShift(null);
        if (dlg.open() != Window.OK) {
            return;
        }

        this.start = TimeRanges.removeSecond(dlg.getStart());
        this.end = TimeRanges.removeSecond(dlg.getEnd());
        this.shift = dlg.getShift();

        if (this.shift == null || this.start == null || this.end == null || this.start.compareTo(this.end) > 0) {
            return;
        }

        super.run();
    }

    /**
     * Sets a default date value.
     * 
     * @param date
     *            the date value or null
     */
    public void setDate(Date date) {
        if ((this.date == null && date != null) || (this.date != null && date == null) || (this.date != null && date != null && !date.equals(this.date))) {
            Date oldValue = this.date;
            this.date = date;
            firePropertyChange(DATE, oldValue, this.date);
        }
    }
}