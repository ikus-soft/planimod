/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.databinding.ManagedObjectComputedSet;
import com.planimod.core.NonAvailabilityManager;
import com.planimod.core.Task;
import com.planimod.core.TaskManager;
import com.planimod.core.planif.GeneratePlanifContext;
import com.planimod.ui.PlanimodPolicy;

/**
 * This computed set is used to wrap the function
 * {@link NonAvailabilityManager#listProductionEvents(Planif, Date, Date)} and make it observable.
 * 
 */
public class TaskForContextComputedSet extends ManagedObjectComputedSet {

    private GeneratePlanifContext context;

    /**
     * Property change listener.
     */
    private PropertyChangeListener listener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            getRealm().exec(new Runnable() {
                @Override
                public void run() {
                    makeDirty();
                }
            });
        }
    };

    /**
     * Create an observable set.
     * 
     * @param managers
     *            the managers to use
     * @param planif
     *            the planif observable
     * @param start
     *            the start date observable
     * @param end
     *            the end date observable
     * @return the observable set
     */
    public TaskForContextComputedSet(GeneratePlanifContext context) {
        super(context.getManagers(), Task.class);
        this.context = context;
    }

    /**
     * This implementation run the {@link TaskManager#listByPlanif(Planif, Date, Date) }. function.
     */
    @Override
    protected Collection doList() throws ManagerException {
        try {
            return this.context.listTask();
        } catch (Exception e) {
            PlanimodPolicy.showException(e);
            return Collections.EMPTY_SET;
        }
    }

    @Override
    protected void makeDirty() {
        super.makeDirty();
    }

    @Override
    protected void startListening() {
        super.startListening();
        this.context.addPropertyChangeListener(GeneratePlanifContext.LIST_TASK_BY_EMPLOYEE, this.listener);
    }

    @Override
    protected void stopListening() {
        super.stopListening();
        this.context.removePropertyChangeListener(GeneratePlanifContext.LIST_TASK_BY_EMPLOYEE, this.listener);
    }

}