/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
/**
 * 
 */
package com.planimod.ui.views.admin.position;

import static com.planimod.ui.Localized._;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.window.IShellProvider;

import com.patrikdufresne.managers.ManagedObject;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.jface.AbstractAddAction;
import com.planimod.core.IntegrityException;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Position;
import com.planimod.core.Section;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * Action to create a new Rider object and add it to the database.
 * 
 * @author dufresne
 * 
 */
public class AddPositionAction extends AbstractAddAction implements ISelectionChangedListener {

    /**
     * Create a new action.
     * 
     * @param shellProvider
     *            a shell provider in case the action need to display a message box.
     */
    public AddPositionAction(PlanimodManagers managers, IShellProvider shellProvider, ISelectionProvider provider) {
        super(managers, shellProvider, provider);
        setText(_("Ajouter"));
        setToolTipText(_("Ajouter un nouveau poste"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_LIST_ADD_16));
    }

    /**
     * This implementation create rider object.
     * 
     * @throws IntegrityException
     *             if the object can't be created for integrity reason.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#createObjects()
     */
    @Override
    protected List<? extends ManagedObject> createObjects() throws IntegrityException {
        List<Section> sections;
        try {
            sections = ((PlanimodManagers) getManagers()).getSectionManager().list();
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
            return Collections.EMPTY_LIST;
        }

        Position position = new Position();
        position.setName(_("Nouveau poste"));

        // Create a new section if none exists.
        if (sections.size() == 0) {
            throw new IntegrityException(_("Veuillez ajouter une nouvelle section avant d'exécuter cette oppération."));
        }
        position.setSection(sections.get(0));
        return Arrays.asList(position);
    }

    @Override
    public void selectionChanged(SelectionChangedEvent event) {
        // TODO Auto-generated method stub

    }

    @Override
    protected boolean canCreateObject() {
        return true;
    }

}