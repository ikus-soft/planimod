/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin;

import static com.planimod.ui.Localized._;

import com.patrikdufresne.ui.PageBookViewPart;
import com.planimod.ui.views.admin.position.PositionPageViewPart;
import com.planimod.ui.views.admin.product.ProductPageViewPart;
import com.planimod.ui.views.admin.section.SectionPageViewPart;
import com.planimod.ui.views.admin.team.TeamPageViewPart;

/**
 * This view part is used to administer the advance parameter of the application.
 * 
 * @author Patrik Dufresne
 * 
 */
public class AdminViewPart extends PageBookViewPart {

    /**
     * The part id.
     */
    public static final String ID = "com.planimod.ui.views.admin.AdminViewPart";

    /**
     * Create a new view part.
     */
    public AdminViewPart() {
        super(ID);
        setTitle(_("Administration"));
    }

    /**
     * This implementation ass the views to this book.
     */
    @Override
    protected void addViews() {
        addView(new AdminPageViewPart());
        addView(new SectionPageViewPart());
        addView(new PositionPageViewPart());
        addView(new ProductPageViewPart());
        addView(new TeamPageViewPart());
    }

}
