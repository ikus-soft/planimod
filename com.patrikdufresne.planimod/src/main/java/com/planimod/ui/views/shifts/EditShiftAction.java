/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.shifts;

import static com.planimod.ui.Localized._;

import java.util.Arrays;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.widgets.Shell;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Shift;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * Action to edit a non-availability event.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EditShiftAction extends Action {
    /**
     * Property name for shift (value <code>"shift"</code> ).
     */
    public static final String SHIFT = "shift";

    /**
     * Static function to edit a shift event.
     * 
     * @param managers
     *            the managers
     * @param shell
     *            the parent shell
     * @param event
     *            the calendar event to edit
     */
    public static void edit(PlanimodManagers managers, Shell shell, Shift event) {
        // Open the dialog
        ShiftDialog dlg = new ShiftDialog(shell, managers, ShiftDialog.EDIT);
        dlg.setStart(event.getStartDate());
        dlg.setEnd(event.getEndDate());
        dlg.setShift(event.getTeam());
        if (dlg.open() != Window.OK) {
            // Cancel by user
            return;
        }

        // Validation
        if (dlg.getShift() == null || dlg.getStart() == null || dlg.getEnd() == null || dlg.getStart().compareTo(dlg.getEnd()) > 0) {
            return;
        }

        event.setTeam(dlg.getShift());
        event.setStartDate(dlg.getStart());
        event.setEndDate(dlg.getEnd());

        // Save the event
        try {
            managers.getShiftManager().update(Arrays.asList(event));
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
            return;
        }
    }

    /**
     * Managers to use
     */
    private PlanimodManagers managers;
    /**
     * Shell provider.
     */
    private IShellProvider shellProvider;

    /**
     * The shift event to be edited.
     */
    private Shift shift;

    /**
     * Create a new action.
     * 
     * @param managers
     * @param provider
     * @param selectionProvider
     */
    public EditShiftAction(PlanimodManagers managers, IShellProvider provider) {
        this.managers = managers;
        this.shellProvider = provider;
        setText(_("Modifier"));
        setToolTipText(_("Modifier un quart de travail"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_EDIT_16));
        setEnabled(canRun());
    }

    /**
     * This function is used to determine if the action can be run.
     * 
     * @return True if the shift event are define.
     */
    protected boolean canRun() {
        return this.shift != null;
    }

    public Shift getShift() {
        return this.shift;
    }

    /**
     * This implementation open a dialog to edit the event.
     */
    @Override
    public void run() {
        // Retrieve calendar event
        if (this.shift == null) {
            return;
        }
        edit(this.managers, this.shellProvider.getShell(), this.shift);
    }

    /**
     * Sets the shift event value for this action.
     * 
     * @param shift
     */
    public void setShift(Shift shift) {
        if ((this.shift == null && shift != null)
                || (this.shift != null && shift == null)
                || (this.shift != null && shift != null && !shift.equals(this.shift))) {
            Object oldDescription = this.shift;
            this.shift = shift;
            firePropertyChange(SHIFT, oldDescription, this.shift);
            setEnabled(canRun());
        }
    }
}