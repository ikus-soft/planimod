/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin;

import static com.planimod.ui.Localized._;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.ObservablesManager;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CLabel;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;

import com.planimod.ui.preference.PlanimodPreferences;

/**
 * This dialog is used to change the Planimod parameters.
 * 
 * @author Patrik Dufresne
 * 
 */
public class AppearancePreferencePage extends PreferencePage {

    private WritableValue showLines = new WritableValue(null, Boolean.class);

    private Button showLinesButton;

    private DataBindingContext dbc;

    private ObservablesManager om;

    /**
     * Create a new preference page.
     */
    public AppearancePreferencePage() {
        super();
        setTitle(_("Apparence"));
    }

    /**
     * Used to bind the widgets
     */
    protected void bindValues() {

        this.showLines.setValue(Boolean.valueOf(getPreferenceStore().getBoolean(PlanimodPreferences.SHOW_LINES)));

        // Bind the widgets
        this.dbc.bindValue(WidgetProperties.selection().observe(this.showLinesButton), this.showLines);

    }

    @Override
    protected Control createContents(Composite parent) {

        Composite comp = new Composite(parent, SWT.NONE);
        comp.setLayout(new GridLayout(2, false));
        comp.setLayoutData(new GridData(GridData.FILL_BOTH));

        // Show lines
        Label label = new Label(comp, SWT.WRAP);
        label
                .setText(_("Si cette option est activée, les tableaux présentant les données vont s'afficher dans une grille ce qui peut améliorer la convivialité selon votre plateforme."));
        GridData data = new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1);
        data.widthHint = 150;
        label.setLayoutData(data);
        this.showLinesButton = new Button(comp, SWT.CHECK);
        this.showLinesButton.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false, 2, 1));
        this.showLinesButton.setText(_("Afficher les lignes dans les tableaux"));

        /*
         * Bind widgets
         */
        this.dbc = new DataBindingContext();
        this.om = new ObservablesManager();
        this.om.runAndCollect(new Runnable() {
            @Override
            public void run() {
                bindValues();
            }
        });
        this.om.addObservablesFromContext(this.dbc, true, true);

        return comp;
    }

    /**
     * This implementation save the parameters to the managers.
     */
    @Override
    public boolean performOk() {
        Boolean value = (Boolean) (this.showLines.getValue() instanceof Boolean ? this.showLines.getValue() : Boolean.FALSE);
        getPreferenceStore().setValue(PlanimodPreferences.SHOW_LINES, value.booleanValue());
        return true;
    }

}
