/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin;

import static com.planimod.ui.Localized._;

import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.forms.HyperlinkSettings;
import org.eclipse.ui.forms.events.HyperlinkAdapter;
import org.eclipse.ui.forms.events.HyperlinkEvent;
import org.eclipse.ui.forms.events.IHyperlinkListener;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormText;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Hyperlink;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.forms.widgets.TableWrapData;
import org.eclipse.ui.forms.widgets.TableWrapLayout;

import com.patrikdufresne.ui.ViewPartPage;
import com.planimod.core.PlanimodManagers;
import com.planimod.ui.preference.PlanimodPreferences;
import com.planimod.ui.views.admin.position.PositionPageViewPart;
import com.planimod.ui.views.admin.product.ProductPageViewPart;
import com.planimod.ui.views.admin.section.SectionPageViewPart;
import com.planimod.ui.views.admin.team.TeamPageViewPart;

/**
 * This view part is the main page displayed in the administration view. It provide an easy way to display other page
 * like the position page or the product page.
 * 
 * @author Patrik Dufresne
 * 
 */
public class AdminPageViewPart extends ViewPartPage {
    /**
     * The part id.
     */
    public static final String ID = "com.planimod.ui.view.admin.AdminIntroViewPart";

    /**
     * Create a new view part.
     * 
     * @param toolkit
     *            the form toolkit to use by this view part.
     */
    public AdminPageViewPart() {
        super(ID);
        setTitle(_("Administration"));
    }

    /**
     * This implementation will create control using the form toolkit.
     */
    @Override
    public void activate(Composite parent) {

        // Create the form toolkit
        FormToolkit toolkit = new FormToolkit(parent.getDisplay());
        toolkit.getHyperlinkGroup().setHyperlinkUnderlineMode(HyperlinkSettings.UNDERLINE_HOVER);

        // The administration view is build using the Rich Client Platform.
        TableWrapLayout layout = new TableWrapLayout();
        layout.numColumns = 3;
        layout.makeColumnsEqualWidth = true;
        Form form = toolkit.createForm(parent);
        form.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        form.getBody().setLayout(layout);
        form.setText(_("Administration"));

        createSection(
                form.getBody(),
                toolkit,
                _("Postes"),
                _("Vous permet d'ajouter et modifier la liste des postes et des sections."),
                _("Modifier la liste des postes"),
                PositionPageViewPart.ID);

        createSection(
                form.getBody(),
                toolkit,
                _("Produits"),
                _("Permet de modifier la liste des produits disponibles et de leur attribuer les postes requis pour la production."),
                _("Modifier les produits"),
                ProductPageViewPart.ID);

        createSection(
                form.getBody(),
                toolkit,
                _("Sections"),
                _("Vous permet de modifier la liste des sections disponibles."),
                _("Modifier les sections"),
                SectionPageViewPart.ID);

        createSection(
                form.getBody(),
                toolkit,
                _("Équipes de travail"),
                _("Vous permet de définir la liste des équipes de travail."),
                _("Modifier les équipes de travail."),
                TeamPageViewPart.ID);

        createSection(
                form.getBody(),
                toolkit,
                _("Paramètres"),
                _("Permet de personnaliser certains paramètres de Planimod."),
                _("Modifier les paramètres"),
                new HyperlinkAdapter() {
                    @Override
                    public void linkActivated(HyperlinkEvent e) {
                        PreferenceManager mgr = new PreferenceManager();
                        mgr.addToRoot(new GeneralPreferenceNode(getSite().getService(PlanimodManagers.class)));
                        mgr.addToRoot(new AppearancePreferenceNode());
                        PreferenceDialog dlg = new PreferenceDialog(getSite().getShell(), mgr);
                        dlg.setPreferenceStore(PlanimodPreferences.getPreferenceStore());
                        dlg.open();
                    }
                });

        createSection(
                form.getBody(),
                toolkit,
                _("À propos de"),
                _("Afficher les informations à propos du logiciel Planimod: version du logiciel, auteur et site web"),
                _("Afficher l'à-propos"),
                new HyperlinkAdapter() {
                    @Override
                    public void linkActivated(HyperlinkEvent e) {
                        AboutDialog dlg = new AboutDialog(getSite().getShell());
                        dlg.open();
                    }
                });

    }

    /**
     * Create a section in the view part
     * 
     * @param parent
     *            the composite parent
     * @param toolkit
     *            the tool kit
     * @param title
     *            the section title
     * @param description
     *            the section description
     * @param link
     *            the link name
     * @param partId
     *            the part id to activate
     * @return the Section
     */
    private Section createSection(Composite parent, FormToolkit toolkit, String title, String description, String link, final String partId) {
        return createSection(parent, toolkit, title, description, link, new HyperlinkAdapter() {
            @Override
            public void linkActivated(HyperlinkEvent e) {
                getSite().getParentPart().activateView(partId);
            }
        });
    }

    private Section createSection(Composite parent, FormToolkit toolkit, String title, String description, String link, IHyperlinkListener listener) {
        Section section = toolkit.createSection(parent, ExpandableComposite.TITLE_BAR);
        section.setText(title);
        section.setLayoutData(new TableWrapData(TableWrapData.FILL_GRAB));

        // Create the content of the section
        Composite sectionClient = toolkit.createComposite(section);
        section.setClient(sectionClient);
        sectionClient.setLayout(new TableWrapLayout());

        FormText text = new FormText(sectionClient, SWT.WRAP | SWT.READ_ONLY);
        text.setText(description, false, false);

        // Create link to print cumulative results
        Hyperlink hyperLink = toolkit.createHyperlink(sectionClient, link, SWT.WRAP);
        hyperLink.addHyperlinkListener(listener);
        return section;
    }

}
