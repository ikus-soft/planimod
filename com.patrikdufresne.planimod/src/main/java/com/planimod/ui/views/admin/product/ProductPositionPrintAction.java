/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin.product;

import static com.planimod.ui.Localized._;

import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.window.IShellProvider;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.printing.IPrintFactory;
import com.patrikdufresne.printing.PrintAction;
import com.planimod.core.PlanimodManagers;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.preference.PlanimodPreferences;

/**
 * This action allow the user to print a list of product and it's associated position.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ProductPositionPrintAction extends PrintAction {
    /**
     * Define the preference key for this action
     */
    private static final String PREF_KEY = "ProductPositionPrintAction.printPreference";

    /**
     * Managers to use.
     */
    private PlanimodManagers managers;

    /**
     * Create a new Print Employee Action
     * 
     * @param shell
     *            A Shell provider used to create new windows
     * @param operation
     *            type of operation : PRINT or PREVIEW
     */
    public ProductPositionPrintAction(PlanimodManagers managers, IShellProvider shell, int operation, IRunnableContext context) {
        super(shell, new ProductPositionPrintFactory(), operation, context);
        if (managers == null) {
            throw new NullPointerException();
        }
        this.managers = managers;
        setPrefKey(PREF_KEY);
        setPreferenceStore(PlanimodPreferences.getPreferenceStore());
        setEnabled(true);
        if (operation == PrintAction.PRINT_ACTION) {
            setText(_("Imprimer liste des postes par produit"));
        } else {
            setText(_("Aperçu avant impression"));
        }
    }

    /**
     * Returns the managers used by this action.
     * 
     * @return
     */
    protected PlanimodManagers getManagers() {
        return this.managers;
    }

    /**
     * This implementation set the employee to print
     * 
     * @param factory
     *            the factory
     */
    @Override
    protected void initFactory(IPrintFactory factory) {
        try {
            ((ProductPositionPrintFactory) factory).setProductPosition(getManagers().getProductPositionManager().list());
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }
    }
}
