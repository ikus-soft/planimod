/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin.section;

import static com.planimod.ui.Localized._;

import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.beans.IBeanValueProperty;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.databinding.util.JFaceProperties;
import org.eclipse.jface.databinding.viewers.ObservableSetContentProvider;
import org.eclipse.jface.databinding.viewers.ViewerProperties;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import com.patrikdufresne.jface.databinding.viewers.ColumnSupport;
import com.patrikdufresne.jface.viewers.ColumnViewerPreferences;
import com.patrikdufresne.managers.databinding.ManagerUpdateValueStrategy;
import com.patrikdufresne.managers.jface.RemoveAction;
import com.planimod.core.AbstractCalendarEvent;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Section;
import com.planimod.ui.databinding.ListObservableValue;
import com.planimod.ui.databinding.manager.PlanimodObservables;
import com.planimod.ui.preference.PlanimodPreferences;
import com.planimod.ui.views.AbstractViewPart;

/**
 * View to display and edit the section and it's related.
 * 
 * @author Patrik Dufresne
 * 
 */
public class SectionPageViewPart extends AbstractViewPart {
    /**
     * The part id.
     */
    public static final String ID = "com.planimod.ui.views.admin.section.SectionPageViewPart";
    /**
     * Preference key to access the column's width.
     */
    private static final String PREFERENCE_COLUMNS = "SectionViewPart.columns";

    private RemoveAction removeAction;

    /**
     * The table viewer.
     */
    private TableViewer viewer;

    /**
     * Create a new view part.
     */
    public SectionPageViewPart() {
        super(ID);
        setTitle(_("Sections"));
    }

    /**
     * This implementation create toolbar actions.
     */
    @Override
    public void activate(Composite parent) {

        this.viewer = createTableViewer(parent);
        this.viewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
        this.viewer.getTable().setLinesVisible(PlanimodPreferences.getPreferenceStore().getBoolean(PlanimodPreferences.SHOW_LINES));

        // Populate the tool bar
        fillToolBar();

        // Create binding
        bind();

    }

    @Override
    protected void bindValues() {
        super.bindValues();

        /*
         * Bind viewer
         */

        IObservableSet sections = PlanimodObservables.listSection(getSite().getService(PlanimodManagers.class));
        this.viewer.setContentProvider(new ObservableSetContentProvider());
        createColumns(this.viewer, sections);
        this.viewer.setInput(sections);

        /*
         * Bind remove action
         */
        IObservableList selections = ViewerProperties.multipleSelection().observe(this.viewer);
        getDbc().bindValue(
                JFaceProperties.value(RemoveAction.class, RemoveAction.OBJECTS, RemoveAction.OBJECTS).observe(this.removeAction),
                new ListObservableValue(selections, AbstractCalendarEvent.class));

    }

    /**
     * Create the column in the table viewer.
     * 
     * @param viewerthe
     *            the table viewer
     * @param knownElements
     *            the known section elements
     */
    protected void createColumns(TableViewer viewer, IObservableSet knownSections) {

        // Name
        IBeanValueProperty nameProperty = BeanProperties.value(Section.class, Section.NAME);
        ColumnSupport.create(viewer, _("Nom"), knownSections, nameProperty).addPropertySorting().addTextEditingSupport(
                getDbc(),
                new ManagerUpdateValueStrategy(getSite().getService(PlanimodManagers.class), UpdateValueStrategy.POLICY_CONVERT),
                null).setWeightLayoutData(1, 75).setResizable(false).setMoveable(false);

        ColumnViewerPreferences.create(viewer, PlanimodPreferences.getPreferenceStore(), PREFERENCE_COLUMNS);
    }

    @Override
    public void deactivate() {
        this.removeAction = null;
        this.viewer = null;
        super.deactivate();
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    /**
     * Populate the tool bar.
     */
    void fillToolBar() {

        // Add
        ActionContributionItem aci;
        getSite().getToolBarManager().add(
                aci = new ActionContributionItem(new AddSectionAction(getSite().getService(PlanimodManagers.class), getSite(), this.viewer)));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);

        // Remove
        getSite().getToolBarManager().add(
                aci = new ActionContributionItem(this.removeAction = new RemoveAction(getSite().getService(PlanimodManagers.class), getSite())));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);

    }

}
