/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.employees;

import static com.planimod.ui.Localized._;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.action.Action;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.EmployeePreference;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Team;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * This action is removing one team from the employee preference.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EmployeePreferredTeamRemoveAction extends Action {

    /**
     * The employee preference property name.
     */
    public static final String EMPLOYEE_PREFERENCE = "employeePreference";

    /**
     * The shifts property name.
     */
    public static final String SHIFTS = "shifts";

    /**
     * The current employee preference.
     */
    private EmployeePreference employeePreference;
    private PlanimodManagers managers;

    /**
     * The current shifts.
     */
    private List<Team> shifts;

    /**
     * Create a new action.
     * 
     * @param managers
     */
    public EmployeePreferredTeamRemoveAction(PlanimodManagers managers) {
        if (managers == null) {
            throw new NullPointerException();
        }
        this.managers = managers;

        setText(_("Supprimer"));
        setToolTipText(_("Supprime les équipes sélectionnées"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_LIST_REMOVE_16));
        setEnabled(canRun());
    }

    protected boolean canRun() {
        return this.employeePreference != null && this.shifts != null && this.shifts.size() > 0;
    }

    /**
     * Return the current employee preference.
     * 
     * @return the employee preference or null.
     */
    public EmployeePreference getEmployeePreference() {
        return this.employeePreference;
    }

    /**
     * Return the current shift list.
     * 
     * @return
     */
    public List<Team> getShifts() {
        return this.shifts;
    }

    /**
     * This implementation remove the current shifts from the preferred shift list.
     */
    @Override
    public void run() {
        if (this.employeePreference == null || this.employeePreference.getPreferredTeam() == null || this.shifts == null) {
            return;
        }

        // Remove shifts
        List<Team> preferredTeams = new ArrayList<Team>(this.employeePreference.getPreferredTeam());
        preferredTeams.removeAll(this.shifts);

        this.employeePreference.setPreferredTeam(preferredTeams);

        // Persists modification
        try {
            this.managers.updateAll(Arrays.asList(this.employeePreference));
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }

    }

    /**
     * Sets the employee preference
     * 
     * @param employeePreference
     *            the employee preference or null
     */
    public void setEmployeePreference(EmployeePreference employeePreference) {
        firePropertyChange(EMPLOYEE_PREFERENCE, this.employeePreference, this.employeePreference = employeePreference);
        setEnabled(canRun());
    }

    /**
     * Sets the shifts list.
     * 
     * @param shifts
     *            the shift list or null
     */
    public void setShifts(List<Team> shifts) {
        firePropertyChange(SHIFTS, this.shifts, this.shifts = shifts);
        setEnabled(canRun());
    }
}
