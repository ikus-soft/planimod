/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif;

import java.beans.PropertyDescriptor;

import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.internal.databinding.beans.BeanPropertyHelper;

import com.planimod.core.Employee;
import com.planimod.core.Task;
import com.planimod.ui.databinding.BeanComputedValueProperty;

/**
 * This property is used to locked an employee to a specified task.
 * 
 * @author Patrik Dufresne
 * 
 */
public class TaskLockedValueProperty extends BeanComputedValueProperty {

    /**
     * Static function used to return the list of property descriptors for this computed value property.
     * 
     * @return
     */
    private static PropertyDescriptor[] properties() {
        return new PropertyDescriptor[] {
                BeanPropertyHelper.getPropertyDescriptor(Task.class, Task.EMPLOYEE),
                BeanPropertyHelper.getPropertyDescriptor(Task.class, Task.LOCKED) };
    }

    IObservableValue employee;

    /**
     * Create a new intance of this class with the employee value specified.
     * 
     * @param employee
     *            the employee value to be used when locking the task.
     */
    public TaskLockedValueProperty(IObservableValue employee) {
        super(properties(), Boolean.TYPE);
        if (employee == null) {
            throw new NullPointerException();
        }
        this.employee = employee;
    }

    @Override
    protected Object doGetValue(Object source) {
        return Boolean.valueOf(((Task) source).getLocked());
    }

    @Override
    protected void doSetValue(Object source, Object value) {
        if (value instanceof Boolean) {
            ((Task) source).setLocked(((Boolean) value).booleanValue());
            Employee emp = getEmployee();
            if (emp != null) {
                ((Task) source).setEmployee(emp);
            }
        }
    }

    /**
     * Return the current employee value or null
     * 
     * @return
     */
    protected Employee getEmployee() {
        if (this.employee.getValue() instanceof Employee) {
            return (Employee) this.employee.getValue();
        }
        return null;
    }
}
