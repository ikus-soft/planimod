/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin;

import static com.planimod.ui.Localized._;

import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.ObservablesManager;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.ApplicationSetting;
import com.planimod.core.PlanimodManagers;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * This dialog is used to change the Planimod parameters.
 * 
 * @author Patrik Dufresne
 * 
 */
public class GeneralPreferencePage extends PreferencePage {

    private WritableValue customerName = new WritableValue(null, String.class);

    private Text customerNameText;

    private DataBindingContext dbc;

    /**
     * The Planimod manager used to change the paramaters.
     */
    private PlanimodManagers managers;

    private ObservablesManager om;

    /**
     * Create a new preference page.
     * 
     * @param managers
     *            the Planimod manager
     */
    public GeneralPreferencePage(PlanimodManagers managers) {
        super();
        if (managers == null) {
            throw new IllegalArgumentException();
        }
        this.managers = managers;
        setTitle(_("Général"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_PARAMETERS_16));
    }

    /**
     * Used to bind the widgets
     */
    protected void bindValues() {
        // Get initial data from managers
        try {
            this.customerName.setValue(this.managers.getApplicationSettingManager().getByName(ApplicationSetting.CUSTOMER_NAME));
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }

        // Bind the widgets
        this.dbc.bindValue(WidgetProperties.text(SWT.FocusOut).observe(this.customerNameText), this.customerName);

    }

    @Override
    protected Control createContents(Composite parent) {

        Composite comp = new Composite(parent, SWT.NONE);
        comp.setLayout(new GridLayout(2, false));
        comp.setLayoutData(new GridData(GridData.FILL_BOTH));

        // Customer name
        Label label = new Label(comp, SWT.WRAP);
        label.setText(_("Ce champ est utilisé afin de définir le nom de l'usine à afficher sur les rapports."));
        GridData data = new GridData(SWT.FILL, SWT.FILL, false, false, 2, 1);
        data.widthHint = 150;
        label.setLayoutData(data);
        label = new Label(comp, SWT.NONE);
        label.setText(_("Nom de l'usine :"));
        this.customerNameText = new Text(comp, SWT.BORDER);
        this.customerNameText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));

        /*
         * Bind widgets
         */
        this.dbc = new DataBindingContext();
        this.om = new ObservablesManager();
        this.om.runAndCollect(new Runnable() {
            @Override
            public void run() {
                bindValues();
            }
        });
        this.om.addObservablesFromContext(this.dbc, true, true);

        return comp;
    }

    /**
     * Return the customer name
     * 
     * @return
     */
    public String getCustomerName() {
        if (this.customerName.getValue() instanceof String) {
            return (String) this.customerName.getValue();
        }
        return null;
    }

    /**
     * This implementation save the parameters to the managers.
     */
    @Override
    public boolean performOk() {
        try {
            String value = (String) (this.customerName.getValue() instanceof String ? this.customerName.getValue() : "");
            this.managers.getApplicationSettingManager().setByName(ApplicationSetting.CUSTOMER_NAME, value);
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }
        return true;
    }

    /**
     * Sets the customer name.
     * 
     * @param value
     */
    public void setCustomerName(String value) {
        this.customerName.setValue(value);
    }

}
