/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
/**
 * 
 */
package com.planimod.ui.views.nonavailabilities;

import static com.planimod.ui.Localized._;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;

import com.patrikdufresne.managers.ManagedObject;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.jface.AbstractAddAction;
import com.planimod.core.Employee;
import com.planimod.core.NonAvailability;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.TimeRanges;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * Action to create a new non-availability event and add it to the database.
 * 
 * @author dufresne
 * 
 */
public class AddNonAvailabilityAction extends AbstractAddAction {
    /**
     * Property name of an action's date (value <code>"date"</code>). The date property is used sets a default time range.
     */
    public static final String DATE = "date";
    /**
     * Define the date state to update.
     */
    private Date date;
    /**
     * Temporary variable to store the selected employee value.
     */
    private Employee employee;
    /**
     * Temporary variable to store the end date.
     */
    private Date end;
    /**
     * Temporary variable to store the start date.
     */
    private Date start;
    /**
     * Temporary variable to store the summary date.
     */
    private String summary;
    /**
     * Temporary variable to store the visibility.
     */
    private boolean visible;
    private PlanimodManagers managers;

    /**
     * Create a new action.
     * 
     * @param shellProvider
     *            a shell provider in case the action need to display a message box.
     */
    public AddNonAvailabilityAction(PlanimodManagers managers, IShellProvider shellProvider, ISelectionProvider selectionProvider) {
        super(managers, shellProvider, selectionProvider);
        this.managers = managers;
        setText(_("Ajouter"));
        setToolTipText(_("Ajoute une nouvelle absence"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_LIST_ADD_16));
    }

    /**
     * This implementation always return true.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#canCreateObject()
     */
    @Override
    protected boolean canCreateObject() {
        return true;
    }

    /**
     * This implementation create non-availability event object.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#createObjects()
     */
    @Override
    protected List<? extends ManagedObject> createObjects() {
        List<ManagedObject> list = new ArrayList<ManagedObject>();

        // Create the event
        NonAvailability event = new NonAvailability();
        event.setEmployee(this.employee);
        event.setSummary(this.summary);
        event.setStartDate(this.start);
        event.setEndDate(this.end);
        event.setVisible(this.visible);
        list.add(event);
        return list;
    }

    /**
     * Returns the current date state of null if not define.
     * 
     * @return the date state or null.
     */
    public Date getDate() {
        return this.date;
    }

    @Override
    public void run() {
        int firstDayOfWeek = Calendar.SUNDAY;
        try {
            firstDayOfWeek = this.managers.getApplicationSettingManager().getFirstDayOfWeek();
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }
        NonAvailabilityDialog dlg = new NonAvailabilityDialog(getShellProvider(), (PlanimodManagers) getManagers(), NonAvailabilityDialog.NEW);
        Date defaultDate = this.date == null ? new Date() : this.date;
        dlg.setStartDate(TimeRanges.getWeekStart(defaultDate, firstDayOfWeek));
        dlg.setEndDate(TimeRanges.getWeekEnd(defaultDate, firstDayOfWeek));
        dlg.setEmployee(null);

        if (dlg.open() != Window.OK) {
            return;
        }

        this.start = dlg.getStartDate();
        this.end = dlg.getEndDate();
        this.summary = dlg.getSummary();
        this.employee = dlg.getEmployee();
        this.visible = dlg.getVisible();

        // TODO Do extra validation
        // e.g.: employee must not be null
        // start should be before end

        super.run();
    }

    /**
     * Sets a default date value.
     * 
     * @param date
     *            the date value or null
     */
    public void setDate(Date date) {
        if ((this.date == null && date != null) || (this.date != null && date == null) || (this.date != null && date != null && !date.equals(this.date))) {
            Date oldValue = this.date;
            this.date = date;
            firePropertyChange(DATE, oldValue, this.date);
        }
    }
}