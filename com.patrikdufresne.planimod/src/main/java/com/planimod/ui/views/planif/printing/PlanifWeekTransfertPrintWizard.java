/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif.printing;

import static com.planimod.ui.Localized._;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.eclipse.core.databinding.observable.value.WritableValue;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.printing.IPrintFactory;
import com.patrikdufresne.printing.PrintWizard;
import com.planimod.core.ApplicationSetting;
import com.planimod.core.NonAvailability;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Task;
import com.planimod.core.TimeRanges;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.printing.PlanimodPrintFactory;

/**
 * This action intend to print the planif week transfert.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanifWeekTransfertPrintWizard extends PrintWizard {

    WritableValue date = new WritableValue(null, Date.class);

    private Date dateValue;

    PlanimodManagers managers;

    WritableValue previousDate = new WritableValue(null, Date.class);

    private Date previousDateValue;

    /**
     * Create a new print wizard.
     */
    public PlanifWeekTransfertPrintWizard(PlanimodManagers managers) {
        super(new PlanifWeekTransfertPrintFactory());
        if (managers == null) {
            throw new NullPointerException();
        }
        this.managers = managers;
        setWindowTitle(_("Impression programmation des équipes de travail", 0));
        addPage(new PlanifWeekTransfertWizardPage());
    }

    public Date getDate() {
        return (Date) this.date.getValue();
    }

    public Date getPreviousDate() {
        return (Date) this.previousDate.getValue();
    }

    @Override
    protected void initFactory(IPrintFactory factory) throws InvocationTargetException {

        // Get matching event for the time range.
        try {
            // Sets subtitle
            String customerName = this.managers.getApplicationSettingManager().getByName(ApplicationSetting.CUSTOMER_NAME);
            ((PlanimodPrintFactory) factory).setSubTitle(customerName);

            // Query the first day of week
            int firstDayOfWeek = this.managers.getApplicationSettingManager().getFirstDayOfWeek();

            Date start = TimeRanges.getWeekStart(this.dateValue, firstDayOfWeek);
            Date end = TimeRanges.getWeekEnd(this.dateValue, firstDayOfWeek);

            Date previousStart = TimeRanges.getWeekStart(this.previousDateValue, firstDayOfWeek);
            Date previousEnd = TimeRanges.getWeekEnd(this.previousDateValue, firstDayOfWeek);

            // Query data
            Collection<Task> tasks = this.managers.getTaskManager().list(previousStart, previousEnd);

            // Query the current week.
            tasks.addAll(this.managers.getTaskManager().list(start, end));

            // Sets the tasks to the print factory
            ((PlanifWeekTransfertPrintFactory) factory).setTasks(tasks);

            // Query the non-availabilities for previous week.
            List<NonAvailability> nonAvailabilities = this.managers.getNonAvailabilityManager().list(previousStart, previousEnd);
            // Query non-availabilities for current week
            nonAvailabilities.addAll(this.managers.getNonAvailabilityManager().list(start, end));

            // Sets the non-availabilities to the print factory
            ((PlanifWeekTransfertPrintFactory) factory).setNonAvailabilties(nonAvailabilities);

        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }

    }

    @Override
    public boolean performFinish() {
        this.dateValue = (Date) this.date.getValue();
        this.previousDateValue = (Date) this.previousDate.getValue();
        return super.performFinish();
    }

    /**
     * Sets the date to the current used date.
     */
    public void setDate(Date date) {
        this.date.setValue(date);
    }

    public void setPreviousDate(Date date) {
        this.previousDate.setValue(date);
    }

}