/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin;

import static com.planimod.ui.Localized._;

import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.program.Program;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import com.planimod.ui.theme.Resources;

/**
 * This dialog show general information about the application.
 * 
 * @author Patrik Dufresne
 * 
 */
public class AboutDialog extends Dialog {

    /**
     * Create a new About dialog.
     * 
     * @param shell
     *            the parent of this dialog.
     */
    public AboutDialog(Shell shell) {
        super(shell);
    }

    /**
     * This implementation close the dialog.
     * 
     * @see org.eclipse.jface.dialogs.Dialog#buttonPressed(int)
     */
    @Override
    protected void buttonPressed(int buttonId) {
        cancelPressed();
    }

    /**
     * This implementation create a close button.
     * 
     * @see org.eclipse.jface.dialogs.Dialog#createButtonsForButtonBar(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected void createButtonsForButtonBar(Composite parent) {
        createButton(parent, IDialogConstants.CLOSE_ID, IDialogConstants.CLOSE_LABEL, true);
    }

    /**
     * This implementation set the windows title
     */
    @Override
    protected void configureShell(Shell newShell) {
        super.configureShell(newShell);
        newShell.setText(_("À propos de"));
    }

    /**
     * This implementation create the content of the about dialog.
     * 
     * @see org.eclipse.jface.dialogs.Dialog#createDialogArea(org.eclipse.swt.widgets.Composite)
     */
    @Override
    protected Control createDialogArea(Composite parent) {

        // Create the composite to hold all the controls
        Composite comp = new Composite(parent, SWT.NONE);

        // Configure parent layout
        GridLayout gridLayout = new GridLayout(1, false);
        comp.setLayout(gridLayout);

        // Image
        Label imageLabel = new Label(comp, SWT.NONE);
        imageLabel.setImage(Resources.getImage(Resources.LOGO_ICON_375));
        imageLabel.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, false));

        // App version
        String version = this.getClass().getPackage().getImplementationVersion();
        if (version == null) {
            version = "";
        } else {
            version = _("Version %s", version);
        }
        Text appVersion = new Text(comp, SWT.CENTER | SWT.READ_ONLY | SWT.MULTI | SWT.WRAP);
        appVersion.setText(version);
        appVersion.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        appVersion.setBackground(this.getShell().getBackground());

        // App description
        Text appDescText = new Text(comp, SWT.CENTER | SWT.READ_ONLY | SWT.MULTI | SWT.WRAP);
        appDescText.setText(_("Planimod est un logiciel de répartition des employés."));
        appDescText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        appDescText.setBackground(this.getShell().getBackground());

        // App copyright
        Text appCopyrightText = new Text(comp, SWT.CENTER | SWT.READ_ONLY | SWT.MULTI);
        appCopyrightText.setText(_("Copyright (c) 2021 Logiciel IKUS inc."));
        appCopyrightText.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
        appCopyrightText.setBackground(this.getShell().getBackground());

        // Application web site
        Link appWebSite = new Link(comp, SWT.NONE);
        appWebSite.setText(_("<a>Site web de Planimod (https://planimod.com)</a>"));
        appWebSite.setLayoutData(new GridData(SWT.CENTER, SWT.FILL, true, false));
        appWebSite.addSelectionListener(new SelectionAdapter() {
            @Override
            public void widgetSelected(SelectionEvent event) {
                Program.launch(_("https://planimod.com"));
            }
        });

        return imageLabel;

    }
}