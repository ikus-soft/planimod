/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Collection;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.databinding.ManagedObjectComputedSet;
import com.planimod.core.Employee;
import com.planimod.core.planif.GeneratePlanifContext;

/**
 * This observable set provide collection of employees with offer. This function is wrapping the
 * {@link GeneratePlanifContext#listEmployeeWithTaskProposed()}.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EmployeesWithTaskProposedForContextComputedSet extends ManagedObjectComputedSet {
    /**
     * The managers.
     */
    private GeneratePlanifContext context;
    private PropertyChangeListener listener = new PropertyChangeListener() {
        @Override
        public void propertyChange(PropertyChangeEvent evt) {
            getRealm().exec(new Runnable() {
                @Override
                public void run() {
                    makeDirty();
                }
            });
        }
    };

    /**
     * Create a new observable set of employee.
     * 
     * @param realm
     *            the realm
     * @param site
     *            The view site to get the generate planif context
     * @param lockedElements
     *            the observable locked elements
     */
    public EmployeesWithTaskProposedForContextComputedSet(GeneratePlanifContext context) {
        super(context.getManagers(), Employee.class, null, null);
        this.context = context;
    }

    @Override
    public synchronized void dispose() {
        super.dispose();
        this.context = null;
    }

    /**
     * This implementation query the database to get the employees qualify for all null planif event.
     */
    @Override
    protected Collection doList() throws ManagerException {
        return this.context.getProposedTasks().employees();
    }

    @Override
    protected void makeDirty() {
        super.makeDirty();
    }

    @Override
    protected void startListening() {
        super.startListening();
        this.context.addPropertyChangeListener(GeneratePlanifContext.PROPOSED_TASKS, this.listener);
    }

    @Override
    protected void stopListening() {
        super.stopListening();
        this.context.removePropertyChangeListener(GeneratePlanifContext.PROPOSED_TASKS, this.listener);
    }

}