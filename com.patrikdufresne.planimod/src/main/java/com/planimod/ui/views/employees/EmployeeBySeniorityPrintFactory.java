/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.employees;

import static com.planimod.ui.Localized._;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.eclipse.nebula.paperclips.core.ColumnPrint;
import org.eclipse.nebula.paperclips.core.grid.DefaultGridLook;
import org.eclipse.nebula.paperclips.core.grid.GridPrint;
import org.eclipse.nebula.paperclips.core.border.LineBorder;
import org.eclipse.nebula.paperclips.core.Print;
import org.eclipse.nebula.paperclips.core.text.TextPrint;

import com.patrikdufresne.printing.FontDataUtil;
import com.planimod.core.EmployeePreference;
import com.planimod.core.comparators.EmployeePreferenceComparators;
import com.planimod.ui.printing.PlanimodPrintFactory;
import com.planimod.ui.printing.PlanimodPrintUtil;

/**
 * This class create a Employee Print Factory. It create a print listing employees.
 * 
 * @author patapouf
 * 
 */
public class EmployeeBySeniorityPrintFactory extends PlanimodPrintFactory {

    /**
     * Hold employees list.
     */
    private Collection<EmployeePreference> employees = null;

    /**
     * Create a new Factory.
     * 
     * @param jobName
     */
    public EmployeeBySeniorityPrintFactory() {
        super(_("Liste d'ancienneté"));

        setTitle(_("Liste d'ancienneté"));
    }

    /**
     * This implementation create a employees print.
     */
    @Override
    protected Print createBodyArea() {

        // Retrieved a list of employees
        if (this.employees == null || this.employees.size() == 0) {
            return new TextPrint("");
        }
        List<EmployeePreference> list = new ArrayList<EmployeePreference>(this.employees);
        Collections.sort(list, EmployeePreferenceComparators.bySeniority());

        // Create Grid holding the employees list
        GridPrint grid = new GridPrint();

        ColumnPrint column = new ColumnPrint(grid, 2, 15);
        column.setCompressed(false);

        // Set the Grid Look
        DefaultGridLook look = new DefaultGridLook();
        LineBorder border = new LineBorder();
        border.setGapSize(0);
        look.setCellBorder(border);
        look.setCellPadding(3, 3);
        grid.setLook(look);
        grid.setCellClippingEnabled(false);

        // Add column for count
        grid.addColumn("left:preferred");
        grid.addColumn("left:preferred");
        grid.addColumn("left:default:grow");
        grid.addColumn("left:preferred");

        grid.addHeader(new TextPrint(PlanimodPrintUtil.SPACE, FontDataUtil.BOLD));
        grid.addHeader(new TextPrint(_("Date anc."), FontDataUtil.BOLD));
        grid.addHeader(new TextPrint(_("Nom de l'employé"), FontDataUtil.BOLD));
        grid.addHeader(new TextPrint(_("No."), FontDataUtil.BOLD));

        // Draw the employees list
        int count = 1;
        for (EmployeePreference e : list) {
            grid.add(new TextPrint(Integer.toString(count), FontDataUtil.DEFAULT));
            grid.add(new TextPrint(PlanimodPrintUtil.getHireDate(e), FontDataUtil.DEFAULT));
            grid.add(new TextPrint(PlanimodPrintUtil.getName(e.getEmployee()), FontDataUtil.DEFAULT));
            grid.add(new TextPrint(PlanimodPrintUtil.getRefId(e.getEmployee()), FontDataUtil.DEFAULT));
            count++;
        }
        return column;
    }

    /**
     * Returns the employees list.
     * 
     * @return employees list or null
     */
    public Collection<EmployeePreference> getEmployees() {
        return Collections.unmodifiableCollection(this.employees);
    }

    /**
     * Sets the list of employees to print.
     * 
     * @param employees
     *            a list or null
     */
    public void setEmployees(Collection<EmployeePreference> employees) {
        this.employees = employees;
    }

}
