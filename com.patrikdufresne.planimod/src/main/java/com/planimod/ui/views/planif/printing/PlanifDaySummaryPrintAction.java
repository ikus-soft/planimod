/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif.printing;

import static com.planimod.ui.Localized._;

import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.window.IShellProvider;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.printing.IPrintFactory;
import com.patrikdufresne.printing.PrintAction;
import com.planimod.core.ApplicationSetting;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.planif.GeneratePlanifContext;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.preference.PlanimodPreferences;
import com.planimod.ui.printing.PlanimodPrintFactory;

/**
 * This action intend to print the planif.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanifDaySummaryPrintAction extends PrintAction {
    /**
     * Define the preference key for this action
     */
    private static final String PREF_KEY = "PlanifDaySummaryPrintAction.printPreference";

    /**
     * Context to use.
     */
    private GeneratePlanifContext context;

    /**
     * Create a new print action.
     * 
     * @param context
     * @param shell
     * @param operation
     * @param runnableContext
     */
    public PlanifDaySummaryPrintAction(GeneratePlanifContext context, IShellProvider shell, int operation, IRunnableContext runnableContext) {
        super(shell, new PlanifDaySummaryPrintFactory(), operation, runnableContext);
        if (context == null) {
            throw new NullPointerException();
        }
        this.context = context;
        setPrefKey(PREF_KEY);
        setPreferenceStore(PlanimodPreferences.getPreferenceStore());
        setEnabled(canRun());
        if (operation == PRINT_ACTION) {
            setText(_("Imprimer planification main d'oeuvre journalière"));
        } else {
            setText(_("Aperçu avant impression"));
        }
    }

    /**
     * Returns True if the action can be run.
     * 
     * @return
     */
    protected boolean canRun() {
        return this.context != null && !this.context.isDisposed();
    }

    /**
     * Returns the managers used by this action.
     * 
     * @return
     */
    protected PlanimodManagers getManagers() {
        return this.context.getManagers();
    }

    /**
     * This implementation set the employee to print
     * 
     * @param factory
     *            the factory
     */
    @Override
    protected void initFactory(IPrintFactory factory) {
        // Get date from state
        if (this.context == null) {
            return;
        }

        // Get matching event for the time range.
        try {
            // Sets subtitle
            String custonerName = getManagers().getApplicationSettingManager().getByName(ApplicationSetting.CUSTOMER_NAME);
            ((PlanimodPrintFactory) factory).setSubTitle(custonerName);

            // Set tasks
            ((PlanifDaySummaryPrintFactory) factory).setTasks(this.context.listTask());

            ((PlanifDaySummaryPrintFactory) factory).setEmployeePreferences(this.context.listEmployeePreferences());

            ((PlanifDaySummaryPrintFactory) factory).setNonAvailabilties(this.context.getManagers().getNonAvailabilityManager().list(
                    this.context.getStart(),
                    this.context.getEnd()));

        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
            return;
        }
    }

}
