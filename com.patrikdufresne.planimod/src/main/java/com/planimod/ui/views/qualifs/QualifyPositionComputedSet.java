/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.qualifs;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.IObservableValue;

import com.patrikdufresne.managers.ManagerEvent;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.databinding.ManagedObjectComputedSet;
import com.planimod.core.Employee;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Position;
import com.planimod.core.Qualification;
import com.planimod.core.QualificationManager;

/**
 * This computed set wrap the function {@link QualificationManager#listQualifyPositionForEmployee(Collection)}.
 * 
 * @author Patrik Dufresne
 * 
 */
public class QualifyPositionComputedSet extends ManagedObjectComputedSet {

    protected IObservableValue employee;

    /**
     * Create a new computed set
     * 
     * @param managers
     *            the associated manager
     * @param employee
     *            the employee value
     */
    public QualifyPositionComputedSet(PlanimodManagers managers, IObservableValue employee) {
        super(managers, Qualification.class, new IObservable[] { employee });
        this.employee = employee;
    }

    /**
     * This implementation is calling {@link QualificationManager#listQualifyPositionForEmployee(Collection)}.
     */
    @Override
    protected Collection doList() throws ManagerException {
        // Check the current employee value.
        Employee emp = getEmployee();
        if (emp == null) {
            return Collections.EMPTY_SET;
        }
        // List the qualify position
        return ((PlanimodManagers) getManagers()).getQualificationManager().listQualifyPositionForEmployee(Arrays.asList(emp));
    }

    /**
     * Return the employee value.
     * 
     * @return
     */
    protected Employee getEmployee() {
        if (this.employee.getValue() instanceof Employee) {
            return (Employee) this.employee.getValue();
        }
        return null;
    }

    /**
     * This implementation check if the qualification is related to this employee.
     */
    @Override
    protected boolean doSelect(Object element) {
        return element instanceof Qualification && ((Qualification) element).getEmployee().equals(this.employee.getValue());
    }

    @Override
    protected void notifyIfChanged(ManagerEvent event) {
        makeDirty();
    }

    /**
     * This implementation allows to add object to this set. The modification is then apply using the managers.
     */
    @Override
    public boolean add(Object o) {
        Employee emp = getEmployee();
        if (!(o instanceof Position) || emp == null) {
            return false;
        }

        // Add the qualification
        try {
            ((PlanimodManagers) getManagers()).getQualificationManager().setQualification((Position) o, emp, true);
        } catch (ManagerException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * This implementation allows to remove object from this set. The modification is then apply using the managers API.
     */
    @Override
    public boolean remove(Object o) {
        Employee emp = getEmployee();
        if (!(o instanceof Position) || emp == null) {
            return false;
        }
        // Remove the qualification
        try {
            ((PlanimodManagers) getManagers()).getQualificationManager().setQualification((Position) o, emp, false);
        } catch (ManagerException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
