/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif;

import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.set.ISetChangeListener;
import org.eclipse.core.databinding.observable.set.SetChangeEvent;
import org.eclipse.jface.databinding.viewers.ObservableMapLabelProvider;
import org.eclipse.jface.viewers.LabelProviderChangedEvent;

/**
 * This label provide display a label using an IObserbale map and add an annotation if the element is part of an
 * IObservable set.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EmployeeLabelWithAnnotation extends ObservableMapLabelProvider {

    private static final String ANNOTATION = " *";

    private ISetChangeListener setChangeListener = new ISetChangeListener() {
        @Override
        public void handleSetChange(SetChangeEvent event) {
            Set affectedElements = new HashSet();
            affectedElements.addAll(event.diff.getAdditions());
            affectedElements.addAll(event.diff.getRemovals());
            LabelProviderChangedEvent newEvent = new LabelProviderChangedEvent(EmployeeLabelWithAnnotation.this, affectedElements.toArray());
            fireLabelProviderChanged(newEvent);
        }
    };

    private IObservableSet withAnnotationSet;

    /**
     * Create a new label provider.
     * 
     * @param attributeMap
     * @param withAnnotationSet
     */
    public EmployeeLabelWithAnnotation(IObservableMap attributeMap, IObservableSet withAnnotationSet) {
        super(attributeMap);
        this.withAnnotationSet = withAnnotationSet;
        this.withAnnotationSet.addSetChangeListener(this.setChangeListener);
    }

    @Override
    public void dispose() {
        try {
            if (this.withAnnotationSet != null) {
                this.withAnnotationSet.removeSetChangeListener(this.setChangeListener);
                this.withAnnotationSet = null;
            }
        } finally {
            super.dispose();
        }
    }

    public String getColumnText(Object element, int columnIndex) {
        if (columnIndex == 0 && this.withAnnotationSet.contains(element)) {
            return super.getColumnText(element, columnIndex) + ANNOTATION;
        }
        return super.getColumnText(element, columnIndex);
    }

}