/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.employees;

import static com.planimod.ui.Localized._;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.action.Action;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.EmployeePreference;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Team;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.theme.Resources;

/**
 * This action is creating all the teams to fill the employee preferences.
 * 
 * @author Patrik Dufresne
 * 
 */
public class EmployeePreferredTeamAddAllAction extends Action {

    /**
     * The employee preference property.
     */
    public static final String EMPLOYEE_PREFERENCE = "employeePreference";

    /**
     * The employee preference.
     */
    private EmployeePreference employeePreference;
    /**
     * Managers to use.
     */
    private PlanimodManagers managers;

    /**
     * Create a new action.
     * 
     * @param direction
     *            the direction of the move
     */
    public EmployeePreferredTeamAddAllAction(PlanimodManagers managers) {
        if (managers == null) {
            throw new IllegalArgumentException();
        }
        this.managers = managers;
        setText(_("Tout ajouter"));
        setToolTipText(_("Ajoute toutes les équipes de travail"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_LIST_ADD_16));
        setEnabled(canRun());
    }

    protected boolean canRun() {
        return this.employeePreference != null;
    }

    /**
     * Returns the current employee preference.
     * 
     * @return
     */
    public EmployeePreference getEmployeePreference() {
        return this.employeePreference;
    }

    @Override
    public void run() {
        if (this.employeePreference == null) {
            return;
        }

        // Retrieve the current preferred shift list.
        List<Team> preferredTeams = this.employeePreference.getPreferredTeam();
        if (preferredTeams == null) {
            preferredTeams = new ArrayList<Team>();
        } else {
            preferredTeams = new ArrayList<Team>(preferredTeams);
        }

        // Add any missing shift
        List<Team> allShifts;
        try {
            allShifts = this.managers.getTeamManager().list();
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
            return;
        }
        for (Team s : allShifts) {
            if (!preferredTeams.contains(s)) {
                preferredTeams.add(s);
            }
        }
        this.employeePreference.setPreferredTeam(preferredTeams);

        // Persist the modification
        try {
            managers.updateAll(Arrays.asList(this.employeePreference));
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }
    }

    /**
     * Sets the employee preference to update.
     * 
     * @param employeePreference
     */
    public void setEmployeePreference(EmployeePreference employeePreference) {
        firePropertyChange(EMPLOYEE_PREFERENCE, this.employeePreference, this.employeePreference = employeePreference);
        setEnabled(canRun());
    }

}
