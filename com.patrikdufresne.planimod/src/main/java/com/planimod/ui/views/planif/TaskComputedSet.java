/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.eclipse.core.databinding.observable.IObservable;
import org.eclipse.core.databinding.observable.value.IObservableValue;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.databinding.ManagedObjectComputedSet;
import com.planimod.core.TimeRanges;
import com.planimod.core.NonAvailabilityManager;
import com.planimod.core.Task;
import com.planimod.core.TaskManager;
import com.planimod.core.PlanimodManagers;

/**
 * This computed set is used to wrap the function
 * {@link NonAvailabilityManager#listProductionEvents(Planif, Date, Date)} and make it observable.
 * 
 * @param managers
 *            the managers to use
 * @param planif
 *            the planif observable
 * @param start
 *            the start date observable
 * @param end
 *            the end date observable
 * @return the observable set
 */
public class TaskComputedSet extends ManagedObjectComputedSet {

    private final IObservableValue end;
    private final IObservableValue start;

    /**
     * Create an observable set.
     * 
     * @param managers
     *            the managers to use
     * @param planif
     *            the planif observable
     * @param start
     *            the start date observable
     * @param end
     *            the end date observable
     * @return the observable set
     */
    public TaskComputedSet(PlanimodManagers managers, IObservableValue start, IObservableValue end) {
        super(managers, Task.class, new IObservable[] { start, end });
        this.start = start;
        this.end = end;
    }

    /**
     * This implementation run the {@link TaskManager#list(Planif, Date, Date)} function.
     */
    @Override
    protected Collection doList() throws ManagerException {
        // Get the current planif value from the planif state.
        if (getStart() == null || getEnd() == null) {
            return Collections.EMPTY_LIST;
        }
        return ((PlanimodManagers) getManagers()).getTaskManager().list(getStart(), getEnd());
    }

    /**
     * Selector to filter according to the list.
     */
    @Override
    protected boolean doSelect(Object element) {
        // Get the current planif value from the planif state.
        Date start = getStart();
        Date end = getEnd();
        if (start == null || end == null) {
            return false;
        }
        return element instanceof Task && TimeRanges.intersectEquals(((Task) element), start, end);
    }

    /**
     * Returns the start date value.
     * 
     * @return the start date or null.
     */
    protected Date getEnd() {
        if (this.end.getValue() instanceof Date) {
            return (Date) this.end.getValue();
        }
        return null;
    }

    /**
     * Returns the end date value.
     * 
     * @return the end date or null.
     */
    protected Date getStart() {
        if (this.start.getValue() instanceof Date) {
            return (Date) this.start.getValue();
        }
        return null;
    }
}