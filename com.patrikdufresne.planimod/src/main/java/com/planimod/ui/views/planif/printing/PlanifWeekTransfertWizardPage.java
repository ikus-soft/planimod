/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.planif.printing;

import static com.planimod.ui.Localized._;

import java.util.Date;

import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.core.databinding.validation.MultiValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.layout.GridDataFactory;
import org.eclipse.jface.layout.GridLayoutFactory;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.DateTime;

import com.patrikdufresne.jface.wizard.AbstractWizardPage;
import com.planimod.core.PlanimodManagers;
import com.planimod.ui.databinding.ObservableWeekEnd;
import com.planimod.ui.databinding.ObservableWeekStart;
import com.planimod.ui.views.planif.TaskComputedSet;

/**
 * This class is used to create a planif transfert: does a delta between two weeks.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PlanifWeekTransfertWizardPage extends AbstractWizardPage {

    public static final String PAGE_NAME = "PlanifWeekTransfertWizardPage";
    private DateTime dateTime;

    /**
     * Create a new page
     */
    protected PlanifWeekTransfertWizardPage() {
        super(PAGE_NAME);
        setTitle(_("Selectionnez la seconde date pour la programmation des équipes de travail", 0));
        setPageComplete(false);
    }

    @Override
    protected void bindValues() {
        final WritableValue previousDate = ((PlanifWeekTransfertPrintWizard) getWizard()).previousDate;
        final PlanimodManagers managers = ((PlanifWeekTransfertPrintWizard) getWizard()).managers;
        final ObservableWeekStart start = new ObservableWeekStart(managers, ((PlanifWeekTransfertPrintWizard) getWizard()).date);
        final TaskComputedSet tasks = new TaskComputedSet(managers, new ObservableWeekStart(managers, previousDate), new ObservableWeekEnd(
                managers,
                previousDate));

        getDbc().bindValue(WidgetProperties.selection().observe(this.dateTime), previousDate);

        /*
         * Add validation
         */
        getDbc().addValidationStatusProvider(new MultiValidator() {

            @Override
            protected IStatus validate() {

                /*
                 * Validates that both entries are dates
                 */
                if (!(start.getValue() instanceof Date) || !(previousDate.getValue() instanceof Date)) {
                    throw new IllegalStateException();
                }

                /*
                 * Validates that the selected date is before the currently used date
                 */
                if (((Date) start.getValue()).compareTo((Date) previousDate.getValue()) <= 0) {
                    return ValidationStatus.error(_("Selectionez une date précédant la semaine selectionnée", 0));
                }

                /*
                 * Validates that the selected date is not empty
                 */
                if (tasks.size() == 0) {
                    return ValidationStatus.error(_("Selectionez une date contenant des données", 0));
                }
                return ValidationStatus.OK_STATUS;
            }
        });
    }

    /**
     * This implementation create the required widgets to setup the printing.
     * 
     * @see org.eclipse.jface.dialogs.IDialogPage#createControl(org.eclipse.swt.widgets.Composite)
     */
    @Override
    public void createControl(Composite parent) {
        Composite comp = new Composite(parent, SWT.NONE);
        comp.setLayout(GridLayoutFactory.fillDefaults().create());
        this.dateTime = new DateTime(comp, SWT.CALENDAR);
        this.dateTime.setLayoutData(GridDataFactory.fillDefaults().align(SWT.CENTER, SWT.CENTER).grab(true, true).create());
        setControl(comp);

        bind();
    }
}
