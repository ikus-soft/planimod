/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
/**
 * 
 */
package com.planimod.ui.views.admin.product;

import static com.planimod.ui.Localized._;

import java.util.Arrays;
import java.util.List;

import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.window.IShellProvider;

import com.patrikdufresne.managers.ManagedObject;
import com.patrikdufresne.managers.jface.AbstractAddAction;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Product;
import com.planimod.ui.theme.Resources;

/**
 * Action to create a new Rider object and add it to the database.
 * 
 * @author dufresne
 * 
 */
public class AddProductAction extends AbstractAddAction {
    /**
     * Create a new action.
     * 
     * @param shellProvider
     *            a shell provider in case the action need to display a message box.
     */
    public AddProductAction(PlanimodManagers managers, IShellProvider shellProvider, ISelectionProvider selectionProvider) {
        super(managers, shellProvider, selectionProvider);
        setText(_("Ajouter"));
        setToolTipText(_("Ajouter un nouveau produit"));
        setImageDescriptor(Resources.getImageDescriptor(Resources.ICON_LIST_ADD_16));
    }

    /**
     * This implementation always return true.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#canCreateObject()
     */
    @Override
    protected boolean canCreateObject() {
        return true;
    }

    /**
     * This implementation create rider object.
     * 
     * @see net.ekwos.gymkhana.ui.views.AbstractAddAction#createObjects()
     */
    @Override
    protected List<? extends ManagedObject> createObjects() {
        Product p = new Product();
        p.setName(_("Nouveau produit"));
        return Arrays.asList(p);
    }

}