/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.admin;

import org.eclipse.jface.preference.PreferenceNode;

/**
 * Preference node for Planimod appearence parameters
 * 
 * @author Patrik Dufresne
 * 
 */
public class AppearancePreferenceNode extends PreferenceNode {

    /**
     * Node id.
     */
    public static final String ID = "AppearancePreferenceNode";

    /**
     * Create a preference node.
     * 
     * @param managers
     *            The Planimod manager used to access the parameters
     */
    public AppearancePreferenceNode() {
        super(ID, new AppearancePreferencePage());
    }

}
