/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.shifts;

import static com.planimod.ui.Localized._;

import java.util.Date;

import org.eclipse.core.databinding.BindingProperties;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.list.IObservableList;
import org.eclipse.core.databinding.observable.map.IObservableMap;
import org.eclipse.core.databinding.observable.set.IObservableSet;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.observable.value.WritableValue;
import org.eclipse.jface.action.ActionContributionItem;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.databinding.util.JFaceProperties;
import org.eclipse.jface.databinding.viewers.ObservableSetContentProvider;
import org.eclipse.jface.databinding.viewers.ViewersObservables;
import org.eclipse.jface.viewers.IOpenListener;
import org.eclipse.jface.viewers.OpenEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import com.patrikdufresne.jface.databinding.preference.PreferenceStoreProperties;
import com.patrikdufresne.jface.databinding.viewers.PropertyViewerComparator;
import com.patrikdufresne.managers.jface.RemoveAction;
import com.patrikdufresne.planner.Planner;
import com.patrikdufresne.planner.databinding.PlannerProperties;
import com.patrikdufresne.planner.databinding.PlannerViewerUpdater;
import com.patrikdufresne.planner.viewer.PlannerViewer;
import com.planimod.core.AbstractCalendarEvent;
import com.planimod.core.PlanimodManagers;
import com.planimod.core.Shift;
import com.planimod.core.Team;
import com.planimod.core.comparators.ShiftComparators;
import com.planimod.ui.databinding.Converters;
import com.planimod.ui.databinding.DateColorControlStyleSupport;
import com.planimod.ui.databinding.ListObservableValue;
import com.planimod.ui.databinding.manager.ShiftComputedSet;
import com.planimod.ui.preference.PlanimodPreferences;
import com.planimod.ui.viewers.planner.ObservableMapPlannerLabelProviderWithColor;
import com.planimod.ui.viewers.planner.PlanimodPlannerLook;
import com.planimod.ui.views.AbstractViewPart;
import com.planimod.ui.views.ChangeDateAction;

/**
 * This view part display shifts information in a planner view.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ShiftViewPart extends AbstractViewPart {
    private static final IConverter CONVERTER = new Converter(Shift.class, String.class) {
        @Override
        public Object convert(Object fromObject) {
            return ((Shift) fromObject).getTeam().getName();
        }
    };
    private static final String DOT = ".";
    /**
     * The part id.
     */
    public static final String ID = "com.planimod.ui.views.shifts.ShiftViewPart";
    /**
     * Add action.
     */
    private AddShiftAction addShiftAction;
    /**
     * Edit action.
     */
    protected EditShiftAction editShiftAction;

    private ChangeDateAction nextAction;
    private ChangeDateAction previousAction;
    private RemoveAction removeAction;
    private RemoveAllShiftAction removeAllAction;

    private ChangeDateAction todayAction;

    private PlannerViewer viewer;

    /**
     * Create a new view part.
     */
    public ShiftViewPart() {
        super(ID);
        setTitle(_("Quarts de travail"));
    }

    @Override
    public void activate(Composite parent) {

        // Create the planner viewer
        this.viewer = createPlannerViewer(parent);
        this.viewer.getControl().setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

        // Populate the tool bar.
        fillToolBar();

        // Create binding
        bind();

    }

    /**
     * Create data binding.
     */
    @Override
    protected void bindValues() {
        /*
         * Get the states.
         */
        WritableValue date = new WritableValue(null, Date.class);
        getDbc().bindValue(date, PreferenceStoreProperties.value(PlanimodPreferences.DATE, String.class).observe(PlanimodPreferences.getPreferenceStore()));
        if (date.getValue() == null) {
            date.setValue(new Date());
        }

        /*
         * Bind planner date selection
         */
        Planner planner = this.viewer.getPlanner();
        getDbc().bindValue(PlannerProperties.dateSelection().observe(planner), date);
        IObservableValue start = PlannerProperties.startDateValue().observe(planner);
        IObservableValue end = PlannerProperties.endDateValue().observe(planner);

        /*
         * Bind planner viewer.
         */
        IObservableSet shifts = new ShiftComputedSet(getSite().getService(PlanimodManagers.class), start, end);
        IObservableMap shiftLabels = BindingProperties.convertedValue(Converters.removeFrontNumber()).observeDetail(
                BeanProperties.value(Shift.class, Shift.TEAM + DOT + Team.NAME).observeDetail(shifts));
        IObservableMap shiftStart = BeanProperties.value(AbstractCalendarEvent.class, AbstractCalendarEvent.START_DATE).observeDetail(shifts);
        IObservableMap shiftEnd = BeanProperties.value(AbstractCalendarEvent.class, AbstractCalendarEvent.END_DATE).observeDetail(shifts);
        IObservableMap color = BeanProperties.value(Shift.class, Shift.TEAM + DOT + Team.COLOR).value(
                BindingProperties.convertedValue(Converters.StringToRGB())).observeDetail(shifts);
        this.viewer.setContentProvider(new ObservableSetContentProvider(new PlannerViewerUpdater(this.viewer)));
        this.viewer.setLabelProvider(new ObservableMapPlannerLabelProviderWithColor(shiftLabels, shiftStart, shiftEnd, color));
        this.viewer.setComparator(new PropertyViewerComparator(ShiftComparators.byHourAndTeam()));
        this.viewer.setInput(filter(shifts, CONVERTER));

        IObservableValue singleSelection = ViewersObservables.observeSingleSelection(this.viewer);
        IObservableList selection = ViewersObservables.observeMultiSelection(this.viewer);

        /*
         * Bind the planner background color.
         */
        DateColorControlStyleSupport.create(this.viewer.getPlanner(), date);

        /*
         * Bind actions.
         */
        getDbc().bindValue(JFaceProperties.value(ChangeDateAction.class, ChangeDateAction.DATE, ChangeDateAction.DATE).observe(this.previousAction), date);
        getDbc().bindValue(JFaceProperties.value(ChangeDateAction.class, ChangeDateAction.DATE, ChangeDateAction.DATE).observe(this.todayAction), date);
        getDbc().bindValue(JFaceProperties.value(ChangeDateAction.class, ChangeDateAction.DATE, ChangeDateAction.DATE).observe(this.nextAction), date);

        // Add shift event action
        getDbc().bindValue(JFaceProperties.value(AddShiftAction.class, AddShiftAction.DATE, AddShiftAction.DATE).observe(this.addShiftAction), date);

        // Edit action
        getDbc().bindValue(
                JFaceProperties.value(EditShiftAction.class, EditShiftAction.SHIFT, EditShiftAction.SHIFT).observe(this.editShiftAction),
                singleSelection);

        // Remove action.
        getDbc().bindValue(
                JFaceProperties.value(RemoveAction.class, RemoveAction.OBJECTS, RemoveAction.OBJECTS).observe(this.removeAction),
                new ListObservableValue(selection, AbstractCalendarEvent.class));

        // Remove all action
        getDbc().bindValue(
                JFaceProperties.value(RemoveAllShiftAction.class, RemoveAllShiftAction.DATE, RemoveAllShiftAction.DATE).observe(this.removeAllAction),
                date);

    }

    /**
     * This implementation create a planner viewer.
     */
    protected PlannerViewer createPlannerViewer(Composite parent) {

        PlannerViewer viewer = new PlannerViewer(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL | SWT.WRAP);

        // Set widget properties
        viewer.getPlanner().setLook(new PlanimodPlannerLook());

        viewer.addOpenListener(new IOpenListener() {
            @Override
            public void open(OpenEvent event) {
                ShiftViewPart.this.editShiftAction.run();
            }
        });

        return viewer;

    }

    /**
     * This implementation free allocated resources.
     */
    @Override
    public void deactivate() {
        try {
            this.addShiftAction = null;
            this.editShiftAction = null;
            this.nextAction = null;
            this.previousAction = null;
            this.removeAction = null;
            this.todayAction = null;
        } finally {
            super.deactivate();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
    }

    /**
     * Populate the tool bar.
     */
    void fillToolBar() {
        PlanimodManagers managers = getSite().getService(PlanimodManagers.class);
        IToolBarManager toolbar = getSite().getToolBarManager();
        /*
         * Date navigation.
         */
        toolbar.add(this.previousAction = ChangeDateAction.previousAction(managers));
        toolbar.add(this.todayAction = ChangeDateAction.todayAction(managers));
        toolbar.add(this.nextAction = ChangeDateAction.nextAction(managers));

        /*
         * Add action.
         */
        toolbar.add(new Separator());

        // Add
        ActionContributionItem aci;
        toolbar.add(aci = new ActionContributionItem(this.addShiftAction = new AddShiftAction(managers, getSite(), viewer)));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);

        // Edit
        toolbar.add(aci = new ActionContributionItem(this.editShiftAction = new EditShiftAction(managers, getSite())));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);

        // Remove
        toolbar.add(aci = new ActionContributionItem(this.removeAction = new RemoveAction(managers, getSite())));
        this.removeAction.setConfirmationMessage(_("Êtes-vous sûr de vouloir supprimer le quart de travail sélectionné ?"));
        this.removeAction
                .setConfirmationShortDetail(_("Ceci aura pour effet de supprimer le quart de travail pour la semaine en cour ainsi que les événements de production de ce quart ainsi que les tâches associées."));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);

        // Remove all
        toolbar.add(aci = new ActionContributionItem(this.removeAllAction = new RemoveAllShiftAction(managers, getSite(), getSite().getMainWindow())));
        aci.setMode(ActionContributionItem.MODE_FORCE_TEXT);

        // Text filter
        fillToolbarWithTextFilter();
    }

}
