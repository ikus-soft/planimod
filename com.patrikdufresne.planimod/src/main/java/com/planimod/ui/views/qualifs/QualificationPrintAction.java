/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.ui.views.qualifs;

import static com.planimod.ui.Localized._;

import org.eclipse.jface.dialogs.InputDialog;
import org.eclipse.jface.operation.IRunnableContext;
import org.eclipse.jface.window.IShellProvider;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;

import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.printing.IPrintFactory;
import com.patrikdufresne.printing.PrintAction;
import com.planimod.core.ApplicationSetting;
import com.planimod.core.PlanimodManagers;
import com.planimod.ui.PlanimodPolicy;
import com.planimod.ui.preference.PlanimodPreferences;
import com.planimod.ui.printing.PlanimodPrintFactory;

/**
 * This action intend to print the qualification.
 * 
 * @author patapouf
 * 
 */
public class QualificationPrintAction extends PrintAction {
    /**
     * Define the preference key for this action
     */
    private static final String PREF_KEY = "QualificationPrintAction.printPreference";

    /**
     * Managers to use.
     */
    private PlanimodManagers managers;

    private String rev;

    /**
     * Create a new Print Employee Action
     * 
     * @param shell
     *            A Shell provider used to create new windows
     * @param operation
     *            type of operation : PRINT or PREVIEW
     */
    public QualificationPrintAction(PlanimodManagers managers, IShellProvider shell, int operation, IRunnableContext context) {
        super(shell, new QualificationPrintFactory(), operation, context);
        if (managers == null) {
            throw new IllegalArgumentException();
        }
        this.managers = managers;
        setPrefKey(PREF_KEY);
        setPreferenceStore(PlanimodPreferences.getPreferenceStore());
        setEnabled(true);
        if (operation == PrintAction.PRINT_ACTION) {
            setText(_("Imprimer grille de formation"));
        } else {
            setText(_("Aperçu avant impression"));
        }
    }

    /**
     * Returns the managers used by this action.
     * 
     * @return
     */
    protected PlanimodManagers getManagers() {
        return this.managers;
    }

    /**
     * This implementation set the employee to print
     * 
     * @param factory
     *            the factory
     */
    @Override
    protected void initFactory(IPrintFactory factory) {

        QualificationPrintFactory qualifPrintFactory = (QualificationPrintFactory) factory;
        qualifPrintFactory.setOrientation(SWT.HORIZONTAL);
        try {
            // Sets subtitle
            String customerName = getManagers().getApplicationSettingManager().getByName(ApplicationSetting.CUSTOMER_NAME);
            ((PlanimodPrintFactory) factory).setSubTitle(customerName);

            // Sets the employees
            qualifPrintFactory.setEmployeePreferences(getManagers().getEmployeePreferenceManager().list());

            // Sets the qualifications
            qualifPrintFactory.setQualifications(getManagers().getQualificationManager().list());

            // Sets the revision number
            qualifPrintFactory.setRevision(this.rev);
        } catch (ManagerException e) {
            PlanimodPolicy.showException(e);
        }
    }

    @Override
    public void run() {
        // Prompt the user for a revision number
        InputDialog dlg = new InputDialog(
                getShellProvider().getShell(),
                Display.getAppName(),
                _("Entrez un numéro de révision.\nPour l'impression de ce rapport, il est nécéssaire de saisir un numéro de réfivision. "),
                this.rev,
                null);
        if (Window.OK != dlg.open()) {
            return;
        }
        this.rev = dlg.getValue();
        super.run();
    }
}
