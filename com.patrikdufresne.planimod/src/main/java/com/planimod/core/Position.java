/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.patrikdufresne.managers.ArchivableObject;

/**
 * This class represent a position that may be filled by an employee. The position is mostly use to keep track of the
 * employee's qualification.
 */
@Entity
public class Position extends ArchivableObject {

    /**
     * Classified property
     */
    public static final String CLASSIFIED = "classified";

    /**
     * Name property
     */
    public static final String NAME = "name";
    /**
     * RefId property
     */
    public static final String REFID = "refId";
    /**
     * Section property
     */
    public static final String SECTION = "section";

    /**
     * Swappable property.
     */
    public static final String SWAPPABLE = "swappable";

    private static final long serialVersionUID = 6472370093611592367L;

    private boolean classified;

    /**
     * Define a display name for the position.
     */
    private String name;

    /**
     * Define a reference number to the position.
     */
    private String refId;

    /**
     * Define the position's section.
     */
    private Section section;

    /**
     * Define if the position is swappable. A swappable position allow associated tasks to be part of multiple shift.
     */
    private boolean swappable;

    /**
     * Getter for classified position.
     * 
     * @return True if the position is classified.
     */
    public boolean getClassified() {
        return this.classified;
    }

    /**
     * Getter of the property <tt>name</tt>
     * 
     * @return Returns the name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter of the property <tt>refId</tt>
     * 
     * @return Returns the refId.
     */
    @Column(unique = true)
    public String getRefId() {
        return this.refId;
    }

    /**
     * Getter of the property <tt>section</tt>
     * 
     * @return Returns the section.
     */
    @ManyToOne(optional = false)
    public Section getSection() {
        return this.section;
    }

    /**
     * Check if the position is swappable.
     * 
     * @return
     */
    public boolean getSwappable() {
        return this.swappable;
    }

    /**
     * Setter for classified position.
     * 
     * @param classified
     *            True to make the position classified
     */
    public void setClassified(boolean classified) {
        this.changeSupport.firePropertyChange(CLASSIFIED, this.classified, this.classified = classified);
    }

    /**
     * Setter of the property <tt>name</tt>
     * 
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.changeSupport.firePropertyChange(NAME, this.name, this.name = name);
    }

    /**
     * Setter of the property <tt>refId</tt>
     * 
     * @param refId
     *            The refId to set.
     */
    public void setRefId(String refId) {
        this.changeSupport.firePropertyChange(REFID, this.refId, this.refId = refId);
    }

    /**
     * Setter of the property <tt>section</tt>
     * 
     * @param section
     *            The section to set.
     */
    public void setSection(Section section) {
        this.changeSupport.firePropertyChange(SECTION, this.section, this.section = section);
    }

    /**
     * Sets the swappable flag.
     * 
     * @param swappable
     */
    public void setSwappable(boolean swappable) {
        this.changeSupport.firePropertyChange(SWAPPABLE, this.swappable, this.swappable = swappable);
    }

    @Override
    public String toString() {
        return "Position [id=" + this.id + ", refId=" + this.refId + ", name=" + this.name + "]";
    }

}
