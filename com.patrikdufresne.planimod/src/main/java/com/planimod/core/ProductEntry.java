/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Map.Entry;

/**
 * 
 * @author Patrik Dufresne
 * 
 */
public class ProductEntry implements Entry<Product, Integer> {

    private Integer count;

    private Product product;

    public ProductEntry(Product product, Integer count) {
        super();
        this.product = product;
        this.count = count;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ProductEntry other = (ProductEntry) obj;
        if (count == null) {
            if (other.count != null) return false;
        } else if (!count.equals(other.count)) return false;
        if (product == null) {
            if (other.product != null) return false;
        } else if (!product.equals(other.product)) return false;
        return true;
    }

    @Override
    public Product getKey() {
        return this.product;
    }

    @Override
    public Integer getValue() {
        return this.count;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((count == null) ? 0 : count.hashCode());
        result = prime * result + ((product == null) ? 0 : product.hashCode());
        return result;
    }

    @Override
    public Integer setValue(Integer value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        return "ProductEntry [product=" + product + ", count=" + count + "]";
    }

}
