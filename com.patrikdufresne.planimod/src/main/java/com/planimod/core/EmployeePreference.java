/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Collection;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.IndexColumn;

import com.patrikdufresne.managers.ManagedObject;

@Entity
public class EmployeePreference extends ManagedObject {

    /**
     * Employee property
     */
    public static final String EMPLOYEE = "employee";
    /**
     * Preferred position property
     */
    public static final String PREFERRED_POSITION = "preferredPosition";
    /**
     * Preferred position shift property
     */
    public static final String PREFERRED_POSITION_TEAM = "preferredPositionTeam";
    /**
     * Preferred section
     */
    public static final String PREFERRED_SECTION = "preferredSection";
    /**
     * Preferred shifts property
     */
    public static final String PREFERRED_TEAM = "preferredTeam";
    /**
     * 
     */
    private static final long serialVersionUID = -3366336520074905994L;

    /**
     * The associated employee.
     */
    private Employee employee;

    /**
     * Define the preferred position.
     * 
     */
    private Position preferredPosition;
    /**
     * Define the preferred position shifts.
     */
    private Team preferredPositionTeam;

    private Section preferredSection;

    /**
     */
    private List<Team> preferredTeam;

    @OneToOne(optional = false)
    public Employee getEmployee() {
        return this.employee;
    }

    /**
     * Getter of the property <tt>preferredPosition</tt>
     * 
     * @return Returns the preferredPosition.
     */
    @ManyToOne
    public Position getPreferredPosition() {
        return this.preferredPosition;
    }

    /**
     * Returns the preferred position shifts.
     * 
     * @return
     */
    @ManyToOne(fetch = FetchType.EAGER)
    public Team getPreferredPositionTeam() {
        return this.preferredPositionTeam;
    }

    /**
     * Getter of the property <tt>section</tt>
     * 
     * @return Returns the section.
     */
    @ManyToOne
    public Section getPreferredSection() {
        return this.preferredSection;
    }

    /**
     * Getter of the property <tt>shift</tt>
     * 
     * @return Returns the shift.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @IndexColumn(name = "`order`", nullable = false)
    public List<Team> getPreferredTeam() {
        return this.preferredTeam;
    }

    public void remove(final Collection<? extends Employee> s) {
        throw new UnsupportedOperationException();
    }

    public void setEmployee(Employee employee) {
        this.changeSupport.firePropertyChange(EMPLOYEE, this.employee, this.employee = employee);
    }

    /**
     * Setter of the property <tt>preferredPosition</tt>
     * 
     * @param preferredPosition
     *            The preferredPosition to set.
     */
    public void setPreferredPosition(Position preferredPosition) {
        this.changeSupport.firePropertyChange(PREFERRED_POSITION, this.preferredPosition, this.preferredPosition = preferredPosition);
    }

    /**
     * Sets the preferred position shifts.
     * 
     * @param prefferedPositionShift
     */
    public void setPreferredPositionTeam(Team preferredPositionTeam) {
        this.changeSupport.firePropertyChange(PREFERRED_POSITION_TEAM, this.preferredPositionTeam, this.preferredPositionTeam = preferredPositionTeam);
    }

    /**
     * Setter of the property <tt>preferredSection</tt>
     * 
     * @param preferredSection
     *            The section to set.
     */
    public void setPreferredSection(Section preferredSection) {
        this.changeSupport.firePropertyChange(PREFERRED_SECTION, this.preferredSection, this.preferredSection = preferredSection);
    }

    /**
     * Setter of the property <tt>preferredTeam</tt>
     * 
     * @param preferredTeam
     *            The team to set.
     */
    public void setPreferredTeam(List<Team> preferredTeam) {
        this.changeSupport.firePropertyChange(PREFERRED_TEAM, this.preferredTeam, this.preferredTeam = preferredTeam);
    }

    @Override
    public String toString() {
        return "EmployeePreference [id=" + this.id + ", employee=" + this.employee + "]";
    }

}
