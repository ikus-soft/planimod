/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Entity;

import com.patrikdufresne.managers.ArchivableObject;

/**
 * The Shift class represent a set of schedule.
 */
@Entity
public class Team extends ArchivableObject {

    /**
     * Color property
     */
    public static final String COLOR = "color";

    /**
     * Name property
     */
    public static final String NAME = "name";
    private static final long serialVersionUID = -6572695008297379911L;
    private String color;

    /**
     * Define a display name for the shift.
     * 
     * @uml.property name="name"
     */
    private String name;

    public String getColor() {
        return this.color;
    }

    /**
     * Getter of the property <tt>name</tt>
     * 
     * @return Returns the name.
     * @uml.property name="name"
     */
    public String getName() {
        return this.name;
    }

    public void setColor(String color) {
        this.changeSupport.firePropertyChange(COLOR, this.color, this.color = color);
    }

    /**
     * Setter of the property <tt>name</tt>
     * 
     * @param name
     *            The name to set.
     * @uml.property name="name"
     */
    public void setName(String name) {
        this.changeSupport.firePropertyChange(NAME, this.name, this.name = name);
    }

    @Override
    public String toString() {
        return "Team [id=" + this.id + ", name=" + this.name + "]";
    }

}
