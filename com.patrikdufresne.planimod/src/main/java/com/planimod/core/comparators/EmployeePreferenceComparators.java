/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;
import com.planimod.core.Employee;
import com.planimod.core.EmployeePreference;
import com.planimod.core.Section;

/**
 * Utility class
 * 
 * @author Patrik Dufresne
 */
public class EmployeePreferenceComparators {

    /**
     * Comparator to sort employee by name.
     * 
     * @return
     */
    public static Comparator<EmployeePreference> byName() {
        return ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(EmployeeComparators.byName(), new Transformer() {

            @Override
            public Object transform(Object input) {
                return ((EmployeePreference) input).getEmployee();
            }
        }));
    }

    /**
     * Comparator to sort employee by section and then by seniority. This comparator doesn't consider the field
     * preferencialSeniority.
     * 
     * @return
     */
    public static Comparator<EmployeePreference> byPreferredSectionAndSeniority() {
        ComparatorChain chain = new ComparatorChain();
        // Compare section
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(SectionComparators.byName(), new Transformer() {

            @Override
            public Object transform(Object input) {
                return ((EmployeePreference) input).getPreferredSection();
            }
        })));
        // Compare hire date
        chain.addComparator(bySeniority());
        return chain;
    }

    /**
     * Compare employee preference by the preferred section and the seniority.
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Comparator<Employee> byPreferredSectionAndSeniority(final Section firstSection, final Collection<EmployeePreference> preferences) {
        final Comparator<Employee> seniority = EmployeeComparators.bySeniority();
        return ComparatorUtils.nullHighComparator(new Comparator<Employee>() {

            protected boolean matchesPreferredSection(Employee e) {
                // Search the preference.
                Iterator<EmployeePreference> iter = preferences.iterator();
                EmployeePreference pref = null;
                while (iter.hasNext() && !(pref = iter.next()).getEmployee().equals(e)) {
                    // Nothing to do;
                }
                return pref != null && pref.getEmployee().equals(e) && firstSection.equals(pref.getPreferredSection());
            }

            @Override
            public int compare(Employee o1, Employee o2) {
                boolean pref1 = matchesPreferredSection(o1);
                boolean pref2 = matchesPreferredSection(o2);
                if (pref1 && pref2) {
                    return seniority.compare(o1, o2);
                } else if (pref1 && !pref2) {
                    return -1;
                } else if (!pref1 && pref2) {
                    return 1;
                }
                return seniority.compare(o2, o1);
            }
        });
    }

    /**
     * Seniority comparator. It use the hireDate and employee's name for comparison. The smaller hireDate is first. A null
     * hireDate is last.
     * <p>
     * When used to sort, this comparator will sort the employee by increasing hire date. Meaning the first employee is the
     * most senior.
     * 
     * @return
     */
    // TODO This comparator is a business rule and may change according to
    // customer. For this reason this should be define in application setting.
    // Ticket #
    public static Comparator<EmployeePreference> bySeniority() {
        ComparatorChain chain = new ComparatorChain();
        // Preferences
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(
                ComparatorUtils.booleanComparator(true),
                new Transformer() {

                    @Override
                    public Object transform(Object input) {
                        EmployeePreference pref = (EmployeePreference) input;
                        return hasPreferences(pref);
                    }
                })));
        // Hire date
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(EmployeeComparators.bySeniority(), new Transformer() {

            @Override
            public Object transform(Object arg0) {
                return ((EmployeePreference) arg0).getEmployee();
            }
        })));
        return chain;
    }

    public static boolean hasPreferences(EmployeePreference pref) {
        return pref.getPreferredSection() != null
                || pref.getPreferredPosition() != null
                || pref.getPreferredPositionTeam() != null
                || (pref.getPreferredTeam() != null && pref.getPreferredTeam().size() > 0);
    }

    /**
     * Return true if the employee has a preferred position.
     * 
     * @param pref
     * @return
     */
    public static boolean hasPreferredPosition(EmployeePreference pref) {
        return pref.getPreferredPosition() != null && pref.getPreferredPositionTeam() != null;
    }
}
