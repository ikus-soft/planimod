/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Calendar;
import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;

import com.planimod.core.Shift;
import com.planimod.core.TimeRange;

public class ShiftComparators {

    /**
     * Compare calendar event by hours (and not date).
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Comparator<Shift> byHourAndTeam() {
        ComparatorChain chain = new ComparatorChain();

        chain.addComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(ComparatorUtils.naturalComparator()), new Transformer() {
            @Override
            public Object transform(Object input) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(((TimeRange) input).getStartDate());
                return Integer.valueOf(cal.get(Calendar.HOUR_OF_DAY));
            }
        }));

        chain.addComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(ComparatorUtils.naturalComparator()), new Transformer() {
            @Override
            public Object transform(Object input) {
                Calendar cal = Calendar.getInstance();
                cal.setTime(((TimeRange) input).getStartDate());
                return Integer.valueOf(cal.get(Calendar.MINUTE));
            }
        }));

        chain.addComparator(ComparatorUtils.transformedComparator(TeamComparators.byName(), new Transformer() {
            @Override
            public Object transform(Object input) {
                return ((Shift) input).getTeam();
            }
        }));

        return chain;
    }

}
