/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;

import com.planimod.core.Position;

/**
 * Utility class to sort position.
 * 
 * @author Patrik Dufresne
 * 
 */
public class PositionComparators {
    /**
     * Comparator to position bye name.
     * 
     * @return
     */
    public static Comparator<Position> byName() {
        return ComparatorUtils.nullLowComparator(ComparatorUtils.transformedComparator(
                ComparatorUtils.nullLowComparator(ComparatorUtils.naturalComparator()),
                new Transformer() {
                    @Override
                    public Object transform(Object arg0) {
                        return ((Position) arg0).getName();
                    }
                }));
    }

    public static Comparator<Position> bySectionClassifiedAndName() {

        ComparatorChain chain = new ComparatorChain();

        // Compare section's name
        chain.addComparator(ComparatorUtils.nullLowComparator(ComparatorUtils.transformedComparator(SectionComparators.byName(), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((Position) arg0).getSection();
            }
        })));

        // Classified & name
        chain.addComparator(byClassifiedName());

        return chain;
    }

    /**
     * Comparator to sort Position by classified position (first) and then by name.
     * 
     * @return
     */
    public static Comparator<Position> byClassifiedName() {
        ComparatorChain chain = new ComparatorChain();

        // Classified
        chain.addComparator(ComparatorUtils.nullLowComparator(ComparatorUtils.transformedComparator(ComparatorUtils.booleanComparator(true), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return Boolean.valueOf(((Position) arg0).getClassified());
            }
        })));

        // Name
        chain.addComparator(byName());

        return chain;
    }
}
