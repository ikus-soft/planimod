/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;

import com.planimod.core.Task;

/**
 * Utility class used to provide comparators for {@link Task} class.
 * 
 * @author Patrik Dufresne
 * 
 */
public class TaskComparators {

    /**
     * Compare {@link Task} by team, by date and Section
     * 
     * @return the comparator
     */
    public static Comparator<Task> byTeam() {
        ComparatorChain chain = new ComparatorChain();

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(TeamComparators.byName(), new Transformer() {
            @Override
            public Object transform(Object input) {
                return ((Task) input).getProductionEvent().getShift().getTeam();
            }
        })));

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(TimeRangeComparators.byDate(), new Transformer() {
            @Override
            public Object transform(Object input) {
                return ((Task) input).getProductionEvent().getShift();
            }
        })));

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(SectionComparators.byName(), new Transformer() {
            @Override
            public Object transform(Object input) {
                return ((Task) input).getPosition().getSection();
            }
        })));

        return chain;
    }

}
