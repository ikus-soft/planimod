/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;

import com.planimod.core.AbstractCalendarEvent;
import com.planimod.core.ProductionEvent;

public class ProductionEventComparators {

    @SuppressWarnings("unchecked")
    public static Comparator<AbstractCalendarEvent> byDate() {
        return ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(ComparatorUtils.naturalComparator()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((ProductionEvent) arg0).getShift().getStartDate();
            }
        });
    }

    @SuppressWarnings("unchecked")
    public static Comparator<? super ProductionEvent> byProductRefId() {
        return ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(new HumanNaturalComparator()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((ProductionEvent) arg0).getProduct().getRefId();
            }
        });
    }

    /**
     * Compare Production event using the familly.
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Comparator<? super ProductionEvent> byRefIdFamilyName() {
        return ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ProductComparators.byRefIdFamilyName(), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((ProductionEvent) arg0).getProduct();
            }
        }));
    }

}
