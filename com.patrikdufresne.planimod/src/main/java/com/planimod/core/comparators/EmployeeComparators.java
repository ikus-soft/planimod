/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Comparator;
import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;
import com.planimod.core.Employee;

/**
 * Utility class
 * 
 * @author Patrik Dufresne
 */
public class EmployeeComparators {

    /**
     * Seniority comparator. It use the hireDate and employee's name for comparison. The smaller hireDate is first. A null
     * hireDate is last.
     * <p>
     * When used to sort, this comparator will sort the employee by increasing hire date. Meaning the first employee is the
     * most senior.
     * 
     * @return
     */
    // TODO This comparator should be removed
    public static Comparator<Employee> bySeniority() {
        ComparatorChain chain = new ComparatorChain();
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullHighComparator(ComparatorUtils
                .naturalComparator()), new Transformer() {

            @Override
            public Object transform(Object arg0) {
                return ((Employee) arg0).getHireDate();
            }
        })));
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullHighComparator(ComparatorUtils
                .naturalComparator()), new Transformer() {

            @Override
            public Object transform(Object arg0) {
                return ((Employee) arg0).getFirstname();
            }
        })));
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullHighComparator(ComparatorUtils
                .naturalComparator()), new Transformer() {

            @Override
            public Object transform(Object arg0) {
                return ((Employee) arg0).getLastname();
            }
        })));
        return chain;
    }

    /**
     * Comparator to sort employee by name.
     * 
     * @return
     */
    public static Comparator<Employee> byName() {
        ComparatorChain chain = new ComparatorChain();
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(ComparatorUtils
                .nullHighComparator(ComparatorUtils.naturalComparator())), new Transformer() {

            @Override
            public Object transform(Object arg0) {
                return ((Employee) arg0).getFirstname();
            }
        })));
        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullHighComparator(ComparatorUtils
                .naturalComparator()), new Transformer() {

            @Override
            public Object transform(Object arg0) {
                return ((Employee) arg0).getLastname();
            }
        })));
        return chain;
    }
}
