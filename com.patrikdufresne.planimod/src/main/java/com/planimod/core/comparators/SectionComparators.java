/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;

import com.planimod.core.Section;

/**
 * Provie comparator for section object.
 * 
 * @author Patrik Dufresne
 * 
 */
public class SectionComparators {

    /**
     * Comparator to sort section by name.
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Comparator<Section> byName() {
        return ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(
                ComparatorUtils.nullHighComparator(new HumanNaturalComparator()),
                new Transformer() {
                    @Override
                    public Object transform(Object arg0) {
                        return ((Section) arg0).getName();
                    }
                }));
    }

}
