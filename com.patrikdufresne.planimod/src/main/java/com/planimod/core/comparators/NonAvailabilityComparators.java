/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;

import com.planimod.core.AbstractCalendarEvent;
import com.planimod.core.NonAvailability;

public class NonAvailabilityComparators {

    /**
     * Private constructor for utility class.
     */
    private NonAvailabilityComparators() {

    }

    /**
     * Create a new comparator.
     * 
     * @return
     */
    public static Comparator<NonAvailability> byDateAndEmployeeName() {

        ComparatorChain chain = new ComparatorChain();

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(ComparatorUtils
                .naturalComparator()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((AbstractCalendarEvent) arg0).getStartDate();
            }
        })));

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(EmployeeComparators
                .byName()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((NonAvailability) arg0).getEmployee();
            }
        })));

        return chain;
    }

    /**
     * Compare Non-Availability by employee's name.
     * 
     * @return
     */
    @SuppressWarnings("unchecked")
    public static Comparator<? super NonAvailability> byEmployeeName() {

        return ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(EmployeeComparators.byName(), new Transformer() {
            @Override
            public Object transform(Object input) {
                return ((NonAvailability) input).getEmployee();
            }
        }));

    }
}
