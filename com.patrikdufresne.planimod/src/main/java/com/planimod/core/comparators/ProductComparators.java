/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.comparators;

import java.util.Comparator;

import org.apache.commons.collections.ComparatorUtils;
import org.apache.commons.collections.Transformer;
import org.apache.commons.collections.comparators.ComparatorChain;

import com.planimod.core.Product;

/**
 * Utility class used to create comparator for {@link Product} objects.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ProductComparators {
    /**
     * Create a comparator to sort Product by name
     * 
     * @return the comparator
     */
    public static Comparator<Product> byName() {
        return ComparatorUtils.transformedComparator(ComparatorUtils.nullLowComparator(ComparatorUtils.naturalComparator()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((Product) arg0).getName();
            }
        });
    }

    public static Comparator<? super Product> byRefIdFamilyName() {
        ComparatorChain chain = new ComparatorChain();

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullHighComparator(HumanNaturalComparator
                .getInstance()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                String refId = ((Product) arg0).getRefId();
                return refId == null || refId.isEmpty() ? null : refId;
            }
        })));

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullHighComparator(ComparatorUtils
                .naturalComparator()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((Product) arg0).getFamily();
            }
        })));

        chain.addComparator(ComparatorUtils.nullHighComparator(ComparatorUtils.transformedComparator(ComparatorUtils.nullHighComparator(ComparatorUtils
                .naturalComparator()), new Transformer() {
            @Override
            public Object transform(Object arg0) {
                return ((Product) arg0).getName();
            }
        })));

        return chain;
    }

}
