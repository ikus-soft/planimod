/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import com.patrikdufresne.managers.ManagedObject;

/**
 * This object represent the relation between products and position.
 * 
 * @author Patrik Dufresne
 * 
 */
@Entity
public class ProductPosition extends ManagedObject {
    private static final long serialVersionUID = 1396115605522547622L;
    /**
     * Number property
     */
    public static final String NUMBER = "number";
    /**
     * Position property
     */
    public static final String POSITION = "position";
    /**
     * Product property
     */
    public static final String PRODUCT = "product";

    /**
     * Define the number of the given position required to produce the attached product.
     * 
     * @uml.property name="number"
     */
    private Integer number;

    /**
     * Define the position required to be fill to produce the product.
     * 
     * @uml.property name="position"
     * @uml.associationEnd inverse="productPosition:com.planimou.core.Position"
     */
    private Position position;

    /**
     * The product to be produce.
     * 
     * @uml.property name="product"
     * @uml.associationEnd inverse="productPosition:com.planimou.core.Product"
     */
    private Product product;

    /**
     * Getter of the property <tt>number</tt>
     * 
     * @return Returns the number.
     * @uml.property name="number"
     */
    @Column(nullable = false)
    public Integer getNumber() {
        return this.number;
    }

    /**
     * Getter of the property <tt>position</tt>
     * 
     * @return Returns the position.
     * @uml.property name="position"
     */
    @ManyToOne(optional = false)
    public Position getPosition() {
        return this.position;
    }

    /**
     * Getter of the property <tt>product</tt>
     * 
     * @return Returns the product.
     * @uml.property name="product"
     */
    @ManyToOne(optional = false)
    public Product getProduct() {
        return this.product;
    }

    /**
     * Setter of the property <tt>number</tt>
     * 
     * @param number
     *            The number to set.
     * @uml.property name="number"
     */
    public void setNumber(Integer number) {
        this.changeSupport.firePropertyChange(NUMBER, this.number, this.number = number);
    }

    /**
     * Setter of the property <tt>position</tt>
     * 
     * @param position
     *            The position to set.
     * @uml.property name="position"
     */
    public void setPosition(Position position) {
        this.changeSupport.firePropertyChange(POSITION, this.position, this.position = position);
    }

    /**
     * Setter of the property <tt>product</tt>
     * 
     * @param product
     *            The product to set.
     * @uml.property name="product"
     */
    public void setProduct(Product product) {
        this.changeSupport.firePropertyChange(PRODUCT, this.product, this.product = product);
    }

    @Override
    public String toString() {
        return "ProductPosition [number=" + this.number + ", position=" + this.position + ", product=" + this.product + "]";
    }

}
