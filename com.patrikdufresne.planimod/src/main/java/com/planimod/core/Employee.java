/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.patrikdufresne.managers.ArchivableObject;

/**
 * The Employee class represent a physical person that has qualification and may fill a position.
 */
@Entity
public class Employee extends ArchivableObject {
    /**
     * Firstname property
     */
    public static final String FIRSTNAME = "firstname";
    /**
     * Hire date property
     */
    public static final String HIRE_DATE = "hireDate";
    /**
     * Lastname property
     */
    public static final String LASTNAME = "lastname";
    /**
     * Preferencial seniority property
     */
    public static final String PREFERENCIAL_SENIORITY = "preferencialSeniority";
    /**
     * Ref id property
     */
    public static final String REFID = "refId";
    /**
     * 
     */
    private static final long serialVersionUID = 1645082586972364175L;

    /**
     * Define the employee's first name.
     * 
     */
    private String firstname;

    /**
     * Define the employee hire date.
     * 
     */
    private Date hireDate;

    /**
     * Define the employee's lastname.
     * 
     */
    private String lastname;

    /**
     * 
     */
    private boolean preferencialSeniority;

    /**
     * A reference number to the employee.
     * 
     */
    private String refId;

    /**
     * Create a new employee.
     */
    public Employee() {

    }

    /**
     * Getter of the property <tt>Firstname</tt>
     * 
     * @return Returns the firstname.
     */
    @Column(nullable = true)
    public String getFirstname() {
        return this.firstname;
    }

    /**
     * Getter of the property <tt>hireDate</tt>
     * 
     * @return Returns the hireDate.
     */
    @Column(nullable = true)
    public Date getHireDate() {
        return this.hireDate;
    }

    /**
     * Getter of the property <tt>Lastname</tt>
     * 
     * @return Returns the lastname.
     */
    @Column(nullable = true)
    public String getLastname() {
        return this.lastname;
    }

    /**
     * Getter of the property <tt>preferencialSeniority</tt>
     * 
     * @return Returns the preferencialSeniority.
     */
    @Column(nullable = false)
    public boolean getPreferencialSeniority() {
        return this.preferencialSeniority;
    }

    /**
     * Getter of the property <tt>refId</tt>
     * 
     * @return Returns the refId.
     */
    @Column(unique = true, nullable = true)
    public String getRefId() {
        return this.refId;
    }

    /**
     * Setter of the property <tt>Firstname</tt>
     * 
     * @param Firstname
     *            The firstname to set.
     */
    public void setFirstname(String firstname) {
        this.changeSupport.firePropertyChange(FIRSTNAME, this.firstname, this.firstname = firstname);
    }

    /**
     * Setter of the property <tt>hireDate</tt>
     * 
     * @param hireDate
     *            The hireDate to set.
     */
    public void setHireDate(Date hireDate) {
        this.changeSupport.firePropertyChange(HIRE_DATE, this.hireDate, this.hireDate = hireDate);
    }

    /**
     * Setter of the property <tt>Lastname</tt>
     * 
     * @param Lastname
     *            The lastname to set.
     */
    public void setLastname(String lastname) {
        this.changeSupport.firePropertyChange(LASTNAME, this.lastname, this.lastname = lastname);
    }

    /**
     * Setter of the property <tt>preferencialSeniority</tt>
     * 
     * @param preferencialSeniority
     *            The preferencialSeniority to set.
     * @uml.property name="preferencialSeniority"
     */
    public void setPreferencialSeniority(boolean preferencialSeniority) {
        this.changeSupport.firePropertyChange(PREFERENCIAL_SENIORITY, this.preferencialSeniority, this.preferencialSeniority = preferencialSeniority);
    }

    /**
     * Setter of the property <tt>refId</tt>
     * 
     * @param refId
     *            The refId to set.
     * @uml.property name="refId"
     */

    public void setRefId(String refId) {
        this.changeSupport.firePropertyChange(REFID, this.refId, this.refId = refId);
    }

    @Override
    public String toString() {
        return "Employee [id=" + this.id + ", refId=" + this.refId + ", name=" + this.firstname + " " + this.lastname + "]";
    }

}
