/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class NonAvailability extends AbstractCalendarEvent {

    private static final long serialVersionUID = 6384148771431566674L;

    /**
     * Employee property
     */
    public static final String EMPLOYEE = "employee";

    public static final String VISIBLE = "visible";

    private Employee employee;

    private boolean visible = true;

    public boolean isVisible() {
        return this.visible;
    }

    /**
     * Sets the visibility of this non availability. This field is used to determine which non-availabilities to be
     * displayed on report.
     * 
     * @param visible
     *            True to display the non availability
     */
    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @ManyToOne(optional = false)
    public Employee getEmployee() {
        return this.employee;
    }

    public void setEmployee(Employee employee) {
        this.changeSupport.firePropertyChange(EMPLOYEE, this.employee, this.employee = employee);
    }

}
