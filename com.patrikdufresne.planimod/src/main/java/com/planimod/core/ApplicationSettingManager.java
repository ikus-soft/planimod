/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Arrays;
import java.util.Calendar;
import org.hibernate.criterion.Restrictions;
import com.patrikdufresne.managers.AbstractManager;
import com.patrikdufresne.managers.Exec;
import com.patrikdufresne.managers.ManagerContext;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.Query;

/**
 * Object manager for section.
 * 
 * @author Patrik Dufresne
 */
public class ApplicationSettingManager extends AbstractManager<ApplicationSetting> {

    public ApplicationSettingManager(PlanimodManagers managers) {
        super(managers);
    }

    String get(final String name) throws ManagerException {
        return getManagers().query(new Query<String>() {

            @Override
            public String run() throws ManagerException {
                ApplicationSetting setting = (ApplicationSetting) ManagerContext.getDefaultSession().createCriteria(objectClass()).add(
                        Restrictions.eq(ApplicationSetting.NAME, name)).uniqueResult();
                if (setting == null) {
                    return null;
                }
                return setting.getValue();
            }
        });
    }

    /**
     * This implementation query the database
     * 
     * @param name
     * @return
     */
    public String getByName(String name) throws ManagerException {
        if (ApplicationSetting.FIRST_DAY_OF_WEEK.equals(name)) {
            return Integer.toString(getFirstDayOfWeek());
        }
        return get(name);
    }

    /**
     * Get the first day of week.
     * 
     * @return
     * @throws ManagerException
     */
    public int getFirstDayOfWeek() throws ManagerException {
        String value = get(ApplicationSetting.FIRST_DAY_OF_WEEK);
        if (value != null) {
            try {
                return Integer.valueOf(value).intValue();
            } catch (NumberFormatException e) {
                // Swallow
            }
        }
        // Return the default value.
        return Integer.valueOf(Calendar.SUNDAY).intValue();
    }

    @Override
    public Class<ApplicationSetting> objectClass() {
        return ApplicationSetting.class;
    }

    /**
     * This implementation query the database
     * 
     * @param name
     * @return
     */
    public void setByName(final String name, final String value) throws ManagerException {
        getManagers().exec(new Exec() {

            @Override
            public void run() throws ManagerException {
                ApplicationSetting setting = (ApplicationSetting) ManagerContext.getDefaultSession().createCriteria(objectClass()).add(
                        Restrictions.eq(ApplicationSetting.NAME, name)).uniqueResult();
                if (setting == null) {
                    setting = new ApplicationSetting();
                    setting.setName(name);
                    setting.setValue(value);
                    add(Arrays.asList(setting));
                } else {
                    setting.setValue(value);
                    update(Arrays.asList(setting));
                }
            }
        });
    }

    /**
     * Sets the first day of the week.
     * 
     * @param dayOfWeek
     *            should be one of the Calendar constant
     * @throws ManagerException
     */
    public void setFirstDayOfWeek(int dayOfWeek) throws ManagerException {
        setByName(ApplicationSetting.FIRST_DAY_OF_WEEK, Integer.valueOf(dayOfWeek).toString());
    }
}
