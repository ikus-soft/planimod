/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.List;

import org.hibernate.criterion.Restrictions;

import com.patrikdufresne.managers.AbstractManager;
import com.patrikdufresne.managers.ManagerContext;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.Query;

/**
 * Object manager for product.
 * 
 * @author Patrik Dufresne
 */
public class ProductPositionManager extends AbstractManager<ProductPosition> {

    public ProductPositionManager(PlanimodManagers managers) {
        super(managers);
    }

    /**
     * List product-position elements for the given product.
     * 
     * @param product
     *            a product
     * 
     * @return list of product-position
     * @throws ManagerException
     */
    public List<ProductPosition> listByProduct(final Product product) throws ManagerException {
        return getManagers().query(new Query<List<ProductPosition>>() {
            @Override
            public List<ProductPosition> run() throws ManagerException {
                return ManagerContext.getDefaultSession().createCriteria(objectClass()).add(Restrictions.eq(ProductPosition.PRODUCT, product)).list();
            }
        });
    }

    @Override
    public Class<ProductPosition> objectClass() {
        return ProductPosition.class;
    }

}
