/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Entity;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.patrikdufresne.managers.ArchivableObject;

/**
 * The Product class represent one product that may be schedule.
 */
@Entity
@NamedQueries( {
        @NamedQuery(name = "listProductTableByShifts", query = "SELECT S, P, SUM(PP.number), S.team "
                + "FROM Product as P, Shift as S, ProductionEvent as PE, ProductPosition as PP "
                + "WHERE PP.product = P "
                + "AND PE.product = P "
                + "AND PE.shift = S "
                + "AND S in (:shifts) "
                + "group by S, P"),
        @NamedQuery(name = "listProductTable", query = "SELECT P, SUM(PP.number) "
                + "FROM Product as P, ProductPosition as PP "
                + "WHERE PP.product = P "
                + "AND P in (:products) "
                + "group by P") })
public class Product extends ArchivableObject {

    /**
     * Family property
     */
    public static final String FAMILY = "family";

    /**
     * Name property
     */
    public static final String NAME = "name";
    /**
     * RefId property
     */
    public static final String REFID = "refId";
    private static final long serialVersionUID = 1833794757829504482L;

    /**
     * Define the product family.
     */
    private String family;

    /**
     * Define the product's name.
     * 
     */
    private String name;

    /**
     * Define a reference number to the product.
     * 
     */
    private String refId;

    public String getFamily() {
        return this.family;
    }

    /**
     * Getter of the property <tt>name</tt>
     * 
     * @return Returns the name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Getter of the property <tt>refId</tt>
     * 
     * @return Returns the refId.
     */
    public String getRefId() {
        return this.refId;
    }

    public void setFamily(String family) {
        this.changeSupport.firePropertyChange(FAMILY, this.family, this.family = family);
    }

    /**
     * Setter of the property <tt>name</tt>
     * 
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.changeSupport.firePropertyChange(NAME, this.name, this.name = name);
    }

    /**
     * Setter of the property <tt>refId</tt>
     * 
     * @param refId
     *            The refId to set.
     */
    public void setRefId(String refId) {
        this.changeSupport.firePropertyChange(REFID, this.refId, this.refId = refId);
    }

    @Override
    public String toString() {
        return "Product [id=" + this.id + ", name=" + this.name + ", refId=" + this.refId + "]";
    }

}
