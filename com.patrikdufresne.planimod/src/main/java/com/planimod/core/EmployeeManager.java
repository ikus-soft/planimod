/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.patrikdufresne.managers.AbstractArchivableManager;
import com.patrikdufresne.managers.Exec;
import com.patrikdufresne.managers.ManagerContext;
import com.patrikdufresne.managers.ManagerException;

/**
 * Object manager for employees.
 * 
 * @author Patrik Dufresne
 */
public class EmployeeManager extends AbstractArchivableManager<Employee> {

    public EmployeeManager(PlanimodManagers managers) {
        super(managers);
    }

    /**
     * Return the managers
     * 
     * @return
     */
    @Override
    public PlanimodManagers getManagers() {
        return (PlanimodManagers) super.getManagers();
    }

    @Override
    public Class<Employee> objectClass() {
        return Employee.class;
    }

    /**
     * This implementation remove the {@link Employee} and the {@link EmployeePreference} objects from the managers.
     * 
     * @see com.patrikdufresne.managers.IManager#remove(java.util.Collection)
     */
    @Override
    public void remove(final Collection<? extends Employee> s) throws ManagerException {
        checkObject(s);
        getManagers().exec(new Exec() {
            @Override
            public void run() throws ManagerException {
                List<EmployeePreference> prefs = getManagers().getEmployeePreferenceManager().listByEmployees(s);
                for (EmployeePreference pref : prefs) {
                    ManagerContext.getDefaultSession().delete(pref);
                    ManagerContext.getDefaultSession().delete(pref.getEmployee());
                }
            }
        });
    }

    /**
     * This implementation add an Employee preference for the employee.
     * 
     * @param t
     *            the employee object
     * @throws ManagerException
     */
    @Override
    protected void postAddObject(Employee e) throws ManagerException {
        super.postAddObject(e);

        // Add a employee-preference
        EmployeePreference pref = new EmployeePreference();
        pref.setEmployee(e);
        getManagers().getEmployeePreferenceManager().add(Arrays.asList(pref));
    }

}
