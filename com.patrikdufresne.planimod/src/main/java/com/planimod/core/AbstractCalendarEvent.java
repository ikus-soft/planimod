/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.patrikdufresne.managers.ManagedObject;

@MappedSuperclass
public abstract class AbstractCalendarEvent extends ManagedObject implements TimeRange {

    /**
     * End date property
     */
    public static final String END_DATE = "endDate";

    private static final long serialVersionUID = -5828220038114866993L;
    /**
     * Start date property
     */
    public static final String START_DATE = "startDate";

    /**
     * Summary property
     */
    public static final String SUMMARY = "summary";

    /**
     * The end date and time of this event.
     * 
     * @uml.property name="endDate"
     */
    private Date endDate;

    /**
     * The start date and time of this event.
     * 
     * @uml.property name="startDate"
     */
    private Date startDate;

    /**
     * A short description of this event.
     * 
     * @uml.property name="summary"
     */
    private String summary;

    /**
     * Getter of the property <tt>endDate</tt>
     * 
     * @return Returns the endDate.
     * @uml.property name="endDate"
     */
    @Override
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * Getter of the property <tt>startDate</tt>
     * 
     * @return Returns the startDate.
     * @uml.property name="startDate"
     */
    @Override
    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * Getter of the property <tt>summary</tt>
     * 
     * @return Returns the summary.
     * @uml.property name="summary"
     */
    @Column(nullable = true)
    public String getSummary() {
        return this.summary;
    }

    /**
     * Setter of the property <tt>endDate</tt>
     * 
     * @param endDate
     *            The endDate to set.
     * @uml.property name="endDate"
     */
    public void setEndDate(Date endDate) {
        this.changeSupport.firePropertyChange("endDate", this.endDate, this.endDate = endDate);
    }

    /**
     * Setter of the property <tt>startDate</tt>
     * 
     * @param startDate
     *            The startDate to set.
     * @uml.property name="startDate"
     */
    public void setStartDate(Date startDate) {
        this.changeSupport.firePropertyChange("startDate", this.startDate, this.startDate = startDate);
    }

    /**
     * Setter of the property <tt>summary</tt>
     * 
     * @param summary
     *            The summary to set.
     * @uml.property name="summary"
     */
    public void setSummary(String summary) {
        this.changeSupport.firePropertyChange("summary", this.summary, this.summary = summary);
    }

    @Override
    public String toString() {
        return "CalendarEvent [id=" + this.id + ", summary=" + this.summary + "]";
    }

}
