/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.patrikdufresne.managers.ManagedObject;

/**
 * A production class define an event that should normally occur once in a planif. The production involved a product
 * being produce.
 * 
 * @author Patrik Dufresne
 * 
 */
@Entity
@NamedQueries( { @NamedQuery(name = "listProductionEventTableByShifts", query = "SELECT PE, SUM(PP.number), PE.product, PE.shift, S.team "
        + "FROM ProductPosition as PP, ProductionEvent as PE JOIN PE.product JOIN PE.shift as S JOIN S.team "
        + "WHERE PP.product = PE.product "
        + "AND PE.shift in (:shifts) "
        + "group by PE.id") })
public class ProductionEvent extends ManagedObject {

    /**
     * Product property.
     */
    public static final String PRODUCT = "product";

    private static final long serialVersionUID = 7747140188013018709L;
    /**
     * Shift property
     */
    public static final String SHIFT = "shift";

    private Product product;

    private Shift shift;

    /**
     * Getter of the property <tt>product</tt>
     * 
     * @return Returns the product.
     */
    @ManyToOne(optional = false)
    public Product getProduct() {
        return this.product;
    }

    /**
     * Define the associated shift event.
     * 
     * @return
     */
    @ManyToOne(optional = false)
    public Shift getShift() {
        return this.shift;
    }

    /**
     * Setter of the property <tt>product</tt>
     * 
     * @param product
     *            The product to set.
     */
    public void setProduct(Product product) {
        this.changeSupport.firePropertyChange(PRODUCT, this.product, this.product = product);
    }

    public void setShift(Shift shift) {
        this.changeSupport.firePropertyChange(SHIFT, this.shift, this.shift = shift);
    }

    @Override
    public String toString() {
        return "ProductionEvent [id=" + this.id + "]";
    }

}
