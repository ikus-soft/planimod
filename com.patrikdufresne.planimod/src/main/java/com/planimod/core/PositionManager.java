/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.List;

import org.hibernate.criterion.Restrictions;

import com.patrikdufresne.managers.AbstractManager;
import com.patrikdufresne.managers.ManagerContext;
import com.patrikdufresne.managers.ManagerException;
import com.patrikdufresne.managers.Query;

/**
 * Object manager for position.
 * 
 * @author Patrik Dufresne
 */
public class PositionManager extends AbstractManager<Position> {

    public PositionManager(PlanimodManagers managers) {
        super(managers);
    }

    @Override
    public Class<Position> objectClass() {
        return Position.class;
    }

    /**
     * List positions for the given Section.
     * 
     * @param section
     *            the section
     * @return list of position
     * @throws ManagerException
     */
    public List<Position> listBySection(final Section section) throws ManagerException {
        return getManagers().query(new Query<List<Position>>() {
            @Override
            public List<Position> run() throws ManagerException {
                return ManagerContext.getDefaultSession().createCriteria(objectClass()).add(Restrictions.eq(Position.SECTION, section)).list();
            }
        });
    }

    /**
     * List classified positions only.
     * 
     * @return list of position
     * @throws ManagerException
     */
    public List<Position> listClassified() throws ManagerException {
        return getManagers().query(new Query<List<Position>>() {
            @Override
            public List<Position> run() throws ManagerException {
                return ManagerContext.getDefaultSession().createCriteria(objectClass()).add(Restrictions.eq(Position.CLASSIFIED, true)).list();
            }
        });
    }

}
