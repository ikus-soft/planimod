/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.patrikdufresne.managers.ArchivableObject;

/**
 * The Section is only used to logically regroup position.
 */
@Entity
public class Section extends ArchivableObject {

    /**
     * Name property
     */
    public static final String NAME = "name";

    /**
     * 
     */
    private static final long serialVersionUID = 3059101410954013234L;

    /**
     * Define a display name for the section.
     * 
     * @uml.property name="name"
     */
    private String name;

    /**
     * Getter of the property <tt>name</tt>
     * 
     * @return Returns the name.
     */
    @Column(nullable = true)
    public String getName() {
        return this.name;
    }

    /**
     * Setter of the property <tt>name</tt>
     * 
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.changeSupport.firePropertyChange(NAME, this.name, this.name = name);
    }

    @Override
    public String toString() {
        return "Section [name=" + this.name + "]";
    }

}
