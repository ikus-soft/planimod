/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
public class Shift extends AbstractCalendarEvent {

    private static final long serialVersionUID = -6588149832619562142L;

    /**
     * Team property
     */
    public static final String TEAM = "team";

    private Team team;

    @ManyToOne(optional = false)
    public Team getTeam() {
        return this.team;
    }

    public void setTeam(Team team) {
        this.changeSupport.firePropertyChange(TEAM, this.team, this.team = team);
    }

    @Override
    public String toString() {
        return "Shift [team=" + team + ", getEndDate()=" + getEndDate() + ", getStartDate()=" + getStartDate() + "]";
    }
}
