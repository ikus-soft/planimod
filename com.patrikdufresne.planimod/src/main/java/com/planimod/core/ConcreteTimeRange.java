/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Date;

/**
 * Concrete implementation of TimeRange.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ConcreteTimeRange implements TimeRange {

    private Date end;

    private Date start;

    /**
     * Create a new time range from a starting and ending date. The dates can't be null.
     * 
     * @param start
     * @param end
     */
    public ConcreteTimeRange(Date start, Date end) {
        if (start == null || end == null) {
            throw new IllegalArgumentException();
        }
        this.start = start;
        this.end = end;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ConcreteTimeRange other = (ConcreteTimeRange) obj;
        if (!this.end.equals(other.end)) return false;
        if (!this.start.equals(other.start)) return false;
        return true;
    }

    /**
     * Return the ending date.
     */
    @Override
    public Date getEndDate() {
        return this.end;
    }

    /**
     * Return the starting date.
     */
    @Override
    public Date getStartDate() {
        return this.start;
    }

    /**
     * This implementation return a hash code using the starting and ending dates.
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.end.hashCode();
        result = prime * result + this.start.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "ConcreteTimeRange [from:" + this.start + ", to:" + this.end + "]";
    }

}