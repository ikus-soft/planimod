/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.NamedQueries;
import org.hibernate.annotations.NamedQuery;

import com.patrikdufresne.managers.ManagedObject;

/**
 * A qualification represent a position known by an employee.
 * 
 * @author Patrik Dufresne
 * 
 */
@Entity
@NamedQueries( { @NamedQuery(name = "getQualificationTableByPositions", query = "SELECT E, Po, Se "
        + "FROM Qualification Q JOIN Q.employee as E JOIN Q.position as Po JOIN Po.section as Se "
        + "WHERE Q.position in :positions AND E.archivedDate is null") })
public class Qualification extends ManagedObject {

    /**
     * Employee property.
     */
    public static final String EMPLOYEE = "employee";
    /**
     * Position property
     */
    public static final String POSITION = "position";
    private static final long serialVersionUID = -7961409326939904862L;

    private Employee employee;

    private Position position;

    /**
     * Getter of the property <tt>employee</tt>
     * 
     * @return Returns the employee.
     */
    @ManyToOne(optional = false)
    public Employee getEmployee() {
        return this.employee;
    }

    /**
     * Getter of the property <tt>position</tt>
     * 
     * @return Returns the position.
     */
    @ManyToOne(optional = false)
    public Position getPosition() {
        return this.position;
    }

    /**
     * Setter of the property <tt>employee</tt>
     * 
     * @param employee
     *            The employee to set.
     */
    public void setEmployee(Employee employee) {
        this.changeSupport.firePropertyChange(EMPLOYEE, this.employee, this.employee = employee);
    }

    /**
     * Setter of the property <tt>position</tt>
     * 
     * @param position
     *            The position to set.
     */
    public void setPosition(Position position) {
        this.changeSupport.firePropertyChange(POSITION, this.position, this.position = position);
    }

    @Override
    public String toString() {
        return "Qualification [position=" + this.position + ", employee=" + this.employee + "]";
    }

}
