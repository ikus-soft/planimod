/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.patrikdufresne.managers.ManagedObject;

/**
 * This class represent one setting for the application.
 * 
 * @author patapouf
 */
@Entity
public class ApplicationSetting extends ManagedObject {
    /**
     * Key to access the first day of week value.
     */
    public static final String FIRST_DAY_OF_WEEK = "firstDayOfWeek";
    /**
     * Key to access the customer name. Commonly used to sets the plan name.
     */
    public static final String CUSTOMER_NAME = "customerName";

    /**
     * Name property
     */
    public static final String NAME = "name";
    /**
     * Value property
     */
    public static final String VALUE = "value";

    private String name;
    private String value;

    public ApplicationSetting() {
        // No default value to set.
    }

    /**
     * Unique name of the setting.
     * 
     * @return the name of the setting.
     */
    @Column(nullable = false, length = 50)
    public String getName() {
        return this.name;
    }

    /**
     * Get value of the setting.
     * 
     * @return the value.
     */
    @Column(nullable = false, length = 50)
    public String getValue() {
        return this.value;
    }

    /**
     * Sets the name of the setting.
     * 
     * @param name
     *            the new name
     */
    public void setName(String name) {
        this.changeSupport.firePropertyChange(NAME, this.name, this.name = name);
    }

    /**
     * Sets the value of thr setting
     * 
     * @param value
     *            the value.
     */
    public void setValue(String value) {
        this.changeSupport.firePropertyChange(VALUE, this.value, this.value = value);
    }

    @Override
    public String toString() {
        return "ApplicationSetting [name=" + this.name + ", value=" + this.value + "]";
    }
}
