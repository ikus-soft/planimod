/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core;

import java.util.Collection;

import com.patrikdufresne.util.BidiMultiHashMap;
import com.patrikdufresne.util.BidiMultiMap;

/**
 * This class
 * 
 * @author Patrik Dufresne
 * 
 */
public class TaskNonAvailabilityTable extends BidiMultiHashMap<Employee, Task> {

    public TaskNonAvailabilityTable(Collection<NonAvailability> nonavailabilities, Collection<Task> tasks) {

        BidiMultiMap<Employee, Task> table = new BidiMultiHashMap<Employee, Task>();
        for (Task t : tasks) {
            for (NonAvailability na : nonavailabilities) {
                if (TimeRanges.intersectEquals(na, t)) {
                    put(na.getEmployee(), t);
                }
            }
        }

    }

}