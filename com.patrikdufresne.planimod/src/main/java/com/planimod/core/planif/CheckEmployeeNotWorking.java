/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.patrikdufresne.ilp.util.Bound;

/**
 * This runnable is used to check if their is a feasible solution with this employee not working.
 * 
 * @author Patrik Dufresne
 */
@SuppressWarnings("unqualified-field-access")
public class CheckEmployeeNotWorking extends SolveRunnable {

    private static final transient Logger LOGGER = LoggerFactory.getLogger(CheckEmployeeNotWorking.class);

    /**
     * The employee index to avoid assignment.
     */
    private final int i;

    /**
     * Bounds of the xi variable before running this class.
     */
    private Bound originalBound;

    /**
     * Bounds of the xi variable when release was called.
     */
    private Bound snapshotBound;

    /**
     * Create a new runnable
     * 
     * @param problem
     *            reference to ProblemBuilder
     * @param i
     *            the employee index to avoid making work.
     */
    public CheckEmployeeNotWorking(ProblemBuilder problem, int i) {
        super(problem);
        this.i = i;
        setFeasibilityPumpHeuristic(true);
    }

    /**
     * This implementation restore the snapshot bound.
     */
    @Override
    public void engage() {
        // Nothing to engage if xi is null.
        if (pb.xi[i] == null) {
            return;
        }
        if (this.snapshotBound == null) {
            throw new RuntimeException("snapshot is not available");
        }
        // Restore the bounds. Probably [0..0] or [1..1]
        this.snapshotBound.restore(pb.xi[i]);
        this.snapshotBound = null;
    }

    /**
     * This implementation does nothing.
     */
    @Override
    protected void feasible() {
        // Feasible solution with the employee not working.
        LOGGER.info("Employee " + pb.employeeToString(i) + " is NOT required");
    }

    /**
     * Used to restore the original variable bounds.
     * 
     * @throws GeneratePlanifException
     */
    @Override
    public void release() {
        // Nothing to release if xi is null.
        if (pb.xi[i] == null) {
            return;
        }
        // Restore the variable bounds.
        if (this.originalBound == null) {
            throw new RuntimeException("original bound not available");
        }
        // Capture the bound
        this.snapshotBound = Bound.create(pb.xi[i]);
        // Restore the original bound (should be [0..1]
        this.originalBound.restore(pb.xi[i]);
    }

    @Override
    protected boolean setUp() {
        // Skip this runnable if the variable is not set
        if (pb.xi[i] == null) {
            feasible = true;
            return false;
        }
        // Capture the original bounds
        if (this.originalBound == null) {
            this.originalBound = new Bound(pb.xi[i].getLowerBound(), pb.xi[i].getUpperBound());
        }
        // Fix the variable to ZERO.
        pb.xi[i].setLowerBound(ZERO);
        pb.xi[i].setUpperBound(ZERO);
        // Check if the previous snapshot is not assigning the employee.
        if (pb.snapshot != null && ZERO.equals(pb.snapshot.get(pb.xi[i]))) {
            LOGGER.debug("Employee {} " + " is already not working according to snapshot.", pb.employeeToString(i));
            feasible = true;
            return false;
        }
        // Reset the objective function
        pb.lp.setObjectiveLinear(null);
        return true;
    }

    /**
     * If it's impossible to assign all tasks, release the constraint.
     */
    @Override
    protected void unfeasible() {
        if (this.originalBound == null) {
            throw new RuntimeException("original bound not available.");
        }
        // Restore the variable bounds
        this.originalBound.restore(pb.xi[i]);
        LOGGER.info("Employee {} is required.", pb.employeeToString(i));
    }
}