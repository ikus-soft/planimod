/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This runnable is used to minimized the number of swapable task. It's believed to be faster to check for feasibility
 * first and then optimize if required.
 * 
 * @author Patrik Dufresne
 */
public class MinSwappableTask extends SolveRunnable {

    private static final transient Logger LOGGER = LoggerFactory.getLogger(MinSwappableTask.class);

    private CheckSwappableTask checkSwappableTask;

    private MinAllSwappableTask minAllSwappableTask;

    /**
     * public constructor.
     * 
     * @param problem
     *            reference to ProblemBuilder
     */
    public MinSwappableTask(ProblemBuilder problem) {
        super(problem);
    }

    /**
     * This implementation is calling the {@link #engage()} function of the underlying runnable.
     */
    @Override
    public void engage() {
        // Only one of the solve-runnable should be instantiated. Engage it.
        if (this.checkSwappableTask != null) {
            this.checkSwappableTask.engage();
        } else if (this.minAllSwappableTask != null) {
            this.minAllSwappableTask.engage();
        } else {
            throw new RuntimeException("release() should be called first.");
        }
    }

    /**
     * This implementation is calling the {@link #release()} of the underlying runnable.
     */
    @Override
    public void release() {
        // Only one of the solve-runnable should be instantiated. Release
        // it.
        if (this.checkSwappableTask != null) {
            this.checkSwappableTask.release();
        } else if (this.minAllSwappableTask != null) {
            this.minAllSwappableTask.release();
        } else {
            throw new RuntimeException("run() should be called first.");
        }
    }

    /**
     * This implementation will
     */
    @Override
    protected boolean runSolver() throws GeneratePlanifException {
        this.checkSwappableTask = new CheckSwappableTask(pb);
        this.checkSwappableTask.run();
        if (this.checkSwappableTask.isFeasible()) {
            return true;
        }
        this.checkSwappableTask = null;
        this.minAllSwappableTask = new MinAllSwappableTask(pb);
        this.minAllSwappableTask.run();
        // Should always be feasible.
        return this.minAllSwappableTask.isFeasible();
    }
}