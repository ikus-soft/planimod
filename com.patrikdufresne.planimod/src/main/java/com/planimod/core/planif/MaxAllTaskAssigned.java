/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.patrikdufresne.ilp.Linear;
import com.patrikdufresne.ilp.LinearProblem;

/**
 * This runnable is used to find the maximum number of task that may be assigned.
 * <p>
 * This implementation optimize the total number of tasks being assigned whatever the task is. This solver is always
 * feasible.
 * 
 * <pre>
 * max(SUM from{i=0} to {n} SUM from{j=0} to {m} SUM from{k=0} to {p} xijk)
 * </pre>
 * 
 * @author Patrik Dufresne
 */
@SuppressWarnings("unqualified-field-access")
public class MaxAllTaskAssigned extends AbstractConstraintRunnable {

    private static final transient Logger LOGGER = LoggerFactory.getLogger(MaxAllTaskAssigned.class);

    /**
     * The constraint's name.
     */
    private static final String CONSTRAINT_NAME = "MaxAllTaskAssigned: SUM from{i=0} to {n} SUM from{j=0} to {m} SUM from{k=0} to {p} xijk >=%d";

    /**
     * The linear being maximized.
     */
    private Linear linear;

    /**
     * Public constructor.
     * 
     * @param problem
     *            reference to ProblemBuilder
     */
    public MaxAllTaskAssigned(ProblemBuilder problem) {
        super(problem);
        setFeasibilityPumpHeuristic(true);
    }

    /**
     * Create a constraint to maintain the number of assignment.
     */
    @Override
    protected void feasible() {
        // Get objective value
        Number value = Integer.valueOf(pb.lp.getObjectiveValue().intValue());
        LOGGER.info("Maximum number of task assigned: {}", value);
        // Sets the number of tasks assigned as a constraint.
        String name = String.format(CONSTRAINT_NAME, value);
        this.constraint = pb.lp.addConstraint(name, this.linear, value, null);
    }

    /**
     * This implementation create the linear to be maximized.
     */
    @Override
    protected boolean setUp() {
        LOGGER.debug("Maximize number of task assigned.");
        // Create a linear
        this.linear = pb.lp.createLinear();
        for (int i = 0; i < pb.xijk.length; i++) {
            for (int j = 0; j < pb.xijk[i].length; j++) {
                for (int k = 0; k < pb.xijk[i][j].length; k++) {
                    if (pb.xijk[i][j][k] != null) {
                        this.linear.add(pb.lp.createTerm(1, pb.xijk[i][j][k]));
                    }
                }
            }
        }
        // Set the linear as an objective
        pb.lp.setObjectiveLinear(this.linear);
        pb.lp.setObjectiveDirection(LinearProblem.MAXIMIZE);
        return true;
    }

    /**
     * Reset the objective function.
     */
    @Override
    protected void tearDown() {
        // Remove objective
        pb.lp.setObjectiveLinear(null);
    }

    /**
     * Should never be called.
     */
    @Override
    protected void unfeasible() throws GeneratePlanifException {
        // It's a maximization problem, it' must be feasible.
        throwProblemUnFeasible();
    }
}