/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Adds a constrain for the preferred seniority Maximize the number of assigned employees with preferred seniority
 * employees.
 * 
 * @author Patrik Dufresne
 */
public class MaxPreferredSeniorities extends SolveRunnable {

    private static final transient Logger LOGGER = LoggerFactory.getLogger(MaxPreferredSeniorities.class);

    /**
     * Public constructor.
     * 
     * @param problem
     *            reference to ProblemBuilder
     */
    public MaxPreferredSeniorities(ProblemBuilder problem) {
        super(problem);
    }

    @Override
    protected boolean runSolver() throws GeneratePlanifException {
        for (int i = 0; i < pb.employees.size(); i++) {
            // Avoid creating constraint for employees without preferred
            // seniority and required preferred team.
            if (pb.employees.get(i) != null && pb.employees.get(i).getPreferencialSeniority()) {
                MaxPreferredSeniority solver = new MaxPreferredSeniority(pb, i);
                solver.run();
            }
        }
        return true;
    }

    @Override
    protected boolean setUp() throws GeneratePlanifException {
        LOGGER.info("Create constraints to assign employees with " + "preferred seniority.");
        return true;
    }
}