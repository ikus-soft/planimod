/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.patrikdufresne.ilp.Linear;
import com.patrikdufresne.ilp.util.Constraints;

/**
 * This runnable check if the problem is feasible without any swappable tasks.
 * 
 * <pre>
 * SUM from{i=0} to{n} SUM from{j=0} to{m} SUM from{k notin RTj} xijk ==0
 * </pre>
 * 
 * @author Patrik Dufresne
 */
public class CheckSwappableTask extends AbstractConstraintRunnable {

    private static final transient Logger LOGGER = LoggerFactory.getLogger(CheckSwappableTask.class);

    /**
     * The constraint's name
     */
    private static final String CONSTRAINT_NAME = "SUM from{i=0} to{n} SUM from{j=0} to{m} SUM from{k notin RTj} xijk <=0";

    /**
     * Public constructor.
     * 
     * @param problem
     *            reference to ProblemBuilder
     */
    public CheckSwappableTask(ProblemBuilder problem) {
        super(problem);
        setFeasibilityPumpHeuristic(true);
    }

    @Override
    protected boolean setUp() {
        // Create a linear function to compute a value using the snapshot.
        Linear linear = pb.lp.createLinear();
        int m = pb.table.tj().size();
        int p = pb.tasks.size();
        for (int k = 0; k < p; k++) {
            for (int j = 0; j < m; j++) {
                if (!pb.table.kInRTj(k, j)) {
                    for (int i = 0; i < pb.xijk.length; i++) {
                        if (pb.xijk[i][j][k] != null) {
                            linear.add(pb.lp.createTerm(ONE, pb.xijk[i][j][k]));
                        }
                    }
                }
            }
        }
        // Create the constraint
        this.constraint = pb.lp.addConstraint(CONSTRAINT_NAME, linear, null, ZERO);
        // If the linear function is empty, their is not need to optimize
        // the problem.
        // If the last snapshot was feasible without swappable tasks, we
        // don't need to optimize either.
        if (pb.snapshot != null && Constraints.isSatisfied(this.constraint, pb.snapshot)) {
            this.feasible = true;
            return false;
        }
        // Fix all swappable tasks variableto zero
        pb.lp.setObjectiveLinear(null);
        // Check feasibility without swappable tasks
        LOGGER.info("Check feasibility without swappable tasks.");
        return true;
    }
}