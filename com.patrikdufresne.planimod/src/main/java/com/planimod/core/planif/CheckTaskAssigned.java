/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.patrikdufresne.ilp.Linear;

/**
 * This runnable is used to check if every tasks can be assigned to an employee.
 * 
 * @author Patrik Dufresne
 */
@SuppressWarnings("unqualified-field-access")
public class CheckTaskAssigned extends AbstractConstraintRunnable {

    private static final transient Logger LOGGER = LoggerFactory.getLogger(CheckTaskAssigned.class);

    /**
     * The constraint's name.
     */
    private static final String CONSTRAINT_NAME = "CheckTaskAssigned: SUM from{i=0} to{n} SUM from{k=0} to{p} xijk >= %d";

    /**
     * Default constructor.
     * 
     * @param problem
     *            reference to ProblemBuilder
     */
    public CheckTaskAssigned(ProblemBuilder problem) {
        super(problem);
        setFeasibilityPumpHeuristic(true);
    }

    /**
     * This implementation does nothing.
     */
    @Override
    protected void feasible() {
        // Every task may be assigned. There is no need to remove the
        // constraint.
        LOGGER.info("All task can be assigned.");
    }

    @Override
    protected boolean setUp() {
        // Create a linear for the sum of every tasks
        Linear linear = pb.lp.createLinear();
        for (int i = 0; i < pb.xijk.length; i++) {
            for (int j = 0; j < pb.xijk[i].length; j++) {
                for (int k = 0; k < pb.xijk[i][j].length; k++) {
                    if (pb.xijk[i][j][k] != null) {
                        linear.add(pb.lp.createTerm(ONE, pb.xijk[i][j][k]));
                    }
                }
            }
        }
        if (linear.size() == 0) {
            feasible = true;
            return false;
        }
        // Create the following constraint
        // SUM xijk >= nbTask
        Number number = Integer.valueOf(pb.tasks.size());
        String name = String.format(CONSTRAINT_NAME, number);
        this.constraint = pb.lp.addConstraint(name, linear, number, null);
        // Reset the objective function
        pb.lp.setObjectiveLinear(null);
        LOGGER.debug("Check if every task can be assigned.");
        return true;
    }

    /**
     * If it's impossible to assign all tasks, release the constraint.
     * 
     * @throws GeneratePlanifException
     */
    @Override
    protected void unfeasible() throws GeneratePlanifException {
        super.unfeasible();
        LOGGER.info("Can't assign all task.");
    }
}