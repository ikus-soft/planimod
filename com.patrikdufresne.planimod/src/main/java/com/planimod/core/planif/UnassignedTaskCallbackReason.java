/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import java.util.Collection;

import com.planimod.core.Task;

/**
 * This class is used in cases where the generator can't assign an employee to every task.
 * 
 * 
 * @author Karl St-Cyr
 * 
 */
public class UnassignedTaskCallbackReason implements ICallbackReason {

    Collection<Task> tasks;

    /**
     * 
     * @param tasks
     *            the collection of unassigned tasks
     */
    public UnassignedTaskCallbackReason(Collection<Task> tasks) {
        if (tasks == null) {
            throw new IllegalArgumentException();
        }
        this.tasks = tasks;
    }

    public Collection<Task> getTasks() {
        return tasks;
    }

}
