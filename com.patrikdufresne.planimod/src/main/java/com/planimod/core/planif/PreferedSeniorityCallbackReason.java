/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import com.planimod.core.Employee;
import com.planimod.core.Team;

/**
 * Used in cases where the generator can't assign an employee with preferred seniority to his preferred team.
 * 
 * @author Karl St-Cyr
 * 
 */
public class PreferedSeniorityCallbackReason implements ICallbackReason {

    private final Employee employee;
    private final Team team;

    /**
     * 
     * @param employee
     *            the employee with preferred seniority targeted by the callbackReason
     * @param team
     *            the preferred team of this employee
     */
    public PreferedSeniorityCallbackReason(Employee employee, Team team) {
        if (employee == null || team == null) {
            throw new IllegalArgumentException();
        }
        this.employee = employee;
        this.team = team;
    }

    public Employee getEmployee() {
        return employee;
    }

    public Team getTeam() {
        return team;
    }

    @Override
    public String toString() {
        return "PreferedSeniorityCallbackReason [employee=" + employee + ", team=" + team + "]";
    }

}
