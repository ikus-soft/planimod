/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import com.planimod.core.Employee;
import com.planimod.core.Team;

/**
 * Used to create a warning when an employee is planned to work on a classified position that is not in his preferences
 * and an employee with less seniority is planned to work on this employee prefered team.
 * 
 * @author Karl St-Cyr
 * 
 */
public class HasSeniorityToWorkOnPreferedTeamWarning extends ProposedWarning {

    protected Employee bumpEmployee;

    protected Team bumpTeam;

    /**
     * 
     * @param employee
     *            the employee who is the target of the warning.
     * @param bumpEmployee
     *            the employee who is planned to work on a position that could be assigned to the tageted employee.
     * @param bumpTeam
     *            the team on which the target could be assigned
     */
    public HasSeniorityToWorkOnPreferedTeamWarning(Employee employee, Employee bumpEmployee, Team bumpTeam) {
        super(employee);
        if (bumpEmployee == null) {
            throw new IllegalArgumentException("employee: " + bumpEmployee);
        }
        if (bumpTeam == null) {
            throw new IllegalArgumentException("team: " + bumpTeam);
        }
        this.bumpEmployee = bumpEmployee;
        this.bumpTeam = bumpTeam;
    }

    public Employee getBumpEmployee() {
        return bumpEmployee;
    }

    public Team getBumpTeam() {
        return bumpTeam;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((bumpEmployee == null) ? 0 : bumpEmployee.hashCode());
        result = prime * result + ((bumpTeam == null) ? 0 : bumpTeam.hashCode());
        result = prime * result + ((employee == null) ? 0 : employee.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        HasSeniorityToWorkOnPreferedTeamWarning other = (HasSeniorityToWorkOnPreferedTeamWarning) obj;
        if (bumpEmployee == null) {
            if (other.bumpEmployee != null) return false;
        } else if (!bumpEmployee.equals(other.bumpEmployee)) return false;
        if (bumpTeam == null) {
            if (other.bumpTeam != null) return false;
        } else if (!bumpTeam.equals(other.bumpTeam)) return false;
        if (employee == null) {
            if (other.employee != null) return false;
        } else if (!employee.equals(other.employee)) return false;
        return true;
    }

}
