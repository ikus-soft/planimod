/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import com.patrikdufresne.managers.ManagerException;
import com.planimod.core.Employee;
import com.planimod.core.Task;
import com.planimod.core.Team;

/**
 * Exception raised when an handle error occurred within the generate planif process.
 * 
 * @author Patrik Dufresne
 */
public class GeneratePlanifException extends ManagerException {

    /**
     * Reason code to identify an operation being cancel by user.
     */
    public static final int CANCEL_BY_USER = 2;

    /**
     * Reason code to identify an employee being locked on a task for which he is not qualied or available. The employee
     * must be unlocked to generate a planif.
     * <p>
     * Should be raised with an employee and a task.
     */
    public static final int EMPLOYEE_LOCKED_UNAVAIL_OR_UNQUALIFY = 1;

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * Reason code raised when the problem is not feasible from the start. This is most probably due to an employee locked
     * on conflicting task or team.
     */
    public static final int STARTING_UNFEASIBLE = 5;

    /**
     * The problem is unfeasible.
     */
    public static final int UNFEASIBLE = 4;

    private Employee employee;

    private int reason;

    private Task task;

    private Team team;

    public GeneratePlanifException(String message, int reason) {
        this(message, reason, null, null, null);
    }

    /**
     * Create a new exception with the reason specified.
     * 
     * @param message
     *            the detail message.
     * @param reason
     *            the reason.
     * @param employee
     *            the related employee or null
     * @param task
     *            the related task or null
     */
    public GeneratePlanifException(String message, int reason, Employee employee, Task task) {
        this(message, reason, employee, task, null);
    }

    public GeneratePlanifException(String message, int reason, Employee employee, Task task, Team team) {
        super(message);
        this.reason = reason;
        this.employee = employee;
        this.task = task;
        this.team = team;
    }

    public GeneratePlanifException(String message, int reason, Employee employee, Team team) {
        this(message, reason, employee, null, team);
    }

    /**
     * Return the related employee or null if not set
     * 
     * @return
     */
    public Employee getEmployee() {
        return this.employee;
    }

    /**
     * Return the exception reason.
     * 
     * @return
     */
    public int getReason() {
        return this.reason;
    }

    /**
     * Return the related task or null is not set
     * 
     * @return
     */
    public Task getTask() {
        return this.task;
    }

    public Team getTeam() {
        return this.team;
    }
}
