/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This runnable is used to find the maximum number of tasks that may be assigned to employee.
 * <p>
 * This implementation will run a sequence of solver to achieve this operation. In case it's not feasible to assign all
 * task, the solver will prompt the user to continue.
 * <ul>
 * <li>CheckTaskAssigned : check if all task can be assigned -- It's faster to check feasibility than optimizing.</li>
 * <li>MaxTaskAssigned : optimize the total number of tasks assigned.</li>
 * </ul>
 * <p>
 * This solver is always feasible.
 * <p>
 * If all task can't be assigned and the user refused to continue, this solver throw a
 * {@link GeneratePlanifException#CANCEL_BY_USER}.
 * 
 * @author Patrik Dufresne
 */
public class MaxTaskAssigned extends SolveRunnable {

    private static final transient Logger LOGGER = LoggerFactory.getLogger(MaxTaskAssigned.class);

    /**
     * Solver used internally.
     */
    private CheckTaskAssigned checkTaskAssigned;

    /**
     * Solver used internally.
     */
    private MaxAllTaskAssigned maxAllTaskAssigned;

    /**
     * Solver used internally.
     */
    private MaxImportantTaskAssigned maxImportantTaskAssigned;

    /**
     * The progress monitor.
     */
    // private IGeneratePlanifProgressMonitor monitor;

    /**
     * Create a new solver.
     * 
     * @param problem
     *            reference to ProblemBuilder
     * 
     * @param monitor
     *            the monitor used to prompt the user.
     */
    public MaxTaskAssigned(ProblemBuilder problem) {
        super(problem);
        if (pb.monitor == null) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * 
     */
    public void engage() {
        if (this.checkTaskAssigned != null) {
            this.checkTaskAssigned.engage();
        }
        if (this.maxImportantTaskAssigned != null) {
            this.maxImportantTaskAssigned.engage();
        }
        if (this.maxAllTaskAssigned != null) {
            this.maxAllTaskAssigned.engage();
        }
    }

    /**
     * Release all the constraint created to assign the maximum number of tasks.
     */
    public void release() {
        // Release the constraints.
        if (this.checkTaskAssigned != null) {
            this.checkTaskAssigned.release();
        }
        if (this.maxImportantTaskAssigned != null) {
            this.maxImportantTaskAssigned.release();
        }
        if (this.maxAllTaskAssigned != null) {
            this.maxAllTaskAssigned.release();
        }
    }

    /**
     * This implementation override the default implementation by running other solver.
     */
    @Override
    protected boolean runSolver() throws GeneratePlanifException {
        // Try to assign all tasks.
        this.checkTaskAssigned = new CheckTaskAssigned(pb);
        this.checkTaskAssigned.run();
        if (this.checkTaskAssigned.isFeasible()) {
            return true;
        }
        // Release the solver.
        this.checkTaskAssigned = null;
        // Ask user if the search should continue
        // TODO Filter tasks list
        if (!pb.monitor.callbackContinue(new UnassignedTaskCallbackReason(pb.tasks))) {
            throw new GeneratePlanifException("Cancel by user", GeneratePlanifException.CANCEL_BY_USER);
        }
        // Maximize important tasks assignment
        this.maxImportantTaskAssigned = new MaxImportantTaskAssigned(pb);
        this.maxImportantTaskAssigned.run();
        // Maximize all the assignment
        this.maxAllTaskAssigned = new MaxAllTaskAssigned(pb);
        this.maxAllTaskAssigned.run();
        // Should always return true.
        return this.maxAllTaskAssigned.isFeasible();
    }
}