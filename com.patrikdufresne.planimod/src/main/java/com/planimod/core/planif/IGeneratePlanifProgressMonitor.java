/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import java.util.Collection;

/**
 * The IGeneratePlanifProgressMonitor interface is implemented by objects that monitor the creation of a planif.
 * 
 * A request to cancel an operation can be signaled using the setCanceled method. Operations taking a progress monitor
 * are expected to poll the monitor (using isCanceled) periodically and abort at their earliest convenience. Operation
 * can however choose to ignore cancelation requests. Since notification is synchronous with the activity itself, the
 * listener should provide a fast and robust implementation. If the handling of notifications would involve blocking
 * operations, or operations which might throw uncaught exceptions, the notifications should be queued, and the actual
 * processing deferred (or perhaps delegated to a separate thread).
 * 
 * 
 * @author Patrik Dufresne
 * 
 */
public interface IGeneratePlanifProgressMonitor {

    /**
     * Step check feasible.
     */
    public static final int STEP_CHECK_FEASIBLE = 1;
    /**
     * Step creating the variables.
     */
    public static final int STEP_CREATE_VARIABLES = 2;
    /**
     * Step maximizing the number of assigned task.
     */
    public static final int STEP_MAXIMIZE_ASSIGNED_TASK = 4;
    /**
     * Step maximizing the preferences.
     */
    public static final int STEP_MAXIMIZE_PREFERENCES = 7;
    /**
     * Step maximizing the number of preferential seniority employees assigned.
     */
    public static final int STEP_MAXIMIZE_PREFERRED_SENIORITIES = 3;
    /**
     * Step maximizing the senior employees
     */
    public static final int STEP_MAXIMIZE_SENIOR = 6;
    /**
     * Step minimizing the number of required employee
     */
    public static final int STEP_MINIMIZE_EMPLOYEE = 5;

    /**
     * Notifies that the search for a solution is beginning. This is only called once for a given progress monitor instance.
     * 
     * @param work
     *            total number of work
     */
    public void beginSearch(int work);

    /**
     * The problem is unfeasible most probably because there is too many production events for the number of available
     * employees or preferential seniority not respected. This function is called to request the user to continue or not.
     * 
     * @param ICallbackReason
     *            to use one of the implementations
     * 
     * @return True to continue, false to stop.
     */
    public boolean callbackContinue(ICallbackReason reason);

    /**
     * Notifies that the search as complete; that is, either the main task is completed or the user canceled it. This method
     * may be called more than once (implementations should be prepared to handle this case).
     */
    public void done();

    /**
     * Returns whether cancelation of current operation has been requested. Long-running operations should poll to see if
     * cancelation has been requested.
     * 
     * @return <code>true</code> if cancellation has been requested, and <code>false</code> otherwise
     * @see #setCanceled(boolean)
     */
    public boolean isCanceled();

    /**
     * Notifies the monitor about the current step being computed. The value of <code>step</code> should be one of the
     * STEP_* constants.
     * 
     * @param step
     *            the step
     */
    public void setStep(GeneratePlanifProgressMonitorStep step);

    /**
     * Notifies that a given number of work unit of the search has been completed. Note that this amount represents an
     * installment, as opposed to a cumulative amount of work done to date.
     * 
     * @param work
     *            a non-negative number of work units just completed
     */
    public void worked(int work);

}
