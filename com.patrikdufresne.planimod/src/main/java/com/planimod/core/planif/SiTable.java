/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.planimod.core.planif;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.planimod.core.Section;
import com.planimod.core.Task;

class SiTable {

    /**
     * Create a new SiTable.
     * 
     * @param tasks
     *            the list of taskst
     * @return instance of SiTable.
     */
    public static SiTable create(List<Task> tasks) {
        Set<Section> set = new HashSet<Section>();
        for (Task t : tasks) {
            set.add(t.getPosition().getSection());
        }
        List<Section> sections = new ArrayList<Section>(set);
        boolean[][] table = new boolean[set.size()][tasks.size()];
        for (int k = 0; k < tasks.size(); k++) {
            Section section = tasks.get(k).getPosition().getSection();
            table[sections.indexOf(section)][k] = true;
        }
        return new SiTable(sections, table);
    }

    /**
     * Known sections.
     */
    List<Section> sections;

    /**
     * Table to represent the association of task to sections.
     */
    boolean[][] table;

    /**
     * Create a new SiTable.
     * 
     * @param sections
     * @param table
     */
    protected SiTable(List<Section> sections, boolean[][] table) {
        this.sections = sections;
        this.table = table;
    }

    /**
     * Return the coefficient list for the section <code>a</code>.
     * 
     * @param a
     *            the section
     * @return the coefficient list
     */
    public List<Double> get(int a) {
        return new CoefficientList(this.table[a]);
    }

    /**
     * Return the coefficient list for the section specified.
     * 
     * @param section
     *            the section
     * @return the coefficent list or null is the section doesn't exists in this table.
     */
    public List<Double> get(Section section) {
        int a = this.sections.indexOf(section);
        if (a < 0) {
            return null;
        }
        return new CoefficientList(this.table[a]);
    }

    /**
     * Return the section index.
     * 
     * @param section
     *            the section
     * @return the index or -1
     */
    public int indexOf(Section section) {
        return this.sections.indexOf(section);
    }

    /**
     * Check if the task <code>k</code> is part of the section <code>a</code>
     * 
     * @param k
     *            the task
     * @param a
     *            the section
     * @return True if the task is part of the section.
     */
    public boolean kInSa(int k, int a) {
        return this.table[a][k];
    }
}