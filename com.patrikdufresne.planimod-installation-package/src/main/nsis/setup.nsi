; Displayed to the user
!define AppName "Planimod"
; Used for paths
!define ShortName "planimod"
!define Vendor "Ikus Soft inc."
!define AppExeFile ""
 
;--------------------------------
;Includes

  !include "MUI2.nsh"
  !include "Sections.nsh"
  !include "x64.nsh"

SetCompressor bzip2

  ; Include Java Install
  !addincludedir ${includedir}
  !addplugindir ${plugindir}
  !define JRE_VERSION "1.8"
  # JAVA 8u241
  !define JRE_URL "https://javadl.oracle.com/webapps/download/AutoDL?BundleId=241534_1f5b5a70bf22433b84d0e960903adac8"
  !define JRE_URL_64 "https://javadl.oracle.com/webapps/download/AutoDL?BundleId=241536_1f5b5a70bf22433b84d0e960903adac8"
  !include "JREDyna_Inetc.nsh"

;--------------------------------
;Configuration
 
  ;General
  Name "${AppName}"
  VIProductVersion "${AppVersion}"
  VIAddVersionKey "ProductName" "${AppName}"
  VIAddVersionKey "Comments" "Optimized human resource management"
  VIAddVersionKey "CompanyName" "${Vendor}"
  VIAddVersionKey "LegalCopyright" "© ${Vendor}"
  VIAddVersionKey "FileDescription" "${AppName} ${AppVersion} Installer"
  VIAddVersionKey "FileVersion" "${AppVersion}"
  OutFile "${OutFile}"

  ; Define icon
  !define MUI_ICON "planimod.ico"
  !define MUI_UNICON "planimod.ico"

  ;Folder selection page
  InstallDir "$PROGRAMFILES64\${ShortName}"
 
  ;Get install folder from registry if available
  InstallDirRegKey HKLM "SOFTWARE\${Vendor}\${ShortName}" ""
 
  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin
  
;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

  ;Show all languages, despite user's codepage
  !define MUI_LANGDLL_ALLLANGUAGES

;--------------------------------
;Language Selection Dialog Settings

  ;Remember the installer language
  !define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
  !define MUI_LANGDLL_REGISTRY_KEY "SOFTWARE\${Vendor}\${ShortName}" 
  !define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"

;--------------------------------
;Pages
 
  ; License page
  !insertmacro MUI_PAGE_LICENSE $(license)
 
  ; Java download page 
  !insertmacro CUSTOM_PAGE_JREINFO
 
  ; Installation directory selection
  !insertmacro MUI_PAGE_DIRECTORY
  
  ; Installation...
  !insertmacro MUI_PAGE_INSTFILES

  ; Finish Page
  !insertmacro MUI_PAGE_FINISH
  
  ; Uninstall confirmation
  !insertmacro MUI_UNPAGE_CONFIRM
  
  ;Uninstall
  !insertmacro MUI_UNPAGE_INSTFILES
 
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"
  !insertmacro MUI_LANGUAGE "French"

  LicenseLangString license ${LANG_ENGLISH} "LICENSE.txt"
  LicenseLangString license ${LANG_FRENCH} "LICENSE.txt"
  
  !insertmacro JREINFO_LANGUAGE
  
;--------------------------------
;Reserve Files
  
  ;If you are using solid compression, files that are required before
  ;the actual installation should be stored first in the data block,
  ;because this will make your installer start faster.
  
  !insertmacro MUI_RESERVEFILE_LANGDLL
 
;--------------------------------
;Language Strings

  ;Description
  LangString DESC_SecAppFiles ${LANG_ENGLISH} "Application files copy"
  LangString DESC_SecAppFiles ${LANG_FRENCH} "Copie des fichiers"
 
;--------------------------------
;Installer Sections
 
Section "Installation of ${AppName}" SecAppFiles

  ; Remove files
  RMDir /r "$INSTDIR\lib"
  
  ; Add files
  SetOutPath $INSTDIR
  SetOverwrite on
  File /r ".\"
  
  ;Store install folder
  WriteRegStr HKLM "SOFTWARE\${Vendor}\${ShortName}" "" $INSTDIR

  !define REG_UNINSTALL "Software\Microsoft\Windows\CurrentVersion\Uninstall\${ShortName}"
  WriteRegStr HKLM "${REG_UNINSTALL}" "DisplayName" "${AppName}"
  WriteRegStr HKLM "${REG_UNINSTALL}" "DisplayIcon" "$INSTDIR\minarca.ico"
  WriteRegStr HKLM "${REG_UNINSTALL}" "DisplayVersion" "${AppVersion}"
  WriteRegStr HKLM "${REG_UNINSTALL}" "Publisher" "${Vendor}"
  WriteRegStr HKLM "${REG_UNINSTALL}" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "${REG_UNINSTALL}" "NoModify" "1"
  WriteRegDWORD HKLM "${REG_UNINSTALL}" "NoRepair" "1"


  ; See https://github.com/h2database/h2database/issues/1935
  ; Disable Caching
  WriteRegDWORD HKLM "SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" "FileInfoCacheLifetime" "0"
  WriteRegDWORD HKLM "SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" "DirectoryCacheLifetime" "0"
  WriteRegDWORD HKLM "SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" "FileNotFoundCacheLifetime" "0"
  WriteRegDWORD HKLM "SYSTEM\CurrentControlSet\Services\LanmanWorkstation\Parameters" "CacheFileTimeout" "0"
 
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"

  ; Download and install java and tweak the secure protocol for download.
  ; http://forums.winamp.com/showthread.php?t=198596&page=14
  !define IE_NETCONFIG "Software\Microsoft\Windows\CurrentVersion\Internet Settings"
  ReadRegDWORD $9 HKCU "${IE_NETCONFIG}" "SecureProtocols"
  WriteRegDWORD HKCU "${IE_NETCONFIG}" "SecureProtocols" 0x00000A80
  call DownloadAndInstallJREIfNecessary
  WriteRegDWORD HKCU "${IE_NETCONFIG}" "SecureProtocols" $9

SectionEnd
 
 
Section "Start menu shortcuts" SecCreateShortcut
  SectionIn 1   ; Can be unselected
  CreateDirectory "$SMPROGRAMS\${AppName}"
  ${If} ${RunningX64}
    CreateShortCut "$DESKTOP\${AppName}.lnk" "$INSTDIR\bin\planimod64.exe" "" "$INSTDIR\planimod.ico" 0
    CreateShortCut "$SMPROGRAMS\${AppName}\${AppName}.lnk" "$INSTDIR\bin\planimod64.exe" "" "$INSTDIR\planimod.ico" 0
  ${Else}
    CreateShortCut "$DESKTOP\${AppName}.lnk" "$INSTDIR\bin\planimod.exe" "" "$INSTDIR\planimod.ico" 0
    CreateShortCut "$SMPROGRAMS\${AppName}\${AppName}.lnk" "$INSTDIR\bin\planimod.exe" "" "$INSTDIR\planimod.ico" 0
  ${EndIf}
  
SectionEnd
 
;--------------------------------
;Descriptions
 
!insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
!insertmacro MUI_DESCRIPTION_TEXT ${SecAppFiles} $(DESC_SecAppFiles)
!insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Installer Functions
 
Function .onInit
  
  ; When running 64bits, read and write to 64bits registry.
  SetRegView 64
  
FunctionEnd
 
;--------------------------------
;Uninstaller Section
 
Section "Uninstall"

  ; remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${ShortName}"
  DeleteRegKey HKLM  "SOFTWARE\${Vendor}\${AppName}"
  ; remove shortcuts, if any.
  Delete "$SMPROGRAMS\${AppName}\*.*"
  RMDir /r "$SMPROGRAMS\${AppName}"
  Delete "$DESKTOP\${AppName}.lnk"
  ; remove files
  RMDir /r "$INSTDIR"
 
SectionEnd

;--------------------------------
;Uninstaller Functions

Function un.onInit

  ; When running 64bits, read and write to 64bits registry.
  SetRegView 64
  
  ; Uninstall for all user
  SetShellVarContext current

  !insertmacro MUI_UNGETLANGUAGE
  
FunctionEnd
