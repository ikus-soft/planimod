# Translation

planimod project uses gettext plugin to support translation using `.pot` and
`.po` files. You may update the translation or add a new translation as follow.

To extract translatable string and update the `.pot` file

    cd ./com.patrikdufresne.planimod
    mvn -Drevision=1.0.0 gettext:gettext

To update the translation file `.po`

    cd ./com.patrikdufresne.planimod
    mvn -Drevision=1.0.0 gettext:merge
