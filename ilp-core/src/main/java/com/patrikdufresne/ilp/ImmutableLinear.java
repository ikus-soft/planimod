/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Immutable linear.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ImmutableLinear implements Linear {

    /**
     * Immutable list of terms.
     */
    private List<Term> terms;

    /**
     * Create a new immutable linear
     * 
     * @param terms
     *            a collection of terms.
     */
    public ImmutableLinear(Collection<Term> terms) {
        this.terms = Collections.unmodifiableList(new ArrayList<Term>(terms));
    }

    /**
     * Create a new immutable linear from an existing linear.
     * 
     * @param linear
     */
    public ImmutableLinear(Linear linear) {
        this.terms = new ArrayList<Term>();
        for (Term term : linear) {
            this.terms.add(term);
        }
        this.terms = Collections.unmodifiableList(this.terms);
    }

    /**
     * Return an un-modifiable iterator.
     */
    @Override
    public Iterator<Term> iterator() {
        return this.terms.iterator();
    }

    /**
     * Throw an exception.
     */
    @Override
    public void add(Term term) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void remove(Term term) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int size() {
        return this.terms.size();
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

}
