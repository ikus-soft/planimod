/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * This interface force the solver to use the feasibility pump heuristic.
 * <p>
 * The feasibility pump is a heuristic that finds an initial feasible solution even in certain very hard mixed integer
 * programming problems (MIPs).
 * 
 * @author Patrik Dufresne
 * 
 */
public interface IFeasibilityPumpHeuristic extends SolverOption {

    /**
     * Enabled or disable the feasibility pump heuristic.
     * 
     * @param enabled
     *            True to enabled the heuristic
     */
    void setFeasibilityPumpHeuristic(boolean enabled);

    /**
     * Check if feasibility pump heuristic is enabled
     * 
     * @return True if the heuristic is enabled
     */
    boolean getFeasibilityPumpHeuristic();

}
