/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * This class is a complete implementation of the {@link Linear} interface. This implementation store all the data in
 * memory.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ConcreteLinear implements Linear {

    @Override
    public String toString() {
        if (this.terms == null || this.terms.isEmpty()) {
            return "[]"; //$NON-NLS-1$
        }

        Iterator<Term> i = this.terms.iterator();
        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (;;) {
            Term e = i.next();
            sb.append(e);
            if (!i.hasNext()) return sb.append(']').toString();
            sb.append(" + "); //$NON-NLS-1$
        }
    }

    private Set<Term> terms;

    @Override
    public Iterator<Term> iterator() {
        if (this.terms == null) {
            return Collections.EMPTY_LIST.iterator();
        }
        return this.terms.iterator();
    }

    @Override
    public void add(Term term) {
        if (this.terms == null) {
            this.terms = new LinkedHashSet<Term>();
        }
        this.terms.add(term);
    }

    @Override
    public void remove(Term term) {
        if (this.terms == null) {
            return;
        }
        this.terms.remove(term);
    }

    @Override
    public void clear() {
        if (this.terms == null) {
            return;
        }
        this.terms.clear();
    }

    @Override
    public int size() {
        if (this.terms == null) {
            return 0;
        }
        return this.terms.size();
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((this.terms == null) ? 0 : this.terms.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ConcreteLinear other = (ConcreteLinear) obj;
        if (this.terms == null) {
            if (other.terms != null) return false;
        } else if (!this.terms.equals(other.terms)) return false;
        return true;
    }

    @Override
    public boolean isEmpty() {
        return this.size() == 0;
    }

}
