/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * The interface is a linear expression consisting of variables and their coefficients.
 * 
 * @author Patrik Dufresne
 * 
 */
public interface Linear extends Iterable<Term> {

    /**
     * Add a new term to the linear expression.
     * 
     * @param term
     *            the term to be added
     */
    void add(Term term);

    /**
     * Remove a term from the linear expression.
     * 
     * @param term
     *            the term to remove
     */
    void remove(Term term);

    /**
     * Remove all terms from this linear expression.
     */
    void clear();

    /**
     * Returns the number of term in the linear expression.
     * 
     * @return the number of term
     */
    int size();

    /**
     * Returns true if the size of the linear is 0
     * 
     * @return true if the linear is empty
     */
    boolean isEmpty();
}
