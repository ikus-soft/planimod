/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * This class provide a complter implementation of the {@link Term} interface.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ConcreteTerm implements Term {
    /**
     * The coefficient value.
     */
    private Double coefficient;

    /**
     * The variable.
     */
    private Variable variable;

    /**
     * Create a new term.
     * 
     * @param coefficient
     * @param variable
     */
    public ConcreteTerm(Number coefficient, Variable variable) {
        if (coefficient == null || variable == null) {
            throw new NullPointerException();
        }
        this.coefficient = Double.valueOf(coefficient.doubleValue());
        this.variable = variable;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        ConcreteTerm other = (ConcreteTerm) obj;
        if (!this.coefficient.equals(other.coefficient)) return false;
        if (!this.variable.equals(other.variable)) return false;
        return true;
    }

    @Override
    public Double getCoefficient() {
        return this.coefficient;
    }

    @Override
    public Variable getVariable() {
        return this.variable;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + this.coefficient.hashCode();
        result = prime * result + this.variable.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return this.coefficient + " * " + this.variable; //$NON-NLS-1$
    }

}
