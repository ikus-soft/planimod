/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

import java.io.File;
import java.io.IOException;

/**
 * IPersistentLinearProblem is a linear problem that can be load from file and save to file.
 * 
 * @author Patrik Dufresne
 * 
 */
public interface IPersistentLinearProblem extends LinearProblem {

    /**
     * Load the linear problem from a file
     * 
     * @param file
     *            the file to load data from
     * @throws IOException
     */
    public void load(File file) throws IOException;

    /**
     * Save the linear problem to a file
     * 
     * @param file
     *            the file
     * @throws IOException
     */
    public void save(File file) throws IOException;

}
