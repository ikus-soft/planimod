/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp.util;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.patrikdufresne.ilp.Variable;

/**
 * Class storing a snapshot of the variable's value.
 * 
 * @author Patrik Dufresne
 * 
 */
public class ValueSnapshot extends HashMap<Variable, Double> {

    private static final long serialVersionUID = 1L;

    /**
     * Create a new value snapshot for the variables specified.
     * 
     * @param vars
     *            the input variable
     * @return the snapshot value
     */
    public static ValueSnapshot create(Collection<? extends Variable> vars) {
        Map<Variable, Double> snapshot = new HashMap<Variable, Double>(vars.size());
        for (Variable var : vars) {
            snapshot.put(var, var.getValue());
        }
        return new ValueSnapshot(snapshot);
    }

    /**
     * Create a new snapshot with the given map.
     * 
     * @param snapshot
     */
    protected ValueSnapshot(Map<Variable, Double> snapshot) {
        super(snapshot);
    }

    @Override
    public void clear() {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set<Entry<Variable, Double>> entrySet() {
        return Collections.unmodifiableSet(super.entrySet());
    }

    @Override
    public Set<Variable> keySet() {
        return Collections.unmodifiableSet(super.keySet());
    }

    @Override
    public Double put(Variable key, Double value) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void putAll(Map<? extends Variable, ? extends Double> m) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Double remove(Object key) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Collection<Double> values() {
        return Collections.unmodifiableCollection(super.values());
    }

}
