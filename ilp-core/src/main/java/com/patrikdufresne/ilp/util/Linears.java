/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp.util;

import com.patrikdufresne.ilp.Linear;
import com.patrikdufresne.ilp.Term;

/**
 * Utility class to manipulate linears.
 * 
 * @author Patrik Dufresne
 * 
 */
public class Linears {

    /**
     * Private constructor to avoid creating instances of utility class.
     */
    private Linears() {

    }

    /**
     * Compute the value of a linear using the variable value from a snapshot.
     * 
     * @param linear
     *            the linear to be computed
     * @param snapshot
     *            the value snapshot
     * @return the computed value.
     * @throws NullPointerException
     *             if the snapshot doesn't contains a variable from the linear.
     */
    public static Double compute(Linear linear, ValueSnapshot snapshot) {
        double value = 0;
        for (Term term : linear) {
            value += term.getCoefficient().doubleValue() * snapshot.get(term.getVariable()).doubleValue();
        }
        return Double.valueOf(value);
    }

}
