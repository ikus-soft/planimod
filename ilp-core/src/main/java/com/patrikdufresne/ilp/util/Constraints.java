/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp.util;

import com.patrikdufresne.ilp.Constraint;
import com.patrikdufresne.ilp.Linear;

/**
 * Utility class to manipulates constraints and associated snapshot.
 * 
 * @author Patrik Dufresne
 * 
 */
public class Constraints {

    /**
     * Check if the constraint is already feasible with the given value snapshot.
     * 
     * @param constraint
     *            the constraint to check
     * @param snapshot
     *            the value snapshot used to evaluate the feasibility
     * @return True if the constraint is satisfied
     */
    public static boolean isSatisfied(Constraint constraint, ValueSnapshot snapshot) {
        if (constraint == null || snapshot == null) {
            throw new IllegalArgumentException();
        }
        // Compute the linear value
        Linear linear = constraint.getLinear();
        double value = linear == null ? 0 : Linears.compute(linear, snapshot).doubleValue();
        // Check if the value is in range
        return (constraint.getLowerBound() == null || constraint.getLowerBound().doubleValue() <= value)
                && (constraint.getUpperBound() == null || constraint.getUpperBound().doubleValue() >= value);
    }

    /**
     * Take a snapshot of the given constraints and then dispose it.
     * 
     * @param constraint
     *            the constraint to be release.
     * @return the snapshot representing the constraints
     */
    public static ConstraintSnapshot release(Constraint constraint) {
        if (constraint == null || constraint.isDisposed()) {
            throw new IllegalArgumentException();
        }
        ConstraintSnapshot snapshot = ConstraintSnapshot.create(constraint);
        constraint.dispose();
        return snapshot;
    }

    /**
     * Private constructor to avoid creating instances of utility class.
     */
    private Constraints() {

    }
}
