/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp.util;

import com.patrikdufresne.ilp.Variable;

/**
 * Utility class for {@link Variable}.
 * 
 * @author Patrik Dufresne
 * 
 */
public class Variables {

    /**
     * Check if the variable bound is fixed.
     * 
     * @param var
     *            the variable
     * @return True if lower bound and the upper bound is fixed.
     */
    public static boolean isFixed(Variable var) {
        return var.getLowerBound() != null && var.getLowerBound().equals(var.getUpperBound());
    }

    /**
     * Private constructor for utility class.
     */
    private Variables() {

    }

}
