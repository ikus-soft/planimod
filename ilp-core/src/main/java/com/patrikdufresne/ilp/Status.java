/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * The <code>Status</code> class is an enumeration of the possible solution status values.
 * 
 * @author Patrik Dufresne
 * 
 */
public enum Status {
    /**
     * The solver has found an optimal solution that can be queried with the method {@link Variable#getValue()}.
     */
    OPTIMAL,
    /**
     * The solver has found a feasible solution that can be queried with the method {@link Variable#getValue()}. However,
     * its optimality (or non-optimality) has not been proven, perhaps due to premature termination of the search.
     */
    FEASIBLE,
    /**
     * The solver has determined that the problem is infeasible.
     */
    INFEASIBLE,
    /**
     * The solver has determined that the problem is unbounded.
     */
    UNBOUNDED,
    /**
     * The solver has determine the solution is undefined.
     */
    UNKNOWN

}
