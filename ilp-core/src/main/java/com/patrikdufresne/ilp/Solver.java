/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * The Solver class is used to create new problem instance and solve it.
 * 
 * @author Patrik Dufresne
 * 
 */
public interface Solver {

    /**
     * Create a new linear problem.
     * 
     * @return the linear problem.
     */
    LinearProblem createLinearProblem();

    /**
     * Create a new solver option with default parameters.
     * 
     * @return
     */
    SolverOption createSolverOption();

    /**
     * This function should be called to free any resources allocated by the solver.
     */
    void dispose();

    /**
     * Solve the linear problem.
     * 
     * @param lp
     *            the linear problem.
     * @param option
     *            the solver option.
     * @return A Boolean value reporting whether a feasible solution has been found. This solution is not necessarily
     *         optimal. If <code>false</code> is returned, a feasible solution may still be present, but the solver has not
     *         been able to prove its feasibility.
     */
    boolean solve(LinearProblem lp, SolverOption option);

}
