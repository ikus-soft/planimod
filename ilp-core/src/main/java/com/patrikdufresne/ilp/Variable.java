/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * Represent a variable in the linear problem.
 * 
 * @author Patrik Dufresne
 * 
 */
public interface Variable {

    /**
     * Returns the lower bound.
     * 
     * @return the lower bound or null if not bounded
     */
    Double getLowerBound();

    /**
     * Returns the variable name.
     * 
     * @return the name.
     */
    String getName();

    /**
     * Returns the solution value for a variable.
     * <p>
     * If the problem was solved using the simplex algorithm value or the variable. The dual value may be retrieved using
     * {@link #getDual()}.
     * </p>
     * 
     * @return the solution value or null if the solution is not available
     * @throws ILPException
     *             is no solution are available
     */
    Double getValue();

    /**
     * Returns the variable type.
     * 
     * @return the variable type.
     */
    VarType getType();

    /**
     * Returns the upper bound.
     * 
     * @return the upper bound or null if unbounded
     */
    Double getUpperBound();

    /**
     * Sets the variable lower bound.
     * 
     * @param bound
     *            the new lower bound value or null for −∞
     */
    void setLowerBound(Number bound);

    /**
     * Sets the variable upper bound.
     * 
     * @param bound
     *            the new upper bound or null for +∞
     */
    void setUpperBound(Number bound);

    /**
     * Check if the variable object is disposed.
     * 
     * @return True if the variable is disposed.
     */
    public boolean isDisposed();

    /**
     * Remove the variable from the linear problem. Does nothing if the variable is already disposed.
     * <p>
     * Sub classes must consider the case when this function is called multiple time for the same object.
     */
    void dispose();

}
