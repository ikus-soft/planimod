/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * The variable type.
 * 
 * @author Patrik Dufresne
 * 
 */
public class VarType {

    /**
     * Used for binary variable.
     */
    public static final VarType BOOL = new VarType("BOOL"); //$NON-NLS-1$
    /**
     * Used for integer variable.
     */
    public static final VarType INTEGER = new VarType("INT"); //$NON-NLS-1$
    /**
     * Used for continuous variable.
     */
    public static final VarType REAL = new VarType("REAL"); //$NON-NLS-1$

    /**
     * The internal variable type.
     */
    private String type;

    /**
     * Private constructor.
     */
    private VarType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "VarType [" + this.type + "]"; //$NON-NLS-1$ //$NON-NLS-2$
    }

    @Override
    public int hashCode() {
        return this.type.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null) return false;
        if (getClass() != obj.getClass()) return false;
        VarType other = (VarType) obj;
        if (!this.type.equals(other.type)) return false;
        return true;
    }

}
