/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * A mechanism to log errors throughout ILP.
 * <p>
 * Clients may provide their own implementation to change how errors are logged from within ILP.
 * </p>
 * 
 * @see ILPPolicy#getLog()
 * @see ILPPolicy#setLog(ILogger)
 */
public interface ILPLogger {

    public static final int DEBUG = 0x01;

    public static final int INFO = 0x02;

    public static final int WARNING = 0x03;

    public static final int ERROR = 0x04;

    public static final int TRACE = 0x05;

    /**
     * Logs the given status.
     * 
     * @param status
     *            the status to log
     */
    public void log(int severity, String message);

    /**
     * Return the current log level.
     * 
     * @return One of the DEBUG, INFO, WARNING, ERROR constants.
     */
    public int getLevel();

}
