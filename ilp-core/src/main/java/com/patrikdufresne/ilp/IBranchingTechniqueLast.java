/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * This interface force the solver to use a specific algorithm to select the next node and it's direction within the
 * branch-and-bound algorithm.
 * <p>
 * This branching technique will select the last fractional variables and round to the nearest integer value.
 * 
 * @author Patrik Dufresne
 * 
 */
public interface IBranchingTechniqueLast extends SolverOption {

    /**
     * Enable or disable this branching technique.
     * 
     * @param enabled
     *            True to enable
     */
    void setBranchingLast(boolean enabled);

    /**
     * Check if this branching technique is enabled.
     * 
     * @return True if enabled.
     */
    boolean getBranchingLast();

}
