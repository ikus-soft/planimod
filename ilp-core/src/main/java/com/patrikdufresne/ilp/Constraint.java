/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp;

/**
 * The class Constraint represent a linear constraint.
 * 
 * @author Patrik Dufresne
 * 
 */
public interface Constraint {

    /**
     * Returns the linear expression.
     * 
     * @return the constraint linear expression or null if empty.
     */
    Linear getLinear();

    /**
     * Returns the lower bound.
     * 
     * @return the lower bound or null if not bounded
     */
    Double getLowerBound();

    /**
     * Returns the constraint name.
     * 
     * @return the name or null if not set
     */
    String getName();

    /**
     * Returns the solution value for this constraint. This value is only available after solving the linear problem.
     * <p>
     * If the problem was solved using simplex algorithm, this function return the primal solution value.
     * 
     * @return the solution value
     * 
     * @throws ILPException
     *             if the solution is not available.
     */
    Double getValue();

    /**
     * Returns the upper bound.
     * 
     * @return the upper bound or null if unbounded
     */
    Double getUpperBound();

    /**
     * Sets the constraint linear expression.
     * 
     * @param linear
     *            the new linear expression or null to reset it
     */
    void setLinear(Linear linear);

    /**
     * Sets the constraint lower bound.
     * 
     * @param bound
     *            the new lower bound value or null for −∞
     */
    void setLowerBound(Number bound);

    /**
     * Sets the constraint upper bound.
     * 
     * @param bound
     *            the new upper bound or null for +∞
     */
    void setUpperBound(Number bound);

    /**
     * Check if the constraint is disposed.
     * 
     * @return True if the problem is disposed.
     */
    boolean isDisposed();

    /**
     * Check if the linear expression is empty.
     * 
     * @return True if the linear expression is empty.
     */
    boolean isEmpty();

    /**
     * Remove the constraint from the linear problem.
     */
    void dispose();

}
