/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne;

import com.patrikdufresne.ilp.Constraint;
import com.patrikdufresne.ilp.Linear;
import com.patrikdufresne.ilp.LinearProblem;
import com.patrikdufresne.ilp.Solver;
import com.patrikdufresne.ilp.SolverOption;
import com.patrikdufresne.ilp.Variable;
import com.patrikdufresne.ilp.cbc.CbcSolverFactory;

public class Main {

    public static void main(String[] args) {
        // Create the solver.
        Solver solver = CbcSolverFactory.instance().createSolver();
        LinearProblem lp = solver.createLinearProblem();

        // Create the model
        Variable x = lp.addIntegerVariable("x", 0, null);
        Variable y = lp.addIntegerVariable("y", 0, null);
        Linear linear = lp.createLinear();
        linear.add(lp.createTerm(1, y));
        lp.setObjectiveLinear(linear);
        lp.setObjectiveDirection(LinearProblem.MAXIMIZE);
        Constraint constraint1 = lp.addConstraint("constraint1", new int[] { -2, 6 }, new Variable[] { x, y }, null, 6);
        Constraint constraint2 = lp.addConstraint("constraint2", new int[] { 2, 6 }, new Variable[] { x, y }, null, 12);

        // Solve the problem.
        SolverOption option = solver.createSolverOption();
        solver.solve(lp, option);

        // Out the data.
        System.out.println("Status: " + lp.getStatus());
        System.out.println("Objective value:" + lp.getObjectiveValue().doubleValue());
        System.out.println("Solution:" + lp.toString());

    }

}
