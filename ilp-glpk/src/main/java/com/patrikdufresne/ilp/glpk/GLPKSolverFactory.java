/**
 * Copyright (C) 2019 Patrik Dufresne Service Logiciel <info@patrikdufresne.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */
package com.patrikdufresne.ilp.glpk;

import com.patrikdufresne.ilp.SolverFactory;
import com.patrikdufresne.ilp.Solver;

/**
 * This implementation create instance of GLPK solver.
 * 
 * @author Patrik Dufresne
 * 
 */
public class GLPKSolverFactory implements SolverFactory {

    private static GLPKSolverFactory instance;

    /**
     * Private constructor for singleton.
     */
    private GLPKSolverFactory() {
        // Nothing to do.
    }

    /**
     * Return the unique instance of this class.
     * 
     * @return
     */
    public static GLPKSolverFactory instance() {
        if (instance == null) {
            instance = new GLPKSolverFactory();
        }
        return instance;
    }

    @Override
    public Solver createSolver() {
        return new GLPKSolver();
    }

}
